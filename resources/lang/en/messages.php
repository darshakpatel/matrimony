<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Master Panel Language Lines
    |--------------------------------------------------------------------------
    */

    'admin.sidebar.dashboard'         => 'Dashboard',
    'admin.sidebar.users'             => 'Users',
    'admin.sidebar.cuisines'          => 'Cuisines',
    'admin.sidebar.add_cuisine'       => 'Add Cuisine',
    'admin.sidebar.cuisine_list'      => 'Cuisines',
    'admin.sidebar.vendors'           => 'Vendors',
    'admin.sidebar.add_vendor'        => 'Add Vendor',
    'admin.sidebar.vendor_list'       => 'Vendors',
    'admin.sidebar.sliders'           => 'Sliders',
    'admin.sidebar.add_slider'        => 'Add Slider',
    'admin.sidebar.add_option'        => 'Add Option',
    'admin.sidebar.option'            => 'Options',
    'admin.sidebar.slider_list'       => 'Sliders',
    'admin.sidebar.pages'             => 'Pages',
    'admin.sidebar.restaurants'       => 'Restaurants',
    'admin.sidebar.settings'          => 'Settings',
    'admin.sidebar.cities'            => 'Cities',
    'admin.sidebar.add_city'          => 'Add City',
    'admin.sidebar.states'            => 'States',
    'admin.sidebar.add_state'         => 'Add State',
    'admin.sidebar.send_notification' => 'Send Notification',

    /*
       |--------------------------------------------------------------------------
       | Master Panel Common Lines
       |--------------------------------------------------------------------------
    */

    'id'              => 'ID',
    'name'            => 'Name',
    'login'           => 'Login',
    'remember_me'     => 'Remember me',
    'submit'          => 'Submit',
    'cancel'          => 'Cancel',
    'apply'           => 'Apply',
    'status'          => 'Status',
    'approve'         => 'Approve',
    'reject'          => 'Reject',
    'action'          => 'Action',
    'edit'            => 'Edit',
    'delete'          => 'Delete',
    'details'         => 'Details',
    'save'            => 'Save',
    'active'          => 'Active',
    'inactive'        => 'InActive',
    'email'           => 'Email',
    'mobile_no'       => 'Mobile No',
    'password'        => 'Password',
    'address'         => 'Address',
    'date'            => 'Date',
    'image'           => 'Image',
    'price'           => 'Price',
    'view'            => 'View',
    'country'         => 'Country',
    'state'           => 'State',
    'city'            => 'City',
    'close'           => 'Close',
    'add'             => 'Add',
    'telephone'       => 'Telephone',
    'remove'          => 'Remove',
    'commission'      => 'Commission',
    'shop'            => 'Shop',
    'yes'             => 'Yes',
    'no'              => 'No',
    'cuisine'         => 'Cuisine',
    'order_accept'    => 'Order Accept',
    'filter'          => 'Filter',
    'description'     => 'Description',
    'country_code'    => 'Country Code',
    'select_option'   => 'Select Option',
    'status_changed'  => 'Status Changed Successfully',
    'status_message'  => 'Are you sure want to change status?',
    'delete_message'  => 'Are you sure want to delete this record?',
    'vendor_name'     => 'Vendor',
    'name_is_taken'   => 'This name is already exists',
    'order_accepted'  => 'Order Accept Status Changed',
    'restaurant_id'   => 'Restaurant',
    'payment_id'      => 'Payment Id',
    'total'           => 'Total',
    'delivery_date'   => 'Delivery Date',
    'delivery_time'   => 'Delivery Time',
    'payment_method'  => 'Payment Method',
    'user'            => 'User',
    'destroy_warning' => 'Are you sure want to delete this record',

    /*
    |--------------------------------------------------------------------------
    | Master Panel Cuisine Lines
    |--------------------------------------------------------------------------
    */

    'admin.cuisine.add_cuisine'      => 'Add Cuisine',
    'admin.cuisine.cuisine'          => 'Cuisines',
    'admin.cuisine.edit_cuisine'     => 'Edit Cuisine',
    'admin.cuisine.active_cuisine'   => 'Active Cuisine?',
    'admin.cuisine.inactive_cuisine' => 'Deactivate Cuisine?',
    'admin.cuisine.destroy_cuisine'  => 'Destroy Cuisine?',
    'admin.cuisine.image'            => 'Image',
    'admin.cuisine.cuisine_added'    => 'Cuisine Added Successfully',
    'admin.cuisine.cuisine_updated'  => 'Cuisine Updated Successfully',
    'admin.cuisine.cuisine_deleted'  => 'Cuisine Successfully Deleted',


    /*
        |--------------------------------------------------------------------------
        | Master Panel Option Lines
        |--------------------------------------------------------------------------
    */

    'admin.option.add_option'     => 'Add Option',
    'admin.option.option'         => 'Options',
    'admin.option.edit_option'    => 'Edit Option',
    'admin.option.destroy_option' => 'Destroy Option?',
    'admin.option.option_added'   => 'Option Added Successfully',
    'admin.option.option_updated' => 'Option Updated Successfully',
    'admin.option.option_deleted' => 'Option Successfully Deleted',
    'admin.option.add_term'       => 'Terms',


    'admin.option.add_option_value'     => 'Add Option Value',
    'admin.option.option_value'         => 'Option Values',
    'admin.option.edit_option_value'    => 'Edit Option Value',
    'admin.option.destroy_option_value' => 'Destroy Option Value?',
    'admin.option.option_value_added'   => 'Option Value Added Successfully',
    'admin.option.option_value_updated' => 'Option Value Updated Successfully',
    'admin.option.option_value_deleted' => 'Option Value Successfully Deleted',
    /*
    |--------------------------------------------------------------------------
    | Master Panel User Lines
    |--------------------------------------------------------------------------
    */

    'admin.user.user'               => 'Users',
    'admin.user.profile'            => 'Profile',
    'admin.user.active_user'        => 'Active User?',
    'admin.user.destroy_user'       => 'Destroy User?',
    'admin.user.inactive_user'      => 'Deactivate User?',
    'admin.user.password_changed'   => 'Password Updated Successfully',
    'admin.user.old_password_wrong' => 'Old Password is incorrect',
    'admin.user.profile_updated'    => 'Profile Updated Successfully',
    'admin.user.user_deleted'       => 'User Successfully Deleted',
    'admin.user.email_is_exists'    => 'This email is already exists',

    /*
    |--------------------------------------------------------------------------
    | Master Panel Item Lines
    |--------------------------------------------------------------------------
    */

    'admin.item.item'             => 'Items',
    'admin.item.item_attribute'   => 'Item',
    'admin.item.add_item'         => 'Add Item',
    'admin.item.edit_item'        => 'Edit Item',
    'admin.item.is_customization' => 'Is Customization',
    'admin.item.is_multiple'      => 'Is Multiple',

    /*
    |--------------------------------------------------------------------------
    | Master Panel City Lines
    |--------------------------------------------------------------------------
    */

    'admin.city.city'          => 'Cities',
    'admin.city.city_added'    => 'City Added',
    'admin.city.city_updated'  => 'City Updated',
    'admin.city.city_deleted'  => 'City Deleted',
    'admin.city.add_city'      => 'Add City',
    'admin.city.edit_city'     => 'Edit City',
    'admin.city.active_city'   => 'Active City',
    'admin.city.inactive_city' => 'InActive City',
    'admin.city.city_destroy'  => 'City Destroy',

    /*
    |--------------------------------------------------------------------------
    | Master Panel State Lines
    |--------------------------------------------------------------------------
    */

    'admin.state.state'              => 'States',
    'admin.state.state_added'        => 'State Added',
    'admin.state.state_updated'      => 'State Updated',
    'admin.state.state_deleted'      => 'State Deleted',
    'admin.state.add_state'          => 'Add State',
    'admin.state.edit_state'         => 'Edit State',
    'admin.state.state_destroy'      => 'State Destroy',
    'admin.state.active_state'       => 'Active State',
    'admin.state.inactive_state'     => 'InActive State',

    /*
    |--------------------------------------------------------------------------
    | Master Panel State Lines
    |--------------------------------------------------------------------------
    */
    'admin.country.active_country'   => 'Active Country',
    'admin.country.inactive_country' => 'InActive Country',

    /*
    |--------------------------------------------------------------------------
    | Master Panel Page Lines
    |--------------------------------------------------------------------------
    */

    'admin.page.page'      => 'Pages',
    'admin.page.add_page'  => 'Add Page',
    'admin.page.edit_page' => 'Edit Page',

    'admin.page.page_added'   => 'Page Added Successfully',
    'admin.page.page_updated' => 'Page Updated Successfully',
    'admin.page.page_deleted' => 'Page Deleted Successfully',

    /*
    |--------------------------------------------------------------------------
    | Master Panel Notification Lines
    |--------------------------------------------------------------------------
    */

    'admin.notification.send_notification'           => 'Send Notification',
    'admin.notification.title'                       => 'Title',
    'admin.notification.message'                     => 'Message',
    /*
    |--------------------------------------------------------------------------
    | Admin Panel Setting Lines
    |--------------------------------------------------------------------------
    */
    'admin.setting.commission'                       => 'Commission',
    'admin.setting.smtp_host'                        => 'SMTP Host',
    'admin.setting.smtp_username'                    => 'SMTP Username',
    'admin.setting.smtp_password'                    => 'SMTP Password',
    'admin.setting.smtp_port'                        => 'SMTP Port',
    'admin.setting.smtp_from_email'                  => 'SMTP From Email',
    'admin.setting.smtp_encryption'                  => 'SMTP Encryption',
    'admin.setting.delivery_charge'                  => 'Delivery Charge',
    'admin.setting.project_login_logo'               => 'Project Login Logo',
    'admin.setting.delivery_boy_commission'          => 'Delivery Boy Commission',
    'admin.setting.delivery_charge_km'               => 'Delivery Charge KM',
    'admin.setting.client_logo'                      => 'Client Logo',
    'admin.setting.google_api_key'                   => 'Google Api KEY',
    'admin.setting.distance'                         => 'Distance',
    'admin.setting.min_charge_delivery'              => 'Min Charge Delivery',
    'admin.setting.packaging_price'                  => 'Packaging Price',
    'admin.setting.payment_gateway'                  => 'Payment Gateway?',
    'admin.setting.cash_on_delivery_on_scratch_card' => 'Cash On Delivery Scratch Card?',
    'admin.setting.project_firebase_key'             => 'Project Firebase Key',
    'admin.setting.cash_on_delivery_limit'           => 'Cash On Delivery Limit',
    'admin.setting.contact_email'                    => 'Contact Email',

    /*
    |--------------------------------------------------------------------------
    | Common Message Api Lines
    |--------------------------------------------------------------------------
   */

    'api.error' => 'Something Went wrong.Please try again after sometime.',

    /*
    |--------------------------------------------------------------------------
    | Api Message Lines
    |--------------------------------------------------------------------------
    */

    'api.login.account_in_activated'      => 'Your account is deactivated by admin. Please contact admin.',
    'api.login.invalid_email_or_password' => 'Invalid email & password.',
    'api.user.no_user_found'              => 'User Not found.',
    'api.user.profile_updated'            => 'Profile update successfully.',
    'api.user.profile_image_updated'      => 'Profile image update successfully.',
    'api.user.password_updated'           => 'Password update successfully.',
    'api.user.old_password_is_wrong'      => 'You have enter old password is wrong.',

    'api.cart.more_than_one_shop'    => 'Replace cart item? \n  Your cart contains item from :old . Do you want to discard the selection and add item from :new?.',
    'api.cart.shop_is_not_available' => 'Sorry this shop is not available.',
    'api.cart.cart_item_deleted'     => 'Your cart item deleted successfully.',
    'api.cart.cart_empty'            => 'Your cart is empty.',

    'api.cart.cart_update'        => 'Card is updated.',
    'api.cart.cart_item_added'    => 'Item added to your cart.',
    'api.cart.item_not_available' => 'This item is not available.',

    'api.register.register_success' => 'Invalid email & password.',

    'api.cuisine.cuisine_not_found' => 'Cuisine not found.',

    'api.country.country_not_found' => 'Country not found.',

    'api.item.items_not_found' => 'Item not found.',

    'api.order.order_placed' => 'Order Place successfully.',

    'api.address.address_empty'   => 'You have no any address added.',
    'api.address.address_save'    => 'Address added successfully.',
    'api.address.address_update'  => 'Address updated successfully.',
    'api.address.address_deleted' => 'Address deleted successfully.',

    'api.contactUs.thank_you_for_contact_us' => 'Thank you for contact us we contact very soon.',

    'api.business.thank_you_for_add_business_request' => 'Thank you for add your business we contact very soon.',

    /*
    |--------------------------------------------------------------------------
    | Master Panel Order Lines
    |--------------------------------------------------------------------------
    */

    'admin.sidebar.orders' => 'Orders',
    'admin.order.order'    => 'Orders',
    'footer_address_ext'   => 'Our office address is',
    'footer_address'       => 'Baroda Gujarat India',
    'from_address'         => 'noreply@hwg.com',
    'copyright'            => 'Copyright © ' . date('Y') . 'Healing Water Group',
];
