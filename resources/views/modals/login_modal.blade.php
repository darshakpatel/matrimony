<div class="modal fade" id="LoginModal">
    <div class="modal-dialog modal-dialog-zoom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title fw-600">{{ trans('messages.Login')}}</h6>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="p-3">
                    <form class="" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input type="number" class="form-control"
                                   placeholder="Mobile number" name="mobile_no"
                                   id="mobile_no">
                        </div>

                        <div class="form-group">
                            <label class="form-label" for="password">{{ trans('messages.Password') }}</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                   name="password" id="password" placeholder="********" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="mb-3 text-right">
                            <a class="link-muted text-capitalize font-weight-normal"
                               href="{{ route('password.request') }}">{{ trans('messages.Forgot Password?') }}</a>
                        </div>

                        {{-- <div class="mb-5"> --}}
                        <button type="submit"
                                class="btn btn-block btn-primary">{{ trans('messages.Login to your Account') }}</button>
                        {{-- </div> --}}
                    </form>
                    <div class="text-center">
                        <p class="text-muted mb-0">{{ trans("Don't have an account?") }}</p>
                        <a href="{{ route('register') }}">{{ trans('messages.Create an account') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
