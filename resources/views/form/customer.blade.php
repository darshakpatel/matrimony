
<style>
    .table_Width { width: 100%; }
    .align-left { text-align: left; }
    .align-right { text-align: right; }
    .align-center { text-align: center; }
    .border { border: 1px solid #000; }
    .border-left { border-left: 1px solid #000; }
    .border-right { border-right: 1px solid #000; }
    .border-top { border-top: 1px solid #000; }
    .border-bottom { border-bottom: 1px solid #000; }
    .border-left_2 { border-left: 2px solid #000; }
    .border-right_2 { border-right: 1px solid #000; }
    .border-top_2 { border-top: 1px solid #000; }
    .border-bottom_2 { border-bottom: 1px solid #000; }
    .padding { padding: 5px; }
    .lineHeight { line-height: 17px; height: 17px; }
    .fontSize10 { font-size: 10px; font-weight: bold; }
    .fontSize9 { font-size: 9px; font-weight: bold; }
    .withLeft { width: 38%; }
    .withright { width: 62%; }
    .fallingsky { font-family: fallingsky; }
</style>
<table cellspacing="0" cellpadding="0">
    <tr>
        <td style="width: 1.5%;"></td>
        <td style="width: 8.5%;line-height: 15px;vertical-align: text-top;"><img style="width: 60px;" src="<?php echo K_PATH_IMAGES . 'enagic-logo.jpg'; ?>" /></td>
        <td class="align-center" style="width: 90%;"><h2 style="font-family:'daibanna'; color: #2957a4;font-size: 29px;line-height: 3px;">Enagic India Kangen Water Pvt. Ltd</h2>
            <table cellspacing="0" cellpadding="0">
                <tr><td style="height: 19px; line-height: 19px;"></td></tr>
                <tr><td style="font-weight: bold; font-size: 11px;">CIN - U41000TN2015PTC100366</td></tr>
                <tr><td style="font-size: 10px;line-height: 24px;">Regd. Off: No.55, Thandalam Vill. Sriperumbadhurt Taluk, Kancheepuram, Chennai TN 602 105. </td></tr>
                <tr><td style="font-size: 10px;">Corp. Office: The Millenia Tower B, 4th Floor, Unit 401 No. 1 &amp; 2, Murphy Road, Ulsoor, Bangalore 560-008. India.</td></tr>
                <tr><td style="font-weight: bold;font-size: 10px;line-height: 24px;">www.enagic.co.in | Ph: 080 46509900 | Fax: 080 46509908</td></tr>
            </table>
        </td>
    </tr>
</table>
<table>
    <tr><td style="line-height: -13px;font-size:8px;font-weight: bold;" class="fallingsky">FORM-DSA-IND.VI</td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td  class="border-left border-top border-bottom fallingsky" style="width: 100%;line-height: 28px;font-size: 14px;font-weight: bold;">&nbsp;&nbsp;PREFERRED CUSTOMER (USER) APPLICATION FORM</td>
    </tr>
</table>
<table>
    <tr><td style="line-height: 5px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td class="border" style="line-height: 12px;width: 15%;font-size: 10px;">&nbsp;CUSTOMER ID# <br/><small>&nbsp;&nbsp;for office use only</small></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td  style="line-height: 12px;width: 1.5%;"></td>
    </tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td style="line-height: 5px;"></td></tr>
    <tr>
        <td  class="border" style="width: 15%;line-height: 23px;font-size: 10px;">&nbsp;&nbsp;For Office Use Only</td>
        <td class="border-right border-top border-bottom" style="width: 36%;"></td>
    </tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 5px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 20px;color: #fff;font-size: 15px;">&nbsp;&nbsp;PRINCIPAL INFORMATION</td>
        <td style="width: 49%;line-height: 0px;">
            <div style="line-height: 1px;"></div>
            <div style="border-bottom: 9px solid #0646a5;line-height: 5px;"></div>
        </td>
    </tr>
    <tr><td class="border-left border-right"  style="line-height: 9px;width: 100%;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="border-right" style="font-size: 9px;">Please tick one your category in</td>
    </tr>
    <tr><td class="border-left border-right" style="line-height: 10px;width: 100%;"></td></tr>
</table>
<table>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="individual" value="Individual" @if(isset($userProfile->user->company_type) && $userProfile->user->company_type==3) checked="checked" @endif > </td>
        <td style="width: 7%;font-size: 9px;background-color: #241f21;line-height: 13px;vertical-align: middle"><label style="color: #fff;">&nbsp;Individual</label></td>
        <td style="width: 5%;"></td>
        <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="sole_proprietorship" value="Sole Proprietorship" @if(isset($userProfile->user->company_type) && $userProfile->user->company_type==2) checked="checked" @endif > </td>
        <td style="width: 12.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Sole Proprietorship</label></td>
        <td style="width: 5%;"></td>
        <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="partnership_firm" value="Partnership Firm" @if(isset($userProfile->user->company_type) && $userProfile->user->company_type==4) checked="checked" @endif /></td>
        <td style="width: 11.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Partnership Firm</label></td>
        <td style="width: 5%;"></td>
        <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="private_limited_company" value="Private Limited Company" @if(isset($userProfile->user->company_type) && $userProfile->user->company_type==5) checked="checked" @endif /></td>
        <td style="width: 15.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Private Limited Company</label></td>
        <td style="width: 5%;"></td>
        <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"> <input style="line-height: 18px;" type="checkbox" name="others" value="Others" @if(isset($userProfile->user->company_type) && $userProfile->user->company_type==6) checked="checked" @endif /></td>
        <td style="width: 5.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Others</label></td>
        <td style="width: 13.5%;font-size: 6.5px;line-height: 13px;border-bottom: 1px solid #000;">
            (Please provide details)
        </td>
        <td class="border-right" style="width: 4%;"></td>
    </tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td class="border-left border-right" style="line-height: 4px;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="border" style="width: 95.5%; line-height: 22px;"><input type="text" style="font-size: 16px;width: 100%; border: none" size="80" name="theentity" value="{{$userProfile->user->company_name}}" /> </td>
        <td class="border-right" style="width: 3.5%;"></td>
    </tr>
    <tr><td class="border-left border-right" style="line-height: 8px;width: 100%;"></td></tr>
    <tr>
        <td class="border-left border-right" style="width: 100%;font-size: 10px;color: #a7a6a6;">
            &nbsp;Kindly give name of the legal entity (the Entity) formed solely to complete this preferred customer application form
        </td>
    </tr>
    <tr><td class="border-left border-right" style="line-height: 15px;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td  style="width: 95.5%;font-size: 14px;font-weight: bold;">Name</td>
        <td class="border-right" style="width: 3.5%;"></td>
    </tr>
    <tr><td class="border-left border-right" style="line-height: 11px;width: 100%;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="border" style="width: 30.8%;line-height: 22px;"><input type="text" style="font-size: 16px;width: 100%;" size="26" name="firstname" value="{{$userProfile->user->first_name}}" > </td>
        <td style="width: 1.4%"></td>
        <td class="border" style="width: 30.8%;line-height: 22px;"><input type="text" style="font-size: 16px;width: 100%;" size="26" name="middlename" value="{{$userProfile->user->middle_name}}" > </td>
        <td style="width: 1.4%"></td>
        <td class="border" style="width: 30.8%;line-height: 22px;"><input type="text" style="font-size: 16px;width: 100%;" size="26" name="lastname" value="{{$userProfile->user->last_name}}" > </td>
        <td class="border-right" style="width: 4%;"></td>
    </tr>
</table>
<table>
    <tr>
        <td class="border-left" style="line-height: 15px;width: 1.5%;"></td>
        <td style="line-height: 15px;width: 28%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;First Name</td>
        <td style="line-height: 15px;width: 4.5%;"></td>
        <td style="line-height: 15px;width: 28%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;Middle Name</td>
        <td style="line-height: 15px;width: 4.5%;"></td>
        <td  style="line-height: 15px;width: 28%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;Last Name</td>
        <td class="border-right" style="width: 6%;"></td>
    </tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td class="border-left border-right" colspan="2"  style="line-height: 7px;width:100%;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="line-height: 10px;">Residency Status <small>(only applicable to individual applicants)</small></td>
        <td style="line-height: 18px;"><input style="" type="checkbox" name="Citizenrese" value="1" @if($userProfile->user->residency_status=='indian') checked="checked" @endif > <label class="fallingsky">Citizen of and resident in India</label></td>
        <td class="border-right" style="width: 6%;"></td>
    </tr>
    <tr><td class="border-left border-right" colspan="2"  style="line-height: 7px;width:100%;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td style="width: 33.8%;" class="fallingsky">Date of Birth</td>
        <td class="fallingsky">&nbsp;&nbsp;Gender</td>
        <td class="border-right" style="width: 6%;"></td>
    </tr>
    <tr><td class="border-left border-right" colspan="2"  style="line-height: 10px;width:100%;"></td></tr>
</table>
@php
$dob=explode('-',$userProfile->user->date_of_birth);
@endphp
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="border" style="width:5%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="6" name="dobdate" value="{{isset($dob[2]) ? $dob[2] : ''}}" ></td>
        <td style="width:1.3%;line-height: 20px;"></td>
        <td class="border" style="width:5%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="6" name="dobmonth" value="{{isset($dob[1]) ? $dob[1] : ''}}" ></td>
        <td style="width:1.3%;line-height: 20px;"></td>
        <td class="border" style="width:8%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="9" name="dobyear" value="{{isset($dob[0]) ? $dob[0] : ''}}" > </td>
        <td style="width:12.8%;"></td>
        <td style="width:2.9%;line-height: 22px;border: 1px solid #000;"><input style="line-height: 22px;width: 50px;" type="checkbox" name="mgender" value="Male" ></td>
        <td style="width: 4%;"></td>
        <td style="width:2.9%;line-height: 22px;border:1px solid #000;"><input style="line-height: 22px;width: 50px;" type="checkbox" name="fgender" value="Feale"  ></td>
        <td style="width: 4%;"></td>
        <td style="width:2.9%;line-height: 22px;border:1px solid #000;"><input style="" type="checkbox" name="ogender" value="Other"  ></td>
        <td class="border-right" style="width: 50%;"></td>
    </tr>
</table>
<table>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td style="line-height: 15px;width: 6.5%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;DD</td>
        <td style="line-height: 15px;width: 6.5%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;MM</td>
        <td style="line-height: 15px;width: 7.5%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;YYYY</td>
        <td style="line-height: 15px;width: 11.8%;font-size: 7px;"></td>
        <td style="line-height: 15px;width: 7%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;&nbsp;&nbsp;Male</td>
        <td style="line-height: 15px;width: 7%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;&nbsp;Female</td>
        <td style="line-height: 15px;width: 5%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;&nbsp;&nbsp;Other</td>
        <td class="border-right" style="width: 50%;"></td>
    </tr>
    <tr><td class="border-left border-right border-bottom" colspan="2"  style="line-height: 6px;width:100%;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 3px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 20px;color: #fff;font-size: 15px;">&nbsp;&nbsp;ADDRESS DETAILS</td>
        <td style="width: 49.5%;line-height: 3px;">
            <div class="fallingsky" style="line-height: 3px;font-size: 8.5px;">&nbsp;&nbsp;</div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 5px;width:100%;"></td></tr>
    <tr>
        <td class="fallingsky border-left border-right"  style="font-size: 6.5px;width:100%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please provide your complete postal address with pin code and attach a valid address proof along with this application form. Your application will be rejected without valid address proof.</td>
    </tr>
</table>
<table>

    <tr><td class=" border-left border-right" colspan="3" style="line-height: 9px;width:100%;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 11%;">
            <div style="border-bottom: 1px solid #000;line-height: 8.5px;">Mailing Address&nbsp;</div></td>
        <td  style="width:86%;"><table><tr><td class="border" style="line-height:22px; width: 98%;"><input type="text" style="font-size: 13px;width: 100%;" size="87" name="address" value="{{$userProfile->user->address}}" ></td></tr></table></td>
        <td class="border-right"></td>
    </tr>
    <tr><td class=" border-left border-right" colspan="3" style="line-height: 4px;width:100%;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 18%;">
            <div style="border-bottom: 1px solid #000;line-height: 8.5px;">City / Town / Village <small>(Mandatory)</small>&nbsp;</div></td>
        <td  style="width:79.4%;"><table><tr><td class="border" style="line-height:22px; width: 97.5%;"><input type="text" style="font-size: 13px;width: 100%;" size="80" name="city" value="{{$userProfile->user->town}}" ></td></tr></table></td>
        <td class="border-right"></td>
    </tr>
</table>
<table>
    <tr><td class=" border-left border-right" colspan="3" style="line-height: 4px;width:100%;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 19.5%;">
            <div style="border-bottom: 1px solid #000;line-height: 8.5px;">Post office <small>(In case of village, Mandatory)</small>&nbsp;</div></td>
        <td class="" style="width:30%;"><table><tr><td class="border" style="line-height:22px; width: 97%;"><input type="text" style="font-size: 13px;width: 100%;" size="30" name="postoffice" value="" ></td></tr></table></td>
        <td class="" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 5%;">
            <div style="border-bottom: 1px solid #000;line-height: 8.5px;">District&nbsp;</div></td>
        <td style="width:42.9%;"><table><tr><td class="border" style="line-height:22px; width: 93%;"><input type="text" style="font-size: 13px;width: 100%;" size="41" name="discrit" value="{{$userProfile->user->city->name}}" ></td></tr></table></td>
        <td class="border-right"></td>
    </tr>
</table>
<table>
    <tr><td class=" border-left border-right" colspan="3" style="line-height: 4px;width:100%;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 12%;">
            <div style="border-bottom: 1px solid #000;line-height: 8.5px;">PIN Code <small>(Mandatory)</small>&nbsp;</div></td>
        <td class="" style="width:15%;"><table><tr><td class="border" style="line-height:22px; width: 95%;"><input type="text" style="font-size: 13px;width: 100%;" size="15" name="pincode" value="{{$userProfile->user->pincode}}" ></td></tr></table></td>
        <td class="fallingsky" style="font-size: 10px;width: 9%;">
            <div style="border-bottom: 1px solid #000;line-height: 8.5px;">State <small>(Mandatory)</small>&nbsp;</div></td>
        <td class="" style="width:27.5%;"><table><tr><td class="border" style="line-height:22px; width: 95.1%;"><input type="text" style="font-size: 13px;width: 100%;" size="27" name="state" value="{{$userProfile->user->state->name}}" ></td></tr></table></td>
        <td class="fallingsky" style="font-size: 10px;width: 12.5%;">
            <div style="border-bottom: 1px solid #000;line-height: 8.5px;">Mobile No. <small>(Mandatory)</small></div></td>
        <td style="width:22.4%;"><table><tr><td class="border" style="line-height:22px; width: 87%;"><input type="text" style="font-size: 13px;width: 100%;" size="20" name="mobileno" value="{{$userProfile->user->mobile_no}}" ></td></tr></table></td>
        <td class="border-right"></td>
    </tr>
</table>
<table>
    <tr><td class=" border-left border-right" colspan="3" style="line-height: 4px;width:100%;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 15%;">
            <div style="border-bottom: 1px solid #000;line-height: 8.5px;">E-mail address: <small>(Mandatory)</small>&nbsp;</div></td>
        <td style="width:83.4%;"><table><tr><td class="border" style="line-height:22px; width: 96.5%;"><input type="text" style="font-size: 13px;width: 100%;" size="83" name="emailadd" value="{{$userProfile->user->email}}" ></td></tr></table></td>
        <td class="border-right"></td>
    </tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td  style="width:98.4%;line-height: 5px;"><small>Photo Identity Proof (Attach photocopy)</small></td>
        <td class="border-right"></td>
    </tr>
    <tr><td class="border-left border-right" colspan="4"  style="line-height:8px;width:100%;"></td></tr>
</table>
<table>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
            <input style="" type="checkbox" name="ElectionCard" value="Election Card" @if($userProfile->identity_proof==1) checked="checked" @endif />
        </td>
        <td style="width: 8%;font-size: 9px;line-height: 16px;"><label>&nbsp;Election Card</label></td>
        <td  style="width: 0.5%;"></td>
        <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
            <input style="" type="checkbox" name="DrivingLicense" value="Driving License" @if($userProfile->identity_proof==2) checked="checked" @endif />
        </td>
        <td style="width: 10%;font-size: 9px;line-height: 16px;"><label>&nbsp;Driving License</label></td>
        <td  style="width: 0.5%;"></td>
        <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
            <input style="" type="checkbox" name="Passport" value="Passport" @if($userProfile->identity_proof==3) checked="checked" @endif />
        </td>
        <td style="width: 6%;font-size: 9px;line-height: 16px;"><label>&nbsp;Passport</label></td>
        <td  style="width: 0.5%;"></td>
        <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
            <input style="" type="checkbox" name="UIDCard" value="UID / Aadhar Card" @if($userProfile->identity_proof==4) checked="checked" @endif />
        </td>
        <td style="width: 11%;font-size: 9px;line-height: 16px;"><label>&nbsp;UID / Aadhar Card</label></td>
        <td  style="width: 0.5%;"></td>
        <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
            <input style="" type="checkbox" name="Anyothe" value="Any othe"  />
        </td>
        <td style="width: 6%;font-size: 9px;line-height: 16px;"><label>&nbsp;Any other</label></td>
        <td style="width: 41.9%;line-height: 15px;"><input type="text" style="font-size: 14px;width: 100%;" size="37" name="Anyothertext" value="" @if($userProfile->identity_proof==5) checked="checked" @endif ></td>
        <td class="border-right"></td>
    </tr>
    <tr><td class="border-left border-right border-bottom" style="line-height: 3px;width: 100%;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 8px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 20px;color: #fff;font-size: 15px;">&nbsp;&nbsp;SALES FACILITATOR</td>
        <td style="width: 49.5%;line-height: 0px;">
            <div class="fallingsky" style="line-height: 3.6px;font-size: 12px;">&nbsp;&nbsp;Identify the Direct Seller who will be your Sales Facilitator.</div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
</table>
<table>
    <tr><td class="border-left border-right" style="line-height: 3px;width: 100%;"></td></tr>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="fallingsky" style="font-size: 12px;line-height: 14px;width: 17%;">Sales Facilitator&#39;s Name</td>
        <td class="border" style="width:29.5%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="33" name="salesname" value="{{$userProfile->sales_facilitator_name}}" > </td>
        <td class=" border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 12px;line-height: 14px;width: 17%;">Sales Facilitator&#39;s ID No.</td>
        <td class="border" style="width:32%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="36" name="salesidno" value="{{$userProfile->sales_facilitator_id}}" > </td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right" style="width: 100%;line-height: 3px;"></td></tr>
    <tr>
        <td class="border-left" style="width:0.8%;"></td>
        <td class="fallingsky" style="width:8.5%;line-height: 12px;font-size: 9.5px">DECLARATION:</td>
        <td class="fallingsky" style="width: 50%;line-height: 12px;font-size: 9.5px;">I hereby declare that the sale/applicant information which I have agreed herewith to<br> allocate to the sponsor mentioned below under my own free will.</td>
        <td class="" style="width:5%;"></td>
        <td style="width: 30%;" class="border-bottom"></td>
        <td class=" border-right"></td>
    </tr>
    <tr>
        <td class="border-left" style="width:70%;"></td>
        <td class="fallingsky border-right" style="width:30%;line-height: 12px;font-size: 9.5px">SALES FACILITATOR&#39;S SIGNATURE</td>
    </tr>
    <tr><td class=" border-left border-right border-bottom" style="width: 100%;line-height: 3px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 8px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 20px;color: #fff;font-size: 15px;">&nbsp;&nbsp;SPONSOR INFORMATION</td>
        <td style="width: 49.5%;line-height: 0px;">
            <div class="fallingsky" style="line-height: 7.2px;font-size: 12px;"></div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 3px;width:100%;"></td></tr>
</table>
<table>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="fallingsky" style="font-size: 12px;line-height: 14px;width: 16%;">Direct Seller Name</td>
        <td class="border" style="width:29.5%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="33" name="Sponsorname" value="{{$userProfile->direct_seller_name}}" > </td>
        <td class=" border-left" style="width: 1%;"></td>
        <td class="fallingsky align-right" style="font-size: 12px;line-height: 14px;width: 14.5%;">E-mail Address&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="border" style="width:35.8%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="40" name="Sponsoremail" value="{{$userProfile->direct_seller_email}}" > </td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right" style="width: 100%;line-height: 5px;"></td></tr>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="fallingsky" style="font-size: 12px;line-height: 14px;width: 16%;">Direct Seller / ID NO.</td>
        <td class="border" style="width:29.5%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="33" name="Sponsorimdid" value="{{$userProfile->direct_seller_id}}" > </td>
        <td class=" border-left" style="width: 1%;"></td>
        <td class="fallingsky align-right" style="font-size: 12px;line-height: 14px;width: 14.5%;">Mobile&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="border" style="width:35.8%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="40" name="Sponsormobile" value="{{$userProfile->direct_seller_mobile_no}}" > </td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right" style="width: 100%;line-height: 5px;"></td></tr>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="fallingsky" style="font-size: 12px;line-height: 14px;width:22%;">Register the Applicant As Your</td>
        <td class="border" style="width:7%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="8" maxlength="5" name="SponsorApplicant" value="" > </td>
        <td style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 14px;line-height: 14px;width:6%;">&nbsp;A <small>(Rank)</small></td>
        <td class="" style="width: 0.8%;"></td>
        <td class="fallingsky " style="font-size: 10px;line-height: 9px;width: 60%;">Note: Applicant will be registered under rank which you have mentioned in left side. <br>Please clarify your doubt if any related to same before submitting your application.</td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right border-bottom" style="width: 100%;line-height: 7px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 6px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 20px;color: #fff;font-size: 15px;">&nbsp;&nbsp;DECLARATION</td>
        <td style="width: 49.5%;line-height: 0px;">
            <div class="fallingsky" style="line-height: 3px;font-size: 9px;">&nbsp;&nbsp;</div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 3px;width:100%;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td class=" border-left" style="width:0.8%;"></td>
        <td  style="font-size: 16px;line-height: 16px;width: 98%;">I do hereby declare that the information furnished abovr is true and I am legally competent to enter in this contract and further id enagic Indian Kangen water Pvt. Ltd. accepts this application, I will be bound by the terms and conditions stated below/in this application from followed by page No. 2/2</td>
        <td class=" border-right" style="width:1.2%;"></td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 45px;width:100%;"></td></tr>
</table>
@php
    $application_date=explode('-',$userProfile->application_date)
    @endphp
<table>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="border-bottom" style="width: 23.5%;line-height: 25px;"></td>
        <td style="width: 1%;"></td>
        <td style="width: 24%;"><table class="table_Width" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="border" style="width:25%;line-height: 17px;"><input type="text" style="font-size: 17px;width: 100%;" size="5" name="date1" value="{{isset($application_date[2]) ? $application_date[2] : ''}}" ></td>
                    <td style="width:3%;line-height: 17px;"></td>
                    <td class="border" style="width:25%;line-height: 17px;"><input type="text" style="font-size: 17px;width: 100%;" size="5" name="month1" value="{{isset($application_date[1]) ? $application_date[1] : ''}}" ></td>
                    <td style="width:3%;line-height: 17px;"></td>
                    <td class="border" style="width:32%;line-height: 17px;"><input type="text" style="font-size: 17px;width: 100%;" size="6" name="year1" value="{{isset($application_date[0]) ? $application_date[0] : ''}}" > </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="line-height: 12px;width: 28%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;DD</td>
                    <td style="line-height: 12px;width: 30%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;MM</td>
                    <td style="line-height: 12px;width: 30%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;YYYY</td>
                </tr>
            </table>
        </td>
        <td style="width: 1%;"></td>
        <td class="border-bottom" style="width: 23.5%;line-height: 25px;"></td>
        <td style="width: 1%;"></td>
        <td style="width: 24%;"><table class="table_Width" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="border" style="width:25%;line-height: 17px;"><input type="text" style="font-size: 17px;width: 100%;" size="5" name="date2" value="{{isset($application_date[2]) ? $application_date[2] : ''}}" ></td>
                    <td style="width:3%;line-height: 17px;"></td>
                    <td class="border" style="width:25%;line-height: 17px;"><input type="text" style="font-size: 17px;width: 100%;" size="5" name="month2" value="{{isset($application_date[1]) ? $application_date[1] : ''}}" ></td>
                    <td style="width:3%;line-height: 17px;"></td>
                    <td class="border" style="width:32%;line-height: 17px;"><input type="text" style="font-size: 17px;width: 100%;" size="6" name="year2" value="{{isset($application_date[0]) ? $application_date[0] : ''}}" > </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="line-height: 12px;width: 28%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;DD</td>
                    <td style="line-height: 12px;width: 30%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;MM</td>
                    <td style="line-height: 12px;width: 30%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;YYYY</td>
                </tr>
            </table>
        </td>
        <td class="border-right " style="width: 1%;"></td>
    </tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky align-center" style="width: 23.5%;line-height: 12px;">APPLICANT&#39;S SIGNATURE:</td>
        <td class="" style="width: 1%;"></td>
        <td class="fallingsky align-center  " style="width: 23.5%;line-height: 12px;">DATE</td>
        <td class="" style="width: 1%;"></td>
        <td class="fallingsky align-center" style="width: 23.5%;line-height: 12px;">SPONSOR&#39;S SIGNATURE:</td>
        <td class="fallingsky align-center" style="width: 23.5%;line-height: 12px;">DATE</td>
        <td class="border-right " ></td>
    </tr>
    <tr><td class="fallingsky border-left border-right "  style="line-height: 10px;width:100%;"></td></tr>
    <tr><td class="fallingsky border-left border-right align-right"  style="line-height: 10px;width:100%;">Page 1/2&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
    <tr><td class=" border-left border-right border-bottom"  style="line-height: 10px;width:100%;"></td></tr>
</table>

<style>
    .table_Width { width: 100%; }
    .align-left { text-align: left; }
    .align-right { text-align: right; }
    .align-center { text-align: center; }
    .border { border: 1px solid #000; }
    .border-left { border-left: 1px solid #000; }
    .border-right { border-right: 1px solid #000; }
    .border-top { border-top: 1px solid #000; }
    .border-bottom { border-bottom: 1px solid #000; }
    .border-left_2 { border-left: 2px solid #000; }
    .border-right_2 { border-right: 1px solid #000; }
    .border-top_2 { border-top: 1px solid #000; }
    .border-bottom_2 { border-bottom: 1px solid #000; }
    .padding { padding: 5px; }
    .lineHeight { line-height: 17px; height: 17px; }
    .fontSize10 { font-size: 10px; font-weight: bold; }
    .fontSize9 { font-size: 9px; font-weight: bold; }
    .withLeft { width: 38%; }
    .withright { width: 62%; }
    .fallingsky { font-family: fallingsky; }
</style>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td style="border: 3px solid #0646a5;">
            <table>
                <tr>
                    <td class="fallingsky align-center" style="font-weight: bold;line-height: 15px;font-size: 14px;">PREFERRED CUSTOMER - TERMS AND CONDITIONS</td>
                </tr>
                <tr><td style="line-height: 4px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 13px;font-size: 12px;">The following Terms and Conditions form part of the Preferred Customer Application (the &#39;Application&#39;) and together with the Application constitute the Preferred Customer Agreement (the &#39;Agreement"). The Agreement shall come into force upon acceptance of the Application by Enagic India Kangen Water Pvt. Ltd on the date of such acceptance (&quot;Effective Date&#39;).</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 14px;font-size: 13px;width: 100%;">1. GENERAL INFORMATION&amp; CONDITION FOR PREFERRED CUSTOMER("PC")</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">a) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11.5px; width: 96%;">Please make sure that you and your servicing direct seller sign the application before submitting it to company for processing.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">b) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11.5px; width: 96%;">Please submit proof of identification and address by way of, Aadhar Card, Election ID,Driving License, Passport, PAN etc.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">c) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11.5px; width: 96%;">Preferred Customer status does not entitle the Purchaser to start sales solicitation activities. The product sale activity by a PC will not be accepted. A Preferred Customer registration is primarily for the purpose of maintenance and after sale service.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">d) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">If you wish to start soliciting activities in products of Company you can change your "Preferred Customer" status to "EnagicDirect seller" by following designated procedure to become "Direct Seller".</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">2. ORDERING:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">The Preferred Customer may place orders for the Company products or services upon coming into force of the Agreement.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">3. PRODUCT DELIVERY TIME &amp; METHOD:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">a) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">The product will be delivered within 5-7 working days by the courier afterthe &quot;Product Purchase Order Form&quot; and payment confirmation.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">b) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">In case of any crisis situation/ any statutory requirement is pending from cutomer side/ any departmental issue, it may get delay till resolving the respective issues.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">c) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">Delivery charges applicable on actual basis.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">4. DURATION:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">This Agreement shall remain valid unless terminated in writing by the Preferred Customer or Enagic India.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">5. AMENDMENT OF THE AGREEMENT BY ENAGIC INDIA:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">Company may from time to time amend the Agreement through notice on its website, www.enagic.co.in. If the Preferred Customer does not agree to be bound by such amendment(s), he/she may terminate the Agreement. Otherwise, the Preferred Customer&#39;s continued relationship with the Company constitutes an affirmative acknowledgment by him/her of the amendment(s), and his/her agreement to be bound by the same.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">6. TERMINATION:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">a) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">Either party may terminate this agreement by giving a written notice to the other.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">b) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">The Agreement shall terminate automatically, with immediate effect, in the event that the Preferred Customer is appointed as a Direct Seller.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">c) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">Enagic India may terminate this Agreement forthwith for cause or due to legal or regulatory requirements by giving a written notice to the Preferred Customer.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">7. SERVICING DISTRIBUTOR:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">The Preferred Customer agrees that a Servicing Distributor, if not indicated on the Application, will be assigned to him/her. The Preferred Customer hereby consents to permit the Servicing Distributor to contact him/her in relation to products offered by the Company, including notifications of promotions, and to the transfer of his/her data to the Servicing Distributor in accordance with the Privacy Policy. The Servicing Distributor will be available to assist the Preferred Customer with Product information, purchases, refunds or complaints.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">8. CONDITIONS FOR SALE:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;"><strong>Ordering and Invoices:</strong> The supply of Products by the Company under an invoice shall be subject to the following provisions:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">a) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">Weights, measures and statements as to quantity, quality, date of manufacture and other descriptive data as contained on the packing shall be presumed to be correct.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">b) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">The Preferred Customer shall have legal title to the Product when it is handed over to the Preferred Customer or the Carrier, as the case may be.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">c) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">The Preferred Customer must verify conformity of the Product with the order before accepting delivery.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">d) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">Prices are inclusive of all taxes as on the date of sale.</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">e) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 11px; width: 96%;">Company is not liable for delays or non-delivery of Products by the carrier due to Force Majeure or other circumstances beyond its reasonable control, or any direct or indirect loss or damages arising therefrom.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"><strong>9.</strong></td>
                    <td class="" style="line-height: 15px;font-size: 13px;width: 96%;"><strong>Self-Consumption:</strong> The Preferred Customer agrees that he/she shall use the products purchased from the Company for his/her self-consumption only, and shall neither resell the products nor use the same for any commercial purpose.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2.5%;"><strong>10.</strong></td>
                    <td class="" style="line-height: 15px;font-size: 13px;width: 96%;"><strong> Returns:</strong> Returns and refunds are subject to the Company Refund Policy available in the Policies and procedures handbook.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2.5%;"><strong>11.</strong></td>
                    <td class="" style="line-height: 15px;font-size: 13px;width: 96%;"><strong> No Assignment:</strong> This Agreement is entered into on a personal basis, and neither this Agreement nor any of the rights or obligations of the Preferred Customer arising hereunder may be assigned or transferred without the prior written consent of the Company.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2.5%;"><strong>12.</strong></td>
                    <td class="" style="line-height: 15px;font-size: 13px;width: 96%;"><strong> Severability:</strong> If any provision of these Terms and Conditions is declared invalid or unenforceable, the remaining provisions shall remain in full force and effect.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>

                <tr><td style="line-height: 10px;"></td></tr>
                <tr><td class="align-right fallingsky"  style="width:100%;line-height: 5px;">Page 2/2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
                <tr><td style="line-height: 10px;"></td></tr>
            </table>
        </td>
    </tr>
</table>
