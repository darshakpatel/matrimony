<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */
// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf.php');

class MYPDF extends TCPDF {

    public function AcceptPageBreak() {
        $pageno = $this->PageNo();
        $this->SetMargins(0, 0, 0, false);
        return $this->AutoPageBreak;
    }

}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Form 1');
// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(0, 4, 0);

$pdf->SetAutoPageBreak(TRUE, 0);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setFontSubsetting(true);

$pdf->AddFont('daibanna');
$pdf->AddFont('fallingsky');
$pdf->SetFont('helvetica', '', 10, '', false);
$pdf->setCellHeightRatio(0);
// Add a page
// This method has several options, check the source code documentation for more information.
ob_start();
?>
<style>
    .table_Width { width: 100%; }
    .align-left { text-align: left; }
    .align-right { text-align: right; }
    .align-center { text-align: center; }
    .border { border: 1px solid #000; }
    .border-left { border-left: 1px solid #000; }
    .border-right { border-right: 1px solid #000; }
    .border-top { border-top: 1px solid #000; }
    .border-bottom { border-bottom: 1px solid #000; }
    .border-left_2 { border-left: 2px solid #000; }
    .border-right_2 { border-right: 1px solid #000; }
    .border-top_2 { border-top: 1px solid #000; }
    .border-bottom_2 { border-bottom: 1px solid #000; }
    .padding { padding: 5px; }
    .lineHeight { line-height: 17px; height: 17px; }
    .fontSize10 { font-size: 10px; font-weight: bold; }
    .fontSize9 { font-size: 9px; font-weight: bold; }
    .withLeft { width: 38%; }
    .withright { width: 62%; }
    .fallingsky { font-family: fallingsky; }
</style>
<table cellspacing="0" cellpadding="0">
    <tr>
        <td style="width: 1.5%;"></td>
        <td style="width: 8.5%;line-height: 15px;vertical-align: text-top;"><img style="width: 60px;" src="<?php echo K_PATH_IMAGES . 'enagic-logo.jpg'; ?>" /></td>
        <td class="align-center" style="width: 90%;"><h2 style="font-family:'daibanna'; color: #2957a4;font-size: 29px;line-height: 3px;">Enagic India Kangen Water Pvt. Ltd</h2>
            <table cellspacing="0" cellpadding="0">
                <tr><td style="height: 19px; line-height: 19px;"></td></tr>
                <tr><td style="font-weight: bold; font-size: 11px;">CIN - U41000TN2015PTC100366</td></tr>
                <tr><td style="font-size: 10px;line-height: 24px;">Regd. Off: No.55, Thandalam Vill. Sriperumbadhurt Taluk, Kancheepuram, Chennai TN 602 105. </td></tr>
                <tr><td style="font-size: 10px;">Corp. Office: The Millenia Tower B, 4th Floor, Unit 401 No. 1 &amp; 2, Murphy Road, Ulsoor, Bangalore 560-008. India.</td></tr>
                <tr><td style="font-weight: bold;font-size: 10px;line-height: 24px;">www.enagic.co.in | Ph: 080 46509900 | Fax: 080 46509908</td></tr>
            </table>
        </td>
    </tr>
</table>
<table>
    <tr><td style="line-height: -13px;font-size:8px;font-weight: bold;" class="fallingsky">FORM-DSA-IND.VI</td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td  class="border-left border-top border-bottom fallingsky" style="width: 26.5%;line-height: 24px;font-size: 12px;font-weight: bold;">&nbsp;&nbsp;DIRECT SELLER APPLICATION</td>
        <td style="width: 76.5%;line-height: 7.5px;font-size: 5px" class="border-right border-top border-bottom">Please complete this Direct Seller Application (the &quot;Application&quot;) in English for free appointment as a Direct Seller for Enagic India Kangen Water Pvt. Ltd.(here in after &#39;Enagic India&#39; or &#39;Company&#39;).<br>Applicants must be 18 years of age of above. All sections are mandatory unless otherwise indicated. Please furnish all required supporting documentation. Acceptance of this direct seller applications is at the sole discretion of Enagic India.<br>Further , Enagic India reserves the right to reject the application for any reason and to terminate or revoke any distributorship for reasons not limited to provision of incomplete, inaccurate, false or misleading information.</td>
    </tr>
</table>
<table>
    <tr><td style="line-height: 3px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td style="background-color: #0646a5;color:#fff;" class="fallingsky">
            <table>
                <tr><td style="line-height: 3px;"></td></tr>
                <tr>
                    <td style="line-height: 8px;font-size: 7px;width: 38%;">&nbsp;Has the Applicant previously been an Enagic India direct seller in India? Tick one</td>
                    <td style="background-color: #fff;line-height: 15px;width: 1.7%;"><input style="line-height: 18px;" type="checkbox" name="appyes1" value="Yes"  /></td>
                    <td style="line-height: 10px;font-size: 8px;width: 4%;">&nbsp;&nbsp;Yes</td>
                    <td style="background-color: #fff;line-height: 15px;width: 1.7%;"><input style="line-height: 18px;" type="checkbox" name="appno1" value="No"  /></td>
                    <td style="line-height: 10px;font-size: 8px;width: 4%;">&nbsp;&nbsp;No</td>
                    <td style="line-height: 10px;font-size: 7px;width: 18%;">No If yes*, your Previous ID Number</td>
                    <td class="border" style="line-height: 10px;font-size: 7px;width: 23%;"><input type="text" style="font-size: 12px;width: 100%;" size="35" name="idnumber1" value="" ></td>
                </tr>
                <tr>
                    <td style="line-height: 10px;font-size: 7px;width: 38%;">&nbsp;Is/was your spouse an Enagic India direct seller in India Tick one</td>
                    <td style="background-color: #fff;line-height: 15px;width: 1.7%;"><input style="line-height: 18px;" type="checkbox" name="appyes2" value="Yes"  /></td>
                    <td style="line-height: 10px;font-size: 8px;width: 4%;">&nbsp;&nbsp;Yes</td>
                    <td style="background-color: #fff;line-height: 15px;width: 1.7%;"><input style="line-height: 18px;" type="checkbox" name="appno2" value="No"  /></td>
                    <td style="line-height: 10px;font-size: 8px;width: 4%;">&nbsp;&nbsp;No</td>
                    <td style="line-height: 10px;font-size: 7px;width: 18%;">No If yes*, your Previous ID Number</td>
                    <td class="border" style="line-height: 10px;font-size: 7px;width: 23%;"><input type="text" style="font-size: 12px;width: 100%;" size="35" name="idnumber2" value="" ></td>
                </tr>
                <tr><td style="line-height: 6px;"></td></tr>
            </table>
        </td>
    </tr>
</table>
<table>
    <tr><td style="line-height: 3px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td class="border" style="line-height: 12px;width: 12%;font-size: 10px;">&nbsp;Direct Seller ID# <br/><small>&nbsp;&nbsp;for office use only</small></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td class="border-right border-top border-bottom" style="line-height: 10px;width: 4%;"></td>
        <td  style="line-height: 12px;width: 1.5%;"></td>
        <td class="border" style="line-height: 23px;width: 51%;font-size: 10px;"></td>
    </tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 2px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 15px;color: #fff;font-size: 12px;font-weight: bold;">&nbsp;&nbsp;PRINCIPAL INFORMATION</td>
        <td style="width: 49.5%;line-height: 0px;">
            <div class="fallingsky" style="line-height: 1px;font-size: 8.5px;">&nbsp;&nbsp;How would you be operating your Enagic india Kangen Water Distributorship?</div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
    <tr><td colspan="2" class="border-left border-right" style="line-height: 2px;width:100%;"></td></tr>
</table>
<table>
    <tr>
        <td class="align-left border-left" style="width: 2.5%;font-size: 9px;line-height: 15px;">&nbsp;&nbsp;<input style="line-height: 18px;" checked="checked" type="checkbox" name="individual" value="Individual" > </td>
        <td style="width: 7%;font-size: 9px;background-color: #241f21;line-height: 13px;vertical-align: middle"><label style="color: #fff;">&nbsp;Individual</label></td>
        <td style="width: 0.8%;"></td>
        <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="sole_proprietorship" value="Sole Proprietorship"  > </td>
        <td style="width: 12.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Sole Proprietorship</label></td>
        <td style="width: 0.8%;"></td>
        <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="partnership_firm" value="Partnership Firm"  /></td>
        <td style="width: 11.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Partnership Firm</label></td>
        <td style="width: 0.8%;"></td>
        <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="private_limited_company" value="Private Limited Company"  /></td>
        <td style="width: 15.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Private Limited Company</label></td>
        <td style="width: 0.8%;"></td>
        <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="others" value="Others"  /></td>
        <td style="width: 5.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Others</label></td>
        <td style="width: 10%;font-size: 6.5px;line-height: 13px;border-bottom: 1px solid #000;">(Please provide details)</td>
        <td class="fallingsky" style="width:23%;line-height: 10px;font-size: 6.5px;background-color: #0646a5;color: #fff;" rowspan="2">
            <table>
                <tr><td style="line-height: 3px;"></td></tr>
                <tr>
                    <td style="width: 2%;"></td>
                    <td>(Note to Institution & Company Applications:<br> Please Provide Corporate Authorisation Form & Documents in addition to Direct Seller Application.)</td>
                    <td style="width: 2%;"></td>
                </tr>
            </table>
        </td>
        <td class="border-right" rowspan="2" style="width: 1%;"></td>
    </tr>
    <tr>
        <td class="border-left" style="width:0.8%;"></td>
        <td class="border" style="width: 74%; line-height: 22px;"><input type="text" style="font-size: 14px;width: 100%; border: none" size="71" name="theentity" value="" /> </td>
    </tr>
    <tr><td class="border-right border-left" style="line-height: 5px;width: 100%;"></td></tr>
    <tr>
        <td class="border-right border-left" style="width: 100%;font-size: 10px;color: #a7a6a6;background-color: #d2d3d5;">
            Kindly give name of the legal entity (the Entity) formed solely to complete this preferred customer application form
        </td>
    </tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td class="border-left border-right" style="line-height: 12px;"></td></tr>
    <tr>
        <td class="border-right border-left" style="width: 100%;font-size: 13px;font-weight: bold;">&nbsp;Name <small style="font-weight: normal; font-size: 7px;">(Only for individual applications)</small></td>
    </tr>
    <tr><td class="border-right border-left" style="line-height: 6px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="border" style="width: 31.1%;line-height: 22px;"><input type="text" style="font-size: 14px;width: 100%;" size="30" name="firstname" value="" > </td>
        <td style="width: 1.4%"></td>
        <td class="border" style="width: 31.1%;line-height: 22px;"><input type="text" style="font-size: 14px;width: 100%;" size="30" name="middlename" value="" > </td>
        <td style="width: 1.4%"></td>
        <td class="border" style="width: 31.1%;line-height: 22px;"><input type="text" style="font-size: 14px;width: 100%;" size="30" name="lastname" value="" > </td>
        <td class="border-right"></td>
    </tr>
</table>
<table>
    <tr>
        <td class=" border-left" style="line-height: 12px;width: 1.5%;"></td>
        <td style="line-height: 12px;width: 29%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;First Name</td>
        <td style="line-height: 12px;width: 3.5%;"></td>
        <td style="line-height: 12px;width: 29%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;Middle Name</td>
        <td style="line-height: 12px;width: 3.5%;"></td>
        <td  style="line-height: 12px;width: 29%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;Last Name</td>
        <td class=" border-right"></td>
    </tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td class=" border-left border-right" colspan="2"  style="line-height: 5px;"></td></tr>
    <tr>
        <td  class="fallingsky border-left" style="line-height:8px;">&nbsp; Residency Status <small>(only applicable to individual applicants)</small></td>
        <td class="border-right" style="line-height: 13px;"><input style="" type="checkbox" name="Citizenrese" value="1"  > <label class="fallingsky">Citizen of and resident in India</label></td>
    </tr>
    <tr><td class=" border-left" colspan="2"  style="line-height: 2px;"></td></tr>
    <tr>
        <td style="width: 33.8%;" class="fallingsky border-left">&nbsp; Date of Birth</td>
        <td class="fallingsky border-right">&nbsp;&nbsp;Gender</td>
    </tr>
    <tr><td colspan="2" class=" border-left border-right" style="line-height: 7px;width:100%;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td class=" border-left border-bottom" style="width: 0.8%;"></td>
        <td class="border" style="width:5.4%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="6" name="dobdate" value="" ></td>
        <td style="width:1.3%;line-height: 20px;"></td>
        <td class="border" style="width:5.4%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="6" name="dobmonth" value="" ></td>
        <td style="width:1.3%;line-height: 20px;"></td>
        <td class="border" style="width:8%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="9" name="dobyear" value="" > </td>
        <td class="border-bottom" style="width:12.8%;"></td>
        <td style="width:2.9%;line-height: 22px;border: 1px solid #000;"><input style="line-height: 22px;width: 50px;" type="checkbox" name="mgender" value="Male" ></td>
        <td style="width: 4%;"></td>
        <td style="width:2.9%;line-height: 22px;border:1px solid #000;"><input style="line-height: 22px;width: 50px;" type="checkbox" name="fgender" value="Feale"  ></td>
        <td style="width: 4%;"></td>
        <td style="width:2.9%;line-height: 22px;border:1px solid #000;"><input style="" type="checkbox" name="ogender" value="Other"  ></td>
        <td class="border-bottom border-right" style="width: 49%;"></td>
    </tr>
</table>
<table>
    <tr>
        <td class="" style="width: 0.8%;"></td>
        <td style="line-height: 15px;width: 7%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;DD</td>
        <td style="line-height: 15px;width: 7%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;MM</td>
        <td style="line-height: 15px;width: 8%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;YYYY</td>
        <td style="line-height: 15px;width: 11.8%;font-size: 7px;"></td>
        <td style="line-height: 15px;width: 7%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;&nbsp;&nbsp;Male</td>
        <td style="line-height: 15px;width: 7%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;&nbsp;Female</td>
        <td style="line-height: 15px;width: 5%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;&nbsp;&nbsp;Other</td>
    </tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 3px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 15px;color: #fff;font-size: 12px;font-weight: bold;">&nbsp;&nbsp;ADDRESS DETAILS</td>
        <td style="width: 49.5%;line-height: 0px;">
            <div class="fallingsky" style="line-height: 1px;font-size: 8.5px;">&nbsp;&nbsp;</div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 5px;width:100%;"></td></tr>
    <tr>
        <td class="fallingsky border-left border-right"  style="font-size: 6.5px;width:100%;">&nbsp;&nbsp;Please provide your complete postal address with pin code and attach a valid address proof along with this application form. Your application will be rejected without valid address proof.</td>
    </tr>
</table>
<table>
    <tr><td class=" border-left border-right" colspan="3" style="line-height: 7px;width:100%;"></td></tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 11%;">
            <div style="border-bottom: 1px solid #000;line-height: 8px;">Mailing Address&nbsp;</div></td>
        <td class="border-right" style="width:88%;"><table><tr><td class="border" style="line-height:22px; width: 97.8%;"><input type="text" style="font-size: 12px;width: 100%;" size="96" name="address" value="" ></td></tr></table></td>
    </tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 18%;">
            <div style="border-bottom: 1px solid #000;line-height:8px;">City / Town / Village <small>(Mandatory)</small>&nbsp;</div></td>
        <td class="border-right" style="width:81%;"><table><tr><td class="border" style="line-height:22px; width: 97.5%;"><input type="text" style="font-size: 12px;width: 100%;" size="88" name="city" value="" ></td></tr></table></td>
    </tr>
</table>
<table>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 19.5%;">
            <div style="border-bottom: 1px solid #000;line-height: 8px;">Post office <small>(In case of village, Mandatory)</small>&nbsp;</div></td>
        <td class="" style="width:30%;"><table><tr><td class="border" style="line-height:22px; width: 97.8%;"><input type="text" style="font-size: 12px;width: 100%;" size="33" name="postoffice" value="" ></td></tr></table></td>
        <td class="" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 5%;">
            <div style="border-bottom: 1px solid #000;line-height: 8px;">District&nbsp;</div></td>
        <td class="border-right" style="width:44%;"><table><tr><td class="border" style="line-height:22px; width: 95.1%;"><input type="text" style="font-size: 12px;width: 100%;" size="47" name="discrit" value="" ></td></tr></table></td>
    </tr>
</table>
<table>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 12%;">
            <div style="border-bottom: 1px solid #000;line-height: 8px;">PIN Code <small>(Mandatory)</small>&nbsp;</div></td>
        <td class="" style="width:15%;"><table><tr><td class="border" style="line-height:22px; width: 95%;"><input type="text" style="font-size: 12px;width: 100%;" size="16" name="pincode" value="" ></td></tr></table></td>
        <td class="fallingsky" style="font-size: 10px;width: 9%;">
            <div style="border-bottom: 1px solid #000;line-height: 8px;">State <small>(Mandatory)</small>&nbsp;</div></td>
        <td class="" style="width:27.5%;"><table><tr><td class="border" style="line-height:22px; width: 94.8%;"><input type="text" style="font-size: 12px;width: 100%;" size="29" name="state" value="" ></td></tr></table></td>
        <td class="fallingsky" style="font-size: 10px;width: 12.5%;">
            <div style="border-bottom: 1px solid #000;line-height: 8px;">Mobile No. <small>(Mandatory)</small></div></td>
        <td class="border-right" style="width:23%;"><table><tr><td class="border" style="line-height:22px; width: 92%;"><input type="text" style="font-size: 12px;width: 100%;" size="24" name="mobileno" value="" ></td></tr></table></td>
    </tr>
</table>
<table>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;width: 15%;">
            <div style="border-bottom: 1px solid #000;line-height: 8px;">E-mail address: <small>(Mandatory)</small>&nbsp;</div></td>
        <td class="border-right" style="width:84%;"><table><tr><td class="border" style="line-height:22px; width: 98%;"><input type="text" style="font-size: 12px;width: 100%;" size="92" name="emailadd" value="" ></td></tr></table></td>
    </tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="border-right" style="width:99%;line-height: 5px;"><small>Photo Identity Proof (Attach photocopy)</small></td>
    </tr>
    <tr><td class="border-left border-right" colspan="4"  style="line-height:3px;width:100%;"></td></tr>
</table>
<table>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
            <input style="" type="checkbox" name="ElectionCard" value="Election Card"  />
        </td>
        <td style="width: 8%;font-size: 9px;line-height: 16px;"><label>&nbsp;Election Card</label></td>
        <td  style="width: 0.5%;"></td>
        <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
            <input style="" type="checkbox" name="DrivingLicense" value="Driving License"  />
        </td>
        <td style="width: 10%;font-size: 9px;line-height: 16px;"><label>&nbsp;Driving License</label></td>
        <td  style="width: 0.5%;"></td>
        <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
            <input style="" type="checkbox" name="Passport" value="Passport"  />
        </td>
        <td style="width: 6%;font-size: 9px;line-height: 16px;"><label>&nbsp;Passport</label></td>
        <td  style="width: 0.5%;"></td>
        <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
            <input style="" type="checkbox" name="UIDCard" value="UID / Aadhar Card"  />
        </td>
        <td style="width: 11%;font-size: 9px;line-height: 16px;"><label>&nbsp;UID / Aadhar Card</label></td>
        <td  style="width: 0.5%;"></td>
        <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
            <input style="" type="checkbox" name="Anyothe" value="Any othe"  />
        </td>
        <td style="width: 6%;font-size: 9px;line-height: 16px;"><label>&nbsp;Any other</label></td>
        <td class="border-right" style="width: 43%;line-height: 15px;"><input type="text" style="font-size: 14px;width: 100%;" size="39" name="Anyothertext" value="" ></td>
    </tr>
    <tr><td colspan="16" class="border-left border-right border-bottom" style="line-height: 3px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 3px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 14px;color: #fff;font-size: 12px;font-weight: bold;">&nbsp;&nbsp;DETAILS OF BANK ACCOUNT <small style="font-weight:normal;font-size: 9px;">(Must be a Ruppe Denominated Account)</small></td>
        <td style="width: 49.5%;line-height: 0px;">
            <div class="fallingsky" style="line-height: 1.8px;font-size: 8.5px;">&nbsp;&nbsp;</div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 5px;width:100%;"></td></tr>
    <tr>
        <td class="fallingsky border-left border-right"  style="font-size: 6.5px;width:100%;">&nbsp;&nbsp;&nbsp;&nbsp;Alc details of the Primary Applicant / Legal Entity.</td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 5px;width:100%;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="border" style="width: 44.5%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="50" name="bankname" value="" > </td>
        <td style="width: 1.4%"></td>
        <td class="border" style="width: 44.5%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="50" name="accno" value="" > </td>
        <td class=" border-right" style="width: 14%"></td>
    </tr>
</table>
<table>
    <tr>
        <td class=" border-left" style="line-height: 12px;width: 1.5%;"></td>
        <td style="line-height: 12px;width: 40%;background-color: #d2d3d5;font-size: 7.5px;font-weight: bold;">&nbsp;&nbsp;Bank Name</td>
        <td style="line-height: 12px;width: 6.5%;"></td>
        <td style="line-height: 12px;width: 40%;background-color: #d2d3d5;font-size: 7.5px;font-weight: bold;">&nbsp;&nbsp;Your Account Number</td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 3px;width:100%;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="border" style="width: 44.5%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="50" name="ifsccode" value="" > </td>
        <td style="width: 1.4%"></td>
        <td class="border" style="width: 44.5%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="50" name="branchcode" value="" > </td>
        <td class=" border-right" style="width: 14%"></td>
    </tr>
</table>
<table>
    <tr>
        <td class=" border-left" style="line-height: 12px;width: 1.5%;"></td>
        <td style="line-height: 12px;width: 40%;background-color: #d2d3d5;font-size: 7.5px;font-weight: bold;">&nbsp;&nbsp;IFSC Code</td>
        <td style="line-height: 12px;width: 6.5%;"></td>
        <td style="line-height: 12px;width: 40%;background-color: #d2d3d5;font-size: 7.5px;font-weight: bold;">&nbsp;&nbsp;Branch Code</td>
        <td class=" border-right"></td>
    </tr>
</table>
<table>
    <tr><td class=" border-left border-right" style="width: 100%;line-height: 3px;"></td></tr>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class=" border-bottom" style="font-size: 8px;line-height: 14px;width: 8%;">Bank Address:</td>
        <td class="border" style="width: 90%;line-height: 22px;"><input type="text" style="font-size: 8px;width: 100%;" size="151" name="branchcode" value="" > </td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right" style="width: 100%;line-height: 3px;"></td></tr>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td style="width: 99%;line-height: 8px;font-size: 8px;">Please provide cancelled cheque which should have the account holder&#39;s name, bank account number and IFSC code of the bank. In case the cheque does not have the account holder&#39;s name, additionally <br>please provide any other attested bank document which verifies the account holder name, bank account, and IFSC code of the bank.
        </td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right border-bottom" style="width: 100%;line-height: 3px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 3px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 60%;background-color: #0646a5;line-height: 15px;color: #fff;font-size: 12px;font-weight: bold;">&nbsp;&nbsp;PERMANENT ACCOUNT NUMBER (PAN) &amp; GSTIN DETAILS</td>
        <td style="width: 39.5%;line-height: 0px;">
            <div class="fallingsky" style="line-height: 1px;font-size: 8.5px;">&nbsp;&nbsp;</div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 3px;width:100%;"></td></tr>
</table>
<table>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class=" border-bottom fallingsky" style="font-size: 10px;line-height: 14px;width: 9%;">PAN CARD NO:</td>
        <td class="border" style="width:22.1%;line-height: 22px;"><input type="text" style="font-size: 10px;width: 100%;" size="30" name="pancard" value="" > </td>
        <td class=" border-left" style="width: 5%;"></td>
        <td class=" border-bottom fallingsky" style="font-size: 10px;line-height: 14px;width: 7%;">GSTIN NO:</td>
        <td class="border" style="width:53%;line-height: 22px;"><input type="text" style="font-size: 10px;width: 100%;" size="71" name="gstinno" value="" > </td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right align-center fallingsky" style="width: 100%;line-height: 12px;font-size: 9.5px;">Note: In case of GSTN Not applicable, it is mandatory to submit GSTN Declaration with application form. Draft available on www.enagic.co.in</td></tr>
    <tr><td class=" border-left border-right border-bottom" style="width: 100%;line-height: 3px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 3px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 15px;color: #fff;font-size: 12px;font-weight: bold;">&nbsp;&nbsp;SALES FACILITATOR</td>
        <td style="width: 49.5%;line-height: 0px;">
            <div class="fallingsky" style="line-height: 1px;font-size: 9px;">&nbsp;&nbsp;Identify the Direct Seller who will be your Sales Facilitator.</div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="fallingsky" style="font-size: 10px;line-height: 14px;width: 14.5%;">Sales Facilitator&#39;s Name</td>
        <td class="border" style="width:29.8%;line-height: 22px;"><input type="text" style="font-size: 10px;width: 100%;" size="40" name="salesname" value="" > </td>
        <td class=" border-left" style="width: 1%;"></td>
        <td class="fallingsky" style="font-size: 10px;line-height: 14px;width: 14.5%;">Sales Facilitator&#39;s ID No.</td>
        <td class="border" style="width:35.8%;line-height: 22px;"><input type="text" style="font-size: 10px;width: 100%;" size="48" name="salesidno" value="" > </td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right" style="width: 100%;line-height: 3px;"></td></tr>
    <tr>
        <td class="border-left" style="width:0.8%;"></td>
        <td class="fallingsky" style="width:8.5%;line-height: 10px;font-size: 9.5px">DECLARATION:</td>
        <td class="fallingsky" style="width: 50%;line-height: 10px;font-size: 9.5px;">I hereby declare that the sale/applicant information which I have agreed herewith to<br> allocate to the sponsor mentioned below under my own free will.</td>
        <td class="" style="width:5%;"></td>
        <td style="width: 30%;" class="border-bottom"></td>
        <td class=" border-right"></td>
    </tr>
    <tr>
        <td class="border-left" style="width:70%;"></td>
        <td class="fallingsky border-right" style="width:30%;line-height: 12px;font-size: 9.5px">SALES FACILITATOR&#39;S SIGNATURE</td>
    </tr>
    <tr><td class=" border-left border-right border-bottom" style="width: 100%;line-height: 3px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 3px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 15px;color: #fff;font-size: 12px;font-weight: bold;">&nbsp;&nbsp;SPONSOR INFORMATION</td>
        <td style="width: 49.5%;line-height: 0px;">
            <div class="fallingsky" style="line-height: 1px;font-size: 9px;">&nbsp;&nbsp;Identify the Direct Seller who will be your Sponsor.</div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 3px;width:100%;"></td></tr>
</table>
<table>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="fallingsky" style="font-size: 10px;line-height: 14px;width: 14.5%;">Sponsor&#39;s Name</td>
        <td class="border" style="width:29.8%;line-height: 22px;"><input type="text" style="font-size: 10px;width: 100%;" size="40" name="Sponsorname" value="" > </td>
        <td class=" border-left" style="width: 1%;"></td>
        <td class="fallingsky align-right" style="font-size: 10px;line-height: 14px;width: 14.5%;">E-mail Address&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="border" style="width:35.8%;line-height: 22px;"><input type="text" style="font-size: 10px;width: 100%;" size="48" name="Sponsoremail" value="" > </td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right" style="width: 100%;line-height: 3px;"></td></tr>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="fallingsky" style="font-size: 10px;line-height: 14px;width: 21%;">ID NO. of your immediate sponsor</td>
        <td class="border" style="width:23.7%;line-height: 22px;"><input type="text" style="font-size: 10px;width: 100%;" size="32" name="Sponsorimdid" value="" > </td>
        <td class=" border-left" style="width: 1%;"></td>
        <td class="fallingsky align-right" style="font-size: 10px;line-height: 14px;width: 14%;">Mobile&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="border" style="width:35.8%;line-height: 22px;"><input type="text" style="font-size: 10px;width: 100%;" size="48" name="Sponsormobile" value="" > </td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right" style="width: 100%;line-height: 3px;"></td></tr>
    <tr>
        <td class=" border-left" style="width: 0.8%;"></td>
        <td class="fallingsky" style="font-size: 10px;line-height: 14px;width:18%;">Register the Applicant As Your</td>
        <td class="border" style="width:5%;line-height: 22px;"><input type="text" style="font-size: 10px;width: 100%;" size="7" maxlength="5" name="SponsorApplicant" value="" > </td>
        <td class="fallingsky" style="font-size: 14px;line-height: 14px;width:6%;">&nbsp;A <small>(Rank)</small></td>
        <td class="" style="width: 0.8%;"></td>
        <td class="fallingsky " style="font-size: 10px;line-height: 9px;width: 60%;font-size: 9px;">Note: Applicant will be registered under rank which you have mentioned in left side. <br>Please clarify your doubt if any related to same before submitting your application.</td>
        <td class=" border-right"></td>
    </tr>
    <tr><td class=" border-left border-right border-bottom" style="width: 100%;line-height: 3px;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr><td colspan="2" style="line-height: 3px;"></td></tr>
    <tr>
        <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 15px;color: #fff;font-size: 12px;font-weight: bold;">&nbsp;&nbsp;DECLARATION</td>
        <td style="width: 49.5%;line-height: 0px;">
            <div class="fallingsky" style="line-height: 1px;font-size: 9px;">&nbsp;&nbsp;</div>
            <div style="border-bottom: 6px solid #0646a5;line-height: 3.2px;"></div>
        </td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 3px;width:100%;"></td></tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td class=" border-left" style="width:0.8%;"></td>
        <td class="fallingsky border-right" style="font-size: 10px;line-height: 9px;width: 100%;font-size: 9px;">The Direct Seller Application constitutes the contractual offer of the undersigned individual addressed to Enagic India to enter into a "Direct Seller&quot; agreement with Enagic<br>India and under the Terms and Conditions specified below/in this application form followed by Page No.2/3 &amp; 3/3. The Applicant hereby certifies that he/she are legally<br>competent to do business in India and not bound by any legal requirement restricting or prohibiting his/her appointment as an Enagic India Direct Seller.</td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 3px;width:100%;"></td></tr>
    <tr>
        <td class=" border-left" style="width:0.8%;"></td>
        <td class="fallingsky border-right" style="font-size: 10px;line-height: 9px;width: 100%;font-size: 9px;">By Signing this application, I confirm that I have been provided with or have undergone Orientation Programme which provided fair and accurate information on all aspects of Enagic India&#39;s direct selling operation, about free joining,, its remuneration system, its refund and return policy, expected remuneration and related rights and obligations as governed under the Enagic<br>Code of Ethics and Policy &amp; procedures. I further understand and agree that this application and any ensuing Enagic India Direct Seller upon acceptance of this application by Enagic India<br>shall be subject to the Terms and Conditions given below including the constituent documents as amended from time to time. Furthermore, I do here by declare that the information<br>furnished above is true and I am legally competent to enter in to this contract and signing this application under my own free will.</td>
    </tr>
    <tr><td class=" border-left border-right"  style="line-height: 3px;width:100%;"></td></tr>
</table>
<table>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="border-bottom" style="width: 23.5%;line-height: 25px;"></td>
        <td style="width: 1%;"></td>
        <td style="width: 24%;"><table class="table_Width" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="border" style="width:25%;line-height: 17px;"><input type="text" style="font-size: 12px;width: 100%;" size="7" name="date1" value="" ></td>
                    <td style="width:3%;line-height: 17px;"></td>
                    <td class="border" style="width:25%;line-height: 17px;"><input type="text" style="font-size: 12px;width: 100%;" size="7" name="month1" value="" ></td>
                    <td style="width:3%;line-height: 17px;"></td>
                    <td class="border" style="width:33%;line-height: 17px;"><input type="text" style="font-size: 12px;width: 100%;" size="9" name="year1" value="" > </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="line-height: 12px;width: 28%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;DD</td>
                    <td style="line-height: 12px;width: 30%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;MM</td>
                    <td style="line-height: 12px;width: 32%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;YYYY</td>
                </tr>
            </table>
        </td>
        <td style="width: 1%;"></td>
        <td class="border-bottom" style="width: 23.5%;line-height: 25px;"></td>
        <td style="width: 1%;"></td>
        <td style="width: 24%;"><table class="table_Width" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="border" style="width:25%;line-height: 17px;"><input type="text" style="font-size: 12px;width: 100%;" size="7" name="date2" value="" ></td>
                    <td style="width:3%;line-height: 17px;"></td>
                    <td class="border" style="width:25%;line-height: 17px;"><input type="text" style="font-size: 12px;width: 100%;" size="7" name="month2" value="" ></td>
                    <td style="width:3%;line-height: 17px;"></td>
                    <td class="border" style="width:33%;line-height: 17px;"><input type="text" style="font-size: 12px;width: 100%;" size="9" name="year2" value="" > </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="line-height: 12px;width: 28%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;DD</td>
                    <td style="line-height: 12px;width: 30%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;MM</td>
                    <td style="line-height: 12px;width: 32%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;YYYY</td>
                </tr>
            </table>
        </td>
        <td class="border-right " style="width: 1%;"></td>
    </tr>
    <tr>
        <td class="border-left" style="width: 1%;"></td>
        <td class="fallingsky align-center" style="width: 23.5%;line-height: 12px;">APPLICANT&#39;S SIGNATURE:</td>
        <td class="" style="width: 1%;"></td>
        <td class="fallingsky" style="width: 23.5%;line-height: 12px;"></td>
        <td class="" style="width: 1%;"></td>
        <td class="fallingsky align-center" style="width: 23.5%;line-height: 12px;">SPONSOR&#39;S SIGNATURE:</td>
        <td class="fallingsky" style="width: 23.5%;line-height: 12px;"></td>
        <td class="border-right " ></td>
    </tr>
    <tr><td class=" border-left border-right border-bottom"  style="line-height: 3px;width:100%;"></td></tr>
</table>
<table>
    <tr>
        <td style="width: 5%;"></td>
        <td class="" style="line-height: 12px;font-size: 8.3px;">PTO to sign 2nd and 3rd pages</td>
    </tr>
</table>
<?php
$html = ob_get_clean();

$pdf->AddPage('P', 'A4');
$pdf->writeHTML($html, true, false, false, false, '');

$pdf->AddPage('P', 'A4');

ob_start();
?>
<style>
    .table_Width { width: 100%; }
    .align-left { text-align: left; }
    .align-right { text-align: right; }
    .align-center { text-align: center; }
    .border { border: 1px solid #000; }
    .border-left { border-left: 1px solid #000; }
    .border-right { border-right: 1px solid #000; }
    .border-top { border-top: 1px solid #000; }
    .border-bottom { border-bottom: 1px solid #000; }
    .border-left_2 { border-left: 2px solid #000; }
    .border-right_2 { border-right: 1px solid #000; }
    .border-top_2 { border-top: 1px solid #000; }
    .border-bottom_2 { border-bottom: 1px solid #000; }
    .padding { padding: 5px; }
    .lineHeight { line-height: 17px; height: 17px; }
    .fontSize10 { font-size: 10px; font-weight: bold; }
    .fontSize9 { font-size: 9px; font-weight: bold; }
    .withLeft { width: 38%; }
    .withright { width: 62%; }
    .fallingsky { font-family: fallingsky; }
</style>
<table>
    <tr>
        <td style="width: 5%;" class="fallingsky"></td>
        <td style="line-height: -15px;font-size:10.5px;font-weight: bold;" class="fallingsky">FORM-DSA-IND.VI</td>
        <td style="width: 10%;" class="fallingsky"></td>
        <td style="line-height: -15px;font-size:10.5px;font-weight: bold;width: 33%;" class="fallingsky">Direct Seller Agreement - Terms and conditions</td>
        <td style="line-height: -15px;font-size:10.5px;font-weight: bold;color:#ccc;width: 30%;" class="align-right">2/3</td>
    </tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td style="border: 3px solid #0646a5;">
            <table>
                <tr><td style="line-height: 6px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;">These Terms &amp; Conditions are to be read together with the Application, Company Policies &amp; Procedures Handbook and collectively they constitute a binding contract between Enagic India Kangen Water Pvt Ltd (Company) and Direct Seller signing this application.</td>
                </tr>
                <tr><td style="line-height: 10px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;">1. Definitions:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">a) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px;">Direct Seller:shall mean a person appointed by the Company on a principal-to-principal basis through this Direct Seller Contract to undertake sale, distribution and marketing of Enagic products and services and to register Preferred Customers. An Enagic Direct Seller may introduce or sponsor another direct sellers and support them to build their direct selling business of Enagic goods &amp; services.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">b) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px;">Direct Seller Contract: shall mean and include the following:<br>
                        <table>
                            <tr>
                                <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">i. </td>
                                <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px;">The Direct Seller Application Form;</td>
                            </tr>
                            <tr>
                                <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">ii. </td>
                                <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 97%;">These Terms and Conditions forming part of Direct Seller Application;</td>
                            </tr>
                            <tr>
                                <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">iii. </td>
                                <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px;">The Enagic Compensation Plan; and</td>
                            </tr>
                            <tr>
                                <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">iv. </td>
                                <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px;width: 96%;">The Policies and Procedures Handbook;as amended from time to time. The Company may notify any such amendment on its website, www.enagic.co.in</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">c) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px;">Territory: shall mean the Republic of India.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">d) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px;">Effective Date: shall mean the date of submission of the duly filled Direct Seller Application, subject to approval by Enagic India Kangen Water Pvt. Ltd.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">2. Legal Requirement:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Applicant confirms that he/she is above the age of 18 years, of sound mind and not disqualified from contracting by any law. Applicant is entering into this contract with free consent after undertaking mandatory orientation session about direct selling and remuneration system of the Company.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">3. Applicant must submit the following</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">
                        a) duly filled in Application form<br>
                        b) Copy of Government issued Identity Card<br>
                        c) Copy of residential proof<br>
                        d) Copy of Permanent Account Number (PAN) Card<br>
                        e) Passport size photograph<br>
                        f) Cancelled bank cheque.<br>
                        (Any of the following document could be furnished as proof of address; Aadhar Card/Voter ID/ Driving License/ Ration  Card/Passport.)
                    </td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">4. Distributorship /Direct Selling:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Enagic appoints, without requirement / compulsion to buy the product, as of the Effective Date, the individual(s) identified in the above Direct Seller Application, or if applicable, the legal entity listed therein (the &quot;Entity&quot;), as a Direct Seller of Enagic Products and services, and the Applicant(s) agree(s) to such appointment. As of the Effective Date and upon receipt of ordering information and completion of any required formalities, the Direct Seller may, on a non-exclusive basis, within the Territory as may be communicated by the Company, and otherwise in accordance with the Direct Seller Contract, purchase Enagic Products from the Company in order to sell, distribute and market the same, and also register Preferred Customers.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">5. Registration</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Company rerserves the right, at its sole discretion, to accept or refuse any application. Enagic do not charge any registration, joining or renewal fee. Upon acceptance the person will remain a Direct Seller for a period of 24 months. To keep the Direct Seller status beyond this period the Direct Seller needs to demonstrate his/her activity during the past 12 months.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">6. Duration:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">This Direct Seller Contract, shall remain valid and continue to remain in full force unless terminated earlier by either Party with or without cause by given notice according to the provisions given in Policies and Procedures Handbook.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">7. Cooling Off Period:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Newly joined Direct Seller shall have a cooling off period of 10 days to cancel the contract and receive full refund. Company also offer 30 days buy back guarantee for currently marketable goods or services sold.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">8. Commission or Incentives:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Commission or incentive to Direct Seller are paid solely based on sale of products and no payment will be made only for recruitment of new Direct Seller. Any enquiry on margin will be replied via email only margin-india@enagic.co.in and customer care.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">9. Obligation of Direct Sellers:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">a) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Direct Seller shall carry the Identification card (ID Card) issued by the Company and will seek prior appointment with customer for initiation of sale, Direct Seller shall identify themselves and the Company, provide contact details to customer and would truthfully represent the nature of products in the manner consistent with the claims authorized by the Company.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;">b) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Direct Seller would provide accurate and complete explanations and demonstrations of goods, time and place to inspect the sample and take delivery, prices, credit/payment terms, terms of guarantee, after-sales service, goods return policy, right to cancel the order and complaint redressal mechanism of the Company.</td>
                </tr>
                <tr><td style="line-height: 10px;"></td></tr>
            </table>
            <table>
                <tr>
                    <td class="" style="width: 1%;"></td>
                    <td class="border-bottom" style="width: 23.5%;line-height: 25px;"></td>
                    <td style="width: 5%;"></td>
                    <td style="width: 30%;"><table class="table_Width" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="border" style="width:25%;line-height: 25px;"></td>
                                <td style="width:3%;line-height: 17px;"></td>
                                <td class="border" style="width:25%;line-height: 17px;"></td>
                                <td style="width:3%;line-height: 17px;"></td>
                                <td class="border" style="width:33%;line-height: 17px;"></td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style="line-height: 12px;width: 28%;background-color: #d2d3d5;font-size: 9px;font-weight: bold;">&nbsp;&nbsp;DD</td>
                                <td style="line-height: 12px;width: 30%;background-color: #d2d3d5;font-size: 9px;font-weight: bold;">&nbsp;&nbsp;MM</td>
                                <td style="line-height: 12px;width: 32%;background-color: #d2d3d5;font-size: 9px;font-weight: bold;">&nbsp;&nbsp;YYYY</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="" style="width: 1%;"></td>
                    <td class="fallingsky align-center" style="width: 23.5%;line-height: 12px;">APPLICANT&#39;S SIGNATURE:</td>
                    <td class="" style="width: 5%;"></td>
                    <td class="fallingsky align-center" style="width: 23.5%;line-height: 12px;">DATE</td>
                </tr>
                <tr><td style="line-height: 16px;"></td></tr>
            </table>
        </td>
    </tr>
</table>
<?php
$pdf->SetMargins(0, 0, 0, false);
$html = ob_get_clean();
$pdf->writeHTML($html, true, false, false, false, '');

$pdf->AddPage('P', 'A4');

ob_start();
?>
<style>
    .table_Width { width: 100%; }
    .align-left { text-align: left; }
    .align-right { text-align: right; }
    .align-center { text-align: center; }
    .border { border: 1px solid #000; }
    .border-left { border-left: 1px solid #000; }
    .border-right { border-right: 1px solid #000; }
    .border-top { border-top: 1px solid #000; }
    .border-bottom { border-bottom: 1px solid #000; }
    .border-left_2 { border-left: 2px solid #000; }
    .border-right_2 { border-right: 1px solid #000; }
    .border-top_2 { border-top: 1px solid #000; }
    .border-bottom_2 { border-bottom: 1px solid #000; }
    .padding { padding: 5px; }
    .lineHeight { line-height: 17px; height: 17px; }
    .fontSize10 { font-size: 10px; font-weight: bold; }
    .fontSize9 { font-size: 9px; font-weight: bold; }
    .withLeft { width: 38%; }
    .withright { width: 62%; }
    .fallingsky { font-family: fallingsky; }
</style>
<table>
    <tr>
        <td style="width: 5%;" class="fallingsky"></td>
        <td style="line-height: 15px;font-size:10.5px;font-weight: bold;" class="fallingsky">FORM-DSA-IND.VI</td>
        <td style="width: 10%;" class="fallingsky"></td>
        <td style="line-height: 15px;font-size:10.5px;font-weight: bold;width: 33%;" class="fallingsky">Direct Seller Agreement - Terms and conditions</td>
        <td style="line-height: 15px;font-size:10.5px;font-weight: bold;color:#ccc;width: 30%;" class="align-right">3/3</td>
    </tr>
</table>
<table class="table_Width" cellspacing="0" cellpadding="0">
    <tr>
        <td style="border: 3px solid #0646a5;">
            <table>
                <tr><td style="line-height: 6px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">c) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px;">Direct Seller shall be guided by provisions of Consumer Protection Act 1986 and shall comply with Direct Selling guidelines 2016, be responsible for payment of any tax liability on their earning and all local and municipal laws, ordinances, rules, and regulations, guidelines and shall make all reports and remit all withholdings or other deductions as may be required by any such law.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">d) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px;">The Direct Seller shall, throughout the validity of this Direct Seller Contract, strictly adhere to all applicable laws, regulations and other legal obligations that affect the operation of his/her/their business. The Direct Seller shall be responsible for obtaining any applicable registration, license, approval or authorization, to carry out the business as Direct Seller, copy of which shall be provided to the Company upon request.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 3%;">e) </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px;">Direct Seller shall not use misleading deceptive or unfair trade practices and not misrepresent actual or potential sales/earning advantages of direct selling. Direct Seller shall not make any false representation/promise relating to direct selling, earning potential, remuneration system etc.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">10. No Assignment:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Direct Seller shall not assign any rights or delegate his duties under this Agreement without the prior written consent of Company.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">11. No Employment Relationship:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Direct Seller confirms that he/she/they has/have entered into this contract as an independent contractor. Nothing in this agreement shall establish either an employment relationship or any other labour relationship between parties or a right for Direct Seller to act as a procurer, broker, commercial agent, contracting representative or other representative of the Company.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">12. Right to Modification:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Company expressly reserve the right to alter, modify or amend product prices, policies and procedure, product availability and compensation plan. If the Direct Seller do not agree to be bound by the said amendment he/she may terminate the contract with immediate effect by giving a written notice to the Company, otherwise Independent Distributor&#39;s continued relationship with the company will constitute an affirmative acknowledgment by the Direct Seller to having agreed to such amendment and be bound by same.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">13. Termination of Direct Seller Contact:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Company reserves the right to terminate the Direct Seller who fails to adhere any of the terms and conditions/ policies and procedure of the Company.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">14. Advertisement:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Direct Seller is allowed to advertise on the internet through an approved company website (www.enagicwebsystem.com) however Direct Seller is strictly prohibited from selling or promoting company products on internet shopping or auction sites.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">15. Severability Clause:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">If any provision of these Terms and Conditions is declared invalid or unenforceable, the remaining provisions shall remain in full force and effect.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">16. Governing Law:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">The Direct Seller Contact and all questions of its interpretation shall be governed by and construed in accordance with the laws of Republic of India, without regards to its principles of conflicts of laws. The Agreement is civil in nature and hence, it is to be governed and construed in accordance with the Indian Contract Act, 1872, the Code of Civil Procedure and other applicable laws of India.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">17. Dispute Settlement:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Any dispute arising out of this agreement or in any manner touching upon it, the same shall be settled through arbitration under Arbitration and Conciliation Act 1996 with all statutory amendments, by a sole arbitrator to be appointed by a Director of the Company, who may be specifically authorised by the Board of Directors of the Company in this regard. The venue of arbitrator shall be Bangalore.</td>
                </tr>
                <tr><td style="line-height: 5px;"></td></tr>
                <tr>
                    <td class="fallingsky" style="font-weight: bold;line-height: 15px;font-size: 14px;width: 100%;">18. Limitation of Liability:</td>
                </tr>
                <tr>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 2%;"> </td>
                    <td class="" style="font-weight: normal;line-height: 15px;font-size: 13px; width: 96%;">Companies Liability whether under contract or otherwise, arising out of or in connection with this contract shall not exceed the less of a) actual damages or loss accessed by the arbitrator b) the total commission earned by the Direct Seller during the six months period preceding the date of the dispute.</td>
                </tr>
                <tr><td style="line-height: 170px;"></td></tr>
            </table>
            <table>
                <tr>
                    <td class="" style="width: 1%;"></td>
                    <td class="border-bottom" style="width: 23.5%;line-height: 25px;"></td>
                    <td style="width: 5%;"></td>
                    <td style="width: 30%;"><table class="table_Width" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="border" style="width:25%;line-height: 25px;"></td>
                                <td style="width:3%;line-height: 17px;"></td>
                                <td class="border" style="width:25%;line-height: 17px;"></td>
                                <td style="width:3%;line-height: 17px;"></td>
                                <td class="border" style="width:33%;line-height: 17px;"></td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style="line-height: 12px;width: 28%;background-color: #d2d3d5;font-size: 9px;font-weight: bold;">&nbsp;&nbsp;DD</td>
                                <td style="line-height: 12px;width: 30%;background-color: #d2d3d5;font-size: 9px;font-weight: bold;">&nbsp;&nbsp;MM</td>
                                <td style="line-height: 12px;width: 32%;background-color: #d2d3d5;font-size: 9px;font-weight: bold;">&nbsp;&nbsp;YYYY</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="" style="width: 1%;"></td>
                    <td class="fallingsky align-center" style="width: 23.5%;line-height: 12px;">APPLICANT&#39;S SIGNATURE:</td>
                    <td class="" style="width: 5%;"></td>
                    <td class="fallingsky align-center" style="width: 23.5%;line-height: 12px;">DATE</td>
                </tr>
                <tr><td style="line-height: 21px;"></td></tr>
            </table>
        </td>
    </tr>
</table>
<?php
$pdf->SetMargins(0, 0, 0, false);
$html = ob_get_clean();
$pdf->writeHTML($html, true, false, false, false, '');
$uniqueid = uniqid();
$pdf->Output(public_path('./uploads/pdf/' . $uniqueid . '.pdf'), 'F');
PromoterProfile::where('id', $promoterProfile->id)->update(['pdf' => 'uploads/pdf/' . $uniqueid . '.pdf']);
//============================================================+
// END OF FILE
//============================================================+
