<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */
// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf.php');

class MYPDF extends TCPDF {

    public function AcceptPageBreak() {
        $pageno = $this->PageNo();
        $this->SetMargins(3, 12.5, 5, true);
        return $this->AutoPageBreak;
    }

}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Purchse Order Form');
// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, 7, 5);

$pdf->SetAutoPageBreak(TRUE, 0);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setFontSubsetting(true);

$pdf->AddFont('daibanna');
$pdf->AddFont('fallingsky');
$pdf->SetFont('helvetica', '', 10, '', false);
$pdf->setCellHeightRatio(0);
// Add a page
// This method has several options, check the source code documentation for more information.
ob_start();
?>

<style>
    .table_Width { width: 100%; }
    .align-left { text-align: left; }
    .align-right { text-align: right; }
    .align-center { text-align: center; }
    .border { border: 1px solid #000; }
    .border-left { border-left: 1px solid #000; }
    .border-right { border-right: 1px solid #000; }
    .border-top { border-top: 1px solid #000; }
    .border-bottom { border-bottom: 1px solid #000; }
    .border-left_2 { border-left: 2px solid #000; }
    .border-right_2 { border-right: 1px solid #000; }
    .border-top_2 { border-top: 1px solid #000; }
    .border-bottom_2 { border-bottom: 1px solid #000; }
    .padding { padding: 5px; }
    .lineHeight { line-height: 17px; height: 17px; }
    .fontSize10 { font-size: 10px; font-weight: bold; }
    .fontSize9 { font-size: 9px; font-weight: bold; }
    .withLeft { width: 38%; }
    .withright { width: 62%; }
    .fallingsky { font-family: fallingsky; }
</style>
<form method="post" action="" enctype="multipart/form-data">
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 9%;"><img style="width: 70px;" src="<?php echo K_PATH_IMAGES . 'enagic-logo.jpg'; ?>" /></td>
            <td class="align-center" style="width: 91%;">
                <h2 style="font-family:'daibanna'; color: #2957a4;font-size: 29px;line-height: 3px;">Enagic India Kangen Water Pvt. Ltd</h2>
                <table cellspacing="0" cellpadding="0">
                    <tr><td style="height: 19px; line-height: 19px;"></td></tr>
                    <tr><td style="font-weight: bold; font-size: 11px;">CIN - U41000TN2015PTC100366</td></tr>
                    <tr><td style="font-size: 10px;line-height: 24px;">Regd. Off: No.55, Thandalam Vill. Sriperumbadhurt Taluk, Kancheepuram, Chennai TN 602 105. </td></tr>
                    <tr><td style="font-size: 10px;">Corp. Office: The Millenia Tower B, 4th Floor, Unit 401 No. 1 &amp; 2, Murphy Road, Ulsoor, Bangalore 560-008. India.</td></tr>
                    <tr><td style="font-weight: bold;font-size: 10px;line-height: 24px;">www.enagic.co.in | Ph: 080 46509900 | Fax: 080 46509908</td></tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr>
            <td  class="border fallingsky" style="width: 100%;line-height: 24px;font-size: 12px;font-weight: bold;">&nbsp;&nbsp;PRODUCT PURCHASE ORDER FORM</td>
        </tr>
        <tr>
            <td style="line-height: 4px;"></td>
        </tr>
        <tr>
            <td class="" style="line-height: 9px;font-size: 8px;font-weight: normal;">NOTE: Please Complete this Purchase order form to purchase products of Enagic India Kangen Water Private Limited (hereinafter
                &#39;Enagic India&#39; or &#39;Company&#39;). Applicant must be 18 years of age or above. All sections are mandatory unless otherwise indicated.</td>
        </tr>
        <tr>
            <td style="line-height: 2px;"></td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr>
            <td class="border" style="line-height: 12px;width: 12%;font-size: 10px;">&nbsp;Customer ID# <br/><small>&nbsp;&nbsp;for office use only</small></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td class="border-right border-top border-bottom" style="line-height: 12px;width: 4%;"></td>
            <td  style="line-height: 12px;width: 1.5%;"></td>
            <td class="border" style="line-height: 23px;width: 18%;font-size: 10px;">&nbsp;For office use only</td>
            <td class="border-right border-top border-bottom" style="line-height: 23px;width: 24.5%;font-size: 10px;"></td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr><td colspan="2" style="line-height: 5px;"></td></tr>
        <tr>
            <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 20px;color: #fff;font-size: 15px;">&nbsp;&nbsp;PRINCIPAL INFORMATION</td>
            <td style="width: 49%;line-height: 0px;">
                <div style="line-height: 1px;"></div>
                <div style="border-bottom: 9px solid #0646a5;line-height: 5px;"></div>
            </td>
        </tr>
        <tr><td colspan="2" style="line-height: 9px;"></td></tr>
        <tr>
            <td colspan="2" style="font-size: 9px;">Please tick one your category in</td>
        </tr>
        <tr><td colspan="2" style="line-height: 10px;"></td></tr>
    </table>
    <table>
        <tr>
            <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="individual" value="Individual" @if($order->company_type==3) checked="checked" @endif> </td>
            <td style="width: 7%;font-size: 9px;background-color: #241f21;line-height: 13px;vertical-align: middle"><label style="color: #fff;">&nbsp;Individual</label></td>
            <td style="width: 5%;"></td>
            <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="sole_proprietorship" value="Sole Proprietorship"  @if($order->company_type==2) checked="checked" @endif> </td>
            <td style="width: 12.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Sole Proprietorship</label></td>
            <td style="width: 5%;"></td>
            <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="partnership_firm" value="Partnership Firm" @if($order->company_type==4) checked="checked" @endif /></td>
            <td style="width: 11.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Partnership Firm</label></td>
            <td style="width: 5%;"></td>
            <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"><input style="line-height: 18px;" type="checkbox" name="private_limited_company" value="Private Limited Company" @if($order->company_type==5) checked="checked" @endif /></td>
            <td style="width: 15.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Private Limited Company</label></td>
            <td style="width: 5%;"></td>
            <td class="align-left" style="width: 1.8%;font-size: 9px;line-height: 15px;"> <input style="line-height: 18px;" type="checkbox" name="others" value="Others" @if($order->company_type==6) checked="checked" @endif /></td>
            <td style="width: 5.8%;font-size: 9px;line-height: 13px;background-color: #241f21;"><label style="color: #fff;">&nbsp;Others</label></td>
            <td style="width: 15%;font-size: 6.5px;line-height: 13px;border-bottom: 1px solid #000;">
                (Please provide details)
            </td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr><td  style="line-height: 4px;"></td></tr>
        <tr>
            <td class="border" style="width: 100.1%; line-height: 22px;"><input type="text" style="font-size: 16px;width: 100%; border: none" size="80" name="theentity" value="{{$order->company_name}}" /> </td>
        </tr>
        <tr><td  style="line-height: 5px;"></td></tr>
        <tr>
            <td  style="width: 100%;font-size: 10px;color: #a7a6a6;">
                Kindly give name of the legal entity (the Entity) formed solely to complete this preferred customer application form
            </td>
        </tr>
        <tr><td  style="line-height: 12px;"></td></tr>
        <tr>
            <td  style="width: 100%;font-size: 14px;font-weight: bold;">Name</td>
        </tr>
        <tr><td  style="line-height: 8px;"></td></tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr>
            <td class="border" style="width: 32.5%;line-height: 22px;"><input type="text" style="font-size: 16px;width: 100%;" size="26" name="firstname" value="{{$order->first_name}}" > </td>
            <td style="width: 1.4%"></td>
            <td class="border" style="width: 32.5%;line-height: 22px;"><input type="text" style="font-size: 16px;width: 100%;" size="26" name="middlename" value="{{$order->middle_name}}" > </td>
            <td style="width: 1.4%"></td>
            <td class="border" style="width: 32.5%;line-height: 22px;"><input type="text" style="font-size: 16px;width: 100%;" size="26" name="lastname" value="{{$order->last_name}}" > </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="line-height: 15px;width: 1.5%;"></td>
            <td style="line-height: 15px;width: 30%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;First Name</td>
            <td style="line-height: 15px;width: 3.5%;"></td>
            <td style="line-height: 15px;width: 30%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;Middle Name</td>
            <td style="line-height: 15px;width: 3.5%;"></td>
            <td style="line-height: 15px;width: 30%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;Last Name</td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr><td colspan="2"  style="line-height: 5px;"></td></tr>
        <tr>
            <td class="fallingsky" style="line-height: 10px;">Residency Status <small>(only applicable to individual applicants)</small></td>
            <td style="line-height: 18px;"><input style="" type="checkbox" name="Citizenrese" value="1" @if($order->residency_status=='indian') checked="checked" @endif > <label class="fallingsky">Citizen of and resident in India</label></td>
        </tr>
        <tr><td colspan="2"  style="line-height: 2px;"></td></tr>
        <tr>
            <td style="width: 33.8%;" class="fallingsky">Date of Birth</td>
            <td class="fallingsky">&nbsp;&nbsp;Gender</td>
        </tr>
        <tr><td colspan="2"  style="line-height: 7px;"></td></tr>
    </table>
    @php
    $date=explode('-',$order->dob);
    @endphp
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr>
            <td class="border" style="width:5.6%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="6" name="dobdate" value="{{$date[2]}}" ></td>
            <td style="width:1.3%;line-height: 20px;"></td>
            <td class="border" style="width:5.6%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="6" name="dobmonth" value="{{$date[1]}}" ></td>
            <td style="width:1.3%;line-height: 20px;"></td>
            <td class="border" style="width:8.2%;line-height: 22px;"><input type="text" style="font-size: 12px;width: 100%;" size="9" name="dobyear" value="{{$date[0]}}" > </td>
            <td style="width:12.8%;"></td>
            <td style="width:2.9%;line-height: 22px;border: 1px solid #000;"><input style="line-height: 22px;width: 50px;" type="checkbox" name="mgender" value="Male" @if($order->gender=='male') checked="checked" @endif></td>
            <td style="width: 4%;"></td>
            <td style="width:2.9%;line-height: 22px;border:1px solid #000;"><input style="line-height: 22px;width: 50px;" type="checkbox" name="fgender" value="Feale"  @if($order->gender=='female') checked="checked" @endif></td>
            <td style="width: 4%;"></td>
            <td style="width:2.9%;line-height: 22px;border:1px solid #000;"><input style="" type="checkbox" name="ogender" value="Other" @if($order->gender=='other') checked="checked" @endif ></td>
            <td style="width: 4%;"></td>
            <td style="width:5%;"></td>
            <td class="fallingsky" style="width:8%;font-size: 12px;line-height: 22px;">GSTIN NO</td>
            <td class="border" style="width:31%;line-height: 22px;"><input style="font-size: 12px;" type="text" size="33" name="gstinno" value="{{$order->gst_no}}" ></td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="line-height: 15px;width: 7%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;DD</td>
            <td style="line-height: 15px;width: 7%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;MM</td>
            <td style="line-height: 15px;width: 8%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;YYYY</td>
            <td style="line-height: 15px;width: 11.8%;font-size: 7px;"></td>
            <td style="line-height: 15px;width: 7%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;&nbsp;&nbsp;Male</td>
            <td style="line-height: 15px;width: 7%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;&nbsp;Female</td>
            <td style="line-height: 15px;width: 5%;background-color: #d2d3d5;font-size: 7px;">&nbsp;&nbsp;&nbsp;&nbsp;Other</td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr><td style="line-height: 5px;"></td></tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr>
            <td class="align-left" style="width: 2.2%;font-size: 9px;line-height: 22px;border-bottom: 1px solid #0646a5"><input style="" type="checkbox" name="shippingAdd1" value="1" checked="checked"  /></td>
            <td class="fallingsky" style="background-color: #0646a5;line-height: 15px;color:#fff;width: 50%;">&nbsp;SHIPPING ADDRESS DETAILS - Please provide details below <span style="font-size: 16px;">OR</span></td>
            <td class="align-left" style="width: 2.2%;font-size: 9px;line-height: 22px;border-bottom: 1px solid #0646a5"><input style="" type="checkbox" name="shippingAdd2" value="1"  /></td>
            <td class="fallingsky" style="background-color: #0646a5;line-height: 20px;color:#fff;width: 45%;">&nbsp;PICK-UP - Agreed to collect within 3 working days</td>
        </tr>
        <tr><td class=" border-left border-right"  colspan="4" style="line-height: 5px;"></td></tr>
        <tr>
            <td class="fallingsky border-left border-right"  colspan="4" style="font-size: 6.5px;">&nbsp;&nbsp;Please provide your complete postal address with pin code and attach a valid address proof along with this application form. Your
                application will be rejected without valid address proof.</td>
        </tr>
    </table>
    <table>
        <tr><td class=" border-left border-right" colspan="3" style="line-height: 7px;width:99.4%;"></td></tr>
        <tr>
            <td class="border-left" style="width: 1%;"></td>
            <td class="fallingsky" style="font-size: 10px;width: 11%;">
                <div style="border-bottom: 1px solid #000;line-height: 8.5px;">Mailing Address&nbsp;</div></td>
            <td class="border-right" style="width:87.4%;"><table><tr><td class="border" style="line-height:22px; width: 98%;"><input type="text" style="font-size: 13px;width: 100%;" size="84" name="address" value="{{$order->orderAddress->address}}" ></td></tr></table></td>
        </tr>
        <tr>
            <td class="border-left" style="width: 1%;"></td>
            <td class="fallingsky" style="font-size: 10px;width: 18%;">
                <div style="border-bottom: 1px solid #000;line-height: 8.5px;">City / Town / Village <small>(Mandatory)</small>&nbsp;</div></td>
            <td class="border-right" style="width:80.4%;"><table><tr><td class="border" style="line-height:22px; width: 97.5%;"><input type="text" style="font-size: 13px;width: 100%;" size="77" name="city" value="{{$order->orderAddress->town}}" ></td></tr></table></td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="border-left" style="width: 1%;"></td>
            <td class="fallingsky" style="font-size: 10px;width: 19.5%;">
                <div style="border-bottom: 1px solid #000;line-height: 8.5px;">Post office <small>(In case of village, Mandatory)</small>&nbsp;</div></td>
            <td class="" style="width:30%;"><table><tr><td class="border" style="line-height:22px; width: 97.5%;"><input type="text" style="font-size: 13px;width: 100%;" size="29" name="postoffice" value="" ></td></tr></table></td>
            <td class="" style="width: 1%;"></td>
            <td class="fallingsky" style="font-size: 10px;width: 5%;">
                <div style="border-bottom: 1px solid #000;line-height: 8.5px;">District&nbsp;</div></td>
            <td class="border-right" style="width:42.9%;"><table><tr><td class="border" style="line-height:22px; width: 95.1%;"><input type="text" style="font-size: 13px;width: 100%;" size="40.5" name="discrit" value="{{$order->orderAddress->city->name}}" ></td></tr></table></td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="border-left" style="width: 1%;"></td>
            <td class="fallingsky" style="font-size: 10px;width: 12%;">
                <div style="border-bottom: 1px solid #000;line-height: 8.5px;">PIN Code <small>(Mandatory)</small>&nbsp;</div></td>
            <td class="" style="width:15%;"><table><tr><td class="border" style="line-height:22px; width: 95%;"><input type="text" style="font-size: 13px;width: 100%;" size="14" name="pincode" value="{{$order->orderAddress->pincode}}" ></td></tr></table></td>
            <td class="fallingsky" style="font-size: 10px;width: 9%;">
                <div style="border-bottom: 1px solid #000;line-height: 8.5px;">State <small>(Mandatory)</small>&nbsp;</div></td>
            <td class="" style="width:27.5%;"><table><tr><td class="border" style="line-height:22px; width: 95.1%;"><input type="text" style="font-size: 13px;width: 100%;" size="26" name="state" value="{{$order->orderAddress->state->name}}" ></td></tr></table></td>
            <td class="fallingsky" style="font-size: 10px;width: 12.5%;">
                <div style="border-bottom: 1px solid #000;line-height: 8.5px;">Mobile No. <small>(Mandatory)</small></div></td>
            <td class="border-right" style="width:22.4%;"><table><tr><td class="border" style="line-height:22px; width: 91%;"><input type="text" style="font-size: 13px;width: 100%;" size="20" name="mobileno" value="{{$order->mobile_no}}" ></td></tr></table></td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="border-left" style="width: 1%;"></td>
            <td class="fallingsky" style="font-size: 10px;width: 15%;">
                <div style="border-bottom: 1px solid #000;line-height: 8.5px;">E-mail address: <small>(Mandatory)</small>&nbsp;</div></td>
            <td class="border-right" style="width:83.4%;"><table><tr><td class="border" style="line-height:22px; width: 97.6%;"><input type="text" style="font-size: 13px;width: 100%;" size="80" name="emailadd" value="{{$order->email}}" ></td></tr></table></td>
        </tr>
        <tr>
            <td class="border-left" style="width: 1%;"></td>
            <td class="border-right" style="width:98.4%;line-height: 5px;"><small>Photo Identity Proof (Attach photocopy)</small></td>
        </tr>
        <tr><td class="border-left border-right" colspan="4"  style="line-height:8px;width:99.4%;"></td></tr>
    </table>
    <table>
        <tr>
            <td class="border-left" style="width: 1%;"></td>
            <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
                <input style="" type="checkbox" name="ElectionCard" value="Election Card" @if($order->identity_proof==1) checked="checked" @endif />
            </td>
            <td style="width: 8%;font-size: 9px;line-height: 16px;"><label>&nbsp;Election Card</label></td>
            <td  style="width: 0.5%;"></td>
            <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
                <input style="" type="checkbox" name="DrivingLicense" value="Driving License" @if($order->identity_proof==2) checked="checked" @endif />
            </td>
            <td style="width: 10%;font-size: 9px;line-height: 16px;"><label>&nbsp;Driving License</label></td>
            <td  style="width: 0.5%;"></td>
            <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
                <input style="" type="checkbox" name="Passport" value="Passport"  @if($order->identity_proof==3) checked="checked" @endif/>
            </td>
            <td style="width: 6%;font-size: 9px;line-height: 16px;"><label>&nbsp;Passport</label></td>
            <td  style="width: 0.5%;"></td>
            <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
                <input style="" type="checkbox" name="UIDCard" value="UID / Aadhar Card" @if($order->identity_proof==4) checked="checked" @endif />
            </td>
            <td style="width: 11%;font-size: 9px;line-height: 16px;"><label>&nbsp;UID / Aadhar Card</label></td>
            <td  style="width: 0.5%;"></td>
            <td style="width: 2.7%;font-size: 9px;line-height: 18px;">
                <input style="" type="checkbox" name="Anyothe" value="Any othe" @if($order->identity_proof==5) checked="checked" @endif />
            </td>
            <td style="width: 6%;font-size: 9px;line-height: 16px;"><label>&nbsp;Any other</label></td>
            <td class="border-right" style="width: 41.9%;line-height: 15px;"><input type="text" style="font-size: 14px;width: 100%;" size="37" name="Anyothertext" value="{{$order->identity_no}}" ></td>
        </tr>
        <tr><td colspan="16" class="border-left border-right border-bottom" style="line-height: 3px;"></td></tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr><td colspan="2" style="line-height: 7px;"></td></tr>
        <tr>
            <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 20px;color: #fff;font-size: 15px;border-bottom:1px solid #0646a5">&nbsp;&nbsp;PRODUCTS OFFERED</td>
            <td class="fallingsky border-bottom" style="width: 50%;line-height: 0px;font-size: 11px;line-height: 18px;">
                Please check product availability before submitting Purchase Order Form
            </td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr>
            <td class="fallingsky border-right border-left border-bottom align-center" style="font-size: 11px;font-weight: bold;line-height: 16px;width: 23%">PRODUCTS <span>[ please tick ]</span></td>
            <td class="fallingsky border-right border-bottom align-center " style="font-weight: bold;font-size: 11px;line-height: 16px;width: 54%;">SERIAL NO. <span>(office use only)</span></td>
            <td class="fallingsky border-right border-left border-bottom align-center" style="font-weight: bold;font-size: 11px;line-height: 18px;width: 23%">UNIT PRICE (INR) <small>(Inclusive of GST)</small></td>
        </tr>
        <tr>
            <td class="fallingsky border-right border-left border-bottom align-left" style="font-size: 10px;line-height: 17px;height: 17px;width: 23%;vertical-align: central;">&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="LEVELUK1" value="LEVELUK JRIV" @if($order->orderProduct->product_id==3) checked="checked" @endif /><label>LEVELUK JRIV</label></td>
            <td class="fallingsky border-right border-bottom align-center " style="line-height: 15px;width: 54%;"></td>
            <td class="fallingsky border-right border-left border-bottom align-center" style="line-height: 15px;width: 23%"><span style="font-family:dejavusans;">&#8377;</span>.&nbsp;2,18,000.00&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="unitPrice1" value="2,18,000" @if($order->orderProduct->product_id==3) checked="checked" @endif /></td>
        </tr>
        <tr>
            <td class="fallingsky border-right border-left border-bottom align-left" style="font-size: 10px;line-height: 17px;height: 17px;width: 23%;vertical-align: central;">&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="LEVELUK2" value="LEVELUK ANESPA DX" @if($order->orderProduct->product_id==4) checked="checked" @endif /><label>LEVELUK ANESPA DX</label></td>
            <td class="fallingsky border-right border-bottom align-center " style="line-height: 15px;width: 54%;"></td>
            <td class="fallingsky border-right border-left border-bottom align-center" style="line-height: 15px;width: 23%"><span style="font-family:dejavusans;">&#8377;</span>.&nbsp;2,00,000.00&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="unitPrice2" value="2,00,000" @if($order->orderProduct->product_id==4) checked="checked" @endif /></td>
        </tr>
        <tr>
            <td class="fallingsky border-right border-left border-bottom align-left" style="font-size: 10px;line-height: 17px;height: 17px;width: 23%;vertical-align: central;">&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="LEVELUK3" value="LEVELUK SD501" @if($order->orderProduct->product_id==5) checked="checked" @endif /><label>LEVELUK SD501</label></td>
            <td class="fallingsky border-right border-bottom align-center " style="line-height: 15px;width: 54%;"></td>
            <td class="fallingsky border-right border-left border-bottom align-center" style="line-height: 15px;width: 23%"><span style="font-family:dejavusans;">&#8377;</span>.&nbsp;2,77,000.00&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="unitPrice3" value="2,77,000" @if($order->orderProduct->product_id==5) checked="checked" @endif /></td>
        </tr>
        <tr>
            <td class="fallingsky border-right border-left border-bottom align-left" style="font-size: 10px;line-height: 17px;height: 17px;width: 23%;vertical-align: central;">&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="LEVELUK4" value="LEVELUK SD501 PLATINUM" @if($order->orderProduct->product_id==6) checked="checked" @endif /><label>LEVELUK SD501 PLATINUM</label></td>
            <td class="fallingsky border-right border-bottom align-center " style="line-height: 15px;width: 54%;"></td>
            <td class="fallingsky border-right border-left border-bottom align-center" style="line-height: 15px;width: 23%"><span style="font-family:dejavusans;">&#8377;</span>.&nbsp;2,97,000.00&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="unitPrice4" value="2,97,000" @if($order->orderProduct->product_id==6) checked="checked" @endif /></td>
        </tr>
        <tr>
            <td class="fallingsky border-right border-left border-bottom align-left" style="font-size: 10px;line-height: 17px;height: 17px;width: 23%;vertical-align: central;">&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="LEVELUK5" value="LEVELUK K8" @if($order->orderProduct->product_id==7) checked="checked" @endif /><label>LEVELUK K8</label></td>
            <td class="fallingsky border-right border-bottom align-center " style="line-height: 15px;width: 54%;"></td>
            <td class="fallingsky border-right border-left border-bottom align-center" style="line-height: 15px;width: 23%"><span style="font-family:dejavusans;">&#8377;</span>.&nbsp;3,43,000.00&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="unitPrice5" value="3,43,000" @if($order->orderProduct->product_id==7) checked="checked" @endif  /></td>
        </tr>
        <tr>
            <td class="fallingsky border-right border-left border-bottom align-left" style="font-size: 10px;line-height: 17px;height: 17px;width: 23%;vertical-align: central;">&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="LEVELUK6" value="LEVELUK SUPER 501" @if($order->orderProduct->product_id==8) checked="checked" @endif /><label>LEVELUK SUPER 501</label></td>
            <td class="fallingsky border-right border-bottom align-center " style="line-height: 15px;width: 54%;"></td>
            <td class="fallingsky border-right border-left border-bottom align-center" style="line-height: 15px;width: 23%"><span style="font-family:dejavusans;">&#8377;</span>.&nbsp;3,97,000.00&nbsp;&nbsp;<input style="width: 10px;" type="checkbox" name="unitPrice6" value="3,97,000" @if($order->orderProduct->product_id==8) checked="checked" @endif /></td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr><td colspan="2" style="line-height: 12px;"></td></tr>
        <tr>
            <td class="fallingsky border-left" style="width: 21%;background-color: #0646a5;line-height: 20px;color: #fff;font-size: 15px;border-bottom:1px solid #0646a5">&nbsp;&nbsp;SHIPPING CHARGES</td>
            <td class="fallingsky align-right" style="border:none;width: 29.5%;line-height: 0px;font-size: 7px;line-height: 10px;background-color: #0646a5;color: #fff;">Please do check Statutory Form requirement of your state with service/customer desk before proceeding your shipment</td>
            <td class="fallingsky align-right" style="border:none;width: 0.5%;line-height: 0px;font-size: 7px;line-height: 10px;background-color: #0646a5;"></td>
            <td class="border-bottom" style="width: 7%;"></td>
            <td style="width: 2%;"></td>
            <td class="fallingsky" style="width: 36%;background-color: #0646a5;line-height: 20px;color: #fff;font-size: 15px;border-bottom:1px solid #0646a5">&nbsp;&nbsp;PAYMENT METHOD</td>
            <td class="border-bottom" style="width: 4%;"></td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 58%;" class="border-left"><table class="table_Width" cellspacing="0" cellpadding="0"><tr><td class="fallingsky border-right border-bottom" style="width: 16%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;ZONE</td>
                        <td class="fallingsky border-right border-bottom" style="width: 54%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;ZONE CLASSIFICATION</td>
                        <td class="fallingsky border-right border-bottom" style="width: 17.8%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;BLUE DART</td>
                        <td class="fallingsky border-right border-bottom" style="width: 12.2%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;DTDC</td>
                    </tr>
                    <tr>
                        <td class="border-right border-bottom" style="width: 16%;line-height: 14px;font-size: 9px; font-weight: normal;">&nbsp;&nbsp;SOUTH</td>
                        <td class="border-right border-bottom" style="width: 54%;line-height: 14px;font-size: 8.3px;">&nbsp;AP, Telangana, Karnataka, Pondicherry, Kerala, Tamilnadu</td>
                        <td class="fallingsky border-right border-bottom" style="width: 17.8%;line-height: 15px;font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table><tr><td class="border" style="width: 20%;"><input style="line-height: 15px;" type="checkbox" class="border" name="bluedart1" value="1800" @if($order->shipping_zone==2 && $order->shipping_courier=='bluedart') checked="checked" @endif ></td><td><label>&nbsp;&nbsp;1800</label></td></tr></table></td>
                        <td class="fallingsky border-right border-bottom" style="width: 12.2%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;<table><tr><td class="border" style="width: 28%;"><input style="line-height: 15px;" type="checkbox" class="border" name="dtdc1" value="1100" @if($order->shipping_zone==2 && $order->shipping_courier=='dtdc') checked="checked" @endif ></td><td><label>&nbsp;1100</label></td></tr></table></td>
                    </tr>
                    <tr>
                        <td class="border-right border-bottom" style="width: 16%;line-height: 14px;font-size: 9px; font-weight: normal;">&nbsp;&nbsp;WEST</td>
                        <td class="border-right border-bottom" style="width: 54%;line-height: 14px;font-size: 8.3px;">&nbsp;Gujarat, Goa, Maharashtra, Madhya Pradesh, Chhattisgarh</td>
                        <td class="fallingsky border-right border-bottom" style="width: 17.8%;line-height: 15px;font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table><tr><td class="border" style="width: 20%;"><input style="line-height: 15px;" type="checkbox" class="border" name="bluedart2" value="2200" @if($order->shipping_zone==3 && $order->shipping_courier=='bluedart') checked="checked" @endif ></td><td><label>&nbsp;&nbsp;2200</label></td></tr></table></td>
                        <td class="fallingsky border-right border-bottom" style="width: 12.2%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;<table><tr><td class="border" style="width: 28%;"><input style="line-height: 15px;" type="checkbox" class="border" name="dtdc2" value="1500" @if($order->shipping_zone==3 && $order->shipping_courier=='dtdc') checked="checked" @endif ></td><td><label>&nbsp;1500</label></td></tr></table></td>
                    </tr>
                    <tr>
                        <td class="border-right border-bottom" style="width: 16%;line-height: 14px;font-size: 9px; font-weight: normal;">&nbsp;&nbsp;NORTH</td>
                        <td class="border-right border-bottom" style="width: 54%;line-height: 11px;font-size: 8.3px;">&nbsp;Chandigarh, Delhi, Haryana, Himachal Pradesh, Punjab, &nbsp;Rajasthan, Uttar Pradesh, Uttarakhand</td>
                        <td class="fallingsky border-right border-bottom" style="width: 17.8%;line-height: 15px;font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table><tr><td class="border" style="width: 20%;"><input style="line-height: 15px;" type="checkbox" class="border" name="bluedart3" value="2200" @if($order->shipping_zone==4 && $order->shipping_courier=='bluedart') checked="checked" @endif ></td><td><label>&nbsp;&nbsp;2200</label></td></tr></table></td>
                        <td class="fallingsky border-right border-bottom" style="width: 12.2%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;<table><tr><td class="border" style="width: 28%;"><input style="line-height: 15px;" type="checkbox" class="border" name="dtdc3" value="1500" @if($order->shipping_zone==4 && $order->shipping_courier=='dtdc') checked="checked" @endif ></td><td><label>&nbsp;1500</label></td></tr></table></td>
                    </tr>
                    <tr>
                        <td class="border-right border-bottom" style="width: 16%;line-height: 14px;font-size: 9px; font-weight: normal;">&nbsp;&nbsp;EAST</td>
                        <td class="border-right border-bottom" style="width: 54%;line-height: 14px;font-size: 8.3px;">&nbsp;Bihar, Jharkhand, Orissa, West Bengal</td>
                        <td class="fallingsky border-right border-bottom" style="width: 17.8%;line-height: 15px;font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table><tr><td class="border" style="width: 20%;"><input style="line-height: 15px;" type="checkbox" class="border" name="bluedart4" value="3100" @if($order->shipping_zone==8 && $order->shipping_courier=='bluedart') checked="checked" @endif ></td><td><label>&nbsp;&nbsp;3100</label></td></tr></table></td>
                        <td class="fallingsky border-right border-bottom" style="width: 12.2%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;<table><tr><td class="border" style="width: 28%;"><input style="line-height: 15px;" type="checkbox" class="border" name="dtdc4" value="2000" @if($order->shipping_zone==8 && $order->shipping_courier=='dtdc') checked="checked" @endif ></td><td><label>&nbsp;2000</label></td></tr></table></td>
                    </tr>
                    <tr>
                        <td class="border-right border-bottom" style="width: 16%;line-height: 14px;font-size: 9px; font-weight: normal;">&nbsp;&nbsp;NORTH EAST</td>
                        <td class="border-right border-bottom" style="width: 54%;line-height: 11px;font-size: 8.3px;">&nbsp;Arunachal Pradesh, Assam, Manipur, Tripura, Meghalaya, Mizoram, Nagaland, Sikkim</td>
                        <td class="fallingsky border-right border-bottom" style="width: 17.8%;line-height: 15px;font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table><tr><td class="border" style="width: 20%;"><input style="line-height: 15px;" type="checkbox" class="border" name="bluedart5" value="3400" @if($order->shipping_zone==5 && $order->shipping_courier=='bluedart') checked="checked" @endif ></td><td><label>&nbsp;&nbsp;3400</label></td></tr></table></td>
                        <td class="fallingsky border-right border-bottom" style="width: 12.2%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;<table><tr><td class="border" style="width: 28%;"><input style="line-height: 15px;" type="checkbox" class="border" name="dtdc5" value="2000" @if($order->shipping_zone==5 && $order->shipping_courier=='dtdc') checked="checked" @endif ></td><td><label>&nbsp;2000</label></td></tr></table></td>
                    </tr>
                    <tr>
                        <td class="border-right border-bottom" style="width: 16%;line-height: 14px;font-size: 9px; font-weight: normal;">&nbsp;&nbsp;J&K</td>
                        <td class="border-right border-bottom" style="width: 54%;line-height: 14px;font-size: 8.3px;">&nbsp;Jammu and Kashmir</td>
                        <td class="fallingsky border-right border-bottom" style="width: 17.8%;line-height: 15px;font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table><tr><td class="border" style="width: 20%;"><input style="line-height: 15px;" type="checkbox" class="border" name="bluedart6" value="3400" @if($order->shipping_zone==6 && $order->shipping_courier=='bluedart') checked="checked" @endif ></td><td><label>&nbsp;&nbsp;3400</label></td></tr></table></td>
                        <td class="fallingsky border-right border-bottom" style="width: 12.2%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;<table><tr><td class="border" style="width: 28%;"><input style="line-height: 15px;" type="checkbox" class="border" name="dtdc6" value="3000" @if($order->shipping_zone==6 && $order->shipping_courier=='dtdc') checked="checked" @endif ></td><td><label>&nbsp;3000</label></td></tr></table></td>
                    </tr>
                    <tr>
                        <td class="border-right border-bottom" style="width: 16%;line-height: 14px;font-size: 9px; font-weight: normal;">&nbsp;&nbsp;Portabliar</td>
                        <td class="border-right border-bottom" style="width: 54%;line-height: 14px;font-size: 8.3px;">&nbsp;Andaman Nicobar Island / Special Destination</td>
                        <td class="fallingsky border-right border-bottom" style="width: 17.8%;line-height: 15px;font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table><tr><td class="border" style="width: 20%;"><input style="line-height: 15px;" type="checkbox" class="border" name="bluedart7" value="3000" @if($order->shipping_zone==7 && $order->shipping_courier=='bluedart') checked="checked" @endif ></td><td><label>&nbsp;&nbsp;3000</label></td></tr></table></td>
                        <td class="fallingsky border-right border-bottom" style="width: 12.2%;line-height: 14px;font-size: 10px;">&nbsp;&nbsp;<table><tr><td class="border" style="width: 28%;"><input style="line-height: 15px;" type="checkbox" class="border" name="dtdc7" value="2000" @if($order->shipping_zone==7 && $order->shipping_courier=='dtdc') checked="checked" @endif ></td><td><label>&nbsp;2000</label></td></tr></table></td>
                    </tr>
                </table>
            </td>
            <td style="width: 2%;"></td>
            <td style="width: 40%;" class="border-left border-right border-bottom">
                <table>
                    <tr><td style="line-height: 5px;"></td></tr>
                    <tr>
                        <td style="width: 5%;line-height: 14px;font-size: 10px;"><input style="line-height: 15px;" type="checkbox" class="border" name="checkno" value="1" @if($order->orderPayment->payment_method=='cheque') checked="checked" @endif ></td>
                        <td style="width: 21.5%;line-height: 14px;font-size: 9px;">CHEQUE NO:</td>
                        <td class="border-bottom" style="width: 69.5%;line-height: 14px;font-size: 10px;"><input type="text" style="font-size: 9px;width: 100%;border: 0px;" size="39" name="checkno" value="{{$order->orderPayment->cheque_no}}" ></td>
                    </tr>
                    <tr><td style="line-height: 3px;"></td></tr>
                    <tr>
                        <td style="width: 5%;line-height: 14px;font-size: 10px;"></td>
                        <td style="width: 40%;line-height: 14px;font-size: 9px;">CHEQUE DEPOSIT DATE:</td>
                        <td class="border-bottom" style="width: 18%;line-height: 14px;font-size: 10px;"><input type="text" style="font-size: 9px;width: 100%;border: 0px;" size="10" name="checkdate" value="{{$order->orderPayment->cheque_date}}" ></td>
                        <td style="width: 10%;line-height: 14px;font-size: 9px;">BANK:</td>
                        <td class="border-bottom" style="width: 22.5%;line-height: 14px;font-size: 10px;"><input type="text" style="font-size: 9px;width: 100%;border: 0px;" size="13" name="bankname" value="{{$order->orderPayment->cheque_bank_name}}" ></td>
                    </tr>
                    <tr><td style="line-height: 3px;"></td></tr>
                    <tr>
                        <td style="width: 5%;line-height: 14px;font-size: 10px;"><input style="line-height: 15px;" type="checkbox" class="border" name="creditcard" value="1" @if($order->orderPayment->payment_method=='credit_card') checked="checked" @endif ></td>
                        <td style="width: 80%;line-height: 14px;font-size: 9px;">CREDIT CARD (single payment):</td>
                    </tr>
                    <tr>
                        <td style="width: 5%;line-height: 14px;font-size: 10px;"><input style="line-height: 15px;" type="checkbox" class="border" name="INSTALLMENT" value="1" @if($order->orderPayment->credit_card_instalment!=null) checked="checked" @endif ></td>
                        <td style="width: 50%;line-height: 14px;font-size: 9px;">CREDIT CARD-INSTALLMENT:[</td>
                        <td style="width: 30%;line-height: 14px;font-size: 9px;">] Month/Bank Name:</td>
                        <td class="border-bottom" style="width: 13%;line-height: 14px;font-size: 10px;"><input type="text" style="font-size: 9px;width: 100%;border: 0px;" size="7" name="Monthnabk" value="" ></td>
                    </tr>
                    <tr><td style="line-height: 3px;"></td></tr>
                    <tr>
                        <td style="width: 5%;line-height: 14px;font-size: 10px;"><input style="line-height: 15px;" type="checkbox" class="border" name="neft" value="1" @if($order->orderPayment->bank_transfer_type=='neft') checked="checked" @endif ></td>
                        <td style="width: 18%;line-height: 14px;font-size: 9px;">NEFT</td>
                        <td style="width: 5%;line-height: 14px;font-size: 10px;"><input style="line-height: 15px;" type="checkbox" class="border" name="rtgs" value="1" @if($order->orderPayment->bank_transfer_type=='rtgs') checked="checked" @endif ></td>
                        <td style="width: 18%;line-height: 14px;font-size: 9px;">RTGS</td>
                        <td style="width: 5%;line-height: 14px;font-size: 10px;"><input style="line-height: 15px;" type="checkbox" class="border" name="imps" value="1" @if($order->orderPayment->bank_transfer_type=='imps') checked="checked" @endif  ></td>
                        <td style="width: 18%;line-height: 14px;font-size: 9px;">IMPS</td>
                    </tr>
                    <tr>
                        <td style="width: 5%;line-height: 14px;font-size: 10px;"><input style="line-height: 15px;" type="checkbox" class="border" name="nach" value="1" @if($order->orderPayment->nach_instalment!=null) checked="checked" @endif ></td>
                        <td style="width: 45%;line-height: 14px;font-size: 9px;">Enagic Instalment Plan/NACH</td>
                        <td style="width: 5%;line-height: 14px;font-size: 10px;"><input style="line-height: 15px;" type="checkbox" class="border" name="10months" value="1" @if($order->orderPayment->nach_instalment==10) checked="checked" @endif ></td>
                        <td style="width: 18%;line-height: 14px;font-size: 9px;">10 Months</td>
                        <td style="width: 5%;line-height: 14px;font-size: 10px;"><input style="line-height: 15px;" type="checkbox" class="border" name="20months" value="1" @if($order->orderPayment->nach_instalment==20) checked="checked" @endif ></td>
                        <td style="width: 18%;line-height: 14px;font-size: 9px;">20 Months</td>
                    </tr>
                    <tr><td style="line-height: 6px;"></td></tr>
                    <tr>
                        <td class="fallingsky" style="width: 100%;line-height: 12px;font-size: 10px;">*KINDLY SHARE ACKNOWLEDGEMENT COPY OF PAYMENT AND REFERENCE NUMBER</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr><td colspan="2" style="line-height: 5px;"></td></tr>
        <tr>
            <td class="fallingsky" style="width: 50%;background-color: #0646a5;line-height: 20px;color: #fff;font-size: 15px;">&nbsp;&nbsp;FOR OFFICE USE ONLY</td>
            <td style="width: 49%;line-height: 0px;">
                <div style="line-height: 1px;"></div>
                <div style="border-bottom: 9px solid #0646a5;line-height: 5px;"></div>
            </td>
        </tr>
    </table>
    <table class="table_Width" cellspacing="0" cellpadding="0">
        <tr>
            <td class="border-left border-right border-bottom" style="line-height: 15px;width: 28%;">
                <table class="table_Width" cellspacing="0" cellpadding="0">
                    <tr><td style="line-height: 5px;"></td></tr>
                    <tr>
                        <td class="fallingsky" style="width: 35%;line-height: 12px;">UNIT PRICE</td>
                        <td class="fallingsky" style="width: 11%;line-height: 12px;">:&nbsp;&nbsp;<span style="font-family:dejavusans;">&#8377;</span>.</td>
                        <td style="width: 51%;" class="border">{{$order->unit_price}}</td>
                    </tr>
                    <tr><td style="line-height: 5px;"></td></tr>
                    <tr>
                        <td class="fallingsky" style="width: 35%;line-height: 12px;">GST %</td>
                        <td class="fallingsky" style="width: 11%;line-height: 12px;">:&nbsp;&nbsp;<span style="font-family:dejavusans;">&#8377;</span>.</td>
                        <td style="width: 51%;" class="border">{{$order->company_gst}}</td>
                    </tr>
                    <tr><td style="line-height: 5px;"></td></tr>
                    <tr>
                        <td class="fallingsky" style="width: 35%;line-height: 12px;">SHIPPING</td>
                        <td class="fallingsky" style="width: 11%;line-height: 12px;">:&nbsp;&nbsp;<span style="font-family:dejavusans;">&#8377;</span>.</td>
                        <td style="width: 51%;" class="border">{{$order->shipping}}</td>
                    </tr>
                    <tr><td style="line-height: 5px;"></td></tr>
                    <tr>
                        <td class="fallingsky" style="width: 35%;line-height: 12px;">TOTAL</td>
                        <td class="fallingsky" style="width: 11%;line-height: 12px;">:&nbsp;&nbsp;<span style="font-family:dejavusans;">&#8377;</span>.</td>
                        <td style="width: 51%;" class="border">{{$order->total}}</td>
                    </tr>
                    <tr><td style="line-height: 9px;"></td></tr>
                </table>
            </td>
            <td class="" style="line-height: 15px;width: 72%;"><table>
                    <tr><td style="line-height: 5px;"></td></tr>
                    <tr>
                        <td style="line-height: 13px;width: 23%;">&nbsp;Received by: Name:</td>
                        <td class="border-bottom" style="line-height: 13px;width: 77%;">{{$order->receiver_name}}</td>
                    </tr>
                    <tr><td style="line-height: 5px;"></td></tr>
                </table>
                @php
                    $application_date=explode('-',$order->application_date);
                @endphp
                <table>
                    <tr><td style="line-height: 5px;"></td></tr>
                    <tr>
                        <td class="border-bottom" style="line-height: 13px;width: 46%;font-size: 12px;"><table><tr><td style="line-height: 15px;"></td></tr></table>
                            <table>
                                <tr>
                                    <td>&nbsp;Application Date:</td>
                                    <td><table><tr>
                                                <td style="width: 20%;"><input style="font-size: 12px;line-height: 10px;" type="text" size="4" maxlength="2" name="appdate" value="{{$application_date[2]}}" /></td>
                                                <td class="align-center" style="width: 8%;font-size: 13px;">/</td>
                                                <td style="width: 20%;"><input style="font-size: 12px;line-height: 10px;" maxlength="2" type="text" size="4" name="appdate" value="{{$application_date[1]}}" /></td>
                                                <td class="align-center" style="width: 8%;font-size: 13px;">/</td>
                                                <td style="width: 30%;"><input style="font-size: 12px;line-height: 10px;" type="text" size="6" maxlength="4" name="appdate" value="{{$application_date[0]}}" /></td>
                                            </tr></table></td>
                                </tr>
                            </table>
                            <table><tr><td style="line-height: 32px;"></td></tr></table>
                        </td>
                        <td class="border-top border-left" rowspan="2">
                            <table><tr><td style="line-height: 25px;"></td></tr></table>
                            <table>
                                <tr>
                                    <td style="width: 50%;font-size: 12px;" class="fallingsky border-bottom align-center"></td>
                                    <td style="width: 10%"></td>
                                    <td style="width: 36%;font-size: 12px;" class="fallingsky border-bottom align-center"><table>
                                            <tr>
                                                <td style="width: 24%;" class="align-left"><input style="font-size: 12px;line-height: 10px;" type="text" size="5" maxlength="2" name="appdate1" value="{{$application_date[2]}}" ></td>
                                                <td class="align-center" style="width: 8%;font-size: 13px;">/</td>
                                                <td style="width: 24%;" class="align-left"><input style="font-size: 12px;line-height: 10px;" type="text" size="5" maxlength="2" name="appdate1" value="{{$application_date[1]}}" ></td>
                                                <td class="align-center" style="width: 8%;font-size: 13px;">/</td>
                                                <td style="width: 24%;" class="align-left"><input style="font-size: 12px;line-height: 10px;" type="text" size="8" maxlength="4" name="appdate1" value="{{$application_date[0]}}" ></td>
                                            </tr>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td style="width: 50%;font-size: 12px;" class="fallingsky align-center">APPLICANT'S SIGNATURE:</td>
                                    <td style="width: 50%;font-size: 12px;" class="fallingsky align-center">DATE</td>
                                </tr>
                            </table>

                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
</form>
<?php
$html = ob_get_clean();

$pdf->AddPage('P', 'A4');
$pdf->writeHTML($html, true, false, false, false, '');
$js = <<<EOD
var f = this.getField("mgender");
f.style = style.ch;
EOD;

$pdf->IncludeJS($js);
$uniqueid=uniqid();
$rootPath = base_path().'/uploads/pdf/' . $uniqueid . '.pdf';
$pdf_string=$pdf->Output($rootPath, 'I');

App\Models\Order::where('id',$order->id)->update(['pdf'=>'uploads/pdf/' . $uniqueid . '.pdf'])
//$pdf->reset();
//============================================================+
// END OF FILE
//============================================================+
