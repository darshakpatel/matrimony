@extends('web.layouts.app')

@section('content')
    <div class="py-4 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-md-8 mx-auto">
                    <div class="card">
                        <div class="card-body">
                            <div class="mb-4 text-center">
                                <h2 class="h3 text-primary mb-0">Create Your Account</h2>
                                <p>Fill out the form to get started.</p>
                            </div>
                            <form class="form-default" id="addEditForm" role="form"
                                  method="POST">
                                @csrf
                                <input type="hidden" value="self" name="on_behalf">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label class="form-label"
                                                   for="name">First Name</label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="first_name" id="first_name"
                                                   placeholder="First Name" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label class="form-label"
                                                   for="name">Last Name</label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="last_name" id="last_name"
                                                   placeholder="Last Name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label class="form-label" for="gender">Gender</label>
                                            <select
                                                    class="form-control aiz-selectpicker "
                                                    name="gender" required>
                                                <option value="">Gender</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label class="form-label"
                                                   for="name">Date Of Birth</label>
                                            <input type="text"
                                                   class="form-control aiz-date-range"
                                                   name="date_of_birth" id="date_of_birth"
                                                   placeholder="Date Of Birth" data-single="true"
                                                   data-format="DD-MM-YYYY"
                                                   data-show-dropdown="true"
                                                   data-min-date="365"
                                                   data-max-date="365"
                                                   autocomplete="off" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group mb-3">
                                            <label class="form-label"
                                                   for="mobile_no">Mobile No</label>
                                            <input type="text" id="mobile_no"
                                                   class="form-control integer"
                                                   placeholder="Mobile No" name="mobile_no"
                                                   autocomplete="off" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group mb-3">
                                            <label class="form-label"
                                                   for="email">Email address</label>
                                            <input type="email"
                                                   class="form-control"
                                                   name="email" id="email"
                                                   placeholder="Email address" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label class="form-label"
                                                   for="password">Password</label>
                                            <input type="password"
                                                   class="form-control"
                                                   name="password" placeholder="********"
                                                   aria-label="********"
                                                   required>
                                            <small>Minimum 8 characters</small>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label class="form-label"
                                                   for="password-confirm">Confirm password</label>
                                            <input type="password" class="form-control"
                                                   name="password_confirmation"
                                                   placeholder="********" required>
                                            <small>Minimum 8 characters</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label class="aiz-checkbox">
                                        <input type="checkbox" name="checkbox_example_1" required>
                                        <span class=opacity-60>By signing up you agree to our
												<a href="{{ env('APP_URL').'/terms-conditions' }}" target="_blank">
                                                    terms and conditions.</a>
											</span>
                                        <span class="aiz-square-check"></span>
                                    </label>
                                </div>
                                <div class="">
                                    <button type="submit"
                                            class="btn btn-block btn-primary">Create Account
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script src="{{asset('user/assets/js/custom/register.js')}}?v={{time()}}"></script>
@endsection
