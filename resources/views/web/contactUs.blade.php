@extends('web.layouts.app')

@section('meta')
@endsection

@section('content')
    <section class="pt-4 mb-4">
        <div class="container text-center">
            <div class="row">
{{--                <div class="col-lg-6">--}}
{{--                    <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">--}}
{{--                        <li class="breadcrumb-item opacity-50">--}}
{{--                            <a class="text-reset" href="{{ route('home') }}">Home</a>--}}
{{--                        </li>--}}
{{--                        <li class="text-dark fw-600 breadcrumb-item">--}}
{{--                            <a class="text-reset" href="{{ route('contact-us') }}">Contact Us</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
    <section class="mb-4">
        <div class="container">
            <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
                <div class="mb-4">
                    <h4 class="text-uppercase text-primary fs-14 border-bottom border-primary pb-4 mb-4">Contact Us</h4>
                    <div class="row  no-gutters">

{{--                        <div class="col-xl col-md-6 mb-4">--}}
{{--                            <div class="mb-3 opacity-60">--}}
{{--                                <i class="las la-home mr-2"></i>--}}
{{--                                <span>Address</span>--}}
{{--                            </div>--}}
{{--                            <div>{{$address}}</div>--}}
{{--                        </div>--}}

                        <div class="col-xl col-md-12 mb-4">
                            <div class="mb-3 opacity-60">
                                <i class="las la-envelope mr-2"></i>
                                <span>Email</span>
                            </div>
                            <div>{{$email}}</div>
                        </div>
{{--                        <div class="col-xl col-md-6 mb-4">--}}
{{--                            <div class="mb-3 opacity-60">--}}
{{--                                <i class="las la-phone mr-2"></i>--}}
{{--                                <span>Phone</span>--}}
{{--                            </div>--}}
{{--                            <div>{{$support_no}}</div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
