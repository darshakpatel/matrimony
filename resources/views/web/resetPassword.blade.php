@extends('web.layouts.app')

@section('content')
    <div class="py-4 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-xxl-4 col-xl-5 col-md-7 mx-auto">
                    <div class="card">
                        <div class="card-body">
                            <div class="mb-5 text-center">
                                <h1 class="h3 text-primary mb-0">Reset Password</h1>
                            </div>
                            <form class="" method="POST" id="addEditForm">
                                @csrf
                                <input type="hidden" name="email" value="{{$email}}">
                                <div class="form-group">
                                    <label class="form-label" for="password">New Password</label>
                                    <input type="text" class="form-control"
                                           name="password" id="password"
                                           placeholder="New Password" required>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="confirm_password">Confirm Password</label>
                                    <input type="text" class="form-control"
                                           name="confirm_password" id="confirm_password"
                                           placeholder="Confirm Password" required>
                                </div>

                                <div class="mb-5">
                                    <button type="submit"
                                            class="btn btn-block btn-primary">Reset Password
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @foreach (session('flash_notification', collect())->toArray() as $message)
        <script>
            successToast('{{ $message['message'] }}', '{{ $message['level'] }}');
        </script>
    @endforeach
    <script src="{{asset('user/assets/js/custom/resetPassword.js')}}"></script>
@endsection