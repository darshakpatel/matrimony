@extends('web.layouts.app')

@section('content')
    <div class="py-4 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-xxl-4 col-xl-5 col-md-7 mx-auto">
                    <div class="card">
                        <div class="card-body">
                            <div class="mb-5 text-center">
                                <h1 class="h3 text-primary mb-0">Forgot Password</h1>
                            </div>
                            <form class="" method="POST" id="addEditForm">
                                @csrf
                                <div class="form-group">
                                    <label class="form-label" for="otp">Email</label>
                                    <input type="email" class="form-control"
                                           name="email" id="email"
                                           placeholder="Email" required>
                                </div>

                                <div class="mb-5">
                                    <button type="submit"
                                            class="btn btn-block btn-primary">Forgot Password
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('user/assets/js/custom/forgotPassword.js')}}"></script>
@endsection
