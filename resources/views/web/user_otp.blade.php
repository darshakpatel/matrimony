@extends('web.layouts.app')

@section('content')
    <?php
    $resend_otp_seconds = DB::table('settings')->where('meta_key', 'resend_otp_seconds')->first()->meta_value;
    ?>
    <div class="py-4 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-xxl-4 col-xl-5 col-md-7 mx-auto">
                    <div class="card">
                        <div class="card-body">
                            <div class="mb-5 text-center">
                                @if(Session::has('email'))
                                    <h1 class="h3 text-primary mb-0">Please verify your email</h1>
                                    <h3>Please check email in INBOX and SPAM folder as well</h3>
                                @else
                                    <h1 class="h3 text-primary mb-0">Please verify mobile number</h1>
                                @endif
                            </div>
                            <form class="" method="POST" id="addEditForm">
                                @csrf
                                <div class="form-group">
                                    <label class="form-label" for="otp">OTP</label>
                                    <input type="number" class="form-control"
                                           name="otp" id="otp"
                                           placeholder="OTP" required>
                                </div>
                                <div class="mb-2">
                                    <button type="submit"
                                            class="btn btn-block btn-primary">Verify OTP
                                    </button>
                                </div>
                                <div class="mb-2">
                                    <button type="button" id="resend_otp" disabled
                                            class="btn btn-danger">Resend OTP
                                    </button>
                                </div>
                                <span id="resend_otp_text">You can resend otp after <span
                                            id="countdown">{{$resend_otp_seconds}}</span> seconds</span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        var resend_otp_seconds ={{$resend_otp_seconds}}
    </script>
    <script src="{{asset('user/assets/js/custom/verifyOtp.js')}}?v={{time()}}"></script>
@endsection
