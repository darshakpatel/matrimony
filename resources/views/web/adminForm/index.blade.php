@extends('web.layouts.app')

@section('content')

    <section class="py-5 bg-white">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="mb-4 text-center">
                        <h2 class="h3 text-primary mb-0">Register for SubAdmin account</h2>
                    </div>
                    <form class="form-default" id="addEditForm" role="form"
                          method="POST">
                        @csrf
                        <input type="hidden" value="101" id="country_id" name="country_id">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="name">Name</label>
                                    <input type="text"
                                           class="form-control"
                                           name="name" id="name"
                                           placeholder="First Name" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="name">Father Name</label>
                                    <input type="text"
                                           class="form-control"
                                           name="father_name" id="father_name"
                                           placeholder="Father Name" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="mobile_no">Mobile No</label>
                                    <input type="text" id="mobile_no"
                                           class="form-control integer"
                                           placeholder="Mobile No" name="mobile_no"
                                           autocomplete="off" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="state_id">State</label>
                                    <select name="state_id" id="state_id" class=" form-control" required>
                                        <option value="">Select State</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="city_id">City</label>
                                    <select name="city_id" id="city_id" class=" form-control" required>
                                        <option value="">Select City</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="password">Password</label>
                                    <input type="password"
                                           class="form-control"
                                           name="password" placeholder="********"
                                           aria-label="********"
                                           required>
                                    <small>Minimum 8 characters</small>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="password-confirm">Confirm password</label>
                                    <input type="password" class="form-control"
                                           name="password_confirmation"
                                           placeholder="********" required>
                                    <small>Minimum 8 characters</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="image">Profile Image</label>
                                    <input type="file" class="dropify" name="image" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="aadhar_card">Aadhar Card</label>
                                    <input type="file" class="dropify" name="aadhar_card">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="pincode">Pincode</label>
                                    <input type="number" class="form-control"
                                           name="pincode"
                                           placeholder="Pincode" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="current_place">Current Place</label>
                                    <input type="text" class="form-control"
                                           name="current_place"
                                           placeholder="Current Place" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-3">
                                    <label class="form-label"
                                           for="address">Current Address</label>
                                    <textarea
                                            class="form-control"
                                            name="address" id="address"
                                            placeholder="Address" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="aiz-checkbox">
                                <input type="checkbox" name="checkbox_example_1" required>
                                <span class=opacity-60>By signing up you agree to our
												<a href="{{ env('APP_URL').'/page/admin-terms-conditions' }}"
                                                   target="_blank">
                                                    terms and conditions.</a>
											</span>
                                <span class="aiz-square-check"></span>
                            </label>
                        </div>
                        <div class="">
                            <button type="submit"
                                    class="btn btn-block btn-primary">Send Request
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('script')
    <script src="{{asset('user/assets/js/custom/adminForm.js')}}"></script>
    <script>
        $(".dropify").dropify()
    </script>
@endsection
