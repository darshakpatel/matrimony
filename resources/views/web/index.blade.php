@extends('web.layouts.app')
@section('content')

    <!-- Homepage Slider Section -->
    <section class="position-relative overflow-hidden min-vh-100 d-flex home-slider-area">
        <div class="absolute-full">
            <div class="aiz-carousel aiz-carousel-full h-100" data-fade='true' data-infinite='true'
                 data-autoplay='false'>
                <img class="img-fit" @if(Auth::check()) style="margin-top:175px"
                     @endif  src="{{ asset($banner->image) }}">
            </div>
            <div class="absolute-full bg-white opacity-70 d-md-none"></div>
        </div>
        <div class="container position-relative d-flex flex-column">
            <div class="row pt-11 pb-8 my-auto align-items-center">
                <div class="col-xl-5 col-lg-6 my-4">
                    <div class="text-dark">
                        <h1 style="color: white; font-family: Poppins, sans-serif;"><span
                                    style="background-color: inherit;"><span
                                        style="font-weight: bolder;">Meet your Soul </span></span>
                        </h1>
                        <h1 style="color: white; font-family: Poppins, sans-serif;"><span
                                    style="background-color: inherit;"><span
                                        style="font-weight: bolder;">Partner</span></span></h1>
                    </div>
                </div>
                @if(!Auth::check())
                    <div class="offset-xxl-2 offset-xl-1 col-lg-6 col-xxl-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-4 text-center">
                                    <h2 class="h3 text-primary mb-0">Create Your Account</h2>
                                    <p>Fill out the form to get started.</p>
                                </div>
                                <form class="form-default" id="addEditForm" role="form"
                                      method="POST">
                                    @csrf
                                    <input type="hidden" name="on_behalf" value="self"/>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group mb-3">
                                                <label class="form-label"
                                                       for="name">First Name</label>
                                                <input type="text"
                                                       class="form-control"
                                                       name="first_name" id="first_name"
                                                       placeholder="First Name" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group mb-3">
                                                <label class="form-label"
                                                       for="name">Last Name</label>
                                                <input type="text"
                                                       class="form-control"
                                                       name="last_name" id="last_name"
                                                       placeholder="Last Name" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group mb-3">
                                                <label class="form-label" for="gender">Gender</label>
                                                <select
                                                        class="form-control aiz-selectpicker "
                                                        name="gender" required>
                                                    <option value="">Gender</option>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group mb-3">
                                                <label class="form-label"
                                                       for="name">Date Of Birth</label>
                                                <input type="text"
                                                       class="form-control aiz-date-range"
                                                       name="date_of_birth" id="date_of_birth"
                                                       placeholder="Date Of Birth" data-single="true"
                                                       data-format="DD-MM-YYYY"
                                                       data-show-dropdown="true" data-max-date="18"
                                                       autocomplete="off" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-lg-12">
                                            <div class="form-group mb-3">
                                                <label class="form-label"
                                                       for="mobile_no">Mobile No</label>
                                                <input type="text" id="mobile_no"
                                                       class="form-control integer"
                                                       placeholder="Mobile No" name="mobile_no"
                                                       autocomplete="off" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group mb-3">
                                                <label class="form-label"
                                                       for="email">Email address</label>
                                                <input type="email"
                                                       class="form-control"
                                                       name="email" id="email"
                                                       placeholder="Email address" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group mb-3">
                                                <label class="form-label"
                                                       for="password">Password</label>
                                                <input type="password"
                                                       class="form-control"
                                                       name="password" placeholder="********"
                                                       aria-label="********"
                                                       required>
                                                <small>Minimum 8 characters</small>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group mb-3">
                                                <label class="form-label"
                                                       for="password-confirm">Confirm password</label>
                                                <input type="password" class="form-control"
                                                       name="password_confirmation"
                                                       placeholder="********" required>
                                                <small>Minimum 8 characters</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label class="aiz-checkbox">
                                            <input type="checkbox" name="checkbox_example_1" required>
                                            <span class=opacity-60>By signing up you agree to our
												<a href="{{ env('APP_URL').'/page/terms-and-condition' }}"
                                                   target="_blank">
                                                    terms and conditions.</a>
											</span>
                                            <span class="aiz-square-check"></span>
                                        </label>
                                    </div>
                                    <div class="">
                                        <button type="submit"
                                                class="btn btn-block btn-primary">Create Account
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

        </div>
    </section>
    <section class="bg-white mt-3">
        <div class="container">
            <div class="row gutters-10">
                @foreach($homeBanners as $homeBanner)
                    <div class="col-xl col-md-6">
                        <div class="mb-3">
                            <a href="#"
                               class="d-block text-reset">
                                <img src="{{ asset($homeBanner->image) }}" style="height: 219px"
                                     data-src=""
                                     alt="{{ env('APP_NAME') }}" class="img-fluid lazyload w-100">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="py-8 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-xl-8 col-xxl-6 mx-auto">
                    <div class="text-center section-title mb-5">
                        <h2 class="fw-600 mb-3">How it Works</h2>
                        <p class="fw-400 fs-16 opacity-60">When you realize you want to spend the rest of your life
                            with somebody, you want the rest of your life to start as soon as possible.</p>
                    </div>
                </div>
            </div>
            <div class="row gutters-10">
                <div class="col-lg">
                    <div class="border p-3 mb-3">
                        <div class=" row align-items-center">
                            <div class="col-7">
                                <div class="text-primary fw-600 h1">1</div>
                                <div
                                        class="text-secondary fs-20 mb-2 fw-600">Sign up
                                </div>
                                <div
                                        class="fs-15 opacity-60">Register for free & put up your Profile
                                </div>
                            </div>
                            <div class="mt-3 col-5 text-right">
                                <img
                                        src="{{ asset('user/assets/image/signup.png') }}"
                                        class="img-fluid h-80px">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg">
                    <div class="border p-3 mb-3">
                        <div class=" row align-items-center">
                            <div class="col-7">
                                <div class="text-primary fw-600 h1">2</div>
                                <div
                                        class="text-secondary fs-20 mb-2 fw-600">Approval
                                </div>
                                <div
                                        class="fs-15 opacity-60">Admin Will Approve Your Profile Within One Week
                                </div>
                            </div>
                            <div class="mt-3 col-5 text-right">
                                <img
                                        src="{{ asset('user/assets/image/connect.png') }}"
                                        class="img-fluid h-80px">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg">
                    <div class="border p-3 mb-3">
                        <div class=" row align-items-center">
                            <div class="col-7">
                                <div class="text-primary fw-600 h1">3</div>
                                <div
                                        class="text-secondary fs-20 mb-2 fw-600">Connect
                                </div>
                                <div
                                        class="fs-15 opacity-60">Connect with member and find your perfect partner
                                </div>
                            </div>
                            <div class="mt-3 col-5 text-right">
                                <img
                                        src="{{ asset('user/assets/image/interact.png') }}"
                                        class="img-fluid h-80px">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="py-9 bg-white">
        <div class="container">
            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="3" data-xl-items="2" data-lg-items="2"
                 data-md-items="3" data-sm-items="2" data-xs-items="1" data-dots='true' data-autoplay="true"
                 data-infinite='true'>
                @foreach ($sliders as $key => $slider)
                    <div class="carousel-box">
                        <div class="rounded border position-relative overflow-hidden">
                            <img src="{{ asset($slider->image) }}" class="img-fit mw-100 h-400px">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    {{--    <section class="py-7 bg-cover bg-center text-white"--}}
    {{--             style="background-image: url('{{ asset('user/assets/image/review.png') }}');">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}

    {{--                <div class="col-lg-10 col-xl-9 col-xxl-6 mx-auto">--}}
    {{--                    <div class="text-center section-title mb-5">--}}
    {{--                        <h2 class="fw-600 mb-3">Reviews</h2>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}

    {{--            <div class="row">--}}
    {{--                <div class="col-xxl-10 mx-auto">--}}
    {{--                    <div class="aiz-carousel large-arrow" data-items="1" data-arrows='true' data-infinite='true'--}}
    {{--                         data-autoplay='true'>--}}
    {{--                        @php $reviews=[]; @endphp--}}
    {{--                        @foreach ($reviews as $key => $review)--}}
    {{--                            <div class="carousel-box">--}}
    {{--                                <div class="text-center px-lg-9">--}}
    {{--                                    <img--}}
    {{--                                            src="{{ $review->user->image }}"--}}
    {{--                                            class="size-180px img-fit mx-auto rounded-circle border border-white border-width-5 shadow-lg mb-5">--}}
    {{--                                    <div class="fs-18 fw-300 font-italic">{{ $review->review }}</div>--}}
    {{--                                    <i class="las la-quote-right la-10x text-dark opacity-30"></i>--}}

    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        @endforeach--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
@endsection

@section('modal')
    @include('modals.login_modal')
@endsection

@section('script')
    <script src="{{asset('user/assets/js/custom/register.js')}}?v={{time()}}"></script>
@endsection
