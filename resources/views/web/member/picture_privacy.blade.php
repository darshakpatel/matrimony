@extends('web.layouts.member_panel')
@section('panel_content')
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{ trans('messages.Picture Privacy') }}</h5>
        </div>
        <div class="card-body">
          <form action="{{ route('member.update_picture_privacy', Auth::user()->id) }}" method="POST">
              @csrf
              <div class="form-group mb-3">
                  <label for="name">{{trans('messages.Profile Picture')}}</label>
                  @php $profile_pic_permission = \App\Models\Member::where('user_id',Auth::user()->id)->first()->profile_picture_privacy; @endphp
                  <select class="form-control aiz-selectpicker" data-live-search="true" name="profile_picture_privacy" required>
                    <option value="0" @if($profile_pic_permission == 0) selected @endif>{{ trans('messages.Only Me') }}</option>
                    <option value="1" @if($profile_pic_permission == 1) selected @endif>{{ trans('messages.All Member') }}</option>
                    <option value="2" @if($profile_pic_permission == 2) selected @endif>{{ trans('messages.Premium Member') }}</option>
                  </select>
              </div>
              <div class="form-group mb-6">
                  <label>{{trans('messages.Gallery Images')}}</label>
                  @php $gallery_pic_permission = \App\Models\Member::where('user_id',Auth::user()->id)->first()->gallery_image_privacy; @endphp
                  <select class="form-control aiz-selectpicker" data-live-search="true" name="gallery_image_privacy" required>
                    <option value="0" @if($gallery_pic_permission == 0) selected @endif>{{ trans('messages.Only Me') }}</option>
                    <option value="1" @if($gallery_pic_permission == 1) selected @endif>{{ trans('messages.All Member') }}</option>
                    <option value="2" @if($gallery_pic_permission == 2) selected @endif>{{ trans('messages.Premium Member') }}</option>
                  </select>
              </div>

              <div class="form-group row text-right pb-7">
                  <div class="col-md-12">
                      <button type="submit" class="btn btn-primary">{{trans('messages.Save')}}</button>
                  </div>
              </div>
            </form>
        </div>
    </div>
@endsection
