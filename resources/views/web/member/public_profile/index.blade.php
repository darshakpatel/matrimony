@extends('web.layouts.app')
@section('content')
    <section class="pt-6 bg-primary-grad text-white">
        <div class="container">
            <div class="row mt-5">
                <div class="col-xl-8 offset-xl-4">
                    <div class="px-3 row align-items-center">
                        <div class="col-md-8 col-xxl-9">
                            <h1 class="fs-24 fw-600">
                                {{ $user->first_name.' '.$user->last_name }}
                                <span class="fs-20 fw-600">
                            @php
                                $profile_match = \App\Models\ProfileMatch::where('user_id',Auth::user()->id)->where('match_id',$user->id)->first();
                                if(!empty($profile_match)){
                                  echo '(Matched - '.$profile_match->match_percentage.'%)';
                                }
                            @endphp
                          </span>
                            </h1>
                            <div class="fs-12">
                                <span class="opacity-60">Member ID: </span>
                                <span class="ml-2">{{ $user->unique_id }}</span>
                            </div>
                            <hr class="border-gray-500">
                            <table class="w-100">
                                <tbody>
                                <tr>
                                    <td width="50%">
                                        {{ !empty($user->date_of_birth) ? \Carbon\Carbon::parse($user->date_of_birth)->age : "" }}
                                        Years
                                    </td>
                                    <td width="50%">
                                        {{ !empty($user->profile->marital_status) ? $user->profile->marital_status : ""  }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @if($user->is_nri==1)
                                            NRI
                                        @else
                                            Lives in {{$user->city?$user->city->name:"India"}}
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="mt-4">
                        <div class="nav nav-tabs aiz-nav-tabs bottom-bordered active-white border-0">
                            <a class="text-black-50 d-inline-block fw-600 fs-15 px-3 py-2 active" data-toggle="tab"
                               href="#profile">Detailed Profile</a>
                            <a class="text-black-50 d-inline-block fw-600 fs-15 px-3 py-2" data-toggle="tab"
                               href="#gallery">Photo Gallery</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style type="text/css">
        @media (min-width: 1199.98px) {
            .aiz-profile-sidebar {
                margin-top: -260px
            }
        }
    </style>
    <section class="py-5 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-xxl-3 order-1 order-xl-0">
                    <div class="aiz-profile-sidebar">
                        <div class="overflow-hidden rounded shadow-lg mb-4 bg-white d-none d-xl-block">
                            <img src="{{ asset($user->image) }}"
                                 onerror="this.onerror=null;this.src='{{ asset('assets/img/avatar-place.png') }}';"
                                 class="img-fluid w-100"
                            >
                        </div>

                        <div class="mb-4 p-4 border rounded border-gray-200 d-none d-xl-block">
                            <div class="fs-12">
                                <span class="opacity-60">Member ID: </span>
                                <span class="ml-1 text-primary">{{ $user->unique_id }}</span>
                            </div>
                            <h2 class="fs-20 fw-500 mb-4">{{ $user->first_name.' '.$user->last_name }}</h2>

                            @if($user->id!=Auth::user()->id)
                                <div class="row gutters-5">
                                    <div class="col">
                                        <a href="avascript:void(0);" onclick="ignore_member({{ $user->id }})"
                                           class="btn btn-block btn-primary text-left">
                                            <i class="las la-ban d-block la-2x"></i>
                                            Ignore
                                        </a>
                                    </div>
                                    <div class="col">
                                        @php
                                            $profile_reported = \App\Models\ReportedUser::where('user_id', $user->id)->where('reported_by',Auth::user()->id)->first();
                                            if(empty($profile_reported)){
                                                $report_onclick  = 1;
                                                $report_text     = 'Report';
                                            }
                                            else{
                                                $report_onclick  = 0;
                                                $report_text     ='Reported';
                                            }
                                        @endphp
                                        <a href="javascript:void(0);" id="report_a_id_{{ $user->id }}"
                                           @if($report_onclick == 1)
                                           onclick="report_member({{ $user->id }})"
                                           @endif
                                           class="btn btn-block btn-primary text-left">
                                            <i class="las la-info-circle d-block la-2x"></i>
                                            <span id="report_id_{{ $user->id }}">{{ $report_text }}
                                        </a>
                                    </div>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="col-xl-8 offset-xxl-1 custom-card">
                    <div class="overflow-hidden rounded shadow-lg mb-4 bg-white d-xl-none">
                        <img
                                src="{{ asset($user->image) }}"
                                onerror="this.onerror=null;this.src='{{ asset('assets/img/avatar-place.png') }}';"
                                class="img-fluid w-100"
                        >
                    </div>

                    <div class="mb-4 p-4 border rounded border-gray-200 d-xl-none">
                        <div class="fs-12">
                            <span class="opacity-60">Member ID: </span>
                            <span class="ml-1 text-primary">{{ $user->unique_id }}</span>
                        </div>

                        <h2 class="fs-20 fw-500 mb-4">{{ $user->first_name.' '.$user->last_name }}</h2>

                        <div class="row gutters-5">
                            <div class="col">
                                <a href="avascript:void(0);" onclick="ignore_member({{ $user->id }})"
                                   class="btn btn-block btn-primary text-left">
                                    <i class="las la-ban d-block la-2x"></i>
                                    Ignore
                                </a>
                            </div>
                            <div class="col">
                                @php
                                    $profile_reported = \App\Models\ReportedUser::where('user_id', $user->id)->where('reported_by',Auth::user()->id)->first();
                                    if(empty($profile_reported)){
                                        $report_onclick  = 1;
                                        $report_text     = 'Report';
                                    }
                                    else{
                                        $report_onclick  = 0;
                                        $report_text     = 'Reported';
                                    }
                                @endphp
                                <a href="avascript:void(0);" id="report_a_id_{{ $user->id }}"
                                   @if($report_onclick == 1)
                                   onclick="report_member({{ $user->id }})"
                                   @endif
                                   class="btn btn-block btn-primary text-left">
                                    <i class="las la-info-circle d-block la-2x"></i>
                                    <span id="report_id_{{ $user->id }}">{{ $report_text }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="profile">
                            <svg style="height: 0;width: 0;opacity: 0;visibility: hidden;">
                                <defs>
                                    <linearGradient id="primary-gradient" x1="0.5" x2="0.5" y2="1"
                                                    gradientUnits="objectBoundingBox">
                                        <stop offset="0" stop-color="#FD655B"/>
                                        <stop offset="1" stop-color="#FD655B"/>
                                    </linearGradient>
                                </defs>
                            </svg>
                            <div class="accordion aiz-timeline-accordion" id="profile-accordion">

                                <!-- about -->
                                <div class="pb-4 accordion-item">
                                    <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                                         data-toggle="collapse" data-target="#about" aria-expanded="true">
                                    <span
                                            class="size-50px border rounded-circle d-flex align-items-center justify-content-center">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="22.034" height="16.525"
                                             viewBox="0 0 22.034 16.525" class="fill-primary-grad">
                                            <path class="fill-dark"
                                                  d="M28.263,16.525V8.263H22.754a5.514,5.514,0,0,1,5.508-5.508V0A8.272,8.272,0,0,0,20,8.263v8.263Z"
                                                  transform="translate(-6.229)"/>
                                            <path fill="url(#primary-gradient)"
                                                  d="M8.263,16.525V8.263H2.754A5.514,5.514,0,0,1,8.263,2.754V0A8.272,8.272,0,0,0,0,8.263v8.263Z"/>
                                        </svg>
                                    </span>
                                        <div class="ml-4">
                                        <span
                                                class="fs-18 fw-600 d-block">About {{ $user->first_name.' '.$user->last_name }}</span>
                                            <span class="fs-11 fw-400">
                                            <span>Member ID:  {{ $user->unique_id }}</span>
                                            <span class="mx-2">|</span>
                                            <span>On Behalf: {{ $user->on_behalf }}</span>
                                        </span>
                                        </div>
                                    </div>
                                    @if(!empty($user->profile->tag_line))
                                        <div id="about" class="collapse show accordion-body ml-3 ml-md-5 pl-25px lh-1-8"
                                             data-parent="#profile-accordion">
                                            {{ $user->profile->tag_line }}
                                        </div>
                                    @endif
                                </div>


                                <!-- basic information -->
                                <div class="pb-4 accordion-item">
                                    <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                                         data-toggle="collapse" data-target="#basic-info" aria-expanded="true">
                                    <span
                                            class="size-50px border rounded-circle d-flex align-items-center justify-content-center">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="22.936" height="27.722"
                                             viewBox="0 0 22.936 27.722" class="fill-primary-grad">
                                            <g transform="translate(-40.061 0)">
                                                <path
                                                        d="M88.314,328.008h-4.3l-2.39,2.39-2.39-2.39h-4.3a2.868,2.868,0,0,0-2.868,2.868v5.258H91.181v-5.258A2.868,2.868,0,0,0,88.314,328.008Z"
                                                        transform="translate(-30.09 -308.411)" class="fill-dark"/>
                                                <path
                                                        d="M6.14,25.333H3.824a1.91,1.91,0,0,1-1.912-1.912v-.968a2.015,2.015,0,0,1-.929-.289,1.757,1.757,0,0,1-.589-.634,2.924,2.924,0,0,1-.308-.9A5.794,5.794,0,0,1,0,19.551a2.856,2.856,0,0,1,2.858-2.822h.488l-.079,0a1.434,1.434,0,0,1-1.355-1.509,1.488,1.488,0,0,1,1.482-1.357h.43V7.648c0-.121,0-.245.009-.37a7.611,7.611,0,0,1,1.433-4.1A7.674,7.674,0,0,1,8.59.562,7.6,7.6,0,0,1,11.465,0c.125,0,.251,0,.376.009a7.347,7.347,0,0,1,2.873.73,7.573,7.573,0,0,1,1.238.755,7.8,7.8,0,0,1,1.071.971,8,8,0,0,1,2.1,5.4v6h.43a1.488,1.488,0,0,1,1.482,1.357c0,.024,0,.05,0,.079A1.434,1.434,0,0,1,19.6,16.729h.423a2.934,2.934,0,0,1,2.867,2.305,3.028,3.028,0,0,1,.043.312c0,.048,0,.1,0,.148a2.04,2.04,0,0,1-2.041,2.015h-.031a2.873,2.873,0,0,0-2.7-1.912h-4.3V16.151a5.231,5.231,0,0,0,2.868-4.679V8.6h-.419a1.911,1.911,0,0,1-1.854-1.448L13.383,3.346,12.4,6.794A2.389,2.389,0,0,1,10.085,8.6H6.214v2.868a5.23,5.23,0,0,0,2.868,4.679V19.6H6.3a2.866,2.866,0,0,1,2.778,2.955,2.947,2.947,0,0,1-2.941,2.78Z"
                                                        transform="translate(40.061 0)" fill="url(#primary-gradient)"/>
                                            </g>
                                        </svg>
                                    </span>
                                        <div class="ml-4">
                                            <span
                                                    class="fs-18 fw-600 d-block">Basic Information</span>
                                        </div>
                                    </div>
                                    <div id="basic-info" class="accordion-body ml-3 ml-md-5 pl-25px collapse show"
                                         data-parent="#profile-accordion">
                                        <div class="border p-3">
                                            <div class="row no-gutters">
                                                <div class="col-sm-6">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <td class="py-1 fw-600">First Name</td>
                                                            <td class="py-1">{{ $user->first_name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Gender</td>
                                                            <td class="py-1">
                                                                {{$user->gender}}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Age</td>
                                                            <td class="py-1">
                                                                {{ !empty($user->date_of_birth) ? \Carbon\Carbon::parse($user->date_of_birth)->age : "" }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Gotra
                                                            </td>
                                                            <td class="py-1">
                                                                {{ !empty($user->profile->gotra) ? $user->profile->gotra : ""  }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Father Mobile No</td>
                                                            <td class="py-1">{{ $user->profile->father_no }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Mobile No</td>
                                                            <td class="py-1">{{ $user->mobile_no }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Alternate Contact No</td>
                                                            <td class="py-1">{{ $user->profile->contact_no }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Mother Mobile No</td>
                                                            <td class="py-1">{{ $user->profile->mother_no }}</td>
                                                        </tr>
                                                        @if(!empty($user->profile->self_intro_text))
                                                            <tr>
                                                                <td class="py-1 fw-600">Self Introduction
                                                                </td>
                                                                <td class="py-1">
                                                                    {{ $user->profile->self_intro_text}}
                                                                </td>
                                                            </tr>
                                                        @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-6 border-sm-left ">
                                                    <table class="w-100 ml-sm-4">
                                                        <tbody>
                                                        <tr>
                                                            <td class="py-1 fw-600">Last Name</td>
                                                            <td class="py-1">{{ $user->last_name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Height</td>
                                                            <td class="py-1">
                                                                {{ !empty($user->profile->height) ? $user->profile->height.'.'.$user->profile->height_inch : "" }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Date of Birth
                                                            </td>
                                                            <td class="py-1">
                                                                {{ !empty($user->date_of_birth) ? date('d/m/Y',strtotime($user->date_of_birth)) : "" }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Marital Status
                                                            </td>
                                                            <td class="py-1">
                                                                {{ !empty($user->profile->marital_status) ? $user->profile->marital_status : ""  }}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="py-1 fw-600">Mangalik
                                                            </td>
                                                            <td class="py-1">
                                                                {{ !empty($user->profile->mangalikName) ? $user->profile->mangalikName->name : ""  }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Birth Place
                                                            </td>
                                                            <td class="py-1">
                                                                {{ $user->profile->place_of_birth  }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-1 fw-600">Birth Time
                                                            </td>
                                                            <td class="py-1">
                                                                {{ $user->profile->time_of_birth  }}
                                                            </td>
                                                        </tr>

                                                        @if(!empty($user->profile->aadhar_photo))
                                                            <tr>
                                                                <td class="py-1 fw-600">Candidate or Father Aadhar Card
                                                                </td>
                                                                <td class="py-1">
                                                                    <a href=" {{ asset($user->profile->aadhar_photo)}}"
                                                                       class="btn btn-primary btn-sm"
                                                                       target="_blank">Front</a>
                                                                    <a href=" {{ asset($user->profile->aadhar_back_photo)}}"
                                                                       class="btn btn-primary btn-sm"
                                                                       target="_blank">Back</a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(!empty($user->profile->self_intro_video))
                                                            <tr>
                                                                <td class="py-1 fw-600">Self Introduction video
                                                                </td>
                                                                <td class="py-1">
                                                                    <a href=" {{ asset($user->profile->self_intro_video)}}"
                                                                       class="btn btn-primary btn-sm"
                                                                       target="_blank">Play</a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Present Address -->
                                <div class="pb-4 accordion-item">
                                    <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                                         data-toggle="collapse" data-target="#present-address">
                                    <span
                                            class="size-50px border rounded-circle d-flex align-items-center justify-content-center">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24.017" height="24.064"
                                             viewBox="0 0 24.017 24.064" class="fill-primary-grad">
                                            <g transform="translate(-1078.993 -1084.468)">
                                                <path
                                                        d="M5.629,9.617A3.992,3.992,0,0,0,9.617,13.6a.938.938,0,0,1,0,1.876A5.864,5.864,0,1,1,15.48,9.617a.938.938,0,0,1-1.876,0,3.987,3.987,0,1,0-7.975,0Zm.737,8.3C3.959,15.279,1.88,13,1.877,9.608a7.74,7.74,0,0,1,15.48,0,.938.938,0,0,0,.938.937h0a.938.938,0,0,0,.937-.939A9.616,9.616,0,0,0,2.821,2.813,9.555,9.555,0,0,0,0,9.607,10.047,10.047,0,0,0,1.588,14.97a28.549,28.549,0,0,0,3.393,4.21,32.029,32.029,0,0,1,3.561,4.4.938.938,0,1,0,1.586-1,33.476,33.476,0,0,0-3.762-4.664Zm17.405.45a.938.938,0,0,1-1.325.059l-.118-.107v3.1a2.645,2.645,0,0,1-2.641,2.643H14.416a2.645,2.645,0,0,1-2.641-2.643V18.36l-.071.064a.938.938,0,1,1-1.266-1.385l4.842-4.427a2.647,2.647,0,0,1,3.59,0l3.1,2.837a.938.938,0,0,1,.1.088l1.643,1.5a.938.938,0,0,1,.059,1.325ZM20.453,16.6,17.6,14a.78.78,0,0,0-1.058,0l-2.9,2.647v4.777a.767.767,0,0,0,.765.767h5.272a.767.767,0,0,0,.765-.767Zm0,0"
                                                        transform="translate(1078.992 1084.467)" class="fill-dark"/>
                                                <path
                                                        d="M5.629,9.617A3.992,3.992,0,0,0,9.617,13.6a.938.938,0,0,1,0,1.876A5.864,5.864,0,1,1,15.48,9.617a.938.938,0,0,1-1.876,0,3.987,3.987,0,1,0-7.975,0Zm.737,8.3C3.959,15.279,1.88,13,1.877,9.608a7.74,7.74,0,0,1,15.48,0,.938.938,0,0,0,.938.937h0a.938.938,0,0,0,.937-.939A9.616,9.616,0,0,0,2.821,2.813,9.555,9.555,0,0,0,0,9.607,10.047,10.047,0,0,0,1.588,14.97a28.549,28.549,0,0,0,3.393,4.21,32.029,32.029,0,0,1,3.561,4.4.938.938,0,1,0,1.586-1,33.476,33.476,0,0,0-3.762-4.664Z"
                                                        transform="translate(1078.992 1084.467)"
                                                        fill="url(#primary-gradient)"/>
                                            </g>
                                        </svg>
                                    </span>
                                        <div class="ml-4">
                                                <span
                                                        class="fs-18 fw-600 d-block">Present Address</span>
                                        </div>
                                    </div>
                                    <div id="present-address" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                                         data-parent="#profile-accordion">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="border p-3">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <td class="py-1 fw-600">
                                                                <i class="las text-primary mr-2 la-map-marker"></i>
                                                                <span>Country</span>
                                                            </td>
                                                            <td class="py-1">
                                                                {{ $user->is_nri==1?'NRI':"India" }}
                                                            </td>
                                                        </tr>
                                                        @if(!empty($user->state_id))
                                                            <tr>
                                                                <td class="py-1 fw-600">
                                                                    <i class="las text-primary mr-2 la-map-marker"></i>
                                                                    <span>State</span>
                                                                </td>
                                                                <td class="py-1">
                                                                    {{ !empty($user->state->name) ? $user->state->name : "" }}
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(!empty($user->city_id))
                                                            <tr>
                                                                <td class="py-1 fw-600">
                                                                    <i class="las text-primary mr-2 la-building"></i>
                                                                    <span>District</span>
                                                                </td>
                                                                <td class="py-1">
                                                                    {{ !empty($user->city->name) ? $user->city->name : "" }}
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(!empty($user->village))
                                                            <tr>
                                                                <td class="py-1 fw-600">
                                                                    <i class="las text-primary mr-2 la-building"></i>
                                                                    <span>City / Village</span>
                                                                </td>
                                                                <td class="py-1">
                                                                    {{ !empty($user->village) ? $user->village : "" }}
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(!empty($user->address))
                                                            <tr>
                                                                <td class="py-1 fw-600">
                                                                    <i class="las text-primary mr-2 la-building"></i>
                                                                    <span>Address</span>
                                                                </td>
                                                                <td class="py-1">
                                                                    {{ !empty($user->address) ? implode(PHP_EOL, str_split($user->address, 25)): "" }}
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        <tr>
                                                            <td class="py-1 fw-600">
                                                                <i class="las text-primary mr-2 la-envelope"></i>
                                                                <span>Postal Code</span>
                                                            </td>
                                                            <td class="py-1">
                                                                {{ !empty($user->pincode) ? $user->pincode : "" }}
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($user->hobbies->hobbies))
                                    <div class="pb-4 accordion-item">
                                        <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                                             data-toggle="collapse" data-target="#hobby">
                                    <span
                                            class="size-50px border rounded-circle d-flex align-items-center justify-content-center">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24.534" height="17.71"
                                             viewBox="0 0 24.534 17.71" class="fill-primary-grad">
                                            <g transform="translate(-1078.174 -2016.025)">
                                                <path
                                                        d="M3.674,3.457a1.455,1.455,0,0,1,2.2-1.25L18.3,9.6a1.454,1.454,0,0,1,0,2.5L5.873,19.5a1.455,1.455,0,0,1-2.2-1.25Z"
                                                        transform="translate(1074.5 2014.025)"
                                                        fill="url(#primary-gradient)"/>
                                                <path
                                                        d="M10.858,1.01A8.849,8.849,0,0,0,2,9.863v8.175a.708.708,0,0,0,.681.681H5.409a2.041,2.041,0,0,0,1.538-.644,2.157,2.157,0,0,0,.505-1.445V12.588a2.139,2.139,0,0,0-.482-1.365,1.82,1.82,0,0,0-1.391-.668H3.366V9.863a7.492,7.492,0,0,1,14.983,0v.692H16.136a1.82,1.82,0,0,0-1.391.668,2.139,2.139,0,0,0-.482,1.365v4.044a2.157,2.157,0,0,0,.505,1.445,2.041,2.041,0,0,0,1.538.644H19.03a.708.708,0,0,0,.681-.681V14.8q0-.023,0-.045v-4.9A8.849,8.849,0,0,0,10.858,1.01Z"
                                                        transform="translate(1082.996 2015.015)" class="fill-dark"/>
                                            </g>
                                        </svg>
                                    </span>
                                            <div class="ml-4">
                                                <span
                                                        class="fs-18 fw-600 d-block">Hobbies & Interest</span>
                                            </div>
                                        </div>
                                        <div id="hobby" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                                             data-parent="#profile-accordion">
                                            <div class="border p-3">
                                                <div class="row no-gutters">
                                                    <div class="col-md-12">
                                                        <table class="w-100">
                                                            <tr>
                                                                <th class="py-1">Hobbies</th>
                                                                <td class="py-1">{{ !empty($user->hobbies->hobbies) ? $user->hobbies->hobbies : "" }}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endif
                            <!-- education -->
                                <div class="pb-4 accordion-item">
                                    <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                                         data-toggle="collapse" data-target="#education-accordion">
                                    <span
                                            class="size-50px border rounded-circle d-flex align-items-center justify-content-center">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="22.034" height="16.525"
                                             viewBox="0 0 22.034 16.525" class="fill-primary-grad">
                                            <path class="fill-dark"
                                                  d="M28.263,16.525V8.263H22.754a5.514,5.514,0,0,1,5.508-5.508V0A8.272,8.272,0,0,0,20,8.263v8.263Z"
                                                  transform="translate(-6.229)"/>
                                            <path fill="url(#primary-gradient)"
                                                  d="M8.263,16.525V8.263H2.754A5.514,5.514,0,0,1,8.263,2.754V0A8.272,8.272,0,0,0,0,8.263v8.263Z"/>
                                        </svg>
                                    </span>
                                        <div class="ml-4">
                                                <span
                                                        class="fs-18 fw-600 d-block">Education and work</span>
                                        </div>
                                    </div>
                                    <div id="education-accordion" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                                         data-parent="#profile-accordion">
                                        <div class="border p-3">
                                            <div class="row no-gutters">
                                                <div class="col-md-6">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">Higher Education</th>
                                                            <td class="py-1">{{$user->info->higherEducation?$user->info->higherEducation->name:''}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Work Profile</th>
                                                            <td class="py-1">{{ !empty($user->info->workProfile) ? $user->info->workProfile->name : "" }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Permanent Address -->
                                <div class="pb-4 accordion-item">
                                    <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                                         data-toggle="collapse" data-target="#permanent-address">
                                    <span
                                            class="size-50px border rounded-circle d-flex align-items-center justify-content-center">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24.659" height="19.623"
                                             viewBox="0 0 24.659 19.623" class="fill-primary-grad">
                                            <g transform="translate(-1078.67 -2909.649)">
                                                <path
                                                        d="M111.028,264.3v7.342a.992.992,0,0,1-.979.979h-5.873V266.75h-3.915v5.873H94.387a.992.992,0,0,1-.979-.979V264.3a.209.209,0,0,1,.008-.046.209.209,0,0,0,.008-.046l8.795-7.25,8.795,7.25A.213.213,0,0,1,111.028,264.3Zm3.411-1.055-.948,1.132a.52.52,0,0,1-.321.168h-.046a.47.47,0,0,1-.321-.107l-10.584-8.825-10.584,8.825a.569.569,0,0,1-.367.107.52.52,0,0,1-.321-.168L90,263.248a.5.5,0,0,1-.107-.359.444.444,0,0,1,.168-.329l11-9.162a1.9,1.9,0,0,1,2.325,0l3.732,3.12v-2.983a.471.471,0,0,1,.489-.489h2.937a.471.471,0,0,1,.489.489v6.24l3.35,2.784a.444.444,0,0,1,.168.329A.5.5,0,0,1,114.439,263.248Z"
                                                        transform="translate(988.781 2656.649)"
                                                        fill="url(#primary-gradient)"/>
                                                <path
                                                        d="M114.439,263.248l-.948,1.132a.52.52,0,0,1-.321.168h-.046a.47.47,0,0,1-.321-.107l-10.584-8.825-10.584,8.825a.569.569,0,0,1-.367.107.52.52,0,0,1-.321-.168L90,263.248a.5.5,0,0,1-.107-.359.444.444,0,0,1,.168-.329l11-9.162a1.9,1.9,0,0,1,2.325,0l3.732,3.12v-2.983a.471.471,0,0,1,.489-.489h2.937a.471.471,0,0,1,.489.489v6.24l3.35,2.784a.444.444,0,0,1,.168.329A.5.5,0,0,1,114.439,263.248Z"
                                                        transform="translate(988.781 2656.649)" class="fill-dark"/>
                                            </g>
                                        </svg>
                                    </span>
                                        <div class="ml-4">
                                                <span
                                                        class="fs-18 fw-600 d-block">Native Address</span>
                                        </div>
                                    </div>
                                    <div id="permanent-address" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                                         data-parent="#profile-accordion">
                                        <div class="border p-3">
                                            <div class="row no-gutters">
                                                <div class="col-md-6">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">City / Village</th>
                                                            <td class="py-1">{{ !empty($user->info->native_village) ? $user->info->native_village : "" }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">District</th>
                                                            <td class="py-1">{{ !empty($user->info->city->name) ? $user->info->city->name : "" }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-6 border-sm-left ">
                                                    <table class="w-100 ml-sm-4">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">State</th>
                                                            <td class="py-1">{{!empty($user->info->state->name) ? $user->info->state->name : "" }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Postal Code</th>
                                                            <td class="py-1">{{ !empty($user->info->pincode) ? $user->info->pincode : "" }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Family Information -->
                                <div class="pb-4 accordion-item">
                                    <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                                         data-toggle="collapse" data-target="#family-info">
                                    <span
                                            class="size-50px border rounded-circle d-flex align-items-center justify-content-center">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="28.145" height="23.875"
                                             viewBox="0 0 28.145 23.875" class="fill-primary-grad">
                                            <g transform="translate(9.187 0)">
                                                <path
                                                        d="M-8182.819-3893.283a2.421,2.421,0,0,0,.14-.813v-8.226a5.652,5.652,0,0,0-.344-1.946h2.176a4.067,4.067,0,0,1,4.063,4.063v4.485a2.442,2.442,0,0,1-2.438,2.438Zm-19.671,0a2.441,2.441,0,0,1-2.437-2.438v-4.485a4.067,4.067,0,0,1,4.063-4.063h2.177a5.64,5.64,0,0,0-.344,1.946v8.226a2.47,2.47,0,0,0,.14.813Zm18.774-12.31a3.657,3.657,0,0,1-1.783-1.641,3.627,3.627,0,0,1-.431-1.715,3.659,3.659,0,0,1,3.655-3.655,3.658,3.658,0,0,1,3.653,3.655,3.657,3.657,0,0,1-3.653,3.653A3.617,3.617,0,0,1-8183.716-3905.592Zm-19.373-3.356a3.656,3.656,0,0,1,3.652-3.655,3.657,3.657,0,0,1,3.653,3.655,3.62,3.62,0,0,1-.429,1.715,3.649,3.649,0,0,1-1.785,1.641,3.607,3.607,0,0,1-1.439.3A3.656,3.656,0,0,1-8203.089-3908.949Z"
                                                        transform="translate(8195.741 3917.158)"
                                                        fill="url(#primary-gradient)"/>
                                                <path
                                                        d="M145.868,234.815h-4.976a4.067,4.067,0,0,0-4.063,4.063V247.1a.813.813,0,0,0,.813.813h11.477a.813.813,0,0,0,.813-.813v-8.226A4.067,4.067,0,0,0,145.868,234.815Z"
                                                        transform="translate(-138.494 -224.042)" class="fill-dark"/>
                                                <path
                                                        d="M172,38.84a4.885,4.885,0,1,0,4.886,4.886A4.892,4.892,0,0,0,172,38.84Z"
                                                        transform="translate(-167.114 -38.84)" class="fill-dark"/>
                                            </g>
                                        </svg>
                                    </span>
                                        <div class="ml-4">
                                                <span
                                                        class="fs-18 fw-600 d-block">Family and Contact Information</span>
                                        </div>
                                    </div>
                                    <div id="family-info" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                                         data-parent="#profile-accordion">
                                        <div class="border p-3">
                                            <div class="row no-gutters">
                                                <div class="col-md-6">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">Father</th>
                                                            <td class="py-1">{{ $user->profile->father_name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Father Occupation</th>
                                                            <td class="py-1">{{ $user->profile->father_profession }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Father Mobile No</th>
                                                            <td class="py-1">{{ $user->profile->father_no }}</td>
                                                        </tr>
                                                        @if(!empty($user->profile->mother_name))
                                                            <tr>
                                                                <th class="py-1">Mother</th>
                                                                <td class="py-1">{{ $user->profile->mother_name }}</td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6 ">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">Grand Father Name</th>
                                                            <td class="py-1">{{ $user->profile->grand_father_name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Nanaji Name</th>
                                                            <td class="py-1">{{ $user->profile->nanaji_name }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="border p-3">
                                            <div class="row no-gutters">
                                                <div class="col-md-6">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">Nanihal Address</th>
                                                            <td class="py-1">{{ $user->profile->nanaji_address }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Nanihal Village/City</th>
                                                            <td class="py-1">{{ $user->profile->nanaji_village }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Nanihal District</th>
                                                            <td class="py-1">{{ $user->profile->city?$user->profile->city->name:'' }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6 ">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">Nanihal State</th>
                                                            <td class="py-1">{{ $user->profile->state?$user->profile->state->name:''  }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Nanihal Counrty</th>
                                                            @if($user->profile->nanaji_is_nri==0)
                                                                <td class="py-1">{{ $user->profile->country?$user->profile->country->name:'' }}</td>
                                                            @else
                                                                <td class="py-1">NRI</td>
                                                            @endif
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="border p-3">
                                            <div class="row no-gutters">
                                                <div class="col-md-6 ">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">Brother Married</th>
                                                            <td class="py-1">{{ $user->profile->brother_married  }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Brother Unmarried</th>
                                                            <td class="py-1">{{ $user->profile->brother_unmarried }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6 ">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">Sister Married</th>
                                                            <td class="py-1">{{ $user->profile->sister_married  }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Sister Unmarried</th>
                                                            <td class="py-1">{{ $user->profile->sister_unmarried }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="pb-4 accordion-item">
                                    <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                                         data-toggle="collapse" data-target="#social-media">
                                    <span
                                            class="size-50px border rounded-circle d-flex align-items-center justify-content-center">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24.659" height="19.623"
                                             viewBox="0 0 24.659 19.623" class="fill-primary-grad">
                                            <g transform="translate(-1078.67 -2909.649)">
                                                <path
                                                        d="M111.028,264.3v7.342a.992.992,0,0,1-.979.979h-5.873V266.75h-3.915v5.873H94.387a.992.992,0,0,1-.979-.979V264.3a.209.209,0,0,1,.008-.046.209.209,0,0,0,.008-.046l8.795-7.25,8.795,7.25A.213.213,0,0,1,111.028,264.3Zm3.411-1.055-.948,1.132a.52.52,0,0,1-.321.168h-.046a.47.47,0,0,1-.321-.107l-10.584-8.825-10.584,8.825a.569.569,0,0,1-.367.107.52.52,0,0,1-.321-.168L90,263.248a.5.5,0,0,1-.107-.359.444.444,0,0,1,.168-.329l11-9.162a1.9,1.9,0,0,1,2.325,0l3.732,3.12v-2.983a.471.471,0,0,1,.489-.489h2.937a.471.471,0,0,1,.489.489v6.24l3.35,2.784a.444.444,0,0,1,.168.329A.5.5,0,0,1,114.439,263.248Z"
                                                        transform="translate(988.781 2656.649)"
                                                        fill="url(#primary-gradient)"/>
                                                <path
                                                        d="M114.439,263.248l-.948,1.132a.52.52,0,0,1-.321.168h-.046a.47.47,0,0,1-.321-.107l-10.584-8.825-10.584,8.825a.569.569,0,0,1-.367.107.52.52,0,0,1-.321-.168L90,263.248a.5.5,0,0,1-.107-.359.444.444,0,0,1,.168-.329l11-9.162a1.9,1.9,0,0,1,2.325,0l3.732,3.12v-2.983a.471.471,0,0,1,.489-.489h2.937a.471.471,0,0,1,.489.489v6.24l3.35,2.784a.444.444,0,0,1,.168.329A.5.5,0,0,1,114.439,263.248Z"
                                                        transform="translate(988.781 2656.649)" class="fill-dark"/>
                                            </g>
                                        </svg>
                                    </span>
                                        <div class="ml-4">
                                            <span class="fs-18 fw-600 d-block">Social Media </span>
                                        </div>
                                    </div>
                                    <div id="social-media" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                                         data-parent="#profile-accordion">
                                        <div class="border p-3">
                                            <div class="row no-gutters">
                                                <div class="col-md-6">
                                                    <table class="w-100">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">Facebook Profile</th>
                                                            <td class="py-1">{{ $user->profile->facebook_url }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Instagram Profile</th>
                                                            <td class="py-1">{{$user->profile->instagram_url }}</td>
                                                        </tr>

                                                        <tr>
                                                            <th class="py-1">Twitter Profile</th>
                                                            <td class="py-1">{{ $user->profile->twitter_url }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-6 border-sm-left ">
                                                    <table class="w-100 ml-sm-4">
                                                        <tbody>
                                                        <tr>
                                                            <th class="py-1">Linkedin Profile</th>
                                                            <td class="py-1">{{$user->profile->linkedin_url }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="py-1">Youtube Profile</th>
                                                            <td class="py-1">{{ $user->profile->youtube_url }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade  " id="gallery">
                            <h2 class="text-primary fs-24 mb-5 pb-4 border-bottom">Gallery</h2>
                            <div class="card-columns">
                                @if(!empty($user->profile->photo1))
                                    <div class="card hov-overlay">
                                        <img src="{{ asset($user->profile->photo1) }}" class="card-img"
                                             alt="photo1">
                                        <div class="overlay">
                                            <a target="_blank" href="{{ asset($user->profile->photo1) }}"
                                               class="text-white absolute-full d-flex justify-content-center align-items-center"
                                               title="View">
                                                <i class="las la-search-plus la-2x"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($user->profile->photo2))
                                    <div class="card hov-overlay">
                                        <img src="{{ asset($user->profile->photo2) }}" class="card-img"
                                             alt="photo1">
                                        <div class="overlay">
                                            <a target="_blank" href="{{ asset($user->profile->photo2) }}"
                                               class="text-white absolute-full d-flex justify-content-center align-items-center"
                                               title="View">
                                                <i class="las la-search-plus la-2x"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($user->profile->photo3))
                                    <div class="card hov-overlay">
                                        <img src="{{ asset($user->profile->photo3) }}" class="card-img"
                                             alt="photo1">
                                        <div class="overlay">
                                            <a target="_blank" href="{{ asset($user->profile->photo3) }}"
                                               class="text-white absolute-full d-flex justify-content-center align-items-center"
                                               title="View">
                                                <i class="las la-search-plus la-2x"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($user->profile->photo4))
                                    <div class="card hov-overlay">
                                        <img src="{{ asset($user->profile->photo4) }}" class="card-img"
                                             alt="photo1">
                                        <div class="overlay">
                                            <a target="_blank" href="{{ asset($user->profile->photo4) }}"
                                               class="text-white absolute-full d-flex justify-content-center align-items-center"
                                               title="View">
                                                <i class="las la-search-plus la-2x"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('modal')
    @include('modals.confirm_modal')
    <!-- Ignore Modal -->
    <div class="modal fade ignore_member_modal" id="modal-zoom">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title h6">Ignore Member!</h5>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to ignore this member?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary"
                            id="ignore_button">Ignore
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Report Profile -->
    <div class="modal fade report_modal" id="modal-zoom">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title h6">Report Member!</h5>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('reportUsers') }}" id="report-modal-form" method="POST">
                        @csrf
                        <input type="hidden" name="member_id" id="member_id" value="">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Report Reason<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <textarea name="reason" rows="4" class="form-control"
                                          placeholder="Report Reason" required></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                            data-dismiss="modal">Cancel
                    </button>
                    <button type="button" class="btn btn-primary"
                            onclick="submitReport()"
                    >Report
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @foreach (session('flash_notification', collect())->toArray() as $message)
        <script>
            @if($message['message']=='thank_you')
            successToast('Thank you for submitting your profile After submission your profile, your profile will be approved by admin within 7 working days.Once it approved by our admin, your profile will be listed Note:Your profile will be valid for 2 years only.', '{{ $message['level'] }}');
            @else
            successToast('{{ $message['message'] }}', '{{ $message['level'] }}');
            @endif
        </script>
    @endforeach
    <script type="text/javascript">

        function do_contact_view(id) {
            $(".view_contact").removeAttr("onclick");
            $.post('{{ route('view_contacts') }}',
                {
                    _token: '{{ csrf_token() }}',
                    id: id
                },
                function (data) {
                    if (data == 1) {
                        $('.confirm_modal').modal('toggle');
                        $('.contact_info').removeClass('d-none');
                        $('.view_contact').addClass('d-none');
                        AIZ.plugins.notify('success', 'Now You Can See This Members Contact Information');
                    } else {
                        AIZ.plugins.notify('danger', 'Something went wrong');
                    }
                    location.reload();
                }
            );
        }

        // Ignore
        function ignore_member(id) {
            $('.ignore_member_modal').modal('show');
            $("#ignore_button").attr("onclick", "do_ignore(" + id + ")");
        }

        function do_ignore(id) {
            $.post('{{ route('add_to_ignore_list') }}',
                {
                    _token: '{{ csrf_token() }}',
                    id: id
                },
                function (data) {
                    if (data == 1) {
                        $("#block_id_" + id).hide();
                        $('.ignore_member_modal').modal('toggle');
                        AIZ.plugins.notify('success', 'You Have Ignored This Member.');
                        window.location.href = "{{ route('memberListing')}}";
                    } else {
                        AIZ.plugins.notify('danger', 'Something went wrong');
                    }
                }
            );
        }

        function report_member(id) {
            $('.report_modal').modal('show');
            $('#member_id').val(id);
        }

        function submitReport() {
            $('#report-modal-form').submit();
        }
    </script>
@endsection
