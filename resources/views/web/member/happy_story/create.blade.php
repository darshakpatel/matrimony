<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{trans('messages.Add Your Story')}}</h5>
    </div>
    <div class="card-body">
      <form action="{{ route('happy-story.store') }}" method="POST">
          @csrf
          <div class="form-group ">
              <label class="form-label" for="name">{{trans('messages.Story Title')}} <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="title"  placeholder="{{trans('messages.Title')}}" required>
              @error('title')
                  <small class="form-text text-danger">{{ $message }}</small>
              @enderror
          </div>
          <div class="form-group">
              <label class="from-label" for="name">{{trans('messages.Story Details')}} <span class="text-danger">*</span></label>
              <textarea name="details" class="aiz-text-editor form-control" data-buttons='[["font", ["bold", "underline", "italic"]],["para", ["ul", "ol"]],["view", ["undo","redo"]]]' placeholder="Type.." data-min-height="200"></textarea>
              @error('details')
                  <small class="form-text text-danger">{{ $message }}</small>
              @enderror
          </div>
          <div class="form-group">
              <label class="from-label" for="name">{{trans('messages.Partner Name')}} <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="partner_name"  placeholder="{{trans('messages.Partner Name')}}" required>
              @error('partner_name')
                  <small class="form-text text-danger">{{ $message }}</small>
              @enderror
          </div>
          <div class="form-group">
              <label class="form-label" for="signinSrEmail">{{trans('messages.Photos')}} <span class="text-danger">*</span></label>
              <div class="input-group" data-toggle="aizuploader" data-type="image" data-multiple="true">
                  <div class="input-group-prepend">
                      <div class="input-group-text bg-soft-secondary font-weight-medium">{{ trans('messages.Browse')}}</div>
                  </div>
                  <div class="form-control file-amount">{{ trans('messages.Choose File') }}</div>
                  <input type="hidden" name="photos" class="selected-files" required>
              </div>
              <div class="file-preview box sm">
              </div>
              @error('photos')
                  <small class="form-text text-danger">{{ $message }}</small>
              @enderror
          </div>
          <div class="form-group ">
    					<label class="from-label">{{trans('messages.Video Provider')}}</label>
  						<select class="form-control aiz-selectpicker" name="video_provider" id="video_provider">
  							<option value="youtube">{{trans('messages.Youtube')}}</option>
  							<option value="dailymotion">{{trans('messages.Dailymotion')}}</option>
  							<option value="vimeo">{{trans('messages.Vimeo')}}</option>
  						</select>
  				</div>
  				<div class="form-group ">
  					<label class="from-label">{{trans('messages.Video Link')}}</label>
  						<input type="text" class="form-control" name="video_link" placeholder="{{ trans('messages.Video Link') }}">
              <small class="text-muted">{{translate("Use proper link without extra parameter. Don't use short share link/embeded iframe code.")}}</small>
  				</div>
          <div class="form-group mb-0 text-right">
              <button type="submit" class="btn btn-primary">{{trans('messages.Save')}}</button>
          </div>
        </from>
    </div>
</div>
