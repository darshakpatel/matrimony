<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">Hobbies</h5>
    </div>
    <div class="card-body">
        <form class="update-form" data-update-type="hobby_update" action="{{ route('hobbies-update') }}" method="POST">

            @csrf
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="hobbies">Hobbies</label>
                    <textarea name="hobbies"
                           class="form-control" placeholder="Hobbies">{{ !empty($member->hobbies->hobbies) ? $member->hobbies->hobbies : "" }}</textarea>
                </div>

            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary btn-sm">Update</button>
            </div>
        </form>
    </div>
</div>
