<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{trans('messages.Spiritual & Social Background')}}</h5>
    </div>
    <div class="card-body">
      <form action="{{ route('spiritual_backgrounds.update', $member->id) }}" method="POST">
          <input name="_method" type="hidden" value="PATCH">
          @csrf
          <input type="hidden" name="address_type" value="present">
          <div class="form-group row">
              <div class="col-md-6">
                  <label for="member_religion_id">{{trans('messages.Religion')}}</label>
                  <select class="form-control aiz-selectpicker" name="member_religion_id" id="member_religion_id" data-live-search="true" required>
                      <option value="">{{trans('messages.Select One')}}</option>
                      @foreach ($religions as $religion)
                          <option value="{{$religion->id}}" @if($religion->id == $member_religion_id) selected @endif> {{ $religion->name }} </option>
                      @endforeach
                  </select>
                  @error('member_religion_id')
                      <small class="form-text text-danger">{{ $message }}</small>
                  @enderror
              </div>
              <div class="col-md-6">
                  <label for="member_caste_id">{{trans('messages.Caste')}}</label>
                  <select class="form-control aiz-selectpicker" name="member_caste_id" id="member_caste_id" data-live-search="true" required>

                  </select>
                  @error('member_caste_id')
                      <small class="form-text text-danger">{{ $message }}</small>
                  @enderror
              </div>
          </div>
          <div class="form-group row">
              <div class="col-md-6">
                  <label for="member_sub_caste_id">{{trans('messages.Sub Caste')}}</label>
                  <select class="form-control aiz-selectpicker" name="member_sub_caste_id" id="member_sub_caste_id" data-live-search="true">

                  </select>
              </div>
              <div class="col-md-6">
                  <label for="ethnicity">{{trans('messages.Ethnicity')}}</label>
                  <input type="text" name="ethnicity" value="{{!empty($member->spiritual_backgrounds->ethnicity) ? $member->spiritual_backgrounds->ethnicity : "" }}" class="form-control" placeholder="{{trans('messages.Ethnicity')}}">
                  @error('ethnicity')
                      <small class="form-text text-danger">{{ $message }}</small>
                  @enderror
              </div>
          </div>
          <div class="form-group row">
              <div class="col-md-6">
                  <label for="personal_value">{{trans('messages.Personal Value')}}</label>
                  <input type="text" name="personal_value" value="{{!empty($member->spiritual_backgrounds->personal_value) ? $member->spiritual_backgrounds->personal_value : "" }}" class="form-control" placeholder="{{trans('messages.Personal Value')}}">
                  @error('personal_value')
                      <small class="form-text text-danger">{{ $message }}</small>
                  @enderror
              </div>
              <div class="col-md-6">
                  <label for="family_value_id">{{trans('messages.Family Value')}}</label>
                  <select class="form-control aiz-selectpicker" name="family_value_id" data-live-search="true">
                      <option value="">{{trans('messages.Select One')}}</option>
                      @foreach ($family_values as $family_value)
                          <option value="{{$family_value->id}}" @if($religion->id == !empty($member->spiritual_backgrounds->ethnicity) ? $member->spiritual_backgrounds->ethnicity : "" ) selected @endif> {{ $family_value->name }}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          <div class="form-group row">
              <div class="col-md-6">
                  <label for="community_value">{{trans('messages.Community Value')}}</label>
                  <input type="text" name="community_value" value="{{!empty($member->spiritual_backgrounds->community_value) ? $member->spiritual_backgrounds->community_value : "" }}" class="form-control" placeholder="{{trans('messages.Community Value')}}">
                  @error('community_value')
                      <small class="form-text text-danger">{{ $message }}</small>
                  @enderror
              </div>
          </div>
          <div class="text-right">
              <button type="submit" class="btn btn-primary btn-sm">{{trans('messages.Update')}}</button>
          </div>
      </form>
    </div>
</div>
