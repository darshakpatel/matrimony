<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{trans('messages.Personal Attitude & Behavior')}}</h5>
    </div>
    <div class="card-body">
      <form action="{{ route('attitudes.update', $member->id) }}" method="POST">
          <input name="_method" type="hidden" value="PATCH">
          @csrf
          <div class="form-group row">
              <div class="col-md-6">
                  <label for="affection">{{trans('messages.Affection')}}</label>
                  <input type="text" name="affection" value="{{ !empty($member->attitude->affection) ? $member->attitude->affection : "" }}" class="form-control" placeholder="{{trans('messages.Affection')}}">
                  @error('affection')
                      <small class="form-text text-danger">{{ $message }}</small>
                  @enderror
              </div>
              <div class="col-md-6">
                  <label for="humor">{{trans('messages.Humor')}}</label>
                  <input type="text" name="humor" value="{{ !empty($member->attitude->humor) ? $member->attitude->humor : "" }}" placeholder="{{ trans('messages.Humor') }}" class="form-control">
                  @error('humor')
                      <small class="form-text text-danger">{{ $message }}</small>
                  @enderror
              </div>
          </div>
          <div class="form-group row">
              <div class="col-md-6">
                  <label for="political_views">{{trans('messages.Political Views')}}</label>
                  <input type="text" name="political_views" value="{{ !empty($member->attitude->political_views) ? $member->attitude->political_views : "" }}" class="form-control" placeholder="{{trans('messages.Political Views')}}">
              </div>
              <div class="col-md-6">
                  <label for="religious_service">{{trans('messages.Religious Service')}}</label>
                  <input type="text" name="religious_service" value="{{ !empty($member->attitude->religious_service) ? $member->attitude->religious_service : "" }}" placeholder="{{ trans('messages.Religious Service') }}" class="form-control">
              </div>
          </div>

          <div class="text-right">
              <button type="submit" class="btn btn-primary btn-sm">{{trans('messages.Update')}}</button>
          </div>
      </form>
    </div>
</div>
