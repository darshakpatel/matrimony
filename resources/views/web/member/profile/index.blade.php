@extends('web.layouts.member_panel')
@section('panel_content')
    <style>
        .card {
            background: lightgrey;
        }
    </style>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css"
          integrity=""/>
    <!-- Basic Information -->

    @if($member->is_profile_updated==0)
        <div class="alert alert-danger" role="alert">
            <h6>
                Step 1) Kindly complete your profile.<br/>
                Step 2) Submit your profile for approval.<br/>
                Step 3) Your profile will be listed only after approval by our admin team within 7 working days.<br/>
                Step 4) Once it approved, your profile will be listed, and all the function will be available for
                you.<br/>
                <br/>
                Note: Your profile will be valid for 2 years only.
            </h6>
        </div>
    @endif
    @if($member->is_profile_updated==1 && $member->is_approved==0)
        <div class="alert alert-danger" role="alert">
            <h6>
                Step 1) Kindly complete your profile.<br/>
                Step 2) Submit your profile for approval.<br/>
                Step 3) Your profile will be listed only after approval by our admin team within 7 working days.<br/>
                Step 4) Once it approved, your profile will be listed, and all the function will be available for
                you.<br/>
                <br/>
                Note: Your profile will be valid for 2 years only.
            </h6>
        </div>
    @endif
    <div class="accordion aiz-timeline-accordion" id="profile-accordion">
        <!-- basic information -->
        <div class="pb-4 accordion-item">
            <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                 data-toggle="collapse" data-target="#basic-info" aria-expanded="true">
                <div class="ml-4">
                    <span class="fs-18 fw-600 d-block">Basic Information
                        @if($member->is_basic_info==1)
                            <span style="color:green;font-weight: bold" id="basic_info_update">(Completed) </span>
                        @else
                            <span style="color:red;font-weight: bold" id="basic_info_update">(Pending) </span>
                        @endif
                    </span>
                </div>
            </div>
            <div id="basic-info" class="accordion-body ml-3 ml-md-5 pl-25px collapse show"
                 data-parent="#profile-accordion">
                @include('web.member.profile.basic_info')
            </div>
        </div>

        <div class="pb-4 accordion-item basic_info" data-scroll="basic_info">
            <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                 data-toggle="collapse" data-target="#photos">
                <div class="ml-4">
                    <span class="fs-18 fw-600 d-block">Your Photos
                     @if($member->is_gallery_photo==1)
                            <span style="color:green;font-weight: bold" id="gallery_photo_update">(Completed) </span>
                        @else
                            <span style="color:red;font-weight: bold" id="gallery_photo_update">(Pending) </span>
                        @endif
                    </span>
                </div>
            </div>
            <div id="photos" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                 data-parent="#profile-accordion">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0 h6">Your Photos</h5>
                    </div>
                    <div class="card-body">
                        <form class="update-form" data-update-type="photos_update"
                              action="{{ route('photos-update', $member->id) }}"
                              method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="photo1">Photo 1 <small>(800x800
                                            Recommended)</small></label>
                                    <input type="file" name="photo1" class="dropify"
                                           data-max-file-size="4M"
                                           data-allowed-file-extensions="jpg png jpeg"
                                           data-default-file="{{ $member->profile->photo1 }}">
                                </div>
                                <div class="col-md-6">
                                    <label for="photo2">Photo 2 <small>(800x800
                                            Recommended)</small></label>
                                    <input type="file" name="photo2" class="dropify"
                                           data-max-file-size="4M"
                                           data-allowed-file-extensions="jpg png jpeg"
                                           data-default-file="{{ $member->profile->photo2 }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="photo3">Photo 3 <small>(800x800
                                            Recommended)</small></label>
                                    <input type="file" name="photo3" class="dropify"
                                           data-max-file-size="4M"
                                           data-allowed-file-extensions="jpg png jpeg"
                                           data-default-file="{{ $member->profile->photo3 }}">
                                </div>
                                <div class="col-md-6">
                                    <label for="photo4">Photo 4 <small>(800x800
                                            Recommended)</small></label>
                                    <input type="file" name="photo4" class="dropify"
                                           data-max-file-size="4M"
                                           data-allowed-file-extensions="jpg png jpeg"
                                           data-default-file="{{ $member->profile->photo4 }}">
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary btn-sm">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="pb-4 accordion-item present_address" data-scroll="present_address">
            <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                 data-toggle="collapse" data-target="#present_address">
                <div class="ml-4">
                    <span class="fs-18 fw-600 d-block">Present Address
                     @if($member->is_present_address==1)
                            <span style="color:green;font-weight: bold" id="present_address_update">(Completed) </span>
                        @else
                            <span style="color:red;font-weight: bold" id="present_address_update">(Pending) </span>
                        @endif
                    </span>
                </div>
            </div>
            <div id="present_address" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                 data-parent="#profile-accordion">
                <div class="border p-3">
                    @include('web.member.profile.present_address')
                </div>
            </div>
        </div>
        <!-- education -->

        <div class="pb-4 accordion-item" data-scroll="education">
            <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                 data-toggle="collapse" data-target="#education-accordion">
                <div class="ml-4">
                    <span class="fs-18 fw-600 d-block">Education and work
                     @if($member->is_education==1)
                            <span style="color:green;font-weight: bold" id="education_update">(Completed) </span>
                        @else
                            <span style="color:red;font-weight: bold"
                                  id="education_update">(Pending) </span>
                        @endif
                    </span>
                </div>
            </div>
            <div id="education-accordion" class="education collapse accordion-body ml-3 ml-md-5 pl-25px"
                 data-parent="#profile-accordion">
                <div class="border p-3">
                    @include('web.member.profile.education.index')
                </div>
            </div>
        </div>

        <div class="pb-4 accordion-item">
            <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                 data-toggle="collapse" data-target="#hobby">
                <div class="ml-4">
                    <span class="fs-18 fw-600 d-block">Hobby
                    @if($member->is_hobby==1)
                            <span style="color:green;font-weight: bold"
                                  id="hobby_update">(Completed) </span>
                        @else
                            <span style="color:red;font-weight: bold"
                                  id="hobby_update">(Pending) </span>
                        @endif
                    </span>
                </div>
            </div>
            <div id="hobby" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                 data-parent="#profile-accordion">
                <div class="border p-3">
                    @include('web.member.profile.hobbies_interest')
                </div>
            </div>
        </div>

        <div class="pb-4 accordion-item">
            <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                 data-toggle="collapse" data-target="#perminent-address">
                <div class="ml-4">
                    <span class="fs-18 fw-600 d-block">Permanent Address
                        @if($member->is_native_address==1)
                            <span style="color:green;font-weight: bold"
                                  id="native_update">(Completed) </span>
                        @else
                            <span style="color:red;font-weight: bold"
                                  id="native_update">(Pending) </span>
                        @endif
                    </span>
                </div>
            </div>
            <div id="perminent-address" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                 data-parent="#profile-accordion">
                <div class="border p-3">
                    @include('web.member.profile.permanent_address')
                </div>
            </div>
        </div>

        <div class="pb-4 accordion-item">
            <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                 data-toggle="collapse" data-target="#family-info">
                <div class="ml-4">
                    <span class="fs-18 fw-600 d-block">Family Information
                    @if($member->is_family_info==1)
                            <span style="color:green;font-weight: bold" id="family_info_update">(Completed) </span>
                        @else
                            <span style="color:red;font-weight: bold"
                                  id="family_info_update">(Pending) </span>
                        @endif
                    </span>
                </div>
            </div>
            <div id="family-info" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                 data-parent="#profile-accordion">
                <div class="border p-3">
                    @include('web.member.profile.family_information')
                </div>
            </div>
        </div>


        <div class="pb-4 accordion-item">
            <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                 data-toggle="collapse" data-target="#introduction">
                <div class="ml-4">
                    <span class="fs-18 fw-600 d-block">Self Introduction
                      @if($member->is_self_intro==1)
                            <span style="color:green;font-weight: bold" id="self_intro_update">(Completed) </span>
                        @else
                            <span style="color:red;font-weight: bold"
                                  id="self_intro_update">(Pending) </span>
                        @endif
                    </span>
                </div>
            </div>
            <div id="introduction" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                 data-parent="#profile-accordion">
                <div class="border p-3">
                    <div class="card">

                        <div class="card-header">
                            <h5 class="mb-0 h6">Introduction</h5>
                        </div>
                        <div class="card-body">
                            <form class="update-form" data-update-type="self_info_update"
                                  action="{{ route('introduction-update', $member->id) }}"
                                  method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Self Introduction</label>
                                    <div class="col-md-10">
                        <textarea type="text"
                                  name="self_info_text" class="form-control" rows="4"
                                  placeholder="Introduction"
                        >{{ $member->profile->self_intro_text }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class=" @if(!empty($member->profile->self_intro_video)) col-md-6 @else col-md-12 @endif">
                                        <label for="self_intro_video">Self Introduction Video (Maximum
                                            30MB
                                            allowed MP4 File)</label>
                                        <input type="file" name="self_intro_video" id="self_intro_video"
                                               onchange="Filevalidation()"
                                               class="form-control"
                                        >
                                    </div>

                                    @if(!empty($member->profile->self_intro_video))
                                        <div class="col-md-6" id="remove_video">
                                            <div class="file-preview box sm">
                                                <video width="250" height="180" controls>
                                                    <source src="{{$member->profile->self_intro_video}}"
                                                            type="video/mp4">
                                                </video>
                                            </div>
                                            <button type="button"
                                                    class="remove_video btn-sm mt-1 btn btn-info">
                                                Remove
                                            </button>
                                        </div>
                                    @endif
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="pb-4 accordion-item">
            <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                 data-toggle="collapse" data-target="#social_media">

                <div class="ml-4">
                    <span class="fs-18 fw-600 d-block">Social Media Info
                     @if($member->is_social_media==1)
                            <span style="color:green;font-weight: bold" id="social_media_update">(Completed) </span>
                        @else
                            <span style="color:red;font-weight: bold"
                                  id="social_media_update">(Pending) </span>
                        @endif
                    </span>
                </div>
            </div>
            <div id="social_media" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                 data-parent="#profile-accordion">
                <div class="border p-3">
                    <div class="card">

                        <div class="card-header">
                            <h5 class="mb-0 h6">Social Media Info</h5>
                        </div>
                        <div class="card-body">
                            <form class="update-form" action="{{ route('socialMediaUpdate') }}"
                                  data-update-type="social_media_update"
                                  method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="facebook_url">Facebook Profile</label>
                                        <input type="text" name="facebook_url" class="form-control"
                                               placeholder="Facebook Profile"
                                               value="{{ $member->profile->facebook_url }}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="linkedin_url">Linkedin Profile</label>
                                        <input type="text" name="linkedin_url" class="form-control"
                                               placeholder="Linkedin Profile"
                                               value="{{ $member->profile->linkedin_url }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="twitter_url">Twitter Profile</label>
                                        <input type="text" name="twitter_url" class="form-control"
                                               placeholder="Twitter Profile"
                                               value="{{ $member->profile->twitter_url }}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="photo1">Instagram Profile</label>
                                        <input type="text" name="instagram_url" class="form-control"
                                               placeholder="Instagram Profile"
                                               value="{{ $member->profile->instagram_url }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label for="youtube_url">Youtube Profile</label>
                                        <input type="text" name="youtube_url" class="form-control"
                                               placeholder="Youtube Profile"
                                               value="{{ $member->profile->youtube_url }}">
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="pb-4 accordion-item">
            <div class="accordion-head c-pointer d-flex align-items-center mb-4"
                 data-toggle="collapse" data-target="#submit_profile">
                <div class="ml-4">
                    <span class="fs-18 fw-600 d-block">Submit your Profile</span>
                </div>
            </div>
            <div id="submit_profile" class="collapse accordion-body ml-3 ml-md-5 pl-25px"
                 data-parent="#profile-accordion">
                <div class="border p-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0 text-center h6">Submit your Profile for Approval</h5>
                        </div>
                        <div class="card-body">
                            <form class="approve-form" action="{{ route('approve-status-update') }}"
                                  method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                @if($member->is_profile_updated==1 && $member->is_approved==0)
                                    <div class="text-center">
                                        <button type="button" class="btn btn-primary btn-sm">Waiting for
                                            Admin
                                            Approval
                                        </button>
                                    </div>
                                @elseif($member->is_profile_updated==1 && $member->is_approved==1)
                                    <div class="text-center">
                                        <button type="button" class=" btn btn-primary btn-sm">Your
                                            Profile is
                                            Successfully Approved
                                        </button>
                                    </div>
                                @else
                                    <div class="text-center">
                                        <button type="button"
                                                class="submit-for-approve btn btn-primary btn-sm">
                                            Submit
                                        </button>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @include('modals.create_edit_modal')
    @include('modals.delete_modal')
    @include('modals.confirm_modal')
@endsection

@section('script')

    <script>
        // $(document).on('change', '#native_country', function () {
        //     var country_id = $(this).val();
        //     axios
        //         .get(APP_URL + '/getState/' + country_id)
        //         .then(function (response) {
        //             $("#native_state").html(response.data.data)
        //         })
        //         .catch(function (error) {
        //             console.log(error.response.data.message)
        //         });
        // })

        $(document).on('click', '.remove_video', function () {
            axios
                .get(APP_URL + '/remove_video')
                .then(function (response) {
                    $('#remove_video').remove()
                })
                .catch(function (error) {
                    console.log(error.response.data.message)
                })
        })
        $(document).on('change', '#native_state', function () {
            var state_id = $(this).val()
            axios
                .get(APP_URL + '/getCity/' + state_id)
                .then(function (response) {
                    $('#native_city').html(response.data.data)
                })
                .catch(function (error) {
                    console.log(error.response.data.message)
                })
        })
    </script>
    <script>
        $(document).on('change', '#nanaji_country', function () {
            var country_id = $(this).val()
            axios
                .get(APP_URL + '/getState/' + country_id)
                .then(function (response) {
                    $('#nanaji_state').html(response.data.data)
                })
                .catch(function (error) {
                    console.log(error.response.data.message)
                })
        })
        $(document).on('change', '#nanaji_state', function () {
            var state_id = $(this).val()
            axios
                .get(APP_URL + '/getCity/' + state_id)
                .then(function (response) {
                    $('#nanaji_city').html(response.data.data)
                })
                .catch(function (error) {
                    console.log(error.response.data.message)
                })
        })
        Filevalidation = () => {
            const fi = document.getElementById('self_intro_video')
            var filePath = fi.value
            var allowedExtensions = /(\.mp4)$/i
            if (!allowedExtensions.exec(filePath)) {
                alert('Invalid file type')
                $('#self_intro_video').val('')
                return false
            }
            // Check if any file is selected.
            if (fi.files.length > 0) {
                for (let i = 0; i <= fi.files.length - 1; i++) {

                    const fsize = fi.files.item(i).size
                    const file = Math.round((fsize / 1024))
                    // The size of the file.
                    if (file >= 30000) {
                        alert('File too Big, please select a file less than 30mb')
                        $('#self_intro_video').val('')
                    } else {
                        document.getElementById('self_intro_video').innerHTML = '<b>'
                            + file + '</b> KB'
                    }
                }
            }
        }

        function isValidFileType(fName, fType) {
            return extensionLists[fType].indexOf(fName.split('.').pop()) > -1
        }

        $('.submit-for-approve').on('click', function () {
            axios
                .get('/checkProfile')
                .then(function (response) {
                    if (response.data.is_pending == 1) {
                        $('.confirm_modal').modal('show')
                        $('#confirm_modal_title').html('Are you sure want to submit profile!')
                        $('#confirm_modal_content').html('<p class=\'fs-14\'>Kindly complete all your mandatory detail first and then submit for approval</small>')
                        $('#confirm_button').attr('onclick', 'submitForm()')
                    } else {
                        $('.confirm_modal').modal('show')
                        $('#confirm_modal_title').html('Are you sure want to submit profile!')
                        $('#confirm_modal_content').html('<p class=\'fs-14\'>Are you sure want to submit your profile for approval?</small>')
                        $('#confirm_button').attr('onclick', 'submitForm()')
                    }
                })
                .catch(function (error) {
                    console.log(error.response.data.message)
                });
        })

        function submitForm() {
            loaderView()
            setTimeout(function () {
                loaderHide()
                $('.approve-form').submit()
            }, 1000)
        }
    </script>
    <script src="{{asset('user/assets/js/custom/profileForm.js')}}?v={{time()}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
    <script>

        $('.dropify').dropify()
        @if($member->is_profile_updated==1 && $member->is_approved==0)
        $('input').attr('disabled', true).addClass('bg-light')
        $('select').attr('disabled', true).addClass('bg-light')
        $('textarea').attr('disabled', true).addClass('bg-light')
        $('button').attr('disabled', true)
        $('.status_btn').attr('disabled', false)
        $('img').attr('disabled', true).attr('data-show-remove', false)
        @endif
        $('#time_of_birth').timepicker();
    </script>

    @foreach (session('flash_notification', collect())->toArray() as $message)
        <script>
            @if($message['message']=='thank_you')
            successToast('Thank you for submitting your profile After submission your profile, your profile will be approved by admin within 7 working days.Once it approved by our admin, your profile will be listed Note:Your profile will be valid for 2 years only.', '{{ $message['level'] }}')
            @elseif($message['message'] == 'reactivation_sent')
            // successToast('Your request sent for reactivation.', 'success')
            @else
            successToast('{{ $message['message'] }}', '{{ $message['level'] }}')
            @endif
        </script>
    @endforeach
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @foreach (session('flash_notification', collect())->toArray() as $message)
        <script>
            @if($message['message']=='thank_you')
            Swal.fire(
                'Request Sent',
                'Please check email in INBOX and SPAM folder as well',
                'success'
            )
            @endif

            @if($message['message']=='reactivation_sent')
            Swal.fire(
                'Request Sent',
                'Your request sent for reactivation.',
                'success'
            )
            @endif

            $('#profile-accordion').on('shown.bs.collapse', function () {
                var panel = $(this).find('.in');
                $('html, body').animate({
                    scrollTop: panel.offset().top
                }, 500);

            });

        </script>
    @endforeach

@endsection
