<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">Residency Information</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('recidencies-update') }}" method="POST">
            <input name="_method" type="hidden" value="PATCH">
            @csrf
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="birth_country_id">Native Country</label>
                    <select class="form-control aiz-selectpicker" name="native_country" data-live-search="true">
                        @foreach ($countries as $country)
                            <option value="{{$country->id}}"
                                    @if($country->id == $member->info->native_country) selected @endif >{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="recidency_country_id">{{trans('messages.Recidency Country')}}</label>
                    <select class="form-control aiz-selectpicker" name="recidency_country_id" data-live-search="true">
                        @foreach ($countries as $country)
                            <option value="{{$country->id}}"
                                    @if($country->id == $recidency_country_id) selected @endif >{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary btn-sm">{{trans('messages.Update')}}</button>
            </div>
        </form>
    </div>
</div>
