<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">Native Address</h5>
    </div>
    <div class="card-body">
        <form class="update-form" action="{{ route('native-address-update') }}" data-update-type="perminent-address" method="POST">
            @csrf
            <input type="hidden" name="address_type" value="permanent">
            <div class="form-group row">

                <div class="col-md-6">
                    <label for="native_state">State
                        <span class="text-danger">*</span></label>
                    </label>
                    <select class="form-control " name="native_state" id="native_state"
                          required>
                        <option value="">Select State</option>
                        @foreach ($states as $state)
                            <option value="{{$state->id}}"
                                    @if($state->id == $member->info->native_state) selected @endif>{{$state->name}}</option>
                        @endforeach
                    </select>
                    @error('native_state')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="native_city">District
                        <span class="text-danger">*</span></label>
                    </label>
                    <select class="form-control " name="native_city" id="native_city"
                            data-live-search="true" required>
                        <option value="">Select District</option>
                        @foreach ($native_cities as $native_city)
                            <option value="{{$native_city->id}}"
                                    @if($native_city->id == $member->info->native_city) selected @endif>{{$native_city->name}}</option>
                        @endforeach
                    </select>
                    @error('native_city')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="pincode">Pincode
                        <span class="text-danger">*</span></label>
                    </label>
                    <input type="number" name="pincode" value="{{$member->info->pincode}}"
                           class="form-control" placeholder="Pincode" required>
                    @error('pincode')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="native_village">Native Village/City
                        <span class="text-danger">*</span></label>
                    </label>
                    <input type="text" name="native_village" value="{{$member->info->native_village}}"
                           class="form-control" placeholder="Native Village/City" required>
                    @error('native_village')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary btn-sm">Update</button>
            </div>
        </form>
    </div>
</div>
<script>

</script>