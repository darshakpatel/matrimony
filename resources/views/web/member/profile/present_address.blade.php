<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">Present Address</h5>
    </div>
    <div class="card-body">
        <form class="update-form" action="{{ route('address-update') }}" method="POST"
              data-update-type="present_address">
            @csrf
            <input type="hidden" name="address_type" value="present">
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="is_nri">Country
                        <span class="text-danger">*</span></label>
                    </label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="0" name="is_nri"
                               id="india" @if($member->is_nri==0) checked @endif required>
                        <label class="form-check-label" for="india">
                            India
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="1" name="is_nri"
                               id="nri" @if($member->is_nri==1) checked @endif required>
                        <label class="form-check-label" for="nri">
                            NRI
                        </label>
                    </div>
                </div>
                <div class="col-md-6 india" @if($member->is_nri==1) style="display:none" @endif>
                    <label for="state_id">State
                        <span class="text-danger">*</span></label>
                    </label>
                    <select class="form-control " name="state_id" id="state_id"
                            data-live-search="true"  @if($member->is_nri==0) required @endif>
                        <option value="">Select One</option>
                        @foreach ($states as $state)
                            <option value="{{$state->id}}"
                                    @if($state->id == $member->state_id) selected @endif>{{$state->name}}</option>
                        @endforeach
                    </select>
                    @error('state_id')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6 india"  @if($member->is_nri==1) style="display:none" @endif>
                    <label for="city_id">District
                        <span class="text-danger">*</span></label>
                    </label>
                    <select class="form-control " name="city_id" id="city_id"
                            data-live-search="true" @if($member->is_nri==0) required @endif>
                        <option value="">Select One</option>
                        @foreach ($cities as $city)
                            <option value="{{$city->id}}"
                                    @if($city->id == $member->city_id) selected @endif>{{$city->name}}</option>
                        @endforeach
                    </select>
                    @error('city_id')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group row india"  @if($member->is_nri==1) style="display:none" @endif>

                <div class="col-md-6">
                    <label for="present_postal_code">Pin Code
                        <span class="text-danger">*</span></label>
                    </label>
                    <input type="number" name="pincode" value="{{$member->pincode}}"
                           id="pincode"
                           class="form-control" placeholder="Pin Code" @if($member->is_nri==0) required @endif>
                    @error('pincode')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6 india"  @if($member->is_nri==1) style="display:none" @endif>
                    <label for="native_village">Village/City
                        <span class="text-danger">*</span></label>
                    </label>
                    <input type="text" name="village" value="{{$member->village}}" id="village"
                           class="form-control" placeholder="Village/City" @if($member->is_nri==0) required @endif>
                    @error('village')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="native_village">Address
                        <span class="text-danger">*</span></label>
                    </label>
                    <textarea name="address" class="form-control" placeholder="Address"
                              required>{{$member->address}}</textarea>
                    @error('village')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary btn-sm">Update</button>
            </div>
        </form>
    </div>
</div>
