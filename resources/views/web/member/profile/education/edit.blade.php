<form action="{{ route('education.update', $education->id) }}" method="POST">
    <input name="_method" type="hidden" value="PATCH">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title h6">{{trans('messages.Edit Education Info')}}</h5>
        <button type="button" class="close" data-dismiss="modal">
        </button>
    </div>
    <div class="modal-body">
        <div class="form-group row">
            <label class="col-md-3 col-form-label">{{trans('messages.Degree')}}</label>
            <div class="col-md-9">
                <input type="text" name="degree" value="{{$education->degree}}" class="form-control" placeholder="{{trans('messages.Degree')}}" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label">{{trans('messages.Institution')}}</label>
            <div class="col-md-9">
                <input type="text" name="institution" value="{{$education->institution}}"  placeholder="{{ trans('messages.Institution') }}" class="form-control" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label">{{trans('messages.Start')}}</label>
            <div class="col-md-9">
                <input type="number" name="education_start" value="{{$education->start}}" class="form-control" placeholder="{{trans('messages.Start')}}" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label">{{trans('messages.End')}}</label>
            <div class="col-md-9">
                <input type="number" name="education_end" value="{{$education->end}}"  placeholder="{{ trans('messages.End') }}" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">{{trans('messages.Close')}}</button>
        <button type="submit" class="btn btn-primary">{{trans('messages.Update Education Info')}}</button>
    </div>
</form>
