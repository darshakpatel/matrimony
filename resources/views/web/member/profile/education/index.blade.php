<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">Highest Education Information</h5>
    </div>
    <div class="card-body">
        <form class="update-form" action="{{ route('education-update') }}" data-update-type="education" method="POST"
              enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="higher_education">Highest Education Info
                        <span class="text-danger">*</span>
                    </label>
                    <select class="form-control" name="higher_education" required>
                        <option value="">Select One</option>
                        @foreach($highestEducations as $highestEducation)
                            <option value="{{$highestEducation->id}}"
                                    @if($member->info->higher_education==$highestEducation->id) selected @endif>{{$highestEducation->name}}
                            </option>
                        @endforeach
                    </select>
                    @error('higher_education')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="degree">Names of Education Qualification</label>
                    <input type="text" name="degree" value="{{$member->info->degree}}" class="form-control"
                           placeholder="Names of Education Qualification" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="photo">Highest Education Qualification Certificate
                        <span class="text-danger">*</span>
                    </label>
                    <input type="file" name="higher_education_pdf" class="dropify"
                           data-default-file="{{ $member->info->higher_education_pdf }}"
                           @if(empty($member->info->higher_education_pdf)) required @endif>

                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="work_profile">Work Profile
                        <span class="text-danger">*</span>
                    </label>
                    <select class="form-control " name="work_profile" required>
                        <option value="">Select Option</option>
                        @foreach($workProfiles as $workProfile)
                            <option value="{{$workProfile->id}}"
                                    @if($member->info->work_profile==$workProfile->id) selected @endif>{{$workProfile->name}}</option>
                        @endforeach
                    </select>
                    @error('work_profile')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary btn-sm">Update</button>
            </div>
        </form>
    </div>
</div>

