<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">Family Information</h5>
    </div>
    <div class="card-body">
        <form class="update-form" action="{{ route('families-update') }}" method="POST" data-update-type="family_info"
              enctype="multipart/form-data">

            @csrf
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="father_name">Father Name
                        <span class="text-danger">*</span></label>
                    <input type="text" name="father_name"
                           value="{{ !empty($member->profile->father_name) ? $member->profile->father_name : "" }}"
                           class="form-control " placeholder="Father Name" required>
                    @error('father')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="father_no">Father Mobile No
                        <span class="text-danger">*</span></label>
                    <input type="text" name="father_no"
                           value="{{ !empty($member->profile->father_no) ? $member->profile->father_no : "" }}"
                           class="form-control integer" placeholder="Father Mobile No" maxlength="10" required>
                    @error('father_no')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="mother_name">Mother Name
                    </label>
                    <input type="text" name="mother_name"
                           value="{{ !empty($member->profile->mother_name) ? $member->profile->mother_name : "" }}"
                           class="form-control " placeholder="Mother Name">
                    @error('mother_name')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="mother_no">Mother Mobile No</label>
                    <input type="text" name="mother_no"
                           value="{{ !empty($member->profile->mother_no) ? $member->profile->mother_no : "" }}"
                           class="form-control integer" placeholder="Mother Mobile No" maxlength="10">
                    @error('mother_no')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="father_profession">Father Occupation
                        <span class="text-danger">*</span></label>
                    <input type="text" name="father_profession"
                           value="{{ !empty($member->profile->father_profession) ? $member->profile->father_profession : "" }}"
                           class="form-control" placeholder="Father Occupation" required>
                    @error('father_profession')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="grand_father_name">Grand Father Name
                        <span class="text-danger">*</span></label>
                    <input type="text" name="grand_father_name"
                           value="{{ !empty($member->profile->grand_father_name) ? $member->profile->grand_father_name : "" }}"
                           class="form-control" placeholder="Grand Father Name" required>
                    @error('grand_father_name')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="nanaji_name">Nanaji Name
                        <span class="text-danger">*</span></label>
                    <input type="text" name="nanaji_name"
                           value="{{ !empty($member->profile->nanaji_name) ? $member->profile->nanaji_name : "" }}"
                           class="form-control" placeholder="Nanaji Name" required>
                    @error('nanaji_name')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="nanaji_gotra">Nanaji Gotra
                        <span class="text-danger">*</span></label>
                    <input type="text" name="nanaji_gotra"
                           value="{{ !empty($member->profile->nanaji_gotra) ? $member->profile->nanaji_gotra : ""}}"
                           placeholder="Nanaji Gotra" class="form-control" required>
                    @error('nanaji_gotra')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="nanaji_is_nri">Nanaji Country
                        <span class="text-danger">*</span></label>
                    </label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="0" name="nanaji_is_nri"
                               id="nanaji_india" @if($member->profile->nanaji_is_nri==0) checked @endif required>
                        <label class="form-check-label" for="nanaji_india">
                            India
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="1" name="nanaji_is_nri"
                               id="nanaji_nri" @if($member->profile->nanaji_is_nri==1) checked @endif required>
                        <label class="form-check-label" for="nanaji_nri">
                            NRI
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="nanaji_address">Nanaji Address
                        <span class="text-danger">*</span></label>
                    <textarea name="nanaji_address" id="nanaji_address"
                              class="form-control" required>{{ $member->profile->nanaji_address}}</textarea>
                    @error('nanaji_address')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group row nanaji_india"
                 @if($member->profile->nanaji_is_nri==1) style="display:none" @endif>
                <div class="col-md-6">
                    <label for="nanaji_state">Nanaji State
                        <span class="text-danger">*</span></label>
                    <select class="form-control " name="nanaji_state" id="nanaji_state"
                            data-live-search="true" @if($member->profile->nanaji_is_nri==0) required @endif>
                        <option value="">Select One</option>
                        @foreach ($nanajiStates as $state)
                            <option value="{{$state->id}}"
                                    @if($state->id == $member->profile->nanaji_state) selected @endif>{{$state->name}}</option>
                        @endforeach
                    </select>
                    @error('nanaji_state')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="nanaji_city">Nanaji District
                        <span class="text-danger">*</span></label>
                    <select class="form-control " name="nanaji_city" id="nanaji_city"
                            data-live-search="true" @if($member->profile->nanaji_is_nri==0) required @endif>
                        <option value="">Select One</option>
                        @foreach ($nanajiCities as $city)
                            <option value="{{$city->id}}"
                                    @if($city->id == $member->profile->nanaji_city) selected @endif>{{$city->name}}</option>
                        @endforeach
                    </select>
                    @error('nanaji_city')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="brother_married">Brother Married
                        <span class="text-danger">*</span></label>
                    <select class="form-control" name="brother_married" required>
                        <option value="">Select One</option>
                        <option value="0"
                                @if(!empty($member->profile->brother_married) && $member->profile->brother_married==0) selected @endif>
                            0
                        </option>
                        <option value="1"
                                @if(!empty($member->profile->brother_married) && $member->profile->brother_married==1) selected @endif>
                            1
                        </option>
                        <option value="2"
                                @if(!empty($member->profile->brother_married) && $member->profile->brother_married==2) selected @endif>
                            2
                        </option>
                        <option value="3"
                                @if(!empty($member->profile->brother_married) && $member->profile->brother_married==3) selected @endif>
                            3
                        </option>
                        <option value="4"
                                @if(!empty($member->profile->brother_married) && $member->profile->brother_married==4) selected @endif>
                            4
                        </option>
                        <option value="5"
                                @if(!empty($member->profile->brother_married) && $member->profile->brother_married==5) selected @endif>
                            5
                        </option>
                        <option value="6"
                                @if(!empty($member->profile->brother_married) && $member->profile->brother_married==6) selected @endif>
                            6
                        </option>
                        <option value="7"
                                @if(!empty($member->profile->brother_married) && $member->profile->brother_married==7) selected @endif>
                            7
                        </option>
                        <option value="8"
                                @if(!empty($member->profile->brother_married) && $member->profile->brother_married==8) selected @endif>
                            8
                        </option>
                        <option value="9"
                                @if(!empty($member->profile->brother_married) && $member->profile->brother_married==9) selected @endif>
                            9
                        </option>
                    </select>
                    @error('brother_married')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="brother_unmarried">Brother Unmarried
                        <span class="text-danger">*</span>
                    </label>
                    <select class="form-control" name="brother_unmarried" required>
                        <option value="">Select One</option>
                        <option value="0"
                                @if(!empty($member->profile->brother_unmarried) && $member->profile->brother_unmarried==0) selected @endif>
                            0
                        </option>
                        <option value="1"
                                @if(!empty($member->profile->brother_unmarried) && $member->profile->brother_unmarried==1) selected @endif>
                            1
                        </option>
                        <option value="2"
                                @if(!empty($member->profile->brother_unmarried) && $member->profile->brother_unmarried==2) selected @endif>
                            2
                        </option>
                        <option value="3"
                                @if(!empty($member->profile->brother_unmarried) && $member->profile->brother_unmarried==3) selected @endif>
                            3
                        </option>
                        <option value="4"
                                @if(!empty($member->profile->brother_unmarried) && $member->profile->brother_unmarried==4) selected @endif>
                            4
                        </option>
                        <option value="5"
                                @if(!empty($member->profile->brother_unmarried) && $member->profile->brother_unmarried==5) selected @endif>
                            5
                        </option>
                        <option value="6"
                                @if(!empty($member->profile->brother_unmarried) && $member->profile->brother_unmarried==6) selected @endif>
                            6
                        </option>
                        <option value="7"
                                @if(!empty($member->profile->brother_unmarried) && $member->profile->brother_unmarried==7) selected @endif>
                            7
                        </option>
                        <option value="8"
                                @if(!empty($member->profile->brother_unmarried) && $member->profile->brother_unmarried==8) selected @endif>
                            8
                        </option>
                        <option value="9"
                                @if(!empty($member->profile->brother_unmarried) && $member->profile->brother_unmarried==9) selected @endif>
                            9
                        </option>
                    </select>
                    @error('brother_unmarried')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="sister_married">Sister Married
                        <span class="text-danger">*</span>
                    </label>
                    <select class="form-control" name="sister_married" required>
                        <option value="">Select One</option>
                        <option value="0"
                                @if(!empty($member->profile->sister_married) && $member->profile->sister_married==0) selected @endif>
                            0
                        </option>
                        <option value="1"
                                @if(!empty($member->profile->sister_married) && $member->profile->sister_married==1) selected @endif>
                            1
                        </option>
                        <option value="2"
                                @if(!empty($member->profile->sister_married) && $member->profile->sister_married==2) selected @endif>
                            2
                        </option>
                        <option value="3"
                                @if(!empty($member->profile->sister_married) && $member->profile->sister_married==3) selected @endif>
                            3
                        </option>
                        <option value="4"
                                @if(!empty($member->profile->sister_married) && $member->profile->sister_married==4) selected @endif>
                            4
                        </option>
                        <option value="5"
                                @if(!empty($member->profile->sister_married) && $member->profile->sister_married==5) selected @endif>
                            5
                        </option>
                        <option value="6"
                                @if(!empty($member->profile->sister_married) && $member->profile->sister_married==6) selected @endif>
                            6
                        </option>
                        <option value="7"
                                @if(!empty($member->profile->sister_married) && $member->profile->sister_married==7) selected @endif>
                            7
                        </option>
                        <option value="8"
                                @if(!empty($member->profile->sister_married) && $member->profile->sister_married==8) selected @endif>
                            8
                        </option>
                        <option value="9"
                                @if(!empty($member->profile->sister_married) && $member->profile->sister_married==9) selected @endif>
                            9
                        </option>
                    </select>
                    @error('sister_married')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="sister_unmarried">Sister Unmarried
                        <span class="text-danger">*</span></label>
                    <select class="form-control" name="sister_unmarried" required>
                        <option value="">Select One</option>
                        <option value="0"
                                @if(!empty($member->profile->sister_unmarried) && $member->profile->sister_unmarried==0) selected @endif>
                            0
                        </option>
                        <option value="1"
                                @if(!empty($member->profile->sister_unmarried) && $member->profile->sister_unmarried==1) selected @endif>
                            1
                        </option>
                        <option value="2"
                                @if(!empty($member->profile->sister_unmarried) && $member->profile->sister_unmarried==2) selected @endif>
                            2
                        </option>
                        <option value="3"
                                @if(!empty($member->profile->sister_unmarried) && $member->profile->sister_unmarried==3) selected @endif>
                            3
                        </option>
                        <option value="4"
                                @if(!empty($member->profile->sister_unmarried) && $member->profile->sister_unmarried==4) selected @endif>
                            4
                        </option>
                        <option value="5"
                                @if(!empty($member->profile->sister_unmarried) && $member->profile->sister_unmarried==5) selected @endif>
                            5
                        </option>
                        <option value="6"
                                @if(!empty($member->profile->sister_unmarried) && $member->profile->sister_unmarried==6) selected @endif>
                            6
                        </option>
                        <option value="7"
                                @if(!empty($member->profile->sister_unmarried) && $member->profile->sister_unmarried==7) selected @endif>
                            7
                        </option>
                        <option value="8"
                                @if(!empty($member->profile->sister_unmarried) && $member->profile->sister_unmarried==8) selected @endif>
                            8
                        </option>
                        <option value="9"
                                @if(!empty($member->profile->sister_unmarried) && $member->profile->sister_unmarried==9) selected @endif>
                            9
                        </option>
                    </select>
                    @error('sister_unmarrieds')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary btn-sm">Update</button>
            </div>
        </form>
    </div>
</div>
