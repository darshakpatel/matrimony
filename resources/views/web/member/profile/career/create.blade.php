<form action="{{ route('career.store') }}" method="POST">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title h6">{{trans('messages.Add New Career Info')}}</h5>
        <button type="button" class="close" data-dismiss="modal">
        </button>
    </div>
    <div class="modal-body">
        <input type="hidden" name="user_id" value="{{ $member_id }}">
        <div class="form-group row">
            <label class="col-md-3 col-form-label">{{trans('messages.Designation')}}</label>
            <div class="col-md-9">
                <input type="text" name="designation" class="form-control" placeholder="{{trans('messages.designation')}}" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label">{{trans('messages.Company')}}</label>
            <div class="col-md-9">
                <input type="text" name="company"  placeholder="{{ trans('messages.company') }}" class="form-control" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label">{{trans('messages.Start')}}</label>
            <div class="col-md-9">
                <input type="number" name="career_start" class="form-control" placeholder="{{trans('messages.Start')}}" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label">{{trans('messages.End')}}</label>
            <div class="col-md-9">
                <input type="number" name="career_end"  placeholder="{{ trans('messages.End') }}" class="form-control">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">{{trans('messages.Close')}}</button>
        <button type="submit" class="btn btn-primary">{{trans('messages.Add New Career Info')}}</button>
    </div>
</form>
