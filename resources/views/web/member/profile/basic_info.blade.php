<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">Basic Information</h5>
    </div>
    <div class="card-body">

        <form class="update-form" action="{{ route('basic_info_update') }}" data-update-type="basic_info" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="on_behalf" value="self"/>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="first_name">First Name
                        <span class="text-danger">*</span>
                    </label>
                    <input type="text" name="first_name" value="{{ $member->first_name }}" class="bg-light form-control"
                           placeholder="First Name" readonly required>
                    @error('first_name')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="first_name">Last Name
                        <span class="text-danger">*</span>
                    </label>
                    <input type="text" name="last_name" readonly value="{{ $member->last_name }}"
                           class="bg-light form-control"
                           placeholder="Last Name" required>
                    @error('last_name')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="first_name">Gender
                        <span class="text-danger">*</span>
                    </label>
                    <select class="form-control bg-light" name="gender" disabled required>
                        <option value="male"
                                @if($member->gender ==  'male') selected @endif >Male
                        </option>
                        <option value="female"
                                @if($member->gender ==  'female') selected @endif >Female
                        </option>
                        @error('gender')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="first_name">Date Of Birth
                        <span class="text-danger">*</span>
                    </label>
                    <input type="text" class="bg-light  form-control" name="date_of_birth" disabled
                           value="@if(!empty($member->date_of_birth)) {{date('d-m-Y', strtotime($member->date_of_birth))}} @endif"
                           placeholder="Select Date" data-single="true" data-show-dropdown="true"
                           data-max-date="{{ get_max_date() }}" autocomplete="off" required>
                    @error('date_of_birth')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="first_name">Phone Number
                        <span class="text-danger">*</span>
                    </label>
                    <input type="number" name="mobile_no" value="{{ $member->mobile_no }}" class="bg-light form-control"
                           placeholder="Phone Number" readonly required/>
                    @error('phone')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="email">Email
                        <span class="text-danger">*</span>
                    </label>
                    <input type="email" name="email" value="{{ $member->email }}" class="bg-light form-control"
                           placeholder="Email" readonly required/>
                    @error('email')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="marital_status">Marital Status
                        <span class="text-danger">*</span>
                    </label>
                    <select class="form-control" name="marital_status"
                            required>
                        <option value="">Select One</option>
                        <option value="Unmarried" @if($member->profile->marital_status=='Unmarried') selected @endif>
                            Unmarried
                        </option>
                        <option value="Divorced" @if($member->profile->marital_status=='Divorced') selected @endif>
                            Divorced
                        </option>
                        <option value="Widowed" @if($member->profile->marital_status=='Widowed') selected @endif>
                            Widowed
                        </option>
                    </select>
                    @error('marital_status')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="col-md-6">
                    <label for="gotra">Gotra
                        <span class="text-danger">*</span>
                    </label>

{{--                    <select class="form-control select2" name="gotra" required>--}}
{{--                        <option value="">Gotra</option>--}}
{{--                        @foreach($gotras as $gotra)--}}
{{--                            <option value="{{$gotra->id}}"--}}
{{--                                    @if($member->profile->gotra==$gotra->id) selected @endif>{{$gotra->name}}</option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
                    <input type="text" name="gotra" value="{{ $member->profile->gotra }}" class="form-control"
                           placeholder="Gotra"  required/>
                    @error('gotra')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <label for="photo">
                        Candidate's Profile Photo <small>(800x800 Recommended)</small>
                        <span class="text-danger">@if($member->gender=='male') * @endif</span>
                    </label>
                    <input type="file" name="image" class="dropify" data-default-file="{{ $member->image }}"
                           data-allowed-file-extensions="jpg png jpeg"
                           @if($member->gender=='male' && empty($member->image)) required @endif>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="photo">Candidate's / Father's Aadhar card - Front page
                        <span class="text-danger">*</span>
                    </label>
                    <input type="file" name="aadhar_photo" class="dropify"
                           data-default-file="{{ $member->profile->aadhar_photo }}"
                           data-allowed-file-extensions="jpg png jpeg"
                           @if(empty($member->profile->aadhar_photo)) required @endif
                    />
                </div>
                <div class="col-md-6">
                    <label for="photo">Candidate's / Father's Aadhar card - Back page
                        <span class="text-danger">*</span>
                    </label>
                    <input type="file" name="aadhar_back_photo" class="dropify"
                           data-default-file="{{ $member->profile->aadhar_back_photo }}"
                           data-allowed-file-extensions="jpg png jpeg"
                           @if(empty($member->profile->aadhar_back_photo)) required @endif
                    />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="height">Height (in feet)
                        <span class="text-danger">*</span>
                    </label>
                    <select class="form-control" name="height" required>
                        <option value="">Select One</option>
                        <option value="4"
                                @if($member->profile->height==4) selected @endif>
                            4
                        </option>
                        <option value="5"
                                @if($member->profile->height==5) selected @endif>
                            5
                        </option>
                        <option value="6"
                                @if($member->profile->height==6) selected @endif>
                            6
                        </option>
                        <option value="7"
                                @if($member->profile->height==7) selected @endif>
                            7
                        </option>
                    </select>
                    @error('height')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="height_inch">Height (in inch)
                        <span class="text-danger">*</span>
                    </label>
                    <select class="form-control" name="height_inch" required>
                        <option value="">Select One</option>
                        <option value="0"
                                @if($member->profile->height_inch==0) selected @endif>
                            0
                        </option>
                        <option value="1"
                                @if($member->profile->height_inch==1) selected @endif>
                            1
                        </option>
                        <option value="2"
                                @if($member->profile->height_inch==2) selected @endif>
                            2
                        </option>
                        <option value="3"
                                @if($member->profile->height_inch==3) selected @endif>
                            3
                        </option>
                        <option value="4"
                                @if($member->profile->height_inch==4) selected @endif>
                            4
                        </option>
                        <option value="5"
                                @if($member->profile->height_inch==5) selected @endif>
                            5
                        </option>
                        <option value="6"
                                @if($member->profile->height_inch==6) selected @endif>
                            6
                        </option>
                        <option value="7"
                                @if($member->profile->height_inch==7) selected @endif>
                            7
                        </option>
                        <option value="8"
                                @if($member->profile->height_inch==8) selected @endif>
                            8
                        </option>
                        <option value="9"
                                @if($member->profile->height_inch==9) selected @endif>
                            9
                        </option>
                        <option value="10"
                                @if($member->profile->height_inch==10) selected @endif>
                            10
                        </option>
                        <option value="11"
                                @if($member->profile->height_inch==11) selected @endif>
                            11
                        </option>
                    </select>
                    @error('height_inch')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">

                    <label for="time_of_birth">Time Of Birth ( 24 hour Format )
                        <span class="text-danger">*</span>
                    </label>
                    <input type="text" name="time_of_birth" id="time_of_birth"
                           value="{{ $member->profile->time_of_birth }}" class=" form-control"
                           placeholder="Time Of Birth ( 24 hour Format )" required/>
                    @error('email')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="place_of_birth">Place of birth
                        <span class="text-danger">*</span>
                    </label>
                    <input type="text" name="place_of_birth" value="{{ $member->profile->place_of_birth }}"
                           class="form-control"
                           placeholder="Place of birth" required/>
                    @error('place_of_birth')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">

                    <label for="contact_no">Contact No (Alternative)
                        <span class="text-danger"></span>
                    </label>
                    <input type="text" name="contact_no" value="{{ $member->profile->contact_no }}"
                           class="form-control integer"
                           maxlength="10" minlength="10"
                           placeholder="Contact No">
                    @error('contact_no')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>


                <div class="col-md-6">
                    <label for="mangalik">Mangalik
                        <span class="text-danger">*</span>
                    </label>
                    <select class="form-control" name="mangalik" required>
                        @foreach($mangaliks as $mangalik)
                            <option value="{{$mangalik->id}}"
                                    @if(!empty($member->profile->mangalik) && $member->profile->mangalik==$mangalik->id) selected
                                    @else @if($mangalik->id==3) selected @endif @endif>{{$mangalik->name}}</option>
                        @endforeach
                    </select>
                    @error('mangalik')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary btn-sm">Update</button>
            </div>
        </form>
    </div>
</div>
