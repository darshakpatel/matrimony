@extends('web.layouts.member_panel')
@section('panel_content')
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">My Interests</h5>
            <a href="{{ route('interest_requests') }}" class="mb-0 h6 btn btn-primary">Interest Requests</a>
        </div>
        <div class="card-body">
            <table class="table aiz-table mb-0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th class="text-center">Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($interests as $key => $interest_id)
                    @php
                        $interest = \App\Models\ExpressInterest::where('id',$interest_id->id)->first();

                          if($interest->user_id==Auth::user()->id){
                              $user = \App\Models\User::where('id',$interest_id->interested_by)->first();
                          }else{
                              $user = \App\Models\User::where('id',$interest_id->user_id)->first();
                          }
                    @endphp

                    <tr id="interested_member_{{ $user->id }}">
                        <td>{{ ($key+1) + ($interests->currentPage() - 1)*$interests->perPage() }}</td>
                        <td>
                            @if ($interest->status == 1)
                                <a href="{{ route('member_profile', $user->id) }}" class="text-reset c-pointer">
                                    @if($user->image != null)
                                        <img class="img-md" src="{{ asset($user->image) }}" height="45px" alt="photo">
                                    @else
                                        <img class="img-md" src="{{ asset('assets/img/avatar-place.png') }}"
                                             height="45px" alt="photo">
                                    @endif
                                </a>
                            @else
                                @if($user->image != null)
                                    <img class="img-md" src="{{ asset($user->image) }}" height="45px" alt="photo">
                                @else
                                    <img class="img-md" src="{{ asset('assets/img/avatar-place.png') }}"
                                         height="45px" alt="photo">
                                @endif
                            @endif
                        </td>
                        <td>
                            @if ($interest->status == 1)
                            <a href="{{ route('member_profile', $user->id) }}"
                            class="text-reset c-pointer">{{ $user->first_name.' '.$user->last_name }}
                            </a>
                            @else
                                {{ $user->first_name.' '.$user->last_name }}
                            @endif
                        </td>
                        <td>{{ \Carbon\Carbon::parse($user->date_of_birth)->age }}</td>
                        <td class="text-center">
                            @if ($interest->status == 1)
                                <span class="badge badge-inline badge-success">Approved</span>
                            @else
                                <span class="badge badge-inline badge-info">Pending</span>
                            @endif
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
            <div class="aiz-pagination">
                {{ $interests->links() }}
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script type="text/javascript">

    </script>
@endsection
