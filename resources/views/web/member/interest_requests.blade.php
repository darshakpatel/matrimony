@extends('web.layouts.member_panel')
@section('panel_content')
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">Interest Requests</h5>
        </div>
        <div class="card-body">
            <table class="table aiz-table mb-0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th class="text-center">Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($interests as $key => $interest)
                    @php $interested_by = \App\Models\User::where('id',$interest->interested_by)->first(); @endphp
                    <tr id="interested_member_{{ $interested_by->id }}">
                        <td>{{$interest->id}}</td>
                        <td>
                            <a href="{{ route('member_profile', $interested_by->id) }}" class="text-reset c-pointer">
                                @if(asset($interested_by->image) != null)
                                    <img class="img-md" src="{{ asset($interested_by->image) }}" height="45px" alt="photo">
                                @else
                                    <img class="img-md" src="{{ asset('assets/img/avatar-place.png') }}" height="45px"
                                         alt="photo">
                                @endif
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('member_profile', $interested_by->id) }}"
                               class="text-reset c-pointer"
                            >
                                {{ $interested_by->name }}
                            </a>
                        </td>
                        <td>{{ \Carbon\Carbon::parse($interest->user->date_of_birth)->age }}</td>
                        <td class="text-center">
                            @if($interest->status != 1)
                                <a href="javascript:void(0);" onclick="accept_interest({{ $interest->id }})"
                                   class="btn btn-soft-success btn-icon btn-circle btn-sm" title="Accept">
                                    <i class="las la-check"></i>
                                </a>
                                <a href="javascript:void(0);" onclick="reject_interest({{ $interest->id }})"
                                   class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" title="Reject">
                                    <i class="las la-trash"></i>
                                </a>
                            @else
                                <span class="badge badge-inline badge-success">Accepted</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="aiz-pagination">
                {{ $interests->links() }}
            </div>
        </div>
    </div>
@endsection
@section('modal')
    {{-- Interest Accept modal--}}
    <div class="modal fade interest_accept_modal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title h6">Interest Accept!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body text-center">
                    <form class="form-horizontal member-block" action="{{ route('accept_interest') }}" method="POST">
                        @csrf
                        <input type="hidden" name="interest_id" id="interest_accept_id" value="">
                        <p class="mt-1">Are you sure you want to accept this interest?</p>
                        <button type="button" class="btn btn-danger mt-2"
                                data-dismiss="modal">Cancel
                        </button>
                        <button type="submit" class="btn btn-info mt-2">Confirm</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- Interest Reject Modal --}}
    <div class="modal fade interest_reject_modal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title h6">Interest Reject !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body text-center">
                    <form class="form-horizontal member-block" action="{{ route('reject_interest') }}" method="POST">
                        @csrf
                        <input type="hidden" name="interest_id" id="interest_reject_id" value="">
                        <p class="mt-1">Are you sure you want to rejet his interest?</p>
                        <button type="button" class="btn btn-danger mt-2"
                                data-dismiss="modal">Cancel
                        </button>
                        <button type="submit" class="btn btn-info mt-2">Confirm</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">

        function accept_interest(id) {
            $('.interest_accept_modal').modal('show');
            $('#interest_accept_id').val(id);
        }

        function reject_interest(id) {
            $('.interest_reject_modal').modal('show');
            $('#interest_reject_id').val(id);
        }

    </script>
@endsection
