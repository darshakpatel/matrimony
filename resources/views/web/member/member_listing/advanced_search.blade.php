<div class="aiz-filter-sidebar collapse-sidebar-wrap sidebar-xl z-1035">
    <div class="overlay overlay-fixed dark c-pointer" data-toggle="class-toggle" data-target=".aiz-filter-sidebar"
         data-same=".filter-sidebar-thumb"></div>
    <div class="card collapse-sidebar c-scrollbar-light shadow-none">
        <div class="card-header pr-1 pl-3">
            <h5 class="mb-0 h6">ADVANCED SEARCH</h5>
            <button class="btn btn-sm p-2 d-xl-none filter-sidebar-thumb" data-toggle="class-toggle"
                    data-target=".aiz-filter-sidebar" type="button">
                <i class="las la-times la-2x"></i>
            </button>
        </div>
        <div class="card-body">
            <div class="pb-4">
                <form id="advance_search" action="{{ route('memberListing') }}" method="get">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group mb-3">
                                <label class="form-label" for="name">Age From</label>
                                <input type="number" name="age_from" value="{{ $age_from }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group mb-3">
                                <label class="form-label" for="name">To</label>
                                <input type="number" name="age_to" value="{{ $age_to }}" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <label class="form-label" for="name">Gender</label>
                                <select class="form-control aiz-selectpicker" name="gender"
                                        data-live-search="true">
                                    <option value="">Select One</option>
                                    <option value="male" @if($gender == 'male') selected @endif>Male</option>
                                    <option value="female" @if($gender == 'female') selected @endif>Female</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <label class="form-label" for="name">Marital Status</label>
                                @php $marital_statuses = \App\Models\MaritalStatus::all(); @endphp
                                <select class="form-control aiz-selectpicker" name="marital_status"
                                        data-live-search="true">
                                    <option value="">Select One</option>
                                    <option value="Unmarried" @if($marital_status == 'Unmarried') selected @endif>
                                        Unmarried
                                    </option>
                                    <option value="Divorced" @if($marital_status == 'Divorced') selected @endif>
                                        Divorced
                                    </option>
                                    <option value="Widowed" @if($marital_status== 'Widowed') selected @endif>Widowed
                                    </option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <label class="form-label" for="name">Country ( Present )</label>
                                <select name="country_id" id="" class="form-control aiz-selectpicker"
                                        data-live-search="true">
                                    <option value="">Select One</option>
                                    <option value="india" @if($country_id=='india') selected @endif>India</option>
                                    <option value="nri" @if($country_id=='nri') selected @endif>NRI</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <?php
                                $educations = App\Models\HighestEducation::get();
                                ?>
                                <label class="form-label" for="name">Education</label>
                                <select name="higher_education" id="" class="form-control"
                                        data-live-search="true">
                                    <option value="">Select One</option>
                                    @foreach($educations as $education)
                                        <option value="{{$education->id}}"
                                                @if($higher_education==$education->id) selected
                                                @endif>{{$education->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <?php
                                $states = App\Models\State::where('country_id', 101)->orderBy('name', 'asc')->get();
                                ?>
                                <label class="form-label" for="name">State ( Present )</label>
                                <select name="state_id" id="state_id" class="form-control">
                                    <option value="">Select One</option>
                                    @foreach($states as $state)
                                        <option value="{{$state->id}}"
                                                @if($state_id==$state->id)selected @endif >{{$state->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <label class="form-label" for="name">District ( Present )</label>
                                <select name="city_id" id="city_id" class="form-control"
                                >
                                    <option value="">Select One</option>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}"
                                                @if($city_id==$city->id)selected @endif>{{$city->name}}</option>
                                    @endforeach
                                </select>
                                </select>
                            </div>
                        </div>
                    </div>
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-lg-12">--}}
                    {{--                            <div class="form-group mb-3">--}}
                    {{--                                <label class="form-label" for="name">Height in feet</label>--}}
                    {{--                                <select class="form-control" name="height" >--}}
                    {{--                                    <option value="">Select One</option>--}}
                    {{--                                    <option value="4"--}}
                    {{--                                            @if($height==4) selected @endif>--}}
                    {{--                                        4--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="5"--}}
                    {{--                                            @if($height==5) selected @endif>--}}
                    {{--                                        5--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="6"--}}
                    {{--                                            @if($height==6) selected @endif>--}}
                    {{--                                        6--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="7"--}}
                    {{--                                            @if($height==7) selected @endif>--}}
                    {{--                                        7--}}
                    {{--                                    </option>--}}
                    {{--                                </select>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-lg-12">--}}
                    {{--                            <div class="form-group mb-3">--}}
                    {{--                                <label class="form-label" for="name">Height in inch</label>--}}
                    {{--                                <select class="form-control" name="height_inch" >--}}
                    {{--                                    <option value="">Select One</option>--}}
                    {{--                                    <option value="0"--}}
                    {{--                                            @if($height_inch==0) selected @endif>--}}
                    {{--                                        0--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="1"--}}
                    {{--                                            @if($height_inch==1) selected @endif>--}}
                    {{--                                        1--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="2"--}}
                    {{--                                            @if($height_inch==2) selected @endif>--}}
                    {{--                                        2--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="3"--}}
                    {{--                                            @if($height_inch==3) selected @endif>--}}
                    {{--                                        3--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="4"--}}
                    {{--                                            @if($height_inch==4) selected @endif>--}}
                    {{--                                        4--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="5"--}}
                    {{--                                            @if($height_inch==5) selected @endif>--}}
                    {{--                                        5--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="6"--}}
                    {{--                                            @if($height_inch==6) selected @endif>--}}
                    {{--                                        6--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="7"--}}
                    {{--                                            @if($height_inch==7) selected @endif>--}}
                    {{--                                        7--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="8"--}}
                    {{--                                            @if($height_inch==8) selected @endif>--}}
                    {{--                                        8--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="9"--}}
                    {{--                                            @if($height_inch==9) selected @endif>--}}
                    {{--                                        9--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="10"--}}
                    {{--                                            @if($height_inch==10) selected @endif>--}}
                    {{--                                        10--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="11"--}}
                    {{--                                            @if($height_inch==11) selected @endif>--}}
                    {{--                                        11--}}
                    {{--                                    </option>--}}
                    {{--                                    <option value="12"--}}
                    {{--                                            @if($height_inch==12) selected @endif>--}}
                    {{--                                        12--}}
                    {{--                                    </option>--}}
                    {{--                                </select>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <label class="form-label" for="name">Member ID</label>
                                <input type="text" name="member_code" value="{{ $member_code }}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-block btn-primary mt-4">Search</button>
                    <button type="button" class="reset btn btn-block btn-primary mt-4">Clear</button>
                </form>
            </div>
        </div>
    </div>
</div>
