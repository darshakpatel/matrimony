@extends('web.layouts.app')
@section('content')
    <section class="py-4 py-lg-5 bg-white">
        <div class="container">
            <div class="row mt-5">
                <div class="col-12">
                    <div class="row mt-5">
                        <div class="col-xl-3">
                            @include('web.member.member_listing.advanced_search')
                        </div>
                        <div class="col-xl-9">
                            <div class="d-flex">
                                <h1 class="h4 fw-600 mb-3 text-body">All Active Members</h1>
                                <div class="d-xl-none ml-auto mb-1 ml-xl-3 mr-0 align-self-end">
                                    <button type="button" class="btn btn-icon p-0" data-toggle="class-toggle"
                                            data-target=".aiz-filter-sidebar">
                                        <i class="la la-list la-2x"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="mb-5">
                                @if(count($users)>0)
                                    @foreach ($users as $key => $user)
                                        <div
                                                class="row no-gutters border border-gray-300 rounded hov-shadow-md mb-4 has-transition position-relative custom-card"
                                                id="block_id_{{ $user->id }}">
                                            <div class="col-md-auto">
                                                <div class="text-center text-md-left pt-3 pt-md-0">
                                                    <img src="{{ asset($user->image) }}"
                                                         onerror="this.onerror=null;this.src='{{ asset('assets/img/avatar-place.png') }}';"
                                                         class="img-fit mw-100 size-150px size-md-250px rounded-circle md-rounded-0"
                                                    >
                                                </div>
                                            </div>
                                            <div class="col-md position-static d-flex align-items-center">
                                                <div class="px-md-4 p-3 flex-grow-1">

                                                    <h2 class="h6 fw-600 fs-18 text-truncate mb-1">{{ $user->first_name.' '.$user->last_name }}</h2>
                                                    <div class="mb-2 fs-12">
                                                        <span class="opacity-60">Member ID: </span>
                                                        <span class="ml-4 text-primary">{{ $user->unique_id }}</span>
                                                    </div>
                                                    <table class="w-100 opacity-70 mb-2 fs-12">
                                                        <tr>
                                                            <td class="py-1 w-25">
                                                                <span>Age: </span>
                                                                <span>{{ \Carbon\Carbon::parse($user->date_of_birth)->age }}</span>
                                                                <br/>
                                                                <span>Height: </span>
                                                                <span>{{ $user->profile->height }}  {{ $user->profile->height_inch }}</span>
                                                                <br/>
                                                                <span>Location: </span>
                                                                <span>
                                                                @if($user->is_nri==1)
                                                                        NRI
                                                                    @else
                                                                        India
                                                                    @endif
                                                            </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="mt-3 row text-center">
                                                        @php
                                                            $interest_class    = "text-primary";
                                                            $do_expressed_interest = \App\Models\ExpressInterest::where('user_id', $user->id)->where('interested_by',Auth::user()->id)->first();
                                                            $expressed_interest = \App\Models\ExpressInterest::where('interested_by', $user->id)->where('user_id',Auth::user()->id)->first();
                                                            $received_expressed_interest = \App\Models\ExpressInterest::where('user_id', Auth::user()->id)->where('interested_by',$user->id)->first();
                                                            if(empty($do_expressed_interest) && empty($received_expressed_interest))
                                                            {
                                                                $interest_onclick  = 1;
                                                                $interest_text     = 'Interest';
                                                                $interest_class    = "text-dark";
                                                            }
                                                            elseif(!empty($received_expressed_interest))
                                                            {
                                                                $interest_onclick  = 'do_response';
                                                                $interest_text     = $received_expressed_interest->status == 0 ? 'Response to Interest' : 'You Accepted Interest';
                                                            }
                                                            else
                                                            {
                                                                $interest_onclick  = 0;
                                                                $interest_text     = $do_expressed_interest->status == 0 ? 'Interest Expressed' : 'Interest Accepted';
                                                            }
                                                        @endphp
                                                        @if($user->gender=='female')
                                                            <div class="col">
                                                                @if($do_expressed_interest && $do_expressed_interest->status==1 || $expressed_interest && $expressed_interest->status==1)
                                                                    <a class="text-reset c-pointer"
                                                                       href="{{ route('member_profile', $user->id) }}">
                                                                        <i class="las la-user fs-20 text-primary"></i>
                                                                        <span
                                                                                class="d-block fs-10 opacity-60">Full Profile</span>
                                                                    </a>
                                                                @elseif($do_expressed_interest && $do_expressed_interest->status==0)
                                                                    <span class="mt-2 d-block fs-10 opacity-60">Your request already sent and status is pending for view profile</span>
                                                                @else
                                                                    <span class="mt-2 d-block fs-10 opacity-60">You have to send interest request to view full profile</span>
                                                                @endif
                                                            </div>
                                                        @else
                                                            <div class="col">
                                                                <a href="{{ route('member_profile', $user->id) }}"
                                                                   class="text-reset c-pointer">
                                                                    <i class="las la-user fs-20 text-primary"></i>
                                                                    <span
                                                                            class="d-block fs-10 opacity-60">Full Profile</span>
                                                                </a>
                                                            </div>
                                                        @endif

                                                        @if($user->gender!=Auth::user()->gender)
                                                            <div class="col">
                                                                <a id="interest_a_id_{{ $user->id }}"
                                                                   @if($interest_onclick == 1)
                                                                   onclick="express_interest({{ $user->id }})"
                                                                   @elseif($interest_onclick == 'do_response')
                                                                   href="{{ route('interest_requests') }}"
                                                                   @endif
                                                                   class="text-reset c-pointer"
                                                                >
                                                                    <i class="la la-heart-o fs-20 text-red"></i>
                                                                    <span id="interest_id_{{ $user->id }}"
                                                                          class="d-block fs-10 opacity-100 {{ $interest_class }}">
                                                            {{ $interest_text }}
                                                        </span>
                                                                </a>
                                                            </div>
                                                        @endif
                                                        <div class="col">
                                                            <a onclick="ignore_member({{ $user->id }})"
                                                               class="text-reset c-pointer">
                                                      <span class="text-dark">
                                                        <i class="las la-ban fs-20 text-primary"></i>
                                                        <span
                                                                class="d-block fs-10 opacity-60">Ignore</span>
                                                      </span>
                                                            </a>
                                                        </div>
                                                        <div class="col">
                                                            @php
                                                                $profile_reported = \App\Models\ReportedUser::where('user_id', $user->id)->where('reported_by',Auth::user()->id)->first();
                                                                if(empty($profile_reported)){
                                                                    $report_onclick  = 1;
                                                                    $report_text     = 'Report';
                                                                    $report_class    = "text-dark";
                                                                }
                                                                else{
                                                                    $report_onclick  = 0;
                                                                    $report_text     = 'Reported';
                                                                    $report_class    = "text-primary";
                                                                }
                                                            @endphp
                                                            <a id="report_a_id_{{ $user->id }}"
                                                               @if($report_onclick == 1)
                                                               onclick="report_member({{ $user->id }})"
                                                               @endif
                                                               class="text-reset c-pointer"
                                                            >
                                                      <span id="report_id_{{ $user->id }}" class="{{ $report_class }}">
                                                        <i class="las la-info-circle fs-20 text-primary"></i>
                                                        <span class="d-block fs-10 opacity-60">{{ $report_text }}</span>
                                                      </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <h3 class="mt-5 text-center">There is no candidate's profile available for
                                        search</h3>
                                @endif
                            </div>
                            <div class="aiz-pagination">
                                {{ $users->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('modal')
    {{--    @include('modals.package_update_alert_modal')--}}
    @include('modals.confirm_modal')

    <!-- Ignore Modal -->
    <div class="modal fade ignore_member_modal" id="modal-zoom">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title h6">Ignore Member!</h5>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to ignore this member?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary"
                            id="ignore_button">Ignore
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Report Profile -->
    <div class="modal fade report_modal" id="modal-zoom">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title h6">Report Member!</h5>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('reportUsers') }}" id="report-modal-form" method="POST">
                        @csrf
                        <input type="hidden" name="member_id" id="member_id" value="">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Report Reason<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <textarea name="reason" rows="4" class="form-control"
                                          placeholder="Report Reason" required></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                            data-dismiss="modal">Cancel
                    </button>
                    <button type="button" class="btn btn-primary"
                            onclick="submitReport()">Report
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    @foreach (session('flash_notification', collect())->toArray() as $message)
        <script>
            successToast('{{ $message['message'] }}', '{{ $message['level'] }}');
        </script>
    @endforeach
    <script type="text/javascript">
        function ignore_member(id) {
            $('.ignore_member_modal').modal('show');
            $("#ignore_button").attr("onclick", "do_ignore(" + id + ")");
        }

        function do_ignore(id) {
            $.post('{{ route('add_to_ignore_list') }}',
                {
                    _token: '{{ csrf_token() }}',
                    id: id
                },
                function (data) {
                    if (data == 1) {
                        $("#block_id_" + id).hide();
                        $('.ignore_member_modal').modal('toggle');
                        AIZ.plugins.notify('success', 'You Have Ignored This Member.');
                    } else {
                        AIZ.plugins.notify('danger', 'Something went wrong');
                    }
                }
            );
        }

        function report_member(id) {
            $('.report_modal').modal('show');
            $('#member_id').val(id);
        }

        function submitReport() {
            $('#report-modal-form').submit();
        }

        function express_interest(id) {
            $('.confirm_modal').modal('show');
            $("#confirm_modal_title").html("Confirm Express Interest!");
            $("#confirm_modal_content").html("<p class='fs-14'>Are you sure want to express the interest?</small>");
            $("#confirm_button").attr("onclick", "do_express_interest(" + id + ")");

        }

        function do_express_interest(id) {
            $("#interest_a_id_" + id).removeAttr("onclick");
            $("#interest_id_" + id).html("'Processing'..");
            $.post('{{ route('express-interest.store') }}',
                {
                    _token: '{{ csrf_token() }}',
                    id: id
                },
                function (data) {
                    // console.log(data);
                    if (data == 1) {
                        $('.confirm_modal').modal('toggle');
                        $("#interest_id_" + id).html("Interest Expressed");
                        $("#interest_id_" + id).attr("class", "d-block fs-10 opacity-60 text-primary");
                        AIZ.plugins.notify('success', 'Interest Expressed Successfully');
                    } else {
                        $("#interest_id_" + id).html("Interest");
                        AIZ.plugins.notify('danger', 'Something went wrong');
                    }
                }
            );
        }

        $(".reset").on('click', function () {
            window.location.href = '/memberListing';
        })
    </script>
@endsection
