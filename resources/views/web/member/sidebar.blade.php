<div class="aiz-user-sidenav-wrap pt-4 sticky-top c-scrollbar-light position-relative z-1 shadow-none custom-card">
    <div class="absolute-top-left d-xl-none">
        <button class="btn btn-sm p-2" data-toggle="class-toggle" data-target=".aiz-mobile-side-nav"
                data-same=".mobile-side-nav-thumb">
            <i class="las la-times la-2x"></i>
        </button>
    </div>
    <div class="aiz-user-sidenav rounded overflow-hidden">
        <div class="px-4 text-center mb-4">
            <span class="avatar avatar-md mb-3">
                @if (Auth::user()->image != null)
                    <img src="{{ asset(Auth::user()->image) }}">
                @else
                    <img src="{{ asset('assets/img/avatar-place.png') }}">
                @endif
            </span>
            <h4 class="h5 fw-600">{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</h4>
            <div class="text-center  mb-2">

            </div>
            <div>
                <span class="rating rating-sm">

                </span>
            </div>
            <div class="mb-1">

                <span class="fw-600">

                </span>
                <span>

                </span>
            </div>
        </div>
        <div class="text-center mb-3 px-3">
            <a href="{{ route('member_profile', Auth::user()->id) }}" class="btn btn-block btn-soft-primary">Public
                Profile</a>
        </div>

        <div class="sidemnenu mb-3">
            <ul class="aiz-side-nav-list" data-toggle="aiz-side-menu">

                <li class="aiz-side-nav-item">
                    <a href="{{ route('dashboard') }}" class="aiz-side-nav-link">
                        <i class="las la-home aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Dashboard</span>
                    </a>
                </li>
{{--                <li class="aiz-side-nav-item">--}}
{{--                    <a href="{{ route('gallery-image.index') }}" class="aiz-side-nav-link">--}}
{{--                        <i class="las la-image aiz-side-nav-icon"></i>--}}
{{--                        <span class="aiz-side-nav-text">Gallery</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="aiz-side-nav-item">--}}
{{--                    <a href="{{ route('happy-story.create')}}" class="aiz-side-nav-link">--}}
{{--                        <i class="las la-handshake aiz-side-nav-icon"></i>--}}
{{--                        <span class="aiz-side-nav-text">Happy Story</span>--}}
{{--                    </a>--}}
{{--                </li>--}}

{{--                <li class="aiz-side-nav-item">--}}
{{--                    <a href="#" class="aiz-side-nav-link">--}}
{{--                        <i class="las la-envelope aiz-side-nav-icon"></i>--}}
{{--                        <span class="aiz-side-nav-text">Messages</span>--}}
{{--                    </a>--}}
{{--                </li>--}}

                <li class="aiz-side-nav-item">
                    <a href="{{route('my_interests.index')}}" class="aiz-side-nav-link">
                        <i class="la la-heart-o aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">My Interests</span>
                    </a>
                </li>

                <li class="aiz-side-nav-item">
                    <a href="{{route('my_ignored_list')}}" class="aiz-side-nav-link">
                        <i class="las la-ban aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Ignored User List</span>
                    </a>
                </li>

                <li class="aiz-side-nav-item">
                    <a href="{{route('change_password')}}" class="aiz-side-nav-link">
                        <i class="las la-key aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Change Password</span>
                    </a>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="{{ route('profile_settings') }}" class="aiz-side-nav-link">
                        <i class="las la-user aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Manage Profile</span>
                    </a>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="javascript:void(0);" class="aiz-side-nav-link" onclick="account_deactivation()">
                        @if(Auth::user()->status == 'active' )
                            <i class="las la-lock aiz-side-nav-icon"></i>
                            <span class="aiz-side-nav-text">Deactivate Account</span>
                        @else
                            <i class="las la-unlock aiz-side-nav-icon"></i>
                            <span class="aiz-side-nav-text">Reactive Account</span>
                        @endif
                    </a>
                </li>
            </ul>
        </div>
        <div>
            <a class="btn btn-block btn-primary" href="{{ route('logout') }}">
                <i class="las la-sign-out-alt"></i>
                <span>Logout</span>
            </a>
        </div>
    </div>
</div>
