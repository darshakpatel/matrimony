@extends('web.layouts.member_panel')
@section('panel_content')
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">Ignored Members</h5>
        </div>
        <div class="card-body">
            <table class="table aiz-table mb-0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Location</th>
                    <th class="text-right">Options</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($ignored_members as $key => $ignored_member)

                    <tr id="ignored_member_{{ $ignored_member->user_id }}">
                        <td>{{ ($key+1) + ($ignored_members->currentPage() - 1)*$ignored_members->perPage() }}</td>
                        <td>
                            @if(asset($ignored_member->user->image) != null)
                                <img class="img-md" src="{{ asset($ignored_member->user->image) }}" height="45px"
                                     alt="Image">
                            @else
                                <img class="img-md" src="{{ asset('assets/img/avatar-place.png') }}" height="45px"
                                     alt="Image">
                            @endif
                        </td>
                        <td>
                            {{ $ignored_member->user->first_name.' '.$ignored_member->user->last_name }}
                        </td>
                        <td>{{ \Carbon\Carbon::parse($ignored_member->date_of_birth)->age }}</td>

                        <td>
                            @if(!empty($ignored_member->country))
                                {{ $ignored_member->country->name }}
                            @endif
                        </td>

                        <td class="text-right">
                            <a onclick="remove_from_ignored_list({{ $ignored_member->user_id }})"
                               class="btn btn-soft-success btn-icon btn-circle btn-sm"
                               title="Remove From Ignore List">
                                <i class="las la-check"></i>
                            </a>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
            <div class="aiz-pagination">
                {{ $ignored_members->links() }}
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script type="text/javascript">
        function remove_from_ignored_list(id) {
            $.post('{{ route('remove_from_ignored_list') }}',
                {
                    _token: '{{ csrf_token() }}',
                    id: id
                },
                function (data) {
                    if (data == 1) {
                        $("#ignored_member_" + id).hide();
                        AIZ.plugins.notify('success', 'You Have Removed This Member From Your Ignored list');
                    } else {
                        AIZ.plugins.notify('danger', 'Something went wrong');
                    }
                }
            );
        }

    </script>
@endsection
