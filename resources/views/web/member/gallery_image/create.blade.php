@extends('web.layouts.member_panel')
@section('panel_content')
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{ trans('messages.Add New Image to Gallery') }}</h5>
        </div>
        <div class="card-body">
          <form action="{{ route('gallery-image.store') }}" method="POST">
                @csrf
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="signinSrEmail">{{trans('messages.Image')}}</label>
                    <div class="col-md-9">
                        <div class="input-group" data-toggle="aizuploader" data-type="image">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-soft-secondary font-weight-medium">{{ trans('messages.Browse')}}</div>
                            </div>
                            <div class="form-control file-amount">{{ trans('messages.Choose File') }}</div>
                            <input type="hidden" name="gallery_image" class="selected-files" required>
                        </div>
                        <div class="file-preview box sm">
                        </div>
                    </div>
                </div>
                <div class="form-group row text-right">
                    <div class="col-md-11">
                        <button type="submit" class="btn btn-primary">{{trans('messages.Confirm')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
