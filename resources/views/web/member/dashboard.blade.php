@extends('web.layouts.member_panel')
@section('panel_content')
    @php $user = Auth::user(); @endphp
    <div class="row gutters-5">
        <div class="col-md-4 mx-auto mb-3">
            <a href="{{route('interest_requests')}}">
                <div class="bg-light rounded overflow-hidden text-center p-3">
                    <i class="la la-heart-o la-3x mb-3 text-primary-grad"></i>
                    <div class="h4 fw-700 text-primary-grad">{{ $interest_requests }}</div>
                    <div class="" style="color:black">Interest Requests</div>
                </div>
            </a>

        </div>
        <div class="col-md-4 mx-auto mb-3">
            <a href="{{route('my_interests.index')}}">
            <div class="bg-light rounded overflow-hidden text-center p-3">
                <i class="la la-heart-o la-3x mb-3 text-primary-grad"></i>
                <div class="h4 fw-700 text-primary-grad">{{$my_interests}}</div>
                <div class="" style="color:black">My Interests</div>
            </div>
            </a>
        </div>
        <div class="col-md-4 mx-auto mb-3">
            <a href="{{route('my_ignored_list')}}">
            <div class="bg-light rounded overflow-hidden text-center p-3">
                <i class="las la-image la-3x mb-3 text-primary-grad"></i>
                <div class="h4 fw-700 text-center text-primary-grad">{{$ingored_members}}</div>
                <div class="text-center" style="color:black">Ignored Members</div>
            </div>
            </a>
        </div>
    </div>
{{--    <div class="row gutters-5">--}}
{{--        <div class="col-md-12">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">--}}
{{--                    <h2 class="fs-16 mb-0">Matched profile</h2>--}}
{{--                </div>--}}
{{--                <div class="card-body">--}}
{{--                    <div>--}}
{{--                        @forelse ($similar_profiles->shuffle()->take(5) as $similar_profile)--}}
{{--                            @if($similar_profile->matched_profile != null)--}}
{{--                                <a href="{{ route('member_profile', $similar_profile->match_id) }}"--}}
{{--                                   class="text-reset border rounded row no-gutters align-items-center mb-3">--}}
{{--                                    <div class="col-auto w-100px">--}}
{{--                                        <img--}}
{{--                                                src="{{ asset($similar_profile->matched_profile->image) }}"--}}
{{--                                                onerror="this.onerror=null;this.src='{{ asset('assets/img/avatar-place.png') }}';"--}}
{{--                                                class="img-fit w-100 size-100px"--}}
{{--                                        >--}}
{{--                                    </div>--}}
{{--                                    <div class="col">--}}
{{--                                        <div class="p-3">--}}
{{--                                            <h5 class="fs-16 text-body text-truncate">{{ $similar_profile->matched_profile->first_name.' '.$similar_profile->matched_profile->last_name }}</h5>--}}
{{--                                            <div class="fs-12 text-truncate-3">--}}
{{--                                          <span class="mr-1 d-inline-block">--}}
{{--                                            @if(!empty($similar_profile->matched_profile->date_of_birth))--}}
{{--                                                  {{ \Carbon\Carbon::parse($similar_profile->matched_profile->date_of_birth)->age }}--}}
{{--                                                  Years.--}}
{{--                                                  ,--}}
{{--                                              @endif--}}
{{--                                          </span>--}}
{{--                                                <span class="mr-1 d-inline-block">--}}
{{--                                            @if(!empty($similar_profile->matched_profile->profile->height))--}}
{{--                                                        {{ $similar_profile->matched_profile->profile->height }} Feet--}}
{{--                                                        ,--}}
{{--                                                    @endif--}}
{{--                                          </span>--}}
{{--                                                <span class="mr-1 d-inline-block">--}}
{{--                                            @if(!empty($similar_profile->matched_profile->profile->marital_status))--}}
{{--                                                        {{ $similar_profile->matched_profile->member->marital_status }}--}}
{{--                                                        ,--}}
{{--                                                    @endif--}}
{{--                                          </span>--}}
{{--                                                <span class="mr-1 d-inline-block">--}}
{{--                                            {{ !empty($similar_profile->matched_profile->profile->self_intro_text) ? $similar_profile->matched_profile->profile->self_intro_text.', ' : "" }}--}}
{{--                                          </span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            @endif--}}
{{--                        @empty--}}
{{--                            <div--}}
{{--                                    class="alert alert-info">No data found--}}
{{--                            </div>--}}
{{--                        @endforelse--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


@endsection
