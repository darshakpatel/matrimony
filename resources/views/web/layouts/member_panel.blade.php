@extends('web.layouts.app')
@section('content')
<section class="mt-3 py-5 bg-white">
	<div class="container">
		<div class="mt-5 d-flex align-items-start">
			@include('web.member.sidebar')
			<div class="aiz-user-panel overflow-hidden">
				@yield('panel_content')
			</div>
		</div>
	</div>
</section>
@endsection

