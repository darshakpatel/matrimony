<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="app-url" content="{{ url('/') }}">
    <meta name="file-base-url" content="{{ public_path() }}">
    <!-- Title -->
    <title>Rajpurohit Bandhan</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content=""/>
    <meta name="keywords" content="">

    @yield('meta')


    <!-- Favicon -->
    <link rel="icon" href="">

    <!-- CSS -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap">
    <link rel="stylesheet" href="{{ asset('assets/css/vendors.css') }}">
    <link rel="stylesheet" href="{{ asset('user/assets/js/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('user/assets/js/sweet-alert/sweet-alert.css') }}">
    <link rel="stylesheet" href="{{ asset('user/assets/js/notify/notifIt.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/aiz-core.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css"/>
    <script>
        var AIZ = AIZ || {};
    </script>
    <style>
        body {
            font-family: 'Poppins', sans-serif;
            font-weight: 900;
        }

        /*.card, .custom-card {*/
        /*    background: lightgrey;*/
        /*    color: black!important;*/
        /*}*/

        :root {
            --primary: #7645D0;
            --hov-primary: #FD2C79;
            --soft-primary: rgba(253, 44, 121, 0.15);
            --secondary: #FF4646;
            --soft-secondary: rgba(255, 70, 70, 0.15);
        }

        .text-primary-grad {
            /* background: rgb(253, 41, 123); */
            background: -moz-linear-gradient(0deg, rgba(118, 69, 208, 1) 0%, rgba(255, 70, 70, 1) 100%);
            /* background: -webkit-linear-gradient(0deg, rgba(118,69,208,1) 0%, rgba(255,70,70,1) 100%); */
            background: linear-gradient(0deg, rgba(118, 69, 208, 1) 0%, rgba(255, 70, 70, 1) 100%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        .btn-primary,
        .bg-primary-grad {
            background: rgb(253, 41, 123);
            background: -moz-linear-gradient(225deg, rgba(118, 69, 208, 1) 0%, rgba(255, 70, 70, 1) 100%);
            background: -webkit-linear-gradient(225deg, rgba(118, 69, 208, 1) 0%, rgba(255, 70, 70, 1) 100%);
            background: linear-gradient(225deg, rgba(118, 69, 208, 1) 0%, rgba(255, 70, 70, 1) 100%);
        }

        .fill-dark {
            fill: #4d4d4d;
        }

        .fill-primary-grad stop:nth-child(1) {
            stop-color: rgba(255, 70, 70, 1);
        }

        .fill-primary-grad stop:nth-child(2) {
            stop-color: rgba(118, 69, 208, 1);
        }
    </style>

    {{--    {!! get_setting('header_script') !!}--}}
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-253003059-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-253003059-1');
    </script>

</head>

<body class="text-left">

<div
        class="aiz-main-wrapper d-flex flex-column position-relative @if(Route::currentRouteName() != 'home') pt-8 pt-lg-10 @endif bg-white">

    @include('web.inc.header')

    @yield('content')

    @include('web.inc.footer')

</div>
<div class="aiz-cookie-alert shadow-xl">
    <div class="p-3 bg-dark rounded">
        <div class="text-white mb-3">
            Notice. {{env('APP_NAME')}} uses cookies to provide necessary website functionality, improve your experience
            and
            analyze our traffic. By using our website, you agree to our Privacy Policy and our Cookies Policy.
        </div>
        <button class="btn btn-primary aiz-cookie-accepet">
            Ok. I Understood
        </button>
    </div>
</div>
@yield('modal')

<div class="modal fade account_status_change_modal" id="modal-zoom">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom">
        <div class="modal-content">
            <div class="modal-body text-center">
                <form class="form-horizontal member-block" action="{{ route('update_account_deactivation_status') }}"
                      method="GET">
                    @csrf
                    <input type="hidden" name="deactivation_status" id="deactivation_status" value="">
                    <h4 class="modal-title h6 mb-3" id="confirmation_note" value=""></h4>
                    <hr>
                    <button type="submit" class="status_btn btn btn-primary mt-2">Yes</button>
                    <button type="button" class="status_btn btn btn-danger mt-2"
                            data-dismiss="modal">No
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var APP_URL = '{{url('/')}}';
</script>
<script src="{{ asset('assets/js/vendors.js') }}"></script>
<script src="{{ asset('user/assets/js/custom/axios.min.js') }}"></script>
<script src="{{ asset('user/assets/js/blockUI.js') }}"></script>
<script src="{{ asset('user/assets/js/parsleyjs/parsley.min.js') }}"></script>
<script src="{{ asset('user/assets/js/select2/select2.min.js') }}"></script>
<script src="{{ asset('user/assets/js/notify/notifIt.js') }}"></script>
<script src="{{ asset('user/assets/js/sweet-alert/sweet-alert.min.js') }}"></script>
<script src="{{ asset('user/assets/js/custom/custom.js') }}"></script>
<script src="{{ asset('assets/js/aiz-core.js') }}?v={{time()}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
@yield('script')
<script>
    @if(Auth::check())
    function account_deactivation() {
        var status = '{{ Auth::user()->status }}'
        $('.account_status_change_modal').modal('show');
        if (status == 'active') {
            $('#deactivation_status').val('inActive');
            $('#confirmation_note').html('Do You Really Want To Deactivate Your Account');
        } else {
            $('#deactivation_status').val('active');
            $('#confirmation_note').html('Are You Sure To Reactive Your Account');
        }
    }

    @endif
</script>
<script>
    $('#india').click(function () {
        if ($(this).is(':checked')) {
            $(".india").show()
            $('#city_id').attr('required', true)
            $('#state_id').attr('required', true)
            $('#pincode').attr('required', true)
            $('#village').attr('required', true)
        }
    });

    $('#nri').click(function () {
        if ($(this).is(':checked')) {
            $(".india").hide()
            $('#city_id').val('')
            $('#state_id').val('')
            $('#pincode').val('')
            $('#village').val('')
            $('#city_id').attr('required', false)
            $('#state_id').attr('required', false)
            $('#pincode').attr('required', false)
            $('#village').attr('required', false)
        }
    });

    $('#nanaji_nri').click(function () {
        if ($(this).is(':checked')) {
            $(".nanaji_india").hide()
            $('#nanaji_city').val('')
            $('#nanaji_state').val('')
            $('#nanaji_village').val('')
            $('#nanaji_city').attr('required', false)
            $('#nanaji_state').attr('required', false)
            $('#nanaji_village').attr('required', false)
        }
    });

    $('#nanaji_india').click(function () {
        if ($(this).is(':checked')) {
            $(".nanaji_india").show()
            $('#nanaji_city').attr('required', true)
            $('#nanaji_state').attr('required', true)
            $('#nanaji_village').attr('required', true)
        }
    });
</script>
{{--{!! get_setting('footer_script') !!}--}}

</body>

</html>
