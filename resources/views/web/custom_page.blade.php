@extends('web.layouts.app')

@section('content')
<section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="mt-5 col-lg-12 text-center text-lg-center">
                <h1 class="fw-600 h4">{{ $page->name }}</h1>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
	<div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
		    {!! $page->description !!}
        </div>
	</div>
</section>
@endsection
