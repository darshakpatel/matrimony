@extends('web.layouts.app')

@section('content')
    <div class="py-4 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-xxl-4 col-xl-5 col-md-7 mx-auto">
                    <div class="card">
                        <div class="card-body">
                            <div class="mb-5 text-center">
                                <h3 class="h3 text-primary mb-0">Your email is successfully verified you can now login</h3>
                            </div>
                            <div class="mb-5">
                                <a href="{{route('login')}}"
                                   class="btn btn-block btn-primary">Login
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection
