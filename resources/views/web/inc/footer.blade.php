<footer class="aiz-footer fs-13 mt-auto text-white fw-400 pt-5">
    <div class="container">

        <div class="row mb-4">
            <div class="col-xxl-6 col-xl-7 col-lg-8 col-md-10 text-center mx-auto">
                <div class="logo mb-4">
                    <a href="{{ route('home') }}" class="d-inline-block py-15px">
                        {{--                        <img src="{{ asset('assets/img/logo.png') }}" alt="{{ env('APP_NAME') }}"--}}
                        {{--                             class="mw-100 h-30px h-md-40px" height="40">--}}
                        <h3>Rajpurohit Bandhan</h3>
                    </a>
                </div>
                <div class="opacity-60">
                    Matrimony Bureau
                </div>
            </div>
        </div>

        <div class="mb-4">
            <h4 class="text-uppercase  fs-14 border-bottom border-primary pb-4 mb-4">Pages</h4>
            <div class="row opacity-60 no-gutters">
                <div class="col-xl-3 col-md-3 mb-4">
                    <div class="mb-3">
                        <a class="text-white" href="{{url('page/terms-and-condition')}}">Terms and Conditions</a>
                    </div>
                    <div class="mb-3">
                        <a class="text-white" href="{{url('page/privacy-policy')}}">Privacy Policy</a>
                    </div>
                    <div class="mb-3">
                        <a class="text-white" href="{{url('page/about-us')}}">About Us</a>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 mb-4">
                    <div class="mb-3">
                        <a href="https://play.google.com/store/apps/details?id=com.rajpurohitmarriagebureau">
                            <img src="{{asset('assets/image/playstore.png')}}" style="max-width:150px">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-top border-primary pt-4 pb-7 pb-xl-4">
            <div class="row">
                <div class="col-lg-6">
                    <div class="lh-1">
                        Copyright {{date('Y')}} &copy; Rajpurohit Bandhan
                    </div>
                </div>
            </div>
        </div>

    </div>
</footer>


<div class="aiz-mobile-bottom-nav d-xl-none fixed-bottom bg-white shadow-lg border-top rounded-top"
     style="box-shadow: 0px -1px 10px rgb(0 0 0 / 15%)!important; ">
    <div class="row align-items-center gutters-5 text-center">
        <div class="col">
            <a href="{{url('/')}}"
               class="text-reset d-block flex-grow-1 text-center py-2">
                <i class="las la-home fs-18 opacity-60 opacity-100"></i>
                <span class="d-block fs-10 opacity-60 opacity-100 fw-600">Home</span>
            </a>
        </div>
        @if(Auth::check())
            <div class="col">
                <a href="{{route('memberListing')}}"
                   class="text-reset d-block flex-grow-1 text-center py-2 ">
              <span class="d-inline-block position-relative px-2">
                  <i class="las la-search fs-18 opacity-60 "></i>
                 </span>
                    <span class="d-block fs-10 opacity-60 ">Search</span>
                </a>
            </div>
        @endif
        <div class="col">
            <a href="{{route('my_interests.index')}}"
               class="text-reset d-block flex-grow-1 text-center py-2 ">
              <span class="d-inline-block position-relative px-2">
                  <i class="las la-comment-dots fs-18 opacity-60 "></i>
                 </span>
                <span class="d-block fs-10 opacity-60 ">Interests</span>
            </a>
        </div>
        <div class="col">
            <a href="{{Auth::check()?'javascript:void(0)':route('login')}}"
               class="text-reset d-block flex-grow-1 text-center py-2 mobile-side-nav-thumb"
               data-toggle="class-toggle" data-target=".aiz-mobile-side-nav">
                        <span class="d-block mx-auto mb-1 opacity-60">
                            @if(Auth::check())
                                <img src="{{asset(Auth::user()->image)}}"
                                     class="rounded-circle size-20px"
                                     onerror="this.onerror=null;this.src='{{asset('assets/img/avatar-place.png')}}';">
                            @else
                                <img src="{{asset('assets/img/avatar-place.png')}}"
                                     class="rounded-circle size-20px"
                                >
                            @endif
                        </span>
                @if(Auth::check())
                    <span class="d-block fs-10 opacity-60">Account</span>
                @else
                    <span class="d-block fs-10 opacity-60">Login</span>
                @endif
            </a>
        </div>
    </div>
</div>
@if (Auth::check())
    <div class="aiz-mobile-side-nav collapse-sidebar-wrap sidebar-xl d-xl-none z-1035">
        <div class="overlay dark c-pointer overlay-fixed" data-toggle="class-toggle" data-target=".aiz-mobile-side-nav"
             data-same=".mobile-side-nav-thumb"></div>
        <div class="collapse-sidebar bg-white">
            @include('web.member.sidebar')
        </div>
    </div>
@endif
