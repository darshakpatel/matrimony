<div class=" position-fixed  w-100 top-0 z-1020">
    <div class="top-navbar bg-white border-bottom z-1035 py-2  d-lg-block">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col">
                    <ul class="list-inline mb-0 d-flex align-items-center justify-content-end ">
                        <li class="list-inline-item mr-3 pr-3 border-right text-reset opacity-60">
                            <span>For queries:</span>
                            <?php
                            $email = DB::table('settings')->where('meta_key', 'support_email')->first()->meta_value;
                            ?>
                            <span>{{$email}}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <header
            class="aiz-header shadow-md bg-white border-gray-300">
        <div class="aiz-navbar position-relative">
            <div class="container">
                <div class="d-lg-flex justify-content-between text-center text-lg-left">
                    <div class="logo">
                        <a href="{{ route('home') }}" class="d-inline-block py-15px">
                            <img src="{{ asset('user/assets/image/only_image.jpg') }}" alt="{{ env('APP_NAME') }}"
                                 class="" height="62">
                            <img src="{{ asset('user/assets/image/text_only.jpg') }}" alt="{{ env('APP_NAME') }}"
                                 class="ml-1" height="62">
                        </a>
                    </div>
                    <ul
                            class="mb-0 pl-0 ml-lg-auto d-lg-flex align-items-stretch justify-content-center justify-content-lg-start mobile-hor-swipe">
                        <li class="d-inline-block d-lg-flex pb-1   {{ Request::is('/')?'bg-primary-grad':'' }} ">
                            <a class="nav-link text-uppercase fw-700 fs-15 d-flex align-items-center bg-white py-2"
                               href="https://play.google.com/store/apps/details?id=com.rajpurohitmarriagebureau">
                                <span class="text-primary-grad mb-n1"><img src="{{asset('assets/image/playstore.png')}}" style="max-width:150px"></span>
                            </a>
                        </li>
                        <li class="d-inline-block d-lg-flex pb-1   {{ Request::is('/')?'bg-primary-grad':'' }} ">
                            <a class="nav-link text-uppercase fw-700 fs-15 d-flex align-items-center bg-white py-2"
                               href="{{ route('home') }}">
                                <span class="text-primary-grad mb-n1">Home</span>
                            </a>
                        </li>
                        <li
                                class="d-inline-block d-lg-flex pb-1 {{ Request::is('page/about-us')?'bg-primary-grad':'' }} ">
                            <a class="nav-link text-uppercase fw-700 fs-15 d-flex align-items-center bg-white py-2"
                               href="{{url('page/about-us')}}">
                                <span class="text-primary-grad mb-n1">About Us</span>
                            </a>
                        </li>
                        <li
                                class="d-inline-block d-lg-flex pb-1 {{ Request::is('contact-us')?'bg-primary-grad':'' }} ">
                            <a class="nav-link text-uppercase fw-700 fs-15 d-flex align-items-center bg-white py-2"
                               href="{{route('contact-us')}}">
                                <span class="text-primary-grad mb-n1">Contact Us</span>
                            </a>
                        </li>
                        @if (Auth::check())
                            <li
                                    class="d-inline-block d-lg-flex pb-1 {{ Request::is('contact-us')?'bg-primary-grad':'' }} ">
                                <a class="nav-link text-uppercase fw-700 fs-15 d-flex align-items-center bg-white py-2"
                                   href="{{route('logout')}}">
                                    <span class="text-primary-grad mb-n1">Logout</span>
                                </a>
                            </li>
                        @endif
                        @if (!Auth::check())
                            <li class="d-inline-block d-lg-flex pb-1 {{ Request::is('login')?'bg-primary-grad':'' }} ">
                                <a class="nav-link text-uppercase fw-700 fs-15 d-flex align-items-center bg-white py-2"
                                   href="{{ route('login') }}">
                                    <span class="text-primary-grad mb-n1">Login</span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        @if (Auth::check())
            <div class="border-top d-none d-lg-block">
                <div class="container">
                    <ul class="list-inline d-flex align-items-center mb-0">
                        <li class="list-inline-item">
                            <a href="{{ route('dashboard') }}"
                               class="text-reset d-inline-block px-4 py-3 fw-600 {{ Request::is('dashboard')?'text-primary-grad opacity-100':'' }}">
                                <i class="las la-tachometer-alt mr-1"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{ route('profile_settings') }}"
                               class="text-reset d-inline-block px-4 py-3 fw-600 {{ Request::is('profile_settings')?'text-primary-grad opacity-100':'' }}">
                                <i class="las la-user mr-1"></i>
                                <span>My Profile</span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{route('my_interests.index')}}"
                               class="text-reset d-inline-block px-4 py-3 fw-600 {{ Request::is('my-interests')?'text-primary-grad opacity-100':'' }}">
                                <i class="la la-heart-o mr-1"></i>
                                <span>My Interest</span>
                            </a>
                        </li>

                        {{--                        <li class="list-inline-item">--}}
                        {{--                            <a href="#"--}}
                        {{--                               class="text-reset d-inline-block px-4 py-3 fw-600 ">--}}
                        {{--                                <i class="las la-envelope mr-1"></i>--}}
                        {{--                                <span>Messaging</span>--}}
                        {{--                            </a>--}}
                        {{--                        </li>--}}
                        <li class="list-inline-item">
                            <a href="{{route('my_ignored_list')}}"
                               class="text-reset d-inline-block px-4 py-3 fw-600 {{ Request::is('ignored-list')?'text-primary-grad opacity-100':'' }}">
                                <i class="las la-ban mr-1"></i>
                                <span>Ignored User List</span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{route('memberListing')}}"
                               class="text-reset d-inline-block px-4 py-3 fw-600 {{ Request::is('memberListing')?'text-primary-grad opacity-100':'' }}">
                                <i class="las la-search mr-1"></i>
                                <span>Search Profile</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    </header>
</div>
