@extends('admin.layouts.vertical', ['title' =>"Add Highest Education"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Highest Education</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" name="id" value="">
                            @csrf
                            <div class="form-group mb-3">
                                <label>{{ trans('messages.name') }}</label>
                                <input type="text" class="form-control" name="name"
                                       placeholder="{{ trans('messages.name') }}" required>
                                <div class="valid-feedback">
                                </div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}</button>
                            <a class="btn btn-dark"
                               href="{{route('admin.highestEducation.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>

    </script>
    <script src="{{ asset('admin/assets/js/custom/highestEducation.js') }}?v={{ time() }}"></script>

@endsection

