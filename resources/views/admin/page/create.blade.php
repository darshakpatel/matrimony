@extends('admin.layouts.vertical', ['title' => trans('messages.admin.page.add_page')])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">{{trans('messages.admin.page.add_page')}}</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" id="form-method" value="edit_value">
                            @csrf

                            @foreach($languages as $language)
                                <div class="form-group mb-3">
                                    <label>{{$language->name}} {{trans('messages.name')}}</label>
                                    <input type="text" class="form-control"
                                           name="name[{{$language->language_code}}]"
                                           placeholder="{{ $language->name }} {{trans('messages.name')}}" required>
                                    <div class="valid-feedback"></div>
                                </div>
                            @endforeach
                            @foreach($languages as $language)
                                <div class="form-group mb-3">
                                    <label>{{$language->name}} {{trans('messages.description')}}</label>
                                    <textarea class="form-control description"
                                              name="description[{{$language->language_code}}]"
                                              placeholder="{{ $language->name }} {{trans('messages.description')}}"
                                              required></textarea>
                                    <div class="valid-feedback"></div>
                                </div>
                            @endforeach
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.save')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.page.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/page.js') }}?v={{ time() }}
        "></script>
@endsection
