@extends('admin.layouts.vertical', ['title' => trans('messages.admin.page.edit_page')])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">{{trans('messages.admin.page.edit_page')}}</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="id" value="{{$page->id}}">
                            @csrf


                                <div class="form-group mb-3">
                                    <label>{{trans('messages.name')}}</label>
                                    <input type="text" class="form-control"
                                           name="name"
                                           value="{{ $page->name }}"
                                           placeholder="{{trans('messages.name')}}" required>
                                    <div class="valid-feedback"></div>
                                </div>

                                <div class="form-group mb-3">
                                    <label>{{trans('messages.description')}}</label>
                                    <textarea class="form-control description"
                                              name="description"
                                              placeholder="{{trans('messages.description')}}"
                                              required>{!! $page->description!!}</textarea>
                                    <div class="valid-feedback"></div>
                                </div>

                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.save')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.page.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/page.js') }}?v={{ time() }}
        "></script>
@endsection
