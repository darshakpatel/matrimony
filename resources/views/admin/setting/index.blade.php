@extends('admin.layouts.vertical', ['title' => 'Settings'])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Settings</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            @csrf
                            <div class="row">
                                @foreach($settings as $setting)
                                    <div class="col-6">
                                        @if($setting->type=='select')
                                            <div class="form-group mb-3">
                                                <label>{{ucfirst(str_replace('_',' ',$setting->meta_key))}}</label>
                                                <select class="form-control" name="{{$setting->meta_key}}"
                                                        value="{{$setting->meta_value}}"
                                                        required>
                                                    <option value="1"
                                                            @if($setting->meta_value==1) selected @endif>Yes
                                                    </option>
                                                    <option
                                                            value="0"
                                                            @if($setting->meta_value==0) selected @endif>No
                                                    </option>
                                                </select>
                                                <div class="valid-feedback"></div>
                                            </div>
                                        @else
                                            <div class="form-group mb-3">
                                                <label>{{ucfirst(str_replace('_',' ',$setting->meta_key))}}</label>
                                                <input type="text" class="form-control" name="{{$setting->meta_key}}"
                                                       value="{{$setting->meta_value}}"
                                                       placeholder="{{ucfirst(str_replace('_',' ',$setting->meta_key))}}"
                                                       required>
                                                <div class="valid-feedback"></div>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}</button>
                            <a class="btn btn-dark"
                               href="{{route('admin.dashboard')}}">{{trans('messages.cancel')}}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/settings.js') }}?v={{ time() }}"></script>
@endsection

