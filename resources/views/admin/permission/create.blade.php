@extends('admin.layouts.vertical', ['title' =>"Add Role"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Permission</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" name="edit_value" value="0">
                            @csrf
                            <div class="form-group">
                                <label for="module_name">Module Name<span
                                        class="error">*</span></label>
                                <input type="text" class="form-control" name="module_name" id="module_name"
                                       placeholder="admin" required/>
                                <div class="help-block with-errors error"></div>
                            </div>
                            <div class="form-group">
                                <label for="name">Choose Permission<span
                                        class="error">*</span></label><br>
                                <div class="form-check form-check-inline">
                                    <label class="ckbox mb-2">
                                        <input type="checkbox" id="all" name="all"
                                               value="1">
                                        <span>Select All</span>
                                    </label>
                                </div>
                                @foreach($array as $value)
                                    <div class="form-check form-check-inline">
                                        <label class="ckbox mb-2">
                                            <input type="checkbox" id="{{$value}}" name="{{$value}}"
                                                   value="1">
                                            <span>{{ $value }}</span>
                                        </label>
                                    </div>
                                @endforeach
                                <div class="help-block with-errors error"></div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}</button>
                            <a class="btn btn-dark"
                               href="{{route('admin.role.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/permission.js') }}?v={{ time() }}"></script>
@endsection

