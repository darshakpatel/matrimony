@extends('admin.layouts.vertical', ['title' =>"User Report"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">

                    </div>
                    <h4 class="page-title">User Report</h4>
                </div>
            </div>
        </div>
        <!-- Add rows table -->
        <section id="add-row">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table add-rows" id="data-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>Reported on</th>
                                            <th>Reported By</th>
                                            <th>Message</th>
                                        </tr>
                                        </thead>
                                        <tfoot class="thead-dark">
                                        <tr>
                                            <th>Reported on</th>
                                            <th>Reported By</th>
                                            <th>Message</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/report.js') }}?v={{ time() }}"></script>
@endsection
