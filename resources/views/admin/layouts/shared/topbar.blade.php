<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-right mb-0">


            {{--            <li class="dropdown notification-list topbar-dropdown">--}}
            {{--                <a class="nav-link dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"--}}
            {{--                   role="button" aria-haspopup="false" aria-expanded="false">--}}
            {{--                    <i class="fe-bell noti-icon"></i>--}}
            {{--                    <span class="badge badge-danger rounded-circle noti-icon-badge">9</span>--}}
            {{--                </a>--}}
            {{--                <div class="dropdown-menu dropdown-menu-right dropdown-lg">--}}
            {{--                    <div class="noti-scroll" data-simplebar>--}}

            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item active">--}}
            {{--                            <div class="notify-icon">--}}
            {{--                                <img src="{{asset('admin/assets/images/users/user-1.jpg')}}"--}}
            {{--                                     class="img-fluid rounded-circle" alt=""/>--}}
            {{--                            </div>--}}
            {{--                            <p class="notify-details">Admin</p>--}}
            {{--                        </a>--}}
            {{--                    </div>--}}

            {{--                    <!-- All-->--}}
            {{--                    <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">--}}
            {{--                        View all--}}
            {{--                        <i class="fe-arrow-right"></i>--}}
            {{--                    </a>--}}

            {{--                </div>--}}
            {{--            </li>--}}

            <li class="dropdown notification-list topbar-dropdown">
                <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown"
                   href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="{{asset('user/assets/image/logo_admin.png')}}" alt="user-image"
                         class="rounded-circle">
                    <span class="pro-user-name ml-1">
                        {{auth()->user()->name}} <i class="mdi mdi-chevron-down"></i>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                    <!-- item-->
                    <a href="{{route('admin.profile')}}" class="dropdown-item notify-item">
                        <i class="fe-user"></i>
                        <span>My Account</span>
                    </a>
                    <a href="{{route('admin.changePassword')}}" class="dropdown-item notify-item">
                        <i class="fe-user"></i>
                        <span>Change Password</span>
                    </a>
                    <a class="dropdown-item notify-item"

                       href="{{ route('admin.logout') }}"
                       onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"
                    >
                        <i class="fe-log-out"></i>
                        <span>Logout</span>

                    </a>

                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST">
                        @csrf
                    </form>

                </div>
            </li>
        </ul>

        <!-- LOGO -->
        <div class="logo-box" style="background-color:#fff">
            <a href="{{route('admin.dashboard')}}" class="logo logo-light text-center">
            <span class="logo-sm">
                 <img src="{{ asset('user/assets/image/only_image.jpg') }}" alt="{{ env('APP_NAME') }}"
                      class="" height="54">


{{--                <h5 class="mt-2">Rajpurohit</h5>--}}
            </span>
                <span class="logo-lg ">
              <img src="{{ asset('user/assets/image/only_image.jpg') }}" alt="{{ env('APP_NAME') }}"
                   class="" height="54">
                            <img src="{{ asset('user/assets/image/text_only.jpg') }}" alt="{{ env('APP_NAME') }}"
                                 class="ml-1" height="54">
{{--                    <h4 class="">Rajpurohit Bandhan</h4>--}}
            </span>
            </a>
        </div>

        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
            <li>
                <button class="button-menu-mobile waves-effect waves-light">
                    <i class="fe-menu"></i>
                </button>
            </li>

            <li>
                <!-- Mobile menu toggle (Horizontal Layout)-->
                <a class="navbar-toggle nav-link" data-toggle="collapse" data-target="#topnav-menu-content">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<!-- end Topbar -->
