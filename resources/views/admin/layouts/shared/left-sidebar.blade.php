<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">
    <div class="h-100" data-simplebar>
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul id="side-menu">
                <li class="menu-title">Navigation</li>
                <li>
                    <a href="{{route('admin.dashboard')}}">
                        <i data-feather="airplay"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                @if(auth()->user()->can('state-read') || auth()->user()->can('city-read'))
                    <li>
                        <a href="#country" data-toggle="collapse">
                            <i class="fa fa-globe"></i>
                            <span>Global Masters</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="country">
                            <ul class="nav-second-level">
                                @if(auth()->user()->can('state-read'))
                                    <li>
                                        <a href="{{route('admin.state.index')}}">States</a>
                                    </li>
                                @endif
{{--                                @if(auth()->user()->can('city-read'))--}}
{{--                                    <li>--}}
{{--                                        <a href="{{route('admin.city.index')}}">Cities</a>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
                            </ul>
                        </div>
                    </li>
                @endif
                @if(auth()->user()->can('user-read'))
                    <li>
                        <a href="#user" data-toggle="collapse">
                            <i class="fa fa-user"></i>
                            <span>Members</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="user">
                            <ul class="nav-second-level">
                                <li>
                                    <a href="{{route('admin.user.index')}}">All Members</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.approvedUser')}}">Approved Members</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.pendingUser')}}">Pending Members</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.incompleteUser')}}">Incomplete Profile Members</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif
                @if(auth()->user()->can('pending-admin-read') || auth()->user()->can('approved-admin-read') || auth()->user()->can('sub-admin-create'))
                    <li>
                        <a href="#subAdmin" data-toggle="collapse">
                            <i class="fa fa-user-tie"></i>
                            <span>Sub Admins</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="subAdmin">
                            <ul class="nav-second-level">
                                @if(auth()->user()->can('pending-admin-read'))
                                    <li>
                                        <a href="{{route('admin.subAdmin.pending')}}">Pending</a>
                                    </li>
                                @endif
                                @if(auth()->user()->can('approved-admin-read'))
                                    <li>
                                        <a href="{{route('admin.subAdmin.approved')}}">Approved</a>
                                    </li>
                                @endif
                                @if(auth()->user()->can('sub-admin-create'))
                                    <li>
                                        <a href="{{route('admin-form')}}" target="_blank">New Registration</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif
                @if(auth()->user()->can('role-read'))
                    <li>
                        <a href="#role" data-toggle="collapse">
                            <i class="fa fa-user-shield"></i>
                            <span>Roles</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="role">
                            <ul class="nav-second-level">
                                @if(auth()->user()->can('role-read'))
                                    <li>
                                        <a href="{{route('admin.role.index')}}">Roles</a>
                                    </li>
                                @endif
                                @if(auth()->user()->can('role-create'))
                                    <li>
                                        <a href="{{route('admin.role.create')}}">Add Role</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif
                @if(auth()->user()->can('admin-log-read'))
                    <li>
                        <a href="{{route('admin.adminLog.index')}}">
                            <i class="fa fa-history"></i>
                            <span>Admin Activities</span>
                        </a>
                    </li>
                @endif
                @if(auth()->user()->can('admin-log-read'))
                    <li>
                        <a href="{{route('admin.report')}}">
                            <i class="fa fa-history"></i>
                            <span>Reports</span>
                        </a>
                    </li>
                @endif
                @if(auth()->user()->can('slider-read'))
                    <li>
                        <a href="#slider" data-toggle="collapse">
                            <i class="fas fa-chart-area"></i>
                            <span>Sliders</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="slider">
                            <ul class="nav-second-level">
                                @if(auth()->user()->can('slider-create'))
                                    <li>
                                        <a href="{{route('admin.slider.create')}}">Add Banner</a>
                                    </li>
                                @endif
                                @if(auth()->user()->can('slider-read'))
                                    <li>
                                        <a href="{{route('admin.slider.index')}}">Banners</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif
                @if(auth()->user()->can('slider-read'))
                    <li>
                        <a href="#banner" data-toggle="collapse">
                            <i class="fas fa-chart-area"></i>
                            <span>Home Banners</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="banner">
                            <ul class="nav-second-level">
                                @if(auth()->user()->can('slider-create'))
                                    <li>
                                        <a href="{{route('admin.homeBanner.create')}}">Add Home Banner</a>
                                    </li>
                                @endif
                                @if(auth()->user()->can('slider-read'))
                                    <li>
                                        <a href="{{route('admin.homeBanner.index')}}">Home Banners</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif
                @if(auth()->user()->can('gotra-read'))
                    <li>
                        <a href="#gotra" data-toggle="collapse">
                            <i class="fas fa-chart-area"></i>
                            <span>Gotras</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="gotra">
                            <ul class="nav-second-level">
                                @if(auth()->user()->can('gotra-create'))
                                    <li>
                                        <a href="{{route('admin.gotra.create')}}">Add Gotra</a>
                                    </li>
                                @endif
                                @if(auth()->user()->can('gotra-read'))
                                    <li>
                                        <a href="{{route('admin.gotra.index')}}">Gotras</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif

                @if(auth()->user()->can('highest-education-read'))
                    <li>
                        <a href="#highestEducation" data-toggle="collapse">
                            <i class="fa fa-book"></i>
                            <span>Highest Education</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="highestEducation">
                            <ul class="nav-second-level">
                                @if(auth()->user()->can('highest-education-create'))
                                    <li>
                                        <a href="{{route('admin.highestEducation.create')}}">Add Highest Education</a>
                                    </li>
                                @endif
                                @if(auth()->user()->can('highest-education-read'))
                                    <li>
                                        <a href="{{route('admin.highestEducation.index')}}">Highest Educations</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif
{{--                @if(auth()->user()->can('mangalik-read'))--}}
{{--                    <li>--}}
{{--                        <a href="#mangalik" data-toggle="collapse">--}}
{{--                            <i class="fas fa-chart-area"></i>--}}
{{--                            <span>Mangaliks</span>--}}
{{--                            <span class="menu-arrow"></span>--}}
{{--                        </a>--}}
{{--                        <div class="collapse" id="mangalik">--}}
{{--                            <ul class="nav-second-level">--}}
{{--                                @if(auth()->user()->can('mangalik-create'))--}}
{{--                                    <li>--}}
{{--                                        <a href="{{route('admin.mangalik.create')}}">Add Mangalik</a>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                @if(auth()->user()->can('mangalik-read'))--}}
{{--                                    <li>--}}
{{--                                        <a href="{{route('admin.mangalik.index')}}">Mangaliks</a>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                @endif--}}
                @if(auth()->user()->can('work-profile-read'))
                    <li>
                        <a href="#workProfile" data-toggle="collapse">
                            <i class="fas fa-chart-area"></i>
                            <span>Work Profiles</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="workProfile">
                            <ul class="nav-second-level">
                                @if(auth()->user()->can('work-profile-create'))
                                    <li>
                                        <a href="{{route('admin.workProfile.create')}}">Add Work Profile</a>
                                    </li>
                                @endif
                                @if(auth()->user()->can('work-profile-read'))
                                    <li>
                                        <a href="{{route('admin.workProfile.index')}}">Work Profiles</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif
                @if(auth()->user()->can('page-read'))
                    <li>
                        <a href="#setting" data-toggle="collapse">
                            <i class="fa fa-cogs"></i>
                            <span>Site Settings</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="setting">
                            <ul class="nav-second-level">
                                <li>
                                    <a href="{{route('admin.page.index')}}">Pages</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.template.index')}}">Templates</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.setting')}}">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
