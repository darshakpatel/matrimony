<!-- bundle -->
<!-- Vendor js -->
<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/admin')) !!};
</script>
<script src="{{asset('admin/assets/js/vendor.min.js')}}"></script>
<script src="{{asset('admin/assets/libs/sweetalert2/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('admin/assets/libs/sweetalert2/sweet-alerts.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="{{ asset('admin/assets/js/custom/axios.min.js') }}"></script>
<!-- App js -->
<script src="{{asset('admin/assets/libs/dropify/js/dropify.min.js')}}"></script>


<script src="{{asset('admin/assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/assets/libs/datatables.net-select/js/dataTables.select.min.js')}}"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.4.11/jquery.autocomplete.min.js"></script>
<script src="{{asset('admin/assets/libs/multiselect/jquery.multi-select.js')}}"></script>


<script src="{{asset('admin/assets/libs/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{ asset('admin/assets/js/custom/custom.js') }}?v={{ time() }}"></script>
<script src="{{asset('admin/assets/js/app.min.js')}}"></script>
<script src="{{asset('admin/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('admin/assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('admin/assets/datepicker/js/datepicker.js')}}" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/i18n/datepicker.en.js"
        charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"
        integrity="sha512-dTu0vJs5ndrd3kPwnYixvOCsvef5SGYW/zSSK4bcjRBcZHzqThq7pt7PmCv55yb8iBvni0TSeIDV8RYKjZL36A=="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/jquery-clockpicker.min.js"></script>
<!-- App js -->
@yield('script')
<script>
    $('.dropify').dropify()
    $('.select2').select2()
</script>
@yield('script-bottom')
