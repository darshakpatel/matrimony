<table class="table table-bordered mb-0" aria-describedby="user-order-data">
    <thead class="thead-dark">
    <tr>
        <th scope="row" class="text-center" colspan="3">Basic Info</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">Full Name</th>
        <td>{{$subAdmin->name}}</td>
    </tr>
    <tr>
        <th scope="col">Image</th>
        <td>
            @if(!empty($subAdmin->image))
                <img src="{{url($subAdmin->image)}}" style="max-height: 90px" alt="user image"/>
            @endif
        </td>
    </tr>
    <tr>
        <th scope="row">Mobile No</th>
        <td>{{$subAdmin->mobile_no}}</td>
    </tr>
    <tr>
        <th scope="row">Father Name</th>
        <td>{{$subAdmin->father_name}}</td>
    </tr>
    <tr>
        <th scope="row">State</th>
        <td>{{$subAdmin->state?$subAdmin->state->name:''}}</td>
    </tr>
    <tr>
        <th scope="row">City</th>
        <td>{{$subAdmin->city?$subAdmin->city->name:''}}</td>
    </tr>
    <tr>
        <th scope="row">Address</th>
        <td>{{$subAdmin->address}}</td>
    </tr>
    <tr>
        <th scope="row">Pincode</th>
        <td>{{$subAdmin->pincode}}</td>
    </tr>
    <tr>
        <th scope="row">Current Place</th>
        <td>{{$subAdmin->current_place}}</td>
    </tr>
    <tr>
        <th scope="col">Aadhar Card</th>
        <td>
            @if(!empty($subAdmin->aadhar_card))
                <img src="{{url($subAdmin->aadhar_card)}}" style="max-height: 90px" alt="user image"/>
            @endif
        </td>
    </tr>
    </tbody>
</table>
<br>
