@extends('admin.layouts.vertical', ['title' =>"Edit Banner"])

@section('content')

    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Edit Banner</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$slider->id}}">
                            @csrf

                            <div class="row">
                                <div class="col-12 form-group mb-3">
                                    <label>{{ trans('messages.image') }}</label>
                                    <input type="file" class="form-control dropify" name="image"
                                           data-default-file="{{url($slider->image)}}"
                                           >
                                    <div class="valid-feedback">
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}</button>
                            <a class="btn btn-dark"
                               href="{{route('admin.slider.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/slider.js') }}?v={{ time() }}"></script>
@endsection

