<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layouts.shared.title-meta', ['title' => trans('messages.title_password_reset')])

    @include('admin.layouts.shared.head-css')
</head>
<body class="authentication-bg authentication-bg-pattern">
<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card bg-pattern">
                    <div class="card-body p-4">
                        <div class="text-center w-75 m-auto">
                            <div class="auth-logo">
                                <a href="#" class="logo logo-dark text-center">
                                    <span class="logo-lg">
                                        <img src="{{asset('admin/assets/images/main_logo.png')}}" alt="" height="50">
                                    </span>
                                </a>
                            </div>
                        </div>
                        <form action="{{route('resetPasswordSubmit')}}" method="POST" novalidate>
                            @csrf
                            @if (session('success_message'))
                                <div class="mt-3 alert alert-success">
                                    {{ session('success_message') }}
                                </div>
                            @endif
                            @if (session('error_message'))
                                <div class="mt-3 alert alert-danger">
                                    {{ session('error_message') }}
                                </div>
                            @endif
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group">
                                <label for="email">{{trans('messages.email')}}</label>
                                <input class="form-control @if($errors->has('email')) is-invalid @endif"
                                       name="email" type="email" required="" value="{{old('email')}}"
                                       id="email" placeholder="{{trans('messages.email')}}"/>
                            </div>

                            <div class="form-group">
                                <label for="password">{{trans('messages.password')}}</label>
                                <div
                                    class="input-group input-group-merge @if($errors->has('password')) is-invalid @endif">
                                    <input class="form-control @if($errors->has('password')) is-invalid @endif"
                                           name="password" type="password" required=""
                                           id="password" placeholder="{{trans('messages.password')}}"/>
                                    <div class="input-group-append" data-password="false">
                                        <div class="input-group-text">
                                            <span class="password-eye"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="password-confirm">{{ trans('messages.confirm_password') }}</label>
                                <input id="password-confirm" type="password"
                                       class="form-control @if($errors->has('password_confirmation')) is-invalid @endif"
                                       placeholder="{{ trans('messages.confirm_password') }}"
                                       name="password_confirmation" required="" autocomplete="new-password">
                            </div>
                            <div class="form-group mb-0 text-center">
                                <button class="btn btn-primary btn-block"
                                        type="submit">{{trans('messages.reset')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.layouts.shared.footer-script')
</body>
</html>
