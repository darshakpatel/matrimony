<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layouts.shared.title-meta', ['title' => trans('messages.login')])

    @include('admin.layouts.shared.head-css')
</head>

<body class="authentication-bg authentication-bg-pattern">

<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card bg-pattern">
                    <div class="card-body p-4">
                        <div class="text-center w-75 m-auto">
                            <div class="auth-logo">
                                <a href="#" class="logo logo-dark text-center">
                                    <span class="logo-lg">
                                       <img src="{{ asset('user/assets/image/only_image.jpg') }}" alt="{{ env('APP_NAME') }}"
                                            class="" height="62">
                            <img src="{{ asset('user/assets/image/text_only.jpg') }}" alt="{{ env('APP_NAME') }}"
                                 class="ml-1" height="62">
{{--                                        <h3>Rajpurohit Marriage Bureau</h3>--}}
                                    </span>
                                </a>
                            </div>
                            <p class="text-muted mb-4 mt-3">
                                Enter your email address and password to access admin panel.</p>
                        </div>
                        <form action="{{route('admin.login')}}" method="POST" novalidate>
                            @csrf
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            <div class="form-group mb-3">
                                <label for="email">Email or Mobile Number</label>
                                <input class="form-control  @if($errors->has('email')) is-invalid @endif" name="email"
                                       type="text" required=""
                                       value="{{ old('email')}}"
                                       placeholder="{{trans('messages.email')}}"/>

                                @if($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group mb-3">


                                <label for="password">{{trans('messages.password')}}</label>
                                <div
                                    class="input-group input-group-merge @if($errors->has('password')) is-invalid @endif">
                                    <input class="form-control @if($errors->has('password')) is-invalid @endif"
                                           name="password" type="password" required=""
                                           id="password" placeholder="{{trans('messages.password')}}"/>
                                    <div class="input-group-append" data-password="false">
                                        <div class="input-group-text">
                                            <span class="password-eye"></span>
                                        </div>
                                    </div>
                                </div>
                                @if($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="checkbox-signin" checked>
                                    <label class="custom-control-label"
                                           for="checkbox-signin">{{trans('messages.remember_me')}}</label>
                                </div>
                            </div>

                            <div class="form-group mb-0 text-center">
                                <button class="btn btn-primary btn-block"
                                        type="submit">{{trans('messages.login')}}</button>
                            </div>

                        </form>

                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->


@include('admin.layouts.shared.footer-script')

</body>
</html>
