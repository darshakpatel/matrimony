@extends('admin.layouts.vertical', ['title' =>"Add Role"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Role</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" name="edit_value" value="0">
                            @csrf
                            <div class="row">
                                <div class="col-12 form-group mb-3">
                                    <label>{{ trans('messages.name') }}</label>
                                    <input type="text" class="form-control" name="name"
                                           placeholder="{{ trans('messages.name') }}" required>
                                    <div class="valid-feedback">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered mg-b-0 text-md-nowrap">
                                            @foreach($permissions as $permission)
                                                <tr>
                                                    <th scope="row">{{ucfirst($permission->module_name)}}
                                                    </th>
                                                    <?php
                                                    $pers = \Spatie\Permission\Models\Permission::where('module_name', $permission->module_name)->get();
                                                    ?>
                                                    @foreach($pers as $per)
                                                        <td>
                                                            <div class="form-check form-check-inline">
                                                                <label class="ckbox mb-2">
                                                                    <input type="checkbox" name="permission[]"
                                                                           id="{{$permission->module_name}}"
                                                                           value="{{$per->id}}">
                                                                    <span>{{$per->name}}</span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}</button>
                            <a class="btn btn-dark"
                               href="{{route('admin.role.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/role.js') }}?v={{ time() }}"></script>
@endsection

