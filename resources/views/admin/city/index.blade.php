@extends('admin.layouts.vertical', ['title' => trans('messages.admin.city.city')])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <a href="{{route('admin.city.create')}}" class="btn btn-primary">Add New
                        </a>
                    </div>
                    <h4 class="page-title">{{trans('messages.admin.city.city')}}</h4>
                </div>
            </div>
        </div>
        <!-- Add rows table -->
        <section id="add-row">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table add-rows" id="data-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>{{trans('messages.country')}}</th>
                                            <th>{{trans('messages.state')}}</th>
                                            <th>{{trans('messages.name')}}</th>
                                            <th>{{trans('messages.status')}}</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tfoot class="thead-dark">
                                        <tr>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>{{trans('messages.country')}}</th>
                                            <th>{{trans('messages.state')}}</th>
                                            <th>{{trans('messages.name')}}</th>
                                            <th>{{trans('messages.status')}}</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
    <script>
        const city_destroy = '{{trans('messages.admin.city.city_destroy')}}'
        const active_city = '{{trans('messages.admin.city.active_city')}}'
        const inactive_city = '{{trans('messages.admin.city.inactive_city')}}'
        const status_message = '{{trans('messages.status_message')}}'
        const destroy_warning = '{{trans('messages.destroy_warning')}}'
    </script>
    <script src="{{ asset('admin/assets/js/custom/cities.js') }}?v={{ time() }}"></script>
@endsection
