@extends('admin.layouts.vertical', ['title' =>trans('messages.admin.city.add_city')])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">{{trans('messages.admin.city.add_city')}}</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" name="id" value="">
                            @csrf
                            <div class="form-group mb-3">
                                <label>{{ __('messages.country')}}</label>
                                <select id="country_id" class="form-control"
                                        name="country_id" required>
                                    <option>{{ __('messages.select_option')}}</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group mb-3">
                                <label>{{ __('messages.state')}}</label>
                                <select id="state_id" class="form-control"
                                        name="state_id" required>
                                    <option>{{ __('messages.select_option')}}</option>
                                </select>
                            </div>

                            <div class="form-group mb-3">
                                <label>{{ trans('messages.name') }}</label>
                                <input type="text" class="form-control" name="name"
                                       placeholder="{{ trans('messages.name') }}" required>
                                <div class="valid-feedback">
                                </div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}</button>
                            <a class="btn btn-dark"
                               href="{{route('admin.city.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script>

    </script>
    <script src="{{ asset('admin/assets/js/custom/cities.js') }}?v={{ time() }}"></script>

@endsection

