@extends('admin.layouts.vertical', ['title' => trans('messages.admin.state.state')])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">

                    </div>
                    <h4 class="page-title">{{trans('messages.admin.state.state')}}</h4>
                </div>
            </div>
        </div>
        <!-- Add rows table -->
        <section id="add-row">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table add-rows" id="data-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>{{trans('messages.country')}}</th>
                                            <th>{{trans('messages.name')}}</th>
                                            <th>{{trans('messages.status')}}</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tfoot class="thead-dark">
                                        <tr>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>{{trans('messages.country')}}</th>
                                            <th>{{trans('messages.name')}}</th>
                                            <th>{{trans('messages.status')}}</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
    <script>
        const state_destroy = '{{trans('messages.state_destroy')}}'
        const active_state = '{{trans('messages.admin.state.active_state')}}'
        const inactive_state = '{{trans('messages.admin.state.inactive_state')}}'
        const status_message = '{{trans('messages.status_message')}}'
        const destroy_warning = '{{trans('messages.destroy_warning')}}'
    </script>
    <script src="{{ asset('admin/assets/js/custom/state.js') }}?v={{ time() }}"></script>
@endsection
