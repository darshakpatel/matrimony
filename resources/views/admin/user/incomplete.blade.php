@extends('admin.layouts.vertical', ['title' =>'Incomplete Profile Members'])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Incomplete Profile Members</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table add-rows" id="addrows">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>{{trans('messages.id')}}</th>
                                        <th>{{trans('messages.name')}}</th>
                                        <th>{{trans('messages.email')}}</th>
                                        <th>{{trans('messages.mobile_no')}}</th>
                                        <th>{{trans('messages.date')}}</th>
                                        <th>{{trans('messages.status')}}</th>
                                        <th>{{trans('messages.action')}}</th>
                                    </tr>
                                    </thead>
                                    <tfoot class="thead-dark">
                                    <tr>
                                        <th>{{trans('messages.id')}}</th>
                                        <th>{{trans('messages.name')}}</th>
                                        <th>{{trans('messages.email')}}</th>
                                        <th>{{trans('messages.mobile_no')}}</th>
                                        <th>{{trans('messages.date')}}</th>
                                        <th>{{trans('messages.status')}}</th>
                                        <th>{{trans('messages.action')}}</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var type = 'incomplete';
        const destroy_user = '{{trans('messages.admin.user.destroy_user')}}'
        const delete_message = '{{trans('messages.delete_message')}}'
        const active_user = '{{trans('messages.admin.user.active_user')}}'
        const inactive_user = '{{trans('messages.admin.user.inactive_user')}}'
    </script>
    <script src="{{ asset('admin/assets/js/custom/user.js') }}?v={{ time() }}"></script>

@endsection
