@extends('admin.layouts.vertical', ['title' => 'Member Details'])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Member Details</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
        <div class="row">
            <div class="col-xl-6">
                <!-- project card -->
                <h4 class="page-title">Personal Details</h4>
                <div class="card d-block">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">First Name</th>
                                        <td>{{$user->first_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Last Name</th>
                                        <td>{{$user->last_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>{{$user->email}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Mobile No</th>
                                        <td>{{$user->mobile_no}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Gender</th>
                                        <td>{{$user->gender}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Date of Birth</th>
                                        <td>{{$user->date_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Contact No</th>
                                        <td>{{$user->profile->contact_no}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Self Intro Text</th>
                                        <td>{{$user->profile->self_intro_text}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Self Intro Video</th>
                                        <td>
                                            @if(!empty($user->profile->self_intro_video))
                                                <video width="320" height="240" controls>
                                                    <source src="{{url($user->profile->self_intro_video)}}"
                                                            type="video/mp4">
                                                </video>
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div>
            <div class="col-xl-6" style="margin-top: 38px;">
                <div class="card d-block">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Cadidate's Profile Photo</th>
                                        <td><a target="_blank" href="{{asset($user->image)}}"><img src="{{asset($user->image)}}" style="max-width: 200px"/></a></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Marital Status</th>
                                        <td>{{$user->profile->marital_status}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Mangalik</th>
                                        <td>{{$user->profile->mangalikName?$user->profile->mangalikName->name: ''}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Gotra</th>
                                        <td>{{$user->profile->gotra}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Time of birth</th>
                                        <td>{{$user->profile->time_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Place of birth</th>
                                        <td>{{$user->profile->place_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Height</th>
                                        <td>{{$user->profile->height}}.{{$user->profile->height_inch}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Hobby</th>
                                        <td>{{$user->hobbies?$user->hobbies->hobbies:""}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Facebook</th>
                                        <td>{{$user->profile->facebook_url}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Linkedin</th>
                                        <td>{{$user->profile->linkedin_url}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Twiiter</th>
                                        <td>{{$user->profile->twitter_url}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Instagram</th>
                                        <td>{{$user->profile->instagram_url}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Youtube</th>
                                        <td>{{$user->profile->youtube_url}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Cadidate's / Father's Aadhar card - Front page</th>
                                        <td>
                                            <a target="_blank" href="{{asset($user->profile->aadhar_photo)}}">
                                                <img src="{{asset($user->profile->aadhar_photo)}}"
                                                     style="max-width: 200px;"/>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Cadidate's / Father's Aadhar card - Back page</th>
                                        <td>
                                            <a target="_blank" href="{{asset($user->profile->aadhar_back_photo)}}">
                                                <img src="{{asset($user->profile->aadhar_back_photo)}}"
                                                     style="max-width: 200px;"/>
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div>


            <div class="col-xl-6">
                <!-- project card -->
                <h4 class="page-title">Family Details</h4>
                <div class="card d-block">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Father Name</th>
                                        <td>{{$user->profile->father_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Father Occupation</th>
                                        <td>{{$user->profile->father_profession}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Father Mobile No</th>
                                        <td>{{$user->profile->father_no}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Mother Name</th>
                                        <td>{{$user->profile->mother_name}}</td>
                                    </tr>

                                    <tr>
                                        <th scope="row">Mother Mobile No</th>
                                        <td>{{$user->profile->mother_no}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Grand Father Name</th>
                                        <td>{{$user->profile->grand_father_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Brother Married</th>
                                        <td>{{$user->profile->brother_married}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Brother Unmarried</th>
                                        <td>{{$user->profile->brother_unmarried}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Sister Married</th>
                                        <td>{{$user->profile->sister_married}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Sister Unmarried</th>
                                        <td>{{$user->profile->sister_unmarried}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div>
            <div class="col-xl-6" style="margin-top: 38px;">
                <div class="card d-block">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Nanaji Name</th>
                                        <td>{{$user->profile->nanaji_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nanihal Gotra</th>
                                        <td>{{$user->profile->nanaji_gotra}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nanihal Address</th>
                                        <td>{{$user->profile->nanaji_address}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nanihal Village / City</th>
                                        <td>{{$user->profile->nanaji_village}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nanihal District</th>
                                        <td>{{$user->profile->city?$user->profile->city->name:''}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nanihal State</th>
                                        <td>{{$user->profile->state?$user->profile->state->name:''}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nanihal Country</th>
                                        @if($user->profile->nanaji_is_nri==1)
                                            <td>NRI</td>
                                        @else
                                            <td>{{$user->profile->country?$user->profile->country->name:''}}</td>
                                        @endif
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div>

            <div class="col-xl-6">
                <!-- project card -->
                <h4 class="page-title">Current Address Details</h4>
                <div class="card d-block">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Address</th>
                                        <td>{{$user->address}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">City/Village</th>
                                        <td>{{$user->village}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Pincode</th>
                                        <td>{{$user->pincode}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">District</th>
                                        <td>{{$user->city?$user->city->name:""}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">State</th>
                                        <td>{{$user->state?$user->state->name:""}}</td>
                                    </tr>

                                    <tr>
                                        <th scope="row">Country</th>
                                        @if($user->is_nri==1)
                                            <td>NRI</td>
                                        @else
                                            <td>{{$user->country?$user->country->name:""}}</td>
                                        @endif
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <!-- project card -->
                <h4 class="page-title">Permanent Address Details</h4>
                <div class="card d-block">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Native Village / City</th>
                                        <td>{{$user->info->native_village}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Pincode</th>
                                        <td>{{$user->info->pincode}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">District</th>
                                        <td>{{$user->info->city?$user->info->city->name:""}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">State</th>
                                        <td>{{$user->info->state?$user->info->state->name:""}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Country</th>
                                        <td>{{$user->info->country?$user->info->country->name:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12">
                <!-- project card -->
                <h4 class="page-title">Education Details</h4>
                <div class="card d-block">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Highest Education</th>
                                        <td>{{$user->info->higherEducation?$user->info->higherEducation->name:''}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Highest Education Qualification Certificate</th>
                                        <td><a href="{{asset($user->info->higher_education_pdf)}}"
                                               class="btn btn-primary"
                                               target="_blank">View</a></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Work Profile</th>
                                        <td>{{$user->info->workProfile?$user->info->workProfile->name:''}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Degree</th>
                                        <td>{{$user->info->degree}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--            <div class="col-xl-6">--}}
            {{--                <!-- project card -->--}}
            {{--                <h4 class="page-title">Reference Details</h4>--}}
            {{--                <div class="card d-block">--}}
            {{--                    <div class="card-body">--}}
            {{--                        <div class="clearfix"></div>--}}
            {{--                        <div class="row">--}}
            {{--                            <div class="table-responsive">--}}
            {{--                                <table class="table table-bordered mb-0">--}}
            {{--                                    <tbody>--}}
            {{--                                    <tr>--}}
            {{--                                        <th scope="row">Reference 1 name</th>--}}
            {{--                                        <td>{{$user->profile->reference_1_name}}</td>--}}
            {{--                                    </tr>--}}
            {{--                                    <tr>--}}
            {{--                                        <th scope="row">Reference 1 Mobile no</th>--}}
            {{--                                        <td>{{$user->profile->reference_1_mobile_no}}</td>--}}
            {{--                                    </tr>--}}
            {{--                                    <tr>--}}
            {{--                                        <th scope="row">Reference 1 Village</th>--}}
            {{--                                        <td>{{$user->profile->reference_1_village}}</td>--}}
            {{--                                    </tr>--}}
            {{--                                    <tr>--}}
            {{--                                        <th scope="row">Reference 2 name</th>--}}
            {{--                                        <td>{{$user->profile->reference_2_name}}</td>--}}
            {{--                                    </tr>--}}
            {{--                                    <tr>--}}
            {{--                                        <th scope="row">Reference 2 Mobile no</th>--}}
            {{--                                        <td>{{$user->profile->reference_2_mobile_no}}</td>--}}
            {{--                                    </tr>--}}
            {{--                                    <tr>--}}
            {{--                                        <th scope="row">Reference 2 Village</th>--}}
            {{--                                        <td>{{$user->profile->reference_2_village}}</td>--}}
            {{--                                    </tr>--}}
            {{--                                    </tbody>--}}
            {{--                                </table>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            @if(!empty($user->profile->photo1) || !empty($user->profile->photo2) || !empty($user->profile->photo3) || !empty($user->profile->photo4))
                <div class="col-xl-12">
                    <!-- project card -->
                    <h4 class="page-title">Gallery</h4>
                    <div class="card d-block">
                        <div class="card-body">
                            <div class="row">
                                @if(!empty($user->profile->photo1))
                                    <div class="col-3">
                                        <a target="_blank" href="{{asset($user->profile->photo1)}}">
                                            <img src="{{asset($user->profile->photo1)}}" style="max-width: 200px;"/>
                                        </a>
                                    </div>
                                @endif
                                @if(!empty($user->profile->photo2))
                                    <div class="col-3">
                                        <a target="_blank" href="{{asset($user->profile->photo2)}}">
                                            <img src="{{asset($user->profile->photo2)}}" style="max-width: 200px;"/>
                                        </a>
                                    </div>
                                @endif
                                @if(!empty($user->profile->photo3))
                                    <div class="col-3">
                                        <a target="_blank" href="{{asset($user->profile->photo3)}}">
                                            <img src="{{asset($user->profile->photo3)}}" style="max-width: 200px;"/>
                                        </a>
                                    </div>
                                @endif
                                @if(!empty($user->profile->photo4))
                                    <div class="col-3">
                                        <a target="_blank" href="{{asset($user->profile->photo4)}}">
                                            <img src="{{asset($user->profile->photo4)}}" style="max-width: 200px;"/>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('script')

@endsection
