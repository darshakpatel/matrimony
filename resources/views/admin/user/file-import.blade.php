@extends('admin.layouts.vertical', ['title' =>"Import Users Using Excel File"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Import Users Using Excel File</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" name="edit_value" value="0">
                            @csrf
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label for="name">File<span
                                            class="error">*</span></label>
                                    <input type="file" class="form-control" name="file" id="file"
                                           required/>
                                    <div class="help-block with-errors error"></div>
                                </div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}</button>
                            <a href="{{url('demo.xlsx')}}" class="btn btn-success">Download Demo</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/import.js') }}?v={{ time() }}"></script>
@endsection

