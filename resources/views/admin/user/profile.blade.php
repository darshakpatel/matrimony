@extends('admin.layouts.vertical', ['title' =>trans('messages.admin.user.profile')])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">{{trans('messages.admin.user.profile')}}</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="id" value="">
                            @csrf
                            <div class="form-group mb-3">
                                <label>{{trans('messages.name')}}</label>
                                <input type="text" class="form-control" name="name"
                                       value="{{$profile->name}}" placeholder="{{trans('messages.name')}}" required>
                                <div class="valid-feedback">

                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label>{{trans('messages.email')}}</label>
                                <input type="email" class="form-control" name="email"
                                       value="{{$profile->email}}" placeholder="{{trans('messages.email')}}" required>
                                <div class="valid-feedback">

                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label>{{trans('messages.mobile_no')}}</label>
                                <input type="text" class="form-control" name="mobile_no"
                                       value="{{$profile->mobile_no}}" placeholder="{{trans('messages.mobile_no')}}"
                                       required>
                                <div class="valid-feedback">

                                </div>
                            </div>
                            <button class="btn btn-primary" id="update-btn"
                                    type="submit">{{trans('messages.submit')}}
                            </button>

                            <a class="btn btn-dark"
                               href="{{route('admin.dashboard')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/profile.js') }}?v={{ time() }}"></script>

@endsection

