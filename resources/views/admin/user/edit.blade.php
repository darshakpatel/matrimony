@extends('admin.layouts.vertical', ['title' =>"Edit User" ])

@section('content')
    @if(auth()->user()->can('city-edit'))
        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">

                        <h4 class="page-title">Edit User</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                                  method="post" class="needs-validation" novalidate>
                                <input type="hidden" id="form-method" value="edit">
                                <input type="hidden" name="edit_value" value="{{$user->id}}">
                                @csrf
                                <div class="row">
                                    <div class="col-12 form-group mb-3">
                                        <label>User Type</label>
                                        <select id="user_type" class="form-control"
                                                name="user_type" required>
                                            <option value="">{{ __('messages.select_option')}}</option>
                                            <option value="product_user"
                                                    @if($user->user_type=='product_user') selected @endif>HWG Customer
                                            </option>
                                            <option value="hwg_distributor"
                                                    @if($user->user_type=='hwg_distributor') selected @endif>HWG
                                                Distributor
                                            </option>
                                            <option value="product_promoter"
                                                    @if($user->user_type=='product_promoter') selected @endif>Enagic
                                                Distributor
                                            </option>
                                            <option value="outsider"
                                                    @if($user->user_type=='outsider') selected @endif>Enagic Customer
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-6 form-group">
                                        <label for="name">Distributor Type<span
                                                    class="error">*</span></label>
                                        <select name="company_type" class="form-control" id="type">
                                            <option value="">Distributor Type</option>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}"
                                                        @if($company->id==$user->company_type) selected @endif>{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors error"></div>
                                    </div>
                                    <div class="col-6 form-group">
                                        <label for="name">Company Name<span
                                                    class="error">*</span></label>
                                        <input type="text" class="form-control" name="company_name"
                                               value="{{$user->company_name}}"
                                               id="company_name"
                                               placeholder="Company Name"/>
                                        <div class="help-block with-errors error"></div>
                                    </div>
                                    {{--                                    <div class="col-12 form-group mb-3">--}}
                                    {{--                                        <div class="custom-control custom-checkbox">--}}
                                    {{--                                            <input type="checkbox"--}}
                                    {{--                                                   class="custom-control-input" id="is_6a" value="1"--}}
                                    {{--                                                   @if($user->is_6a==1) checked @endif--}}
                                    {{--                                                   name="is_6a">--}}
                                    {{--                                            <label class="custom-control-label" for="is_6a">IS He 6A Distributor</label>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <div class="col-12 form-group mb-3">--}}
                                    {{--                                        <label>6A Distributors</label>--}}
                                    {{--                                        <select id="a6_id" class="form-control"--}}
                                    {{--                                                name="a6_id">--}}
                                    {{--                                            <option value="">6A Distributors</option>--}}
                                    {{--                                            @foreach($a6 as $value)--}}
                                    {{--                                                <option value="{{$value->id}}"--}}
                                    {{--                                                        @if($user->a6_id==$value->id) selected @endif>{{$value->name}}</option>--}}
                                    {{--                                            @endforeach--}}
                                    {{--                                        </select>--}}
                                    {{--                                    </div>--}}
                                    <div class="col-4 form-group mb-3">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" name="first_name"
                                               value="{{$user->first_name}}"
                                               placeholder="First Name" required>
                                        <div class="valid-feedback">
                                        </div>
                                    </div>
                                    <div class="col-4 form-group mb-3">
                                        <label>Middle Name</label>
                                        <input type="text" class="form-control"
                                               value="{{$user->middle_name}}"
                                               name="middle_name"
                                               placeholder="Middle Name">
                                        <div class="valid-feedback">
                                        </div>
                                    </div>

                                    <div class="col-4 form-group mb-3">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" name="last_name"
                                               value="{{$user->last_name}}"
                                               placeholder="Last Name" required/>
                                        <div class="valid-feedback">
                                        </div>
                                    </div>

                                    <div class="col-12 form-group mb-3">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email"
                                               value="{{$user->email}}"
                                               placeholder="Email" required/>
                                        <div class="valid-feedback">
                                        </div>
                                    </div>

                                    <div class="col-6 form-group mb-3">
                                        <label>Country Code</label>
                                        <select id="country_code" class="form-control"
                                                name="country_code" required>
                                            <option>{{ __('messages.select_option')}}</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->code}}"
                                                        @if($user->country_code==$country->code) selected @endif>{{$country->name}}
                                                    ({{$country->code}})
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-6 form-group mb-3">
                                        <label>Mobile No</label>
                                        <input type="text" class="form-control integer" name="mobile_no"
                                               value="{{$user->mobile_no}}"
                                               placeholder="Mobile No" required>
                                        <div class="valid-feedback">
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label for="name">Residency Status<span
                                                    class="error">*</span></label>
                                        <select name="residency_status" class="form-control" id="residency_status">
                                            <option value="">Status</option>
                                            <option value="indian"
                                                    @if($user->residency_status=='indian') selected @endif>Indian
                                            </option>
                                            <option value="non_indian"
                                                    @if($user->residency_status=='non_indian') selected @endif>Non
                                                Indian
                                            </option>
                                        </select>
                                        <div class="help-block with-errors error"></div>
                                    </div>
                                    <div class="col-12 form-group mb-3">
                                        <label>Image</label>
                                        <input type="file" class="form-control dropify" name="image"
                                               @if(!empty($user->image))
                                               data-default-file="{{url($user->image)}}"
                                                @endif>
                                        <div class="valid-feedback">
                                        </div>
                                    </div>
                                    <div class="col-6 form-group mb-3">
                                        <label>{{ __('messages.country')}}</label>
                                        <select id="country_id" class="form-control select2"
                                                name="country_id">
                                            <option>{{ __('messages.select_option')}}</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}"
                                                        @if($user->country_id==$country->id) selected @endif>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-6 form-group mb-3">
                                        <label>{{ __('messages.state')}}</label>
                                        <select id="state_id" class="form-control select2"
                                                name="state_id" required>
                                            <option>{{ __('messages.select_option')}}</option>
                                            @foreach($states as $state)
                                                <option value="{{$state->id}}"
                                                        @if($user->state_id==$state->id) selected @endif>{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-12 form-group mb-3">
                                        <label>{{ __('messages.city')}}</label>
                                        <select id="city_id" class="form-control select2"
                                                name="city_id" required>
                                            <option value="">{{ __('messages.select_option')}}</option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}"
                                                        @if($user->city_id==$city->id) selected @endif>{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-6 form-group mb-3">
                                        <label>Gender</label>
                                        <select id="gender" class="form-control select2"
                                                name="gender">
                                            <option value="">{{ __('messages.select_option')}}</option>
                                            <option value="male" @if($user->gender=='male') selected @endif>Male
                                            </option>
                                            <option value="female" @if($user->gender=='female') selected @endif>Female
                                            </option>
                                            <option value="other" @if($user->gender=='other') selected @endif>Other
                                            </option>
                                        </select>
                                        <div class="valid-feedback">
                                        </div>
                                    </div>
                                    <div class="col-6 form-group mb-3">
                                        <label>Date of Birth</label>
                                        <input type="text" class="form-control"
                                               value="{{$user->date_of_birth}}"
                                               id="date_of_birth"
                                               name="date_of_birth">
                                        <div class="valid-feedback">
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary"
                                        type="submit">{{trans('messages.submit')}}</button>
                                <a class="btn btn-dark"
                                   href="{{route('admin.user.index')}}">{{trans('messages.cancel')}}</a>
                            </form>

                        </div>
                    </div>
                </div>


            </div>


        </div> <!-- container -->
    @else
        <h3 class="mt-5">
            <center>{{trans('messages.permission_error')}}</center>
        </h3>
    @endif
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/user.js') }}?v={{ time() }}"></script>

@endsection

