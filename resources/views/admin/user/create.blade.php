@extends('admin.layouts.vertical', ['title' =>"Add User"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add User</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" name="edit_value" value="0">
                            @csrf
                            <div class="row">
                                <div class="col-12 form-group mb-3">
                                    <label>User Type</label>
                                    <select id="user_type" class="form-control"
                                            name="user_type" required>
                                        <option value="">{{ __('messages.select_option')}}</option>
                                        <option value="product_user">HWG Customer</option>
                                        <option value="hwg_distributor">HWG Distributor</option>
                                        <option value="product_promoter">Enagic Distributor</option>
                                        <option value="outsider">Enagic Customer</option>
                                    </select>
                                </div>
{{--                                <div class="col-12 form-group">--}}
{{--                                    <label for="package_id">Buy Package<span--}}
{{--                                                class="error">*</span></label>--}}
{{--                                    <select name="package_id" class="form-control" id="package_id">--}}
{{--                                        <option value="">Select Package</option>--}}
{{--                                        @foreach($packages as $package)--}}
{{--                                            <option value="{{$package->id}}">{{$package->name}}--}}
{{--                                                -Rs.{{$package->price}}({{$package->day}} Days)--}}
{{--                                            </option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                    <div class="help-block with-errors error"></div>--}}
{{--                                </div>--}}
                                <div class="col-6 form-group">
                                    <label for="name">Distributor Type<span
                                                class="error">*</span></label>
                                    <select name="company_type" class="form-control" id="type">
                                        <option value="">Distributor Type</option>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}">{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block with-errors error"></div>
                                </div>
                                <div class="col-6 form-group">
                                    <label for="name">Company Name<span
                                                class="error">*</span></label>
                                    <input type="text" class="form-control" name="company_name" id="company_name"
                                           placeholder="Company Name" />
                                    <div class="help-block with-errors error"></div>
                                </div>
{{--                                <div class="col-12 form-group mb-3">--}}
{{--                                    <div class="custom-control custom-checkbox">--}}
{{--                                        <input type="checkbox"--}}
{{--                                               class="custom-control-input" id="is_6a" value="1"--}}
{{--                                               name="is_6a">--}}
{{--                                        <label class="custom-control-label" for="is_6a">Is He 6A Distributor</label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-12 form-group mb-3">--}}
{{--                                    <label>6A Distributors</label>--}}
{{--                                    <select id="a6_id" class="form-control"--}}
{{--                                            name="a6_id">--}}
{{--                                        <option value="">6A Distributors</option>--}}
{{--                                        @foreach($a6 as $value)--}}
{{--                                            <option value="{{$value->id}}">{{$value->name}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
                                <div class="col-4 form-group mb-3">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="first_name"
                                           placeholder="First Name" required>
                                    <div class="valid-feedback">
                                    </div>
                                </div>

                                <div class="col-4 form-group mb-3">
                                    <label>Middle Name</label>
                                    <input type="text" class="form-control" name="middle_name"
                                           placeholder="Middle Name" >
                                    <div class="valid-feedback">
                                    </div>
                                </div>

                                <div class="col-4 form-group mb-3">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="last_name"
                                           placeholder="Last Name" required>
                                    <div class="valid-feedback">
                                    </div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email"
                                           placeholder="Email" required>
                                    <div class="valid-feedback">
                                    </div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password"
                                           placeholder="Password" required>
                                    <div class="valid-feedback">
                                    </div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Country Code</label>
                                    <select id="country_code" class="form-control"
                                            name="country_code" required>
                                        <option>{{ __('messages.select_option')}}</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->code}}">{{$country->name}}({{$country->code}})
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Mobile No</label>
                                    <input type="text" class="form-control integer" name="mobile_no"
                                           placeholder="Mobile No" required>
                                    <div class="valid-feedback">
                                    </div>
                                </div>
                                <div class="col-12 form-group">
                                    <label for="name">Residency Status<span
                                                class="error">*</span></label>
                                    <select name="residency_status" class="form-control" id="residency_status">
                                        <option value="">Status</option>
                                        <option value="indian">Indian</option>
                                        <option value="non_indian">Non Indian</option>
                                    </select>
                                    <div class="help-block with-errors error"></div>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Distributor ID</label>
                                    <input type="text" class="form-control" name="distributor_id"
                                           placeholder="Distributor ID" required>
                                    <div class="valid-feedback">
                                    </div>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Image</label>
                                    <input type="file" class="form-control dropify" name="image">
                                    <div class="valid-feedback">
                                    </div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>{{ __('messages.country')}}</label>
                                    <select id="country_id" class="form-control select2"
                                            name="country_id" required>
                                        <option>{{ __('messages.select_option')}}</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>{{ __('messages.state')}}</label>
                                    <select id="state_id" class="form-control select2"
                                            name="state_id" required>
                                        <option>{{ __('messages.select_option')}}</option>
                                    </select>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>{{ __('messages.city')}}</label>
                                    <select id="city_id" class="form-control select2"
                                            name="city_id" required>
                                        <option>{{ __('messages.select_option')}}</option>
                                    </select>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Gender</label>
                                    <select id="gender" class="form-control select2"
                                            name="gender" required>
                                        <option value="">{{ __('messages.select_option')}}</option>
                                        <option value="male">Male
                                        </option>
                                        <option value="female">Female
                                        </option>
                                        <option value="other">Other
                                        </option>
                                    </select>
                                    <div class="valid-feedback">
                                    </div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Date of Birth</label>
                                    <input type="text" class="form-control"
                                           id="date_of_birth"
                                           name="date_of_birth">
                                    <div class="valid-feedback">
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}</button>
                            <a class="btn btn-dark"
                               href="{{route('admin.user.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/user.js') }}?v={{ time() }}"></script>
@endsection

