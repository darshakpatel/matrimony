<?php

namespace App\Providers;

use App\Models\Setting;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $settings = Setting::all();
        foreach ($settings as $setting) {
            if ($setting->meta_key == 'smtp_host') {
                $array['host'] = $setting->meta_value;
            }
            if ($setting->meta_key == 'smtp_username') {
                $array['username'] = $setting->meta_value;
                $array['driver'] = 'smtp';
            }
            if ($setting->meta_key == 'smtp_password') {
                $array['password'] = $setting->meta_value;
            }
            if ($setting->meta_key == 'smtp_port') {
                $array['port'] = $setting->meta_value;
            }
            if ($setting->meta_key == 'from_mail') {
                $array['from']['address'] = $setting->meta_value;
                $array['from']['name'] = 'Rajpurohit Bandhan';
            }
            if ($setting->meta_key == 'smtp_encryption') {
                $array['encryption'] = $setting->meta_value;
            }
        }
        $array['stream'] = [
            'ssl' => [
                'allow_self_signed' => true,
                'verify_peer'       => false,
                'verify_peer_name'  => false,
            ],
        ];
        $data = var_export($array, 1);
        if (\File::put(base_path() . '/config/mail.php', "<?php\n return $data ;")) {
        }
        Artisan::call('config:clear');
    }

}
