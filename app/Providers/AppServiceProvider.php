<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Setting;
use Illuminate\Support\Facades\Config;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $server_key = DB::table('settings')->where('meta_key', 'server_key')->first()->meta_value;
        $sms_notification = DB::table('settings')->where('meta_key', 'sms_notification')->first()->meta_value;
        $email_notification = DB::table('settings')->where('meta_key', 'email_notification')->first()->meta_value;
        $push_notification = DB::table('settings')->where('meta_key', 'push_notification')->first()->meta_value;
        $support_no = DB::table('settings')->where('meta_key', 'support_no')->first()->meta_value;
        Config::set('server_key', $server_key);
        Config::set('sms_notification', $sms_notification);
        Config::set('email_notification', $email_notification);
        Config::set('push_notification', $push_notification);
        Config::set('support_no', $support_no);
    }
}
