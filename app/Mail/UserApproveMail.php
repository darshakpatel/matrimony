<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use App\Models\Template;
use Illuminate\Queue\SerializesModels;

class UserApproveMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    public function __construct( $details )
    {
        $this->details = $details;
    }

    public function build()
    {
        if ($this->details['is_reactive'] == 1) {
            $template = Template::where('name_key', 'profile_is_reactivated')->first();
        } else {
            $template = Template::where('name_key', 'profile_is_approved')->first();
        }

        $title = $template->title;
        $subject = $template->subject;
        $body = str_replace('{{name}}', $this->details['name'], $template->description);
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->with([
                'body'            => $body,
                'main_title_text' => $title,
            ])
            ->subject($subject)
            ->view('emails.sendEmail');

    }
}
