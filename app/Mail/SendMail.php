<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use App\Models\Template;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    public function __construct($details)
    {
        $this->details = $details;
    }

    public function build()
    {
        $title = $this->details['title'];
        $subject = $this->details['subject'];
        $body = $this->details['description'];
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->with([
                'body'            => $body,
                'main_title_text' => $title,
            ])
            ->subject($subject)
            ->view('emails.otpEmail');

    }
}
