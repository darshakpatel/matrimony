<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Template;

class ResetPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $details )
    {
        $this->details = $details;
    }

    public function build()
    {
        $template = Template::where('name_key', 'reset_password_mail')->first();
        $title = $template->title;
        $subject = $template->subject;
        $body = str_replace('{{name}}', $this->details['name'], $template->description);
        return $this->from(config('mail.from.address'))
            ->with([
                'name'            => $this->details['name'],
                'actionUrl'       => $this->details['actionUrl'],
                'app_local'       => app()->getLocale(),
                'main_title_text' => $title,
                'body'            => $body,
            ])
            ->subject($subject)
            ->view('emails.resetPassword');

    }
}
