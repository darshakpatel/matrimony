<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToModel
{

    public function model(array $row)
    {
        if (User::where('email', $row[2])->count() == 0) {
            return new User([
                'user_type' => "product_user",
                'first_name' => $row[0],
                'last_name' => $row[1],
                'name' => $row[0] . ' ' . $row[1],
                'email' => $row[2],
                'country_code' => 91,
                'mobile_no' => $row[3],
                'password' => Hash::make($row[3])
            ]);
        }
    }
}
