<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Requests;
use Carbon\Carbon;
use Response;
use DB;
use Log;
use App\Models\PlayedReminder;
use App\Models\Reminder;
use App\Models\UserDevice;

class PlayedReminderCommand extends Command
{

    protected $signature = 'played_reminder';

    protected $description = 'Played Reminder';


    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $playedReminders = PlayedReminder::all();

        foreach ($playedReminders as $playedReminder) {
            $reminder = Reminder::where('id', $playedReminder->reminder_id)->first();
            $t1 = Carbon::now();
            $t2 = Carbon::parse($reminder->last_played);
            if ($reminder->type == 'daily') {
                $diff = $t1->diffInDays($t2);
                if ($diff >= 1) {
                    PlayedReminder::where('id', $playedReminder->id)->delete();
                }
            } elseif ($reminder->type == 'weekly') {
                $diff = $t1->diffInWeeks($t2);
                if ($diff >= 1) {
                    PlayedReminder::where('id', $playedReminder->id)->delete();
                }
            } elseif ($reminder->type == 'monthly') {
                $diff = $t1->diffInMonths($t2);
                if ($diff >= 1) {
                    PlayedReminder::where('id', $playedReminder->id)->delete();
                }
            }
        }
    }
}
