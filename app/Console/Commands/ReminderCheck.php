<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Requests;
use Response;
use DB;
use Carbon\Carbon;
use Log;
use App\Models\PlayedReminder;
use App\Models\Reminder;
use App\Models\UserDevice;

class ReminderCheck extends Command
{

    protected $signature = 'reminder_check';

    protected $description = 'Play Reminder';


    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $to = [];
        $newDateTime = Carbon::now()->addMinute(1);
            $reminders = Reminder::where("start_time",'<', $newDateTime->format('H:i:s'))
            ->get();

        foreach ($reminders as $reminder) {
            if ($reminder->type == 'other' || $reminder->type == 'monthly') {
                if ($reminder->start_date <= date('Y-m-d')) {
                    if (PlayedReminder::where('reminder_id', $reminder->id)->count() == 0) {
                        $devices = UserDevice::get();
                        foreach ($devices as $device) {
                            $to[] = $device->device_token;
                        }
                    }
                }
            } else {
                if (PlayedReminder::where('reminder_id', $reminder->id)->count() == 0) {
                    $devices = UserDevice::get();
                    foreach ($devices as $device) {
                        $to[] = $device->device_token;
                    }
                }
            }
            $this->sendNotification($to,$reminder->title,$reminder->message);

            PlayedReminder::create([
                'reminder_id' => $reminder->id
            ]);
            Reminder::where('id', $reminder->id)->update(['last_played' => date('Y-m-d H:i:s')]);
        }

    }
    public function sendNotification($to, $title, $message)
    {
        $msg = $message;
        $content = array(
            "en" => $msg
        );
        $headings = array(
            "en" => $title
        );
        $fields = array(
            'app_id' => env('ONESIGNAL_ID'),
            "headings" => $headings,
            'include_player_ids' =>$to ,
            'content_available' => true,
            'contents' => $content
        );

        $headers = array(
            'Authorization: key=' . env('ONESIGNAL_KEY'),
            'Content-Type: application/json; charset=utf-8'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
    }
}
