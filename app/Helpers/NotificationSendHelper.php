<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class NotificationSendHelper
{
    public static function send( $array )
    {
        if (count($array['token']) > 0) {
            $fields = [
                'registration_ids' => $array['token'],
                'notification'     =>
                    [
                        'title'    => $array['title'],
                        'body'     => $array['message'],
                        'vibrate'  => "1",
                        'sound'    => "mySound",
                        "priority" => "high",
                    ],
                'data'             => ['message' => $array['message'], 'title' => $array['title']],
            ];
            Http::withHeaders([
                'Authorization:key' => env('FCM_KEY'),
                'Content-Type'      => 'application/json'
            ])->post('https://fcm.googleapis.com/fcm/send', $fields);
        }
    }

}
