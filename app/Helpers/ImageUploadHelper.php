<?php

namespace App\Helpers;


use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;

class ImageUploadHelper
{
    public static function ImageUpload($image): string
    {
        $path = 'uploads/' . date('Y') . '/' . date('m');
        if(!File::exists(public_path() . "/" . $path)){
            File::makeDirectory(public_path() . "/" . $path, 0777, true);
        }
        $files = $image;
        $extension = $image->getClientOriginalExtension();
        $destination_path = public_path() . '/' . $path;
        $image_name = uniqid() . '.' . $extension;
        $image = $path . '/' . $image_name;
        $files->move($destination_path, $image_name);
        return $image;
    }
}
