<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use App\Models\Cart;
use App\Models\User;

class SubTotal
{
    public static function get( $id )
    {
        $user = User::find($id);
        $sub_total = 0;
        $carts = Cart::where('user_id', $id)->get();
        foreach ($carts as $cart) {
            if ($user->user_type == 'product_promoter') {
                $price = $cart->productAccessory->distributor_price;
            } else {
                $price = $cart->productAccessory->price;
            }
            $sub_total = $sub_total + $cart->quantity*$price;
        }
        return $sub_total;
    }

}
