<?php

namespace App\Helpers;

use App\Models\User;
use Illuminate\Support\Facades\Http;
use App\Models\Setting;

class Utility
{
    public static function memberId()
    {
        $user_id = User::count() + 1;
        return 'RMB-' . str_pad($user_id, 6, '0', STR_PAD_LEFT);
    }
}
