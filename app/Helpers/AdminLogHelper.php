<?php

namespace App\Helpers;

use App\Models\AdminLog;
use Illuminate\Support\Facades\Http;


class AdminLogHelper
{
    public static function log( $id, $message )
    {

        $ExactBrowserNameUA=request()->server('HTTP_USER_AGENT');

        if (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")) {
            // OPERA
            $ExactBrowserNameBR="Opera";
        } elseIf (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "chrome/")) {
            // CHROME
            $ExactBrowserNameBR="Chrome";
        } elseIf (strpos(strtolower($ExactBrowserNameUA), "msie")) {
            // INTERNET EXPLORER
            $ExactBrowserNameBR="Internet Explorer";
        } elseIf (strpos(strtolower($ExactBrowserNameUA), "firefox/")) {
            // FIREFOX
            $ExactBrowserNameBR="Firefox";
        } elseIf (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")==false and strpos(strtolower($ExactBrowserNameUA), "chrome/")==false) {
            // SAFARI
            $ExactBrowserNameBR="Safari";
        } else {
            // OUT OF DATA
            $ExactBrowserNameBR="OUT OF DATA";
        };
        $adminLog = new AdminLog();
        $adminLog->admin_id = \Auth::user()->id;
        $adminLog->user_id = $id;
        $adminLog->message = $message;
        $adminLog->ip_address = request()->ip();
        $adminLog->agent = $ExactBrowserNameBR;
        $adminLog->save();
        return true;
    }
}
