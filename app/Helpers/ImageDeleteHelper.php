<?php

namespace App\Helpers;

class ImageDeleteHelper
{
    public static function deleteImage($files): bool
    {
        if(file_exists(public_path() . "/" . $files)){
            @unlink(public_path() . "/" . $files);
        }
        return true;
    }
}
