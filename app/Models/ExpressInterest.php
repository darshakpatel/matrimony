<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpressInterest extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function interestBy()
    {
        return $this->belongsTo(User::class,'interested_by');
    }
    public function notifications()
    {
        return $this->hasmany(Notification::class);
    }
}
