<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class City extends Model
{
    protected $guarded = [];

    public function country(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Country::Class);
    }

    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(State::Class);
    }
}
