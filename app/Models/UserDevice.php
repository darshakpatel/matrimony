<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    protected $guarded = [];

    public function sendNotification($array)
    {
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
        //send_type selected user

        if($array['send_type'] == 1){
            if($array['device_type'] == 'iOS'){
                $fields = [
                    'registration_ids' => $array['token'],
                    'notification'     => [
                        'title'    => $array['title'],
                        'body'     => $array['message'],
                        'vibrate'  => "1",
                        'sound'    => "mySound",
                        "priority" => "high",
                    ],
                    'data'             => ['message' => $array['message']],
                ];
            } else{
                $fields = [
                    'registration_ids' => $array['token'],
                    'notification'     => [
                        'title'    => $array['title'],
                        'body'     => $array['message'],
                        'vibrate'  => "1",
                        'sound'    => "mySound",
                        "priority" => "high",
                    ],
                    'data'             => ['message' => $array['message'], 'title' => $array['title']],
                ];
            }
        } else{
            /* Topics Message send */
            $fields = [
                'to'           => '/topics/rydezilla',
                'notification' => [
                    'title'   => env('FIREBASE_APP_TITLE'),
                    'body'    => $array['message'],
                    'vibrate' => "1",
                    'sound'   => "mySound",
                ],
                'data'         => ['message' => $array['message']],
            ];

        }
        $headers = [
            'Authorization:key='.config('server_key'),
            'Content-Type:application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);

        curl_close($ch);

        return true;
    }


}
