<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HappyStory extends Model
{

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
