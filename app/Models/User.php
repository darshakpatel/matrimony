<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guarded = [];


    public function country(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Country::Class);
    }

    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(State::Class);
    }

    public function city(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(City::Class);
    }

    public function info(): \Illuminate\Database\Eloquent\Relations\hasOne
    {
        return $this->hasOne(UserInfo::Class, 'user_id');
    }

    public function profile(): \Illuminate\Database\Eloquent\Relations\hasOne
    {
        return $this->hasOne(UserProfile::Class, 'user_id');
    }

    public function hobbies(): \Illuminate\Database\Eloquent\Relations\hasOne
    {
        return $this->hasOne(Hobby::Class, 'user_id');
    }
}
