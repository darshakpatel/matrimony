<?php

namespace App\Http\Requests\API\V1;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class UserProfileUpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $user = \Auth::user();
        return [
            'mobile_no' => 'required|unique:users,mobile_no,' . $user->id,
            'email' => 'required|unique:users,email,' . $user->id,
            'state_id' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'country_code' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        //write your bussiness logic here otherwise it will give same old JSON response
        throw new HttpResponseException(response()->json(['success' => false, 'message' => $validator->errors()->first()]));
    }
}
