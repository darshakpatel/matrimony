<?php

namespace App\Http\Requests\API\V1;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class FamilyRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'father_name'       => 'required',
            'grand_father_name' => 'required',
            'father_no'         => 'required',
            'nanaji_name'       => 'required',
            'nanaji_gotra'      => 'required',
//            'nanaji_village'    => 'required',
//            'nanaji_city'       => 'required',
//            'nanaji_country'    => 'required',
            'brother_married'   => 'required',
            'brother_unmarried' => 'required',
            'sister_married'    => 'required',
            'sister_unmarried'  => 'required',
        ];
    }

    public function failedValidation( Validator $validator )
    {
        //write your bussiness logic here otherwise it will give same old JSON response
        throw new HttpResponseException(response()->json([
            'success' => false, 'message' => $validator->errors()->first()
        ]));
    }
}