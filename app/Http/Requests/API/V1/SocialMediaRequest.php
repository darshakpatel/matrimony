<?php

namespace App\Http\Requests\API\V1;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class SocialMediaRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $array = [];
        if ($this->facebook_url) {
            $new_array = [
                'facebook_url' => 'url'
            ];
            $array = array_merge($array, $new_array);
        }
        if ($this->linkedin_url) {
            $new_array = [
                'linkedin_url' => 'url'
            ];
            $array = array_merge($array, $new_array);
        }
        if ($this->instagram_url) {
            $new_array = [
                'instagram_url' => 'url'
            ];
            $array = array_merge($array, $new_array);
        }
        if ($this->youtube_url) {
            $new_array = [
                'youtube_url' => 'url'
            ];
            $array = array_merge($array, $new_array);
        }
        if ($this->twitter_url) {
            $new_array = [
                'twitter_url' => 'url'
            ];
            $array = array_merge($array, $new_array);
        }
        return $array;

    }

    public function failedValidation( Validator $validator )
    {
        //write your bussiness logic here otherwise it will give same old JSON response
        throw new HttpResponseException(response()->json([
            'success' => false, 'message' => $validator->errors()->first()
        ]));
    }
}