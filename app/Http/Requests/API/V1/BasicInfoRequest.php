<?php

namespace App\Http\Requests\API\V1;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class BasicInfoRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'gotra'          => 'required',
            'marital_status' => 'required',
            'height'         => 'required',
            'height_inch'    => 'required',
            'time_of_birth'  => 'required',
            'place_of_birth' => 'required',
        ];
    }

    public function failedValidation( Validator $validator )
    {
        //write your bussiness logic here otherwise it will give same old JSON response
        throw new HttpResponseException(response()->json([
            'success' => false, 'message' => $validator->errors()->first()
        ]));
    }
}