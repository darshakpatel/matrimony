<?php

namespace App\Http\Requests\API\V1;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class OrderStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'sub_total'        => 'required',
            'total'            => 'required',
            'transaction_id'   => 'required',
            'state_id'         => 'required',
            'city_id'          => 'required',
            'address'          => 'required',
            'pincode'          => 'required',
            'shipping_charge'  => 'required',
            'shipping_courier' => 'required',
        ];
    }

    public function failedValidation( Validator $validator )
    {
        //write your bussiness logic here otherwise it will give same old JSON response
        throw new HttpResponseException(response()->json([
            'success' => false, 'message' => $validator->errors()->first()
        ]));
    }
}
