<?php

namespace App\Http\Requests\API\V1;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile_no'     => 'required|digits_between:10,10|unique:users',
            'email'         => 'required|unique:users',
            'date_of_birth' => 'required',
            'gender'        => 'required',
            'first_name'    => 'required',
            'on_behalf'     => 'required',
            'last_name'     => 'required',
            'password'      => 'required',
        ];
    }

    public function failedValidation( Validator $validator )
    {
        //write your bussiness logic here otherwise it will give same old JSON response
        throw new HttpResponseException(response()->json([
            'success' => false, 'message' => $validator->errors()->first()
        ]));
    }
}
