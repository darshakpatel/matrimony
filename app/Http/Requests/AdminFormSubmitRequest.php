<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class AdminFormSubmitRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'                  => 'required',
            'father_name'           => 'required',
            'mobile_no'             => 'required|digits_between:10,10,unique:admins',
            'pincode'               => 'required',
            'state_id'              => 'required',
            'city_id'               => 'required',
            'password'              => 'required|min:8',
            'password_confirmation' => 'required|same:password',
        ];
    }

    public function failedValidation( Validator $validator )
    {
        throw new HttpResponseException(response()->json([
            'message' => $validator->errors()->first()
        ], 422));
    }
}
