<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
//            'last_name' => 'required',
//            'user_type' => 'required',
//            'country_code' => 'required',
            'mobile_no' => 'required|unique:users,mobile_no,' . $this->edit_value,
//            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'email' => 'required|unique:users,email,' . $this->edit_value,
//            'password' => 'required_if:edit_value,0',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        //write your bussiness logic here otherwise it will give same old JSON response
        throw new HttpResponseException(response()->json(['success' => false, 'message' => $validator->errors()->first()]));
    }
}
