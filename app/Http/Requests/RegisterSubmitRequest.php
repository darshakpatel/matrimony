<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class RegisterSubmitRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name'            => 'required',
            'last_name'             => 'required',
            'mobile_no'             => 'required|digits_between:10,10|unique:users',
            'email'                 => 'required|email|unique:users',
            'gender'                => 'required',
            'date_of_birth'         => 'required',
            'on_behalf'             => 'required',
            'password'              => 'required|min:8',
            'password_confirmation' => 'required|same:password',
        ];
    }

    public function failedValidation( Validator $validator )
    {
        throw new HttpResponseException(response()->json([
            'message' => $validator->errors()->first()
        ], 422));
    }
}
