<?php

namespace App\Http\Controllers;

use App\Models\Driver;
use App\Models\User;
use App\Models\Vendor;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class ResetPasswordController extends Controller
{

    public function resetPasswordForm( $token )
    {

        $tokenDatas = DB::table('password_resets')->get();
        $email = null;
        foreach ($tokenDatas as $tokenData) {
            if (Hash::check($token, $tokenData->token)) {
                $email = $tokenData->email;
                break;
            }
        }
        if (empty($email)) {
            abort(404);
        }
        return view('web.resetPassword',
            [
                'email' => $email,
            ]);
    }

    public function resetPassword( Request $request )
    {
        $rules = [
            'email'            => 'required',
            'password'         => 'required|min:8|max:20',
            'confirm_password' => 'required|same:password',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(),
            ], 422);
        }
        $new_password = $request->input('password');
        $user = User::
            where('email', $request->email)
            ->first();
        if ($user) {
            $user->password = bcrypt($new_password);
            $user->update();

            DB::table('password_resets')->where('email', $user->email)
                ->delete();
            return response()->json([
                'message' => "Password Updated Successfully",
            ]);
        } else {
            return response()->json([
                'message' => "This link is expired",
            ], 422);
        }

    }
}
