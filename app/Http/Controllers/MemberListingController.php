<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\IgnoredUser;
use App\Models\ProfileMatch;
use App\Models\State;
use App\Models\UserInfo;
use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MemberListingController extends Controller
{
    public function index( Request $request )
    {
//        dd($request->all());
        $age_from = ($request->age_from != null) ? $request->age_from : null;
        $age_to = ($request->age_to != null) ? $request->age_to : null;
        $member_code = ($request->member_code != null) ? $request->member_code : null;
        $marital_status = ($request->marital_status != null) ? $request->marital_status : null;
        $gender = ($request->gender != null) ? $request->gender : null;
        $country_id = ($request->country_id != null) ? $request->country_id : null;
        $gotra_id = ($request->gotra_id != null) ? $request->gotra_id : null;
        $state_id = ($request->state_id != null) ? $request->state_id : null;
        $city_id = ($request->city_id != null) ? $request->city_id : null;
        $height = ($request->height != null) ? $request->height : null;
        $height_inch = ($request->height_inch != null) ? $request->height_inch : null;
        $higher_education = ($request->higher_education != null) ? $request->higher_education : null;

        $users = User::orderBy('created_at', 'desc')
            ->where('id', '!=', Auth::user()->id)
            ->where('status', 'active')
            ->where('is_approved', 1);

        if ($gender != null) {
            $users->where('gender', $gender);
        }
        if ($state_id != null) {
            $users->where('state_id', $state_id);
        }

        // Ignored member and ignored by member check
        $users = $users->WhereNotIn("id", function ( $query ) {
            $query->select('user_id')
                ->from('ignored_users')
                ->where('ignored_by', Auth::user()->id)
                ->orWhere('id', Auth::user()->id);
        })
            ->whereNotIn("id", function ( $query ) {
                $query->select('ignored_by')
                    ->from('ignored_users')
                    ->where('ignored_by', Auth::user()->id)
                    ->orWhere('id', Auth::user()->id);
            });

        // Sort By age
        if (!empty($age_from)) {
            $age = $age_from;
            $start = date('Y-m-d', strtotime("- $age years"));
            $users = $users->where('date_of_birth', '<=', $start);
        }
        if (!empty($age_to)) {
            $age = $age_to + 1;
            $end = date('Y-m-d', strtotime("- $age years +1 day"));
            $users = $users->where('date_of_birth', '>=', $end);
        }
        if (empty($age_to)) {
            $age = 44 + 1;
            $end = date('Y-m-d', strtotime("- $age years +1 day"));
            $users = $users->where('date_of_birth', '>=', $end);
        }

        // Search by Member Code
        if (!empty($member_code)) {
            $users = $users->where('unique_id', $member_code);
        }

        // Sort by Matital Status
        if ($marital_status != null) {
            $user_ids = User::whereHas('profile', function ( $q ) use ( $marital_status ) {
                $q->where('marital_status', $marital_status);

            })->pluck('id')->toArray();
            $users = $users->WhereIn('id', $user_ids);
        }

        // Sort By location
        if (!empty($city_id)) {
            $users = $users->where('city_id', $city_id);
        }

        if ($country_id == 'india') {
            $users = $users->where('is_nri', 0);
        }
        if ($country_id == 'nri') {
            $users = $users->where('is_nri', 1);
        }
        if (!empty($gotra_id)) {
            $user_ids = UserProfile::where('gotra', $gotra_id)->pluck('user_id')->toArray();
            $users = $users->WhereIn('id', $user_ids);
        }
        if (!empty($higher_education)) {
            $user_ids = UserInfo::where('higher_education', $higher_education)->pluck('user_id')->toArray();
            $users = $users->WhereIn('id', $user_ids);
        }

        // Sort by Height
        if (!empty($height)) {
            $user_ids = UserProfile::where('height', $height)->pluck('user_id')->toArray();
            $users = $users->WhereIn('id', $user_ids);
        }
        if (!empty($height_inch)) {
            $user_ids = UserProfile::where('height_inch', $height_inch)->pluck('user_id')->toArray();
            $users = $users->WhereIn('id', $user_ids);
        }
        $cities = [];
        $users = $users->paginate(10);
        if (!empty($state_id)) {
            $cities = City::where('state_id', $state_id)->orderBy('name', 'asc')->get();
        }
        return view('web.member.member_listing.index',
            compact('users', 'age_from', 'age_to',
                'member_code', 'gender', 'marital_status', 'country_id',
                'state_id', 'city_id', 'height_inch', 'height', 'gotra_id',
                'higher_education', 'cities'
            ));

    }

}
