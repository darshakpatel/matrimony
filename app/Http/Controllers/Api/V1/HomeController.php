<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ZoneResource;
use App\Http\Resources\ProductResource;
use App\Models\User;
use App\Models\Product;
use App\Models\Slider;
use App\Models\ProductQuestion;
use App\Models\Company;
use App\Models\Identity;
use App\Models\Zone;


class HomeController extends Controller
{

    public function sliders()
    {
        $query = Slider::select('id', 'image')
            ->get();
        return response()->json(['data' => $query]);
    }
}
