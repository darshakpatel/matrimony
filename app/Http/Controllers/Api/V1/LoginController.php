<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\LoginWithEmailRequest;
use App\Http\Requests\API\V1\MobileVerifyRequest;
use App\Http\Requests\API\V1\MobileNoLoginRequest;
use App\Helpers\SmsHelper;
use App\Models\User;
use App\Models\Template;
use App\Http\Resources\UserResource;
use App\Models\UserDevice;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\OtpMail;
use App\Mail\ResetPasswordEmail;


class LoginController extends Controller
{
    public function login( Request $request )
    {
        if (!empty($request['mobile_no'])) {
            $user = User::where('mobile_no', $request->input('mobile_no'))
                ->first();
            if (!$user) {
                return response()->json(['message' => 'Invalid Mobile no or Password', 'success' => false]);
            }

            if (!Hash::check($request->input('password'), $user->password)) {
                return response()->json(['message' => 'Invalid Mobile no or Password', 'success' => false]);
            }
            if ($user) {
                $this->deviceToken($user->id, $request['device_token']);
                if ($user->is_mobile_verified == 1) {
                    $tokenResult = $user->createToken('authToken')->plainTextToken;
                    return response()->json([
                        'message'            => 'Login Successfully !',
                        'is_mobile_verified' => 1,
                        'is_email_verified'  => $user->is_email_verified,
                        'email'              => $user->email,
                        'mobile_no'          => $user->mobile_no,
                        'token'              => $tokenResult
                    ]);
                } else {
                    $user->otp = SmsHelper::sendSms(['mobile_no' => $user->mobile_no]);
                    $user->save();
                    return response()->json([
                        'message'            => 'OTP Sent Successfully !',
                        'is_mobile_verified' => 0,
                        'is_email_verified'  => $user->is_email_verified,
                        'email'              => $user->email,
                        'mobile_no'          => $user->mobile_no,
                    ]);
                }

            }
        } else {
            $user = User::where('email', $request->input('email'))
                ->first();
            if (!$user) {
                return response()->json(['message' => 'Invalid Email or Password', 'success' => false]);
            }

            if (!Hash::check($request->input('password'), $user->password)) {
                return response()->json(['message' => 'Invalid Email or Password', 'success' => false]);
            }
            if ($user) {
                $this->deviceToken($user->id, $request['device_token']);
                if ($user->is_email_verified == 1) {
                    $tokenResult = $user->createToken('authToken')->plainTextToken;
                    return response()->json([
                        'message'           => 'Login Successfully !',
                        'is_email_verified' => 1,
                        'email'             => $user->email,
                        'mobile_no'         => $user->mobile_no,
                        'token'             => $tokenResult
                    ]);
                } else {
                    $user->email_otp = rand(111111, 999999);
                    $user->save();

                    Mail::to($request['email'])->send(new OtpMail([
                        'name' => $user->name,
                        'otp'  => $user->email_otp
                    ]));
                    return response()->json([
                        'message'           => 'Email successfully sent for verification',
                        'email'             => $user->email,
                        'mobile_no'         => $user->mobile_no,
                        'is_email_verified' => 0,
                    ]);
                }
            }
        }
    }

    public function verifyOtp( MobileVerifyRequest $request )
    {
        \Log::channel('otp')->info($request->all());
        if ($request['is_mobile'] == 1) {
            $user = User::where('mobile_no', $request['mobile_no'])
                ->first();
        } else {
            $user = User::where('email', $request['email'])
                ->first();
        }
        if ($user) {
            if ($request['is_mobile'] == 1) {
                if ($user->otp == $request['otp']) {
                    $user->is_mobile_verified = 1;
                    $user->save();
                    $tokenResult = $user->createToken('authToken')->plainTextToken;
                    return response()->json([
                        'success' => true,
                        'token'   => $tokenResult,
                    ]);
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => "Invalid OTP",
                    ]);
                }
            } elseif ($user->email_otp == $request['otp']) {
                $user->is_email_verified = 1;
                $user->save();
                $tokenResult = $user->createToken('authToken')->plainTextToken;
                return response()->json([
                    'success' => true,
                    'token'   => $tokenResult,
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => "Invalid OTP",
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => "This mobile no or email is not registered",
            ]);
        }
    }


    public function refreshToken( Request $request )
    {
        $user = $request->user();
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
        $tokenResult = $user->createToken('authToken')->plainTextToken;
        return response()->json([
            'token' => $tokenResult
        ]);
    }

    public function forgotPassword( Request $request )
    {
        $rules = [
            'email' => 'required|email',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first(),
            ]);
        }

        $user = User::where(['email' => $request->input('email')])
            ->first();

        if ($user) {

            $token = Password::getRepository()->create($user);

            $array = [
                'name'                   => $user->name,
                'actionUrl'              => route('reset-password', [$token]),
                'mail_title'             => config('languageString.reset_password'),
                'reset_password_subject' => config('languageString.reset_password_subject'),
                'reset_password_text'    => config('languageString.reset_password_text'),
                'main_title_text'        => config('languageString.reset_password'),
            ];
            Mail::to($request->input('email'))->send(new ResetPasswordEmail($array));


            return response()->json([
                'message' => "Please check your email",
            ]);

        } else {
            return response()->json([
                'success' => false,
                'message' => "Email not found",
            ]);
        }

    }

    public function deviceToken( $user_id, $device_token )
    {
        DB::table('devices')->where('device_token', $device_token)->delete();

        DB::table('devices')->insert([
            'user_id'      => $user_id,
            'device_token' => $device_token,
            'created_at'   => date('Y-m-d H:i:s'),
            'updated_at'   => date('Y-m-d H:i:s'),
        ]);
        return true;
    }
}
