<?php

namespace App\Http\Controllers\Api\V1;

use App\Helpers\ImageDeleteHelper;
use App\Helpers\ImageUploadHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\RegisterRequest;
use App\Helpers\Utility;
use App\Models\User;
use App\Models\UserInfo;
use App\Models\UserProfile;
use Illuminate\Support\Facades\Hash;
use DB;
use Mail;
use App\Mail\WelcomeMail;
use App\Mail\OtpMail;

class RegisterController extends Controller
{
    public function register( RegisterRequest $request ): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $token = \Str::random(40);
            $user = new User();
            $user->on_behalf = $request['on_behalf'];
            $user->name = $request['first_name'] . ' ' . $request['last_name'];
            $user->first_name = $request['first_name'];
            $user->last_name = $request['last_name'];
            $user->country_code = 91;
            $user->password = Hash::make($request['password']);
            $user->mobile_no = $request['mobile_no'];
            $user->gender = $request['gender'];
            $user->email = $request['email'];
            $user->date_of_birth = date('Y-m-d', strtotime($request['date_of_birth']));
            $user->unique_id = Utility::memberId();
            $user->token = $token;
            $user->save();

            $userInfo = new UserInfo();
            $userInfo->user_id = $user->id;
            $userInfo->save();

            $userprofile = new UserProfile();
            $userprofile->user_id = $user->id;

            $userprofile->save();
            if (!empty($request['email'])) {
                $user->email_otp = rand(111111, 666666);
                $user->save();

                Mail::to($request['email'])->send(new OtpMail([
                    'name' => $user->name,
                    'otp'  => $user->email_otp,
                ]));

                Mail::to($request['email'])->send(new WelcomeMail([
                    'name' => $user->name,
                ]));
            }
            DB::commit();
            return response()->json(['message' => 'Registration Successfully !']);
        } catch (\Exception $exception) {
            DB::rollback();
            return response()->json(['message' => 'Something went wrong!', 'error' => $exception->getMessage()], 422);
        }
    }
}
