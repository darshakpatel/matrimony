<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Notification;
use App\Helpers\NotificationSendHelper;
use App\Http\Controllers\Controller;
use App\Models\ExpressInterest;
use App\Models\Member;
use App\Models\User;
use Auth;
use DB;

class ExpressInterestController extends Controller
{

    public function index( Request $request )
    {
        $user = $request->user();
        $interests = ExpressInterest::orderBy('id', 'desc')
            ->where('interested_by', Auth::user()->id)
            ->orwhere(function ( $query ) use ( $user ) {
                $query->where('user_id', Auth::user()->id);
                $query->where('status', 1);
            })
//            ->where('status',1)
            ->distinct()
            ->paginate(10);

        $array = [];
        foreach ($interests as $interest) {
            if ($interest->user_id == $user->id) {
                $userInterest = $interest->interestBy;
            } else {
                $userInterest = $interest->user;
            }
            $array[] = [
                'interest_id' => $interest->id,
                'user_id'     => $userInterest->id,
                'name'        => $userInterest->name,
                'image'       => $userInterest->image,
                'gender'      => $userInterest->gender,
                'status'      => $interest->status,
            ];
        }
        return response()->json([
            'success'    => true,
            'total_page' => ceil($interests->total() / 10),
            'data'       => $array
        ]);
    }

    public function interest_requests()
    {
        $interests = ExpressInterest::where('user_id', Auth::user()->id)->where('status', 0)->latest()->paginate(10);
        $array = [];
        foreach ($interests as $interest) {
            $array[] = [
                'interest_id' => $interest->id,
                'user_id'     => $interest->interestBy->id,
                'name'        => $interest->interestBy->name,
                'image'       => $interest->interestBy->image,
                'gender'      => $interest->interestBy->gender,
                'status'      => $interest->status,
            ];
        }
        return response()->json([
            'success'    => true,
            'total_page' => ceil($interests->total() / 10),
            'data'       => $array
        ]);
    }


    public function store( Request $request )
    {
        $user = $request->user();
        $userProfile = User::find($request->user_id);
        if ($userProfile->gender == $user->gender) {
            return response()->json([
                'success' => false, 'message' => 'Interest Cannot be expressed with same gender..'
            ]);
        }
        $me_sent = ExpressInterest::where('user_id', $request->user_id)->where('interested_by', Auth::user()->id)->count();
        $they_sent = ExpressInterest::where('interested_by', $request->user_id)->where('user_id', Auth::user()->id)->count();
        if ($they_sent == 0 && $me_sent == 0) {
            $express_interest = new ExpressInterest;
            $express_interest->user_id = $request->user_id;
            $express_interest->interested_by = Auth::user()->id;
            $express_interest->save();

            $device_tokens = DB::table('devices')->where('user_id', $request->user_id)->pluck('device_token')->toArray();

            NotificationSendHelper::send([
                'title'   => 'New Interest Request',
                'message' => Auth::user()->name . ' sent you interest request.',
                'token'   => $device_tokens
            ]);
            return response()->json([
                'success' => true, 'message' => 'Interest Expressed successfully.'
            ]);
        } else {
            if ($me_sent > 0) {
                return response()->json([
                    'success' => false, 'message' => 'You have already sent the express request.'
                ]);
            }
            if ($they_sent > 0) {
                return response()->json([
                    'success' => false,
                    'message' => $userProfile->gender == 'male' ? 'He' : "She" . ' already sent the express request to you.'
                ]);
            }

        }

    }

    public function accept_interest( Request $request )
    {
        $interest = ExpressInterest::findOrFail($request->interest_id);
        $interest->status = 1;
        $interest->save();

        $device_tokens = DB::table('devices')->where('user_id', $interest->interested_by)->pluck('device_token')->toArray();

        NotificationSendHelper::send([
            'title'   => 'Your interest is approved',
            'message' => Auth::user()->name . ' approved your interest request.',
            'token'   => $device_tokens
        ]);

        return response()->json([
            'success' => true, 'message' => 'Interest has been accepted successfully.'
        ]);

    }

    public function reject_interest( Request $request )
    {
        $interest = ExpressInterest::findOrFail($request->interest_id);


        $device_tokens = DB::table('devices')->where('user_id', $interest->interested_by)->pluck('device_token')->toArray();

        NotificationSendHelper::send([
            'title'   => 'Your interest is rejected',
            'message' => Auth::user()->name . ' rejected your interest request.',
            'token'   => $device_tokens
        ]);

        ExpressInterest::destroy($request->interest_id);

        return response()->json([
            'success' => true, 'message' => 'Interest has been rejected successfully.'
        ]);
    }

}
