<?php

namespace App\Http\Controllers\Api\V1;

use App\Helpers\ImageDeleteHelper;
use App\Helpers\ImageUploadHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\AddressRequest;
use App\Http\Requests\API\V1\BasicInfoRequest;
use App\Http\Requests\API\V1\FamilyRequest;
use App\Http\Requests\API\V1\NativeAddressRequest;
use App\Http\Requests\API\V1\SocialMediaRequest;
use App\Http\Requests\API\V1\UserChangePasswordRequest;
use App\Http\Requests\API\V1\UserProfileChangeRequest;
use App\Http\Requests\API\V1\UserProfileUpdateRequest;
use App\Http\Resources\UserResource;
use App\Mail\AdminApprovalMail;
use App\Mail\ProfileSubmitMail;
use App\Mail\ResetPasswordEmail;
use App\Models\City;
use App\Models\Gotra;
use App\Models\HighestEducation;
use App\Models\Hobby;
use App\Models\IgnoredUser;
use App\Models\Mangalik;
use App\Models\Order;
use App\Models\Pincode;
use App\Models\ProfileMatch;
use App\Models\ReportedUser;
use App\Models\Setting;
use App\Models\State;
use App\Models\User;
use App\Models\UserDevice;
use App\Models\UserInfo;
use App\Models\UserProfile;
use App\Models\WorkProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    public function show(Request $request)
    {
        $user = $request->user();
        return response()->json([
            'data' => new UserResource($user),
        ]);
    }

    public function introductionUpdate(Request $request)
    {
        $user = $request->user();
        $userProfile = UserProfile::where('user_id', $user->id)->first();
        $userProfile->self_intro_text = $request['self_info_text'];
        if ($request->hasFile('self_intro_video')) {
            $video = ImageUploadHelper::ImageUpload($request['self_intro_video']);
            $userProfile->self_intro_video = $video;
        }
        $userProfile->save();

        $user->is_self_intro = 1;
        $user->save();
        return response()->json([
            'success' => true, 'message' => 'Member introduction info has been updated successfully'
        ]);
    }

    public function changeEmail(Request $request)
    {
        $user = $request->user();
        $user->email = $request['email'];
        $user->save();
        return response()->json([
            'success' => true, 'message' => 'Email has been updated successfully'
        ]);
    }

    public function remove_video(Request $request)
    {
        $user = $request->user();
        UserProfile::where('user_id', $user->id)->update(['self_intro_video' => null]);
        return response()->json(['success' => true, 'message' => 'Video removed successfully']);
    }

    public function gotras()
    {
        $gotras = Gotra::select('id', 'name')->get();
        return response()->json(['success' => true, 'data' => $gotras]);
    }

    public function workProfile()
    {
        $gotras = WorkProfile::select('id', 'name')->get();
        return response()->json(['success' => true, 'data' => $gotras]);
    }

    public function highestEducation()
    {
        $highestEducations = HighestEducation::select('id', 'name')->get();
        return response()->json(['success' => true, 'data' => $highestEducations]);
    }

    public function basic_info_update(BasicInfoRequest $request)
    {
        $user = $request->user();
        if ($user->gender == 'male' && empty($user->profile->aadhar_photo) && !$request->hasFile('aadhar_photo')) {
            return response()->json([
                'success' => false, 'message' => 'Aadhar card is required'
            ]);
        }

        if ($user->gender == 'male' && empty($user->image) && !$request->hasFile('image')) {
            return response()->json([
                'success' => false, 'message' => 'Profile image is required'
            ]);
        }
        if ($user->gender == 'male' && empty($user->profile->aadhar_back_photo) && !$request->hasFile('aadhar_back_photo')) {
            return response()->json([
                'success' => false, 'message' => 'Aadhar card back image is required'
            ]);
        }
        if ($request->hasFile('image')) {
            $image = ImageUploadHelper::ImageUpload($request['image']);
            $user->image = $image;
        }
        $user->is_basic_info = 1;
        $user->save();

        $userProfile = UserProfile::where('user_id', $user->id)->first();
        $userProfile->marital_status = $request['marital_status'];
        $userProfile->height = $request['height'];
        $userProfile->height_inch = $request['height_inch'];
        $userProfile->contact_no = $request['contact_no'];
        $userProfile->mangalik = $request['mangalik'];
        $userProfile->gotra = $request['gotra'];
        $userProfile->time_of_birth = $request['time_of_birth'];
        $userProfile->place_of_birth = $request['place_of_birth'];
        if ($request->hasFile('aadhar_photo')) {
            $aadhar_photo = ImageUploadHelper::ImageUpload($request['aadhar_photo']);
            $userProfile->aadhar_photo = $aadhar_photo;
        }
        if ($request->hasFile('aadhar_back_photo')) {
            $aadhar_back_photo = ImageUploadHelper::ImageUpload($request['aadhar_back_photo']);
            $userProfile->aadhar_back_photo = $aadhar_back_photo;
        }
        $userProfile->save();
        return response()->json([
            'success' => true, 'message' => 'Basic Info has been updated successfully'
        ]);
    }

    public function photosUpdate(Request $request)
    {
        $user = $request->user();
        $user->is_gallery_photo = 1;
        $user->save();
        $userProfile = UserProfile::where('user_id', $user->id)->first();
        if ($request->hasFile('photo1')) {
            $photo1 = ImageUploadHelper::ImageUpload($request['photo1']);
            $userProfile->photo1 = $photo1;
        }
        if ($request->hasFile('photo2')) {
            $photo2 = ImageUploadHelper::ImageUpload($request['photo2']);
            $userProfile->photo2 = $photo2;
        }
        if ($request->hasFile('photo3')) {
            $photo3 = ImageUploadHelper::ImageUpload($request['photo3']);
            $userProfile->photo3 = $photo3;
        }
        if ($request->hasFile('photo4')) {
            $photo4 = ImageUploadHelper::ImageUpload($request['photo4']);
            $userProfile->photo4 = $photo4;
        }
        $userProfile->save();
        return response()->json([
            'success' => true, 'message' => 'Photos has been updated successfully'
        ]);
    }

    public function addressUpdate(AddressRequest $request)
    {
        $user = $request->user();
        $user->country_id = 101;
        $user->address = $request['address'];
        $user->state_id = $request['state_id'];
        $user->city_id = $request['city_id'];
        $user->pincode = $request['pincode'];
        $user->village = $request['village'];
        $user->is_nri = $request['is_nri'];
        $user->is_present_address = 1;
        $user->save();

        return response()->json([
            'success' => true, 'message' => 'Address has been updated successfully'
        ]);
    }

    public function educationUpdate(Request $request)
    {
        \Log::info($request->all());
        $user = $request->user();
        $user->is_education = 1;
        $user->save();
        $userInfo = UserInfo::where('user_id', $user->id)->first();
        $userInfo->higher_education = $request['higher_education'];
        $userInfo->work_profile = $request['work_profile'];
        $userInfo->degree = $request['degree'];
        if ($request->hasFile('higher_education_pdf')) {
            $higher_education_pdf = ImageUploadHelper::ImageUpload($request['higher_education_pdf']);
            $userInfo->higher_education_pdf = $higher_education_pdf;
        }
        $userInfo->save();

        return response()->json([
            'success' => true, 'message' => 'Education details has been updated successfully'
        ]);
    }

    public function hobbiesUpdate(Request $request)
    {
        $user = $request->user();
        $user->is_hobby = 1;
        $user->save();
        $hobby = Hobby::where('user_id', $user->id)->first();
        if (empty($hobby)) {
            $hobby = new Hobby();
        }
        $hobby->user_id = $user['id'];
        $hobby->hobbies = $request['hobbies'];
        $hobby->save();

        return response()->json([
            'success' => true, 'message' => 'Hobby details has been updated successfully'
        ]);
    }

    public function nativeAddressUpdate(NativeAddressRequest $request)
    {
        \Log::info($request->all());
        $user = $request->user();
        $user->is_native_address = 1;
        $user->save();

        $userInfo = UserInfo::where('user_id', $user->id)->first();
        $userInfo->native_village = $request['native_village'];
        $userInfo->native_city = $request['native_city'];
        $userInfo->native_state = $request['native_state'];
        $userInfo->native_address = $request['native_address'];
        $userInfo->native_country = 101;
        $userInfo->pincode = $request['pincode'];
        $userInfo->save();


        return response()->json([
            'success' => true, 'message' => 'Native Address has been updated successfully'
        ]);
    }

    public function social_media_update(SocialMediaRequest $request)
    {


        $user = $request->user();
        $user->is_social_media = 1;
        $user->save();

        $userInfo = UserProfile::where('user_id', $user->id)->first();
        $userInfo->facebook_url = $request['facebook_url'];
        $userInfo->linkedin_url = $request['linkedin_url'];
        $userInfo->instagram_url = $request['instagram_url'];
        $userInfo->youtube_url = $request['youtube_url'];
        $userInfo->twitter_url = $request['twitter_url'];
        $userInfo->save();


        return response()->json([
            'success' => true, 'message' => 'Social Media has been updated successfully'
        ]);
    }

    public function familiesUpdate(FamilyRequest $request)
    {
        $user = $request->user();
        $user->is_family_info = 1;
        $user->save();

        $userProfile = UserProfile::where('user_id', $user->id)->first();
        $userProfile->father_name = $request['father_name'];
        $userProfile->father_profession = $request['father_profession'];
        $userProfile->mother_name = $request['mother_name'];
        $userProfile->grand_father_name = $request['grand_father_name'];
        $userProfile->father_no = $request['father_no'];
        $userProfile->mother_no = $request['mother_no'];
        $userProfile->nanaji_name = $request['nanaji_name'];
        $userProfile->nanaji_gotra = $request['nanaji_gotra'];
        $userProfile->nanaji_village = $request['nanaji_village'];
        $userProfile->nanaji_city = $request['nanaji_city'];
        $userProfile->nanaji_state = $request['nanaji_state'];
        $userProfile->nanaji_address = $request['nanaji_address'];
        $userProfile->nanaji_is_nri = $request['nanaji_is_nri'];
        $userProfile->brother_married = $request['brother_married'];
        $userProfile->brother_unmarried = $request['brother_unmarried'];
        $userProfile->sister_married = $request['sister_married'];
        $userProfile->sister_unmarried = $request['sister_unmarried'];
        $userProfile->save();

        return response()->json([
            'success' => true, 'message' => 'Family Details has been updated successfully'
        ]);
    }

    public function report_user(Request $request)
    {
        $report_member = new ReportedUser();
        $report_member->user_id = $request->user_id;
        $report_member->reported_by = \Auth::user()->id;
        $report_member->reason = $request->reason;
        $report_member->save();

        return response()->json([
            'success' => true, 'message' => 'Reported to this member successfully.'
        ]);
    }

    public function add_to_ignore_list(Request $request)
    {
        $ignore = new IgnoredUser();
        $ignore->user_id = $request->user_id;
        $ignore->ignored_by = \Auth::user()->id;
        $ignore->save();

        return response()->json([
            'success' => true, 'message' => 'Ignored successfully.'
        ]);
    }

    public function remove_from_ignore_list(Request $request)
    {
        $user = $request->user();
        IgnoredUser::where('ignored_by', $user->id)->where('user_id', $request->user_id)->delete();
        return response()->json([
            'success' => true, 'message' => 'Remove from ignore list successfully.'
        ]);
    }

    public function ignore_list(Request $request)
    {
        $user = $request->user();
        $ignoreUsers = IgnoredUser::where('ignored_by', $user->id)->get();

        $array = [];
        foreach ($ignoreUsers as $ignoreUser) {
            $array[] = [
                'user_id' => $ignoreUser->user_id,
                'name' => $ignoreUser->user->name,
                'image' => $ignoreUser->user->image,
            ];
        }
        return response()->json([
            'success' => true,
            'data' => $array
        ]);
    }

    public function change_password(Request $request)
    {
        $rules = [
            'old_password' => ['required'],
            'password' => ['min:8', 'required_with:confirm_password', 'same:confirm_password'],
            'confirm_password' => ['min:8'],
        ];

        $messages = [
            'old_password.required' => 'Old Password is required',
            'password.required_with' => 'Password and Confirm password are required',
            'password.same' => 'Password and Confirmed password did not matched',
            'confirm_password.min' => 'Max 8 characters',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json([
                'success' => false, 'message' => $validator->errors()->first()
            ]);

        }
        $user = $request->user();

        if (Hash::check($request->old_password, $user->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json([
                'success' => true, 'message' => 'Password Updated successfully.'
            ]);
        } else {
            return response()->json([
                'success' => false, 'message' => 'Old password do not matched.'
            ]);
        }
    }

    public function update_account_deactivation_status(Request $request)
    {
        $user = $request->user();
        $user->status = $request->deacticvation_status;
        $deacticvation_msg = $request->deacticvation_status == 1 ? 'Deactivated' : 'Reactivated';
        $user->save();

        return response()->json([
            'success' => true, 'message' => 'Your account ' . $deacticvation_msg . ' successfully!'
        ]);

    }

    public function submitForApproval(Request $request)
    {
        $user = $request->user();
        if ($user->is_basic_info == 0) {
            return response()->json([
                'success' => false, 'message' => 'Basic info is required for admin approval'
            ]);
        }
        if ($user->is_family_info == 0) {
            return response()->json([
                'success' => false, 'message' => 'Family info is required for admin approval'
            ]);
        }
        if ($user->is_native_address == 0) {
            return response()->json([
                'success' => false, 'message' => 'Native Address info is required for admin approval'
            ]);
        }
        if ($user->is_present_address == 0) {
            return response()->json([
                'success' => false, 'message' => 'Present Address info is required for admin approval'
            ]);
        }
        if ($user->is_education == 0) {
            return response()->json([
                'success' => false, 'message' => 'Education info is required for admin approval'
            ]);
        }


        $user->is_profile_updated = 1;
        $user->save();


        Mail::to($user['email'])->send(new ProfileSubmitMail([
            'name' => $user->name,
        ]));
        if (config('email_notification') == 1) {
            $setting = Setting::where('meta_key', 'to_mail')->first();
            if ($setting->meta_value) {
                Mail::to($setting->meta_value)->send(new AdminApprovalMail([
                    'name' => $user->name,
                    'gender' => $user->profile->gender,
                    'age' => \Carbon\Carbon::parse($user->date_of_birth)->age,
                    'mobile_number' => $user->mobile_no,
                ]));
            }
        }
        return response()->json([
            'success' => true, 'message' => 'Your profile is submitted for admin approval successfully!'
        ]);
    }

    public function account_deactivate(Request $request)
    {
        $user = $request->user();
        $user->status = 'inActive';
        $user->is_approved = 0;
        $user->save();

        return response()->json([
            'success' => true, 'message' => 'Your profile is deactivated successfully!'
        ]);
    }

    public function userStatus()
    {
        $user = request()->user();
        return response()->json([
            'success' => true,
            'blocked' => $user->blocked,
            'ban_reason' => $user->ban_reason,
            'status' => $user->status
        ]);
    }

    public function searchPinCode($pin_code)
    {
        $pincode = Pincode::where('pincode', $pin_code)->first();
        $array = [];
        if (!empty($pincode)) {
            $city = City::where('name', $pincode->taluka)->first();
            if (!empty($city)) {
                $array['pincode'] = $pincode->pincode;
                $array['country_id'] = $city->country_id;
                $array['country'] = $city->country->name;
                $array['state_id'] = $city->state_id;
                $array['state'] = $city->state->name;
                $array['city_id'] = $city->id;
                $array['city'] = $city->name;
//                $array['taluka'] = $pincode->taluka;
//                $array['district'] = $pincode->district;
//                $array['state'] = $pincode->state;

            } else {
                $state_id = State::where('name', $pincode->circle)->first();
                if (!empty($state_id)) {
                    $city = new City();
                    $city->name = $pincode->taluka;
                    $city->country_id = 101;
                    $city->state_id = $state_id->id;
                    $city->pincode = $pincode->pincode;
                    $city->save();

                    $array['pincode'] = $pincode->pincode;
                    $array['country_id'] = $city->country_id;
                    $array['country'] = 'India';
                    $array['state_id'] = $city->state_id;
                    $array['state'] = $city->state->name;
                    $array['city_id'] = $city->id;
                    $array['city'] = $city->name;
                }

            }

            return response()->json(['success' => true, 'data' => $array]);
        } else {
            return response()->json(['success' => false, 'message' => "pincode not found"]);
        }

    }

}
