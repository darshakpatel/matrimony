<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\State;
use App\Http\Resources\StateResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StateController extends Controller
{
    public function index($id): \Illuminate\Http\JsonResponse
    {
        $query = State::where('country_id',$id)->get();
        $countries = StateResource::collection($query);
        return response()->json([
            'data' => $countries
        ]);
    }
}
