<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\City;
use App\Http\Resources\CityResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index($id): \Illuminate\Http\JsonResponse
    {
        $query = City::where('state_id', $id)->get();
        $countries = CityResource::collection($query);
        return response()->json([
            'data' => $countries
        ]);
    }
}
