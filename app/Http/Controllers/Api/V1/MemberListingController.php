<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\ExpressInterest;
use App\Models\IgnoredUser;
use App\Models\ProfileMatch;
use App\Models\State;
use App\Models\User;
use App\Models\UserInfo;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MemberListingController extends Controller
{
    public function index(Request $request)
    {
        $loginUser = $request->user();
        $age_from = ($request->age_from != null) ? $request->age_from : null;
        $age_to = ($request->age_to != null) ? $request->age_to : null;
        $member_code = ($request->member_code != null) ? $request->member_code : null;
        $marital_status = ($request->marital_status != null) ? $request->marital_status : null;
        $gender = ($request->gender != null) ? $request->gender : null;
        $country_id = ($request->country_id != null) ? $request->country_id : null;
        $gotra_id = ($request->gotra_id != null) ? $request->gotra_id : null;
        $state_id = ($request->state_id != null) ? $request->state_id : null;
        $city_id = ($request->city_id != null) ? $request->city_id : null;
        $min_height = ($request->min_height != null) ? $request->min_height : null;
        $max_height = ($request->max_height != null) ? $request->max_height : null;
        $higher_education = ($request->higher_education != null) ? $request->higher_education : null;


        $users = User::orderBy('created_at', 'desc')
            ->where('id', '!=', Auth::user()->id)
            ->where('status', 'active')
            ->where('is_approved', 1);

        if ($gender != null) {
            $users->where('gender', $gender);
        }
        if ($state_id != null) {
            $users->where('state_id', $state_id);
        }
        if ($city_id != null) {
            $users->where('city_id', $city_id);
        }
        if ($country_id == 'india') {
            $users = $users->where('is_nri', 0);
        }
        if ($country_id == 'nri') {
            $users = $users->where('is_nri', 1);
        }
        // Ignored member and ignored by member check
        $users = $users->WhereNotIn("id", function ($query) {
            $query->select('user_id')
                ->from('ignored_users')
                ->where('ignored_by', Auth::user()->id)
                ->orWhere('id', Auth::user()->id);
        })
            ->WhereNotIn("id", function ($query) {
                $query->select('ignored_by')
                    ->from('ignored_users')
                    ->where('ignored_by', Auth::user()->id)
                    ->orWhere('id', Auth::user()->id);
            });

        // Sort By age
        // Sort By age
        if (!empty($age_from)) {
            $age = $age_from;
            $start = date('Y-m-d', strtotime("- $age years"));
            $users = $users->where('date_of_birth', '<=', $start);
        }
        if (!empty($age_to)) {
            $age = $age_to + 1;
            $end = date('Y-m-d', strtotime("- $age years +1 day"));
            $users = $users->where('date_of_birth', '>=', $end);
        }
        if (empty($age_to)) {
            $age = 44 + 1;
            $end = date('Y-m-d', strtotime("- $age years +1 day"));
            $users = $users->where('date_of_birth', '>=', $end);
        }

        // Search by Member Code
        if (!empty($member_code)) {
            $users = $users->where('unique_id', $member_code);
        }
        if (!empty($higher_education)) {
            $user_ids = UserInfo::where('higher_education', $higher_education)->pluck('user_id')->toArray();
            $users = $users->WhereIn('id', $user_ids);
        }
        // Sort by Matital Status
        if ($marital_status != null) {
            $users = $users->whereHas('profile', function ($q) use ($marital_status) {
                $q->where('marital_status', $marital_status);
            });
        }

        // Sort by Height
        if (!empty($min_height)) {
            $user_ids = UserProfile::where('height', '>=', $min_height)->pluck('user_id')->toArray();
            if (count($user_ids) > 0) {
                $users = $users->WhereIn('id', $user_ids);
            }
        }
        if (!empty($max_height)) {
            $user_ids = UserProfile::where('height', '<=', $max_height)->pluck('user_id')->toArray();
            if (count($user_ids) > 0) {
                $users = $users->WhereIn('id', $user_ids);
            }
        }

        $users = $users->paginate(10);
        $array = [];
        foreach ($users as $user) {
            $is_expressed = ExpressInterest::where('user_id', $user->id)->where('interested_by', $loginUser->id)->first();
            $array[] = [
                'user_id' => $user->id,
                'name' => $user->name,
                'image' => $user->image,
                'mobile_no' => $user->mobile_no,
                'gender' => $user->gender,
                'date_of_birth' => $user->date_of_birth,
                'member_id' => $user->unique_id,
                'father_name' => $user->profile->father_name,
                'grand_father_name' => $user->profile->grand_father_name,
                'marital_status' => $user->profile->marital_status,
                'height' => $user->profile->height,
                'height_inch' => $user->profile->height_inch,
                'city' => $user->city ? $user->city->name : '',
                'state' => $user->state ? $user->state->name : '',
                'is_express_required' => $user->gender == 'female' ? 1 : 0,
                'is_expressed' => $is_expressed ? 1 : 0,
                'express_status' => $is_expressed ? $is_expressed->status : "",
                'is_nri' => $user->is_nri,
            ];
        }
        return response()->json([
            'success' => true,
            'total_page' => ceil($users->total() / 10),
            'data' => $array
        ]);
    }

    public function memberDetails($id, Request $request)
    {
        $loginUser = $request->user();
        $user = User::find($id);
        if (empty($user)) {
            return response()->json(['success' => false, 'Message' => 'Member not found']);
        }
        $is_expressed = ExpressInterest::where('user_id', $user->id)->where('interested_by', $loginUser->id)->first();
        $array = [
            'user_id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'name' => $user->name,
            'email' => $user->email,
            'image' => $user->image,
            'mobile_no' => $user->mobile_no,
            'gender' => $user->gender,
            'date_of_birth' => $user->date_of_birth,
            'member_id' => $user->unique_id,
            'current_city' => $user->city ? $user->city->name : '',
            'current_city_id' => $user->city_id,
            'current_state' => $user->state ? $user->state->name : '',
            'current_state_id' => $user->state_id,
            'current_village' => $user->village,
            'current_pincode' => $user->pincode,
            'contact_no' => $user->profile->contact_no,
            'father_name' => $user->profile->father_name,
            'father_profession' => $user->profile->father_profession,
            'mother_name' => $user->profile->mother_name,
            'mother_no' => $user->profile->mother_no,
            'father_no' => $user->profile->father_no,
            'grand_father_name' => $user->profile->grand_father_name,
            'nanaji_name' => $user->profile->nanaji_name,
            'nanaji_village' => $user->profile->nanaji_village,
            'nanaji_city' => $user->profile->city ? $user->profile->city->name : '',
            'nanaji_city_id' => $user->profile->nanaji_city,
            'nanaji_state_id' => $user->profile->nanaji_state,
            'nanaji_gotra_id' => $user->profile->nanaji_gotra,
            'nanaji_address' => $user->profile->nanaji_address,
            'nanaji_gotra' => $user->profile->nanajiGotraName ? $user->profile->nanajiGotraName->name : '',
            'nanaji_state' => $user->profile->state ? $user->profile->state->name : '',
            'mangalik' => $user->profile->mangalikName ? $user->profile->mangalikName->name : '',
            'gotra' => $user->profile->gotraName ? $user->profile->gotraName->name : '',
            'gotra_id' => $user->profile->gotra,
            'self_intro_video' => $user->profile->self_intro_video,
            'self_intro_text' => $user->profile->self_intro_text,
            'facebook_url' => $user->profile->facebook_url,
            'linkedin_url' => $user->profile->linkedin_url,
            'brother_married' => $user->profile->brother_married,
            'brother_unmarried' => $user->profile->brother_unmarried,
            'sister_married' => $user->profile->sister_married,
            'sister_unmarried' => $user->profile->sister_unmarried,
            'time_of_birth' => $user->profile->time_of_birth,
            'place_of_birth' => $user->profile->place_of_birth,
            'photo1' => $user->profile->photo1,
            'photo2' => $user->profile->photo2,
            'photo3' => $user->profile->photo3,
            'photo4' => $user->profile->photo4,
            'marital_status' => $user->profile->marital_status,
            'height' => $user->profile->height,
            'height_inch' => $user->profile->height_inch,
            'higher_education_id' => $user->info->higher_education,
            'higher_education' => $user->info->higherEducation ? $user->info->higherEducation->name : "",
            'higher_education_pdf' => $user->info->higher_education_pdf,
            'work_profile_id' => $user->info->work_profile,
            'work_profile' => $user->info->workProfile ? $user->info->workProfile->name : '',
            'native_village' => $user->info->native_village,
            'native_address' => $user->info->native_address,
            'address' => $user->address,
            'native_city' => $user->info->city ? $user->info->city->name : '',
            'native_city_id' => $user->info->native_city,
            'native_state_id' => $user->info->native_state,
            'native_state' => $user->info->state ? $user->info->state->name : '',
            'native_pincode' => $user->info->pincode,
            'hobbies' => $user->hobbies ? $user->hobbies->hobbies : '',
            'aadhar_photo' => $user->profile->aadhar_photo,
            'aadhar_back_photo' => $user->profile->aadhar_back_photo,
            'instagram_url' => $user->profile->instagram_url,
            'youtube_url' => $user->profile->youtube_url,
            'twitter_url' => $user->profile->twitter_url,
            'is_expressed' => $is_expressed ? 1 : 0,
            'express_status' => $is_expressed ? $is_expressed->status : '',
            'is_nri' => $user->is_nri,
            'nanaji_is_nri' => $user->profile->nanaji_is_nri,
            'degree' => $user->info->degree,

        ];
        return response()->json([
            'success' => true,
            'data' => $array
        ]);
    }

}
