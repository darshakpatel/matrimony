<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ZoneResource;
use App\Http\Resources\ProductResource;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductQuestion;
use App\Models\Company;
use App\Models\Identity;
use App\Models\Zone;


class PageController extends Controller
{

    public function show($id)
    {
        $query = Page::where('slug',$id)
            ->select('id', 'name', 'description')
            ->get();
        return response()->json(['data' => $query]);
    }
}
