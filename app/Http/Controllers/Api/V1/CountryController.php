<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Country;
use App\Http\Resources\CountryResource;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function __invoke(): \Illuminate\Http\JsonResponse
    {
        $query = Country::get();
        $countries = CountryResource::collection($query);
        return response()->json([
            'data' => $countries
        ]);
    }
}
