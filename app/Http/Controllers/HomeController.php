<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\IgnoredUser;
use App\Models\ProfileMatch;
use App\Models\ExpressInterest;
use App\Models\State;
use App\Models\Slider;
use App\Models\HomeBanner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        $sliders = Slider::all();
        $banner = HomeBanner::where('id', 1)->first();
        $homeBanners = HomeBanner::where('id', '!=', 1)->get();
        $email = DB::table('settings')->where('meta_key', 'support_email')->first()->meta_value;
        return view('web.index', [
            'sliders'     => $sliders,
            'banner'      => $banner,
            'homeBanners' => $homeBanners,
            'email'       => $email
        ]);
    }

    public function contactUs()
    {
        $support_no = DB::table('settings')->where('meta_key', 'support_no')->first()->meta_value;
        $address = DB::table('settings')->where('meta_key', 'support_address')->first()->meta_value;
        $email = DB::table('settings')->where('meta_key', 'support_email')->first()->meta_value;
        return view('web.contactUs', [
            'support_no' => $support_no,
            'address'    => $address,
            'email'      => $email,
        ]);
    }

    public function user_account_blocked()
    {
        return view('web.user_account_blocked_msg');
    }

    public function getState( $id )
    {
        $html = '<option value="">Select State</option>';
        $states = State::where('country_id', $id)->where('status', 'active')->orderBy('name', 'asc')->get();
        if (count($states) > 0) {
            foreach ($states as $state) {
                $html .= '<option value="' . $state->id . '">' . $state->name . '</option>';
            }
        } else {
            $html .= '<option value="">No State found</option>';
        }

        return response()->json(['data' => $html]);
    }

    public function getCity( $id )
    {
        $html = '<option value="">Select City</option>';
        $cities = City::where('state_id', $id)->where('status', 'active')->orderBy('name', 'asc')->get();
        if (count($cities) > 0) {
            foreach ($cities as $city) {
                $html .= '<option value="' . $city->id . '">' . $city->name . '</option>';
            }
        } else {
            $html .= '<option value="">No City found</option>';
        }
        return response()->json(['data' => $html]);
    }

    public function dashboard( Request $request )
    {
        $similar_profiles = ProfileMatch::orderBy('match_percentage', 'desc')
            ->where('user_id', Auth::user()->id)
            ->where('match_percentage', '>=', 50)
            ->limit(20);

        $ignored_to = IgnoredUser::where('ignored_by', Auth::user()->id)->pluck('user_id')->toArray();
        if (count($ignored_to) > 0) {
            $similar_profiles = $similar_profiles->whereNotIn('match_id', $ignored_to);
        }
        $ignored_by_ids = IgnoredUser::where('user_id', Auth::user()->id)->pluck('ignored_by')->toArray();
        if (count($ignored_by_ids) > 0) {
            $similar_profiles = $similar_profiles->whereNotIn('match_id', $ignored_by_ids);
        }
        $similar_profiles = $similar_profiles->get();
        $interest_requests = ExpressInterest::where('user_id', Auth::user()->id)->where('status', 0)->count();
        $my_interests = ExpressInterest::where('interested_by', Auth::user()->id)->where('status', 1)->count();
        $ingored_members = IgnoredUser::where('ignored_by', Auth::user()->id)->count();
        return view('web.member.dashboard', compact('similar_profiles', 'my_interests', 'ingored_members', 'interest_requests'));
    }
}
