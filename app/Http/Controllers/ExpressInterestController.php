<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\NotificationSendHelper;
use Notification;
use App\Http\Controllers\Controller;
use App\Models\ExpressInterest;
use App\Models\Member;
use App\User;
use Auth;
use DB;

class ExpressInterestController extends Controller
{

    public function index()
    {
        $user = \Auth::user();
        $interests = DB::table('express_interests')
            ->orderBy('id', 'desc')
            ->where('interested_by', Auth::user()->id)
            ->orwhere(function ( $query ) use ( $user ) {
                $query->where('user_id', Auth::user()->id);
                $query->where('status', 1);
            })
//            ->join('users', 'express_interests.user_id', '=', 'users.id')
            ->select('express_interests.id', 'express_interests.interested_by', 'express_interests.user_id')
            ->distinct()
            ->paginate(10);

        return view('web.member.my_interests', compact('interests'));
    }

    public function interest_requests()
    {
//        $interests = ExpressInterest::where('user_id', Auth::user()->id)->latest()->paginate(10);
        $interests = ExpressInterest::where('status', 0)->where('user_id', Auth::user()->id)->latest()->paginate(10);
        return view('web.member.interest_requests', compact('interests'));
    }


    public function store( Request $request )
    {
        $me_sent = ExpressInterest::where('user_id', $request->id)->where('interested_by', Auth::user()->id)->count();
        $they_sent = ExpressInterest::where('interested_by', $request->id)->where('user_id', Auth::user()->id)->count();
        if ($they_sent == 0 && $me_sent == 0) {
            $express_interest = new ExpressInterest;
            $express_interest->user_id = $request->id;
            $express_interest->interested_by = Auth::user()->id;
            $express_interest->save();

            $device_tokens = DB::table('devices')->where('user_id', $request->user_id)->pluck('device_token')->toArray();

            NotificationSendHelper::send([
                'title'   => 'New Interest Request',
                'message' => Auth::user()->name . ' sent you interest request.',
                'token'   => $device_tokens
            ]);
            return 1;
        } else {
            return 0;
        }

    }

    public function accept_interest( Request $request )
    {
        $interest = ExpressInterest::findOrFail($request->interest_id);
        $interest->status = 1;
        $interest->save();

        $device_tokens = DB::table('devices')->where('user_id', $interest->interested_by)->pluck('device_token')->toArray();

        NotificationSendHelper::send([
            'title'   => 'Your interest is approved',
            'message' => Auth::user()->name . ' approved your interest request.',
            'token'   => $device_tokens
        ]);


        flash('Interest has been accepted successfully.')->success();
        return redirect()->route('interest_requests');

    }

    public function reject_interest( Request $request )
    {
        $interest = ExpressInterest::findOrFail($request->interest_id);

        $device_tokens = DB::table('devices')->where('user_id', $interest->interested_by)->pluck('device_token')->toArray();

        NotificationSendHelper::send([
            'title'   => 'Your interest is rejected',
            'message' => Auth::user()->name . ' rejected your interest request.',
            'token'   => $device_tokens
        ]);


        ExpressInterest::destroy($request->interest_id);
        flash('Interest has been rejected successfully.')->success();
        return redirect()->route('interest_requests');
    }

}
