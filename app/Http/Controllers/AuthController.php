<?php

namespace App\Http\Controllers;

use App\Helpers\SmsHelper;
use App\Helpers\ImageUploadHelper;
use App\Helpers\Utility;
use App\Http\Requests\LoginSubmitRequest;
use App\Http\Requests\AdminFormSubmitRequest;
use App\Http\Requests\RegisterSubmitRequest;
use App\Models\Country;
use App\Models\User;
use App\Models\Admin;
use App\Models\UserInfo;
use App\Mail\OtpMail;
use App\Mail\ResetPasswordEmail;
use App\Mail\WelcomeMail;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use DB;
use Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login()
    {
        return view('web.user_login');
    }

    public function adminForm()
    {
        return view('web.adminForm.index');
    }

    public function adminFormSubmit( AdminFormSubmitRequest $request )
    {
        $user = new Admin();
        $user->name = $request['name'];
        $user->father_name = $request['father_name'];
        $user->country_code = 91;
        $user->password = Hash::make($request['password']);
        $user->mobile_no = $request['mobile_no'];
        $user->country_id = $request['country_id'];
        $user->state_id = $request['state_id'];
        $user->city_id = $request['city_id'];
        $user->pincode = $request['pincode'];
        $user->address = $request['address'];
        $user->current_place = $request['current_place'];
        if ($request->hasFile('image')) {
            $image = ImageUploadHelper::ImageUpload($request['image']);
            $user->image = $image;
        }
        if ($request->hasFile('aadhar_card')) {
            $aadhar_card = ImageUploadHelper::ImageUpload($request['aadhar_card']);
            $user->aadhar_card = $aadhar_card;
        }
        $user->save();

        return response()->json(['message' => 'Request Sent Successfully !']);
    }

    public function register()
    {
        $countries = Country::all();
        return view('web.user_registration', [
            'countries' => $countries
        ]);
    }

    public function registerSubmit( RegisterSubmitRequest $request )
    {
        try {
            DB::beginTransaction();
            $token = \Str::random(40);
            $user = new User();
            $user->on_behalf = $request['on_behalf'];
            $user->name = $request['first_name'] . ' ' . $request['last_name'];
            $user->first_name = $request['first_name'];
            $user->last_name = $request['last_name'];
            $user->country_code = 91;
            $user->password = Hash::make($request['password']);
            $user->mobile_no = $request['mobile_no'];
            $user->country_id = $request['country_id'];
            $user->state_id = $request['state_id'];
            $user->city_id = $request['city_id'];
            $user->email = $request['email'];
            $user->gender = $request['gender'];
            $user->date_of_birth = date('Y-m-d', strtotime($request['date_of_birth']));
            $user->unique_id = Utility::memberId();
            $user->token = $token;
            $user->save();

            $userInfo = new UserInfo();
            $userInfo->user_id = $user->id;
            $userInfo->save();

            $userprofile = new UserProfile();
            $userprofile->user_id = $user->id;

            $userprofile->save();
//            if (!empty($request['email'])) {
//                Session::put('email', $user->email);
//                $user->email_otp = rand(111111, 666666);
//                $user->save();
//                if (config('email_notification') == 1) {
//                    Mail::to($request['email'])->send(new OtpMail([
//                        'name' => $user->name,
//                        'otp'  => $user->email_otp,
//                    ]));
//
//                    Mail::to($request['email'])->send(new WelcomeMail([
//                        'name' => $user->name,
//                    ]));
//                }
//            }
            $user->otp = SmsHelper::sendSms(['mobile_no' => $user->mobile_no]);
            $user->save();

            Session::put('mobile_no', $user->mobile_no);
            $url = url('verify-otp');
            DB::commit();
            return response()->json(['message' => 'Registration Successfully !','url'=>$url]);
        } catch (\Exception $exception) {
            DB::rollback();
            return response()->json(['message' => 'Something went wrong!', 'error' => $exception->getMessage()], 422);
        }
    }

    public function loginSubmit( LoginSubmitRequest $request )
    {
        if (is_numeric($request->input('mobile_no'))) {
            $user = User::where('mobile_no', $request->input('mobile_no'))
                ->first();
            if (!$user) {
                return response()->json(['message' => 'Invalid Mobile no or Password'], 422);
            }

            if (!Hash::check($request->input('password'), $user->password)) {
                return response()->json(['message' => 'Invalid Mobile no or Password'], 422);
            }
            if ($user) {
                if ($user->is_mobile_verified == 1) {
                    if ($user->is_email_verified == 0) {
                        if (config('email_notification') == 1) {
                            $user->email_otp = rand(111111, 999999);
                            $user->save();
                            \Session::put('email', $user->email);
                            Mail::to($user->email)->send(new OtpMail([
                                'name' => $user->name,
                                'otp'  => $user->email_otp,
                            ]));
                        } else {
                            $user->email_otp = 123456;
                            $user->save();
                            \Session::put('email', $user->email);
                        }
                        return response()->json([
                            'is_email_verify' => 0,
                            'message'         => 'Please check email in INBOX and SPAM folder as well',
                            'url'             => url('verify-otp')
                        ]);
                    }
                    if ($user->is_approved == 1) {
                        $url = url('/memberListing');
                    } else {
                        $url = url('/dashboard');
                    }
                    Auth::login($user);
                } else {
                    $user->otp = SmsHelper::sendSms(['mobile_no' => $user->mobile_no]);
                    $user->save();

                    Session::put('mobile_no', $user->mobile_no);
                    $url = url('verify-otp');
                }
                return response()->json([
                    'message' => 'Login Successfully !',
                    'url'     => $url
                ]);
            }
        } else {
            $user = User::where('email', $request->input('mobile_no'))
                ->first();
            if (!$user) {
                return response()->json(['message' => 'Invalid Email or Password'], 422);
            }

            if (!Hash::check($request->input('password'), $user->password)) {
                return response()->json(['message' => 'Invalid Email or Password'], 422);
            }
            if ($user) {
                if ($user->is_email_verified == 1) {
                    if ($user->is_mobile_verified == 0) {
                        $user->otp = SmsHelper::sendSms(['mobile_no' => $user->mobile_no]);
                        $user->save();

                        Session::put('mobile_no', $user->mobile_no);
                        $url = url('verify-otp');
                        return response()->json([
                            'message' => 'Login Successfully !',
                            'url'     => $url
                        ]);
                    }
                    if ($user->is_approved == 1) {
                        $url = url('/memberListing');
                    } else {
                        $url = url('/dashboard');
                    }
                    Auth::login($user);
                } else {
                    if (config('email_notification') == 1) {
                        $user->email_otp = rand(111111, 999999);
                        $user->save();
                        \Session::put('email', $request['mobile_no']);
                        Mail::to($request['mobile_no'])->send(new OtpMail([
                            'name' => $user->name,
                            'otp'  => $user->email_otp,
                        ]));
                    } else {
                        $user->email_otp = 123456;
                        $user->save();
                        \Session::put('email', $request['mobile_no']);
                    }
                    return response()->json([
                        'is_email_verify' => 0,
                        'message'         => 'Please check email in INBOX and SPAM folder as well',
                        'url'             => url('verify-otp')
                    ]);
                }
                return response()->json([
                    'message' => 'Login Successfully !',
                    'url'     => $url
                ]);
            }
        }

    }

    public function verifyOtpForm()
    {
        return view('web.user_otp');
    }

    public function resend_otp()
    {
        $email = Session::get('email');
        $mobile_no = Session::get('mobile_no');
        if ($email) {
            $user = User::where('email', $email)
                ->first();
            $user->email_otp = rand(111111, 666666);
            $user->save();
            if (config('email_notification') == 1) {
                Mail::to($email)->send(new OtpMail([
                    'name' => $user->name,
                    'otp'  => $user->email_otp,
                ]));
            }
            return response()->json(['message' => "Otp sent to email successfully"]);
        }
        if ($mobile_no) {
            $user = User::where('mobile_no', $mobile_no)
                ->first();
            $user->otp = SmsHelper::sendSms(['mobile_no' => $user->mobile_no]);
            $user->save();
            return response()->json(['message' => "Otp sent to mobile number successfully"]);
        }
        return response()->json(['success' => false]);
    }

    public function forgotPasswordForm()
    {
        return view('web.forgotPassword');
    }

    public function forgotPassword( Request $request )
    {
        $rules = [
            'email' => 'required|email',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first(),
            ], 422);
        }

        $user = User::where(['email' => $request->input('email')])
            ->first();

        if ($user) {

            $token = Password::getRepository()->create($user);

            $array = [
                'name'                   => $user->name,
                'actionUrl'              => route('reset-password', [$token]),
                'mail_title'             => config('languageString.reset_password'),
                'reset_password_subject' => config('languageString.reset_password_subject'),
                'reset_password_text'    => config('languageString.reset_password_text'),
                'main_title_text'        => config('languageString.reset_password'),
            ];
            Mail::to($request->input('email'))->send(new ResetPasswordEmail($array));


            return response()->json([
                'message' => "Please check your email",
            ]);

        } else {
            return response()->json([
                'message' => "Email not found",
            ], 422);
        }

    }

    public function verifyOtp( Request $request )
    {
        if (Session::has('mobile_no')) {
            $user = User::where('mobile_no', Session::get('mobile_no'))->first();
        } else {
            $user = User::where('email', Session::get('email'))->first();
        }
        if ($user) {
            if (Session::get('email')) {
                if ($user->email_otp == $request['otp']) {
                    $user->is_email_verified = 1;
                    $user->save();
                    Auth::login($user);
                    Session::forget('email');
                    if($user->is_mobile_verified==0){
                        $user->otp = SmsHelper::sendSms(['mobile_no' => $user->mobile_no]);
                        $user->save();
                        Session::put('mobile_no', $user->mobile_no);
                    }
                    $url = url('/dashboard');
                    return response()->json(['message' => 'Email successfully verified', 'url' => $url]);
                } else {
                    return response()->json(['message' => 'Invalid OTP'], 422);
                }
            } else {
                if ($user->otp == $request['otp']) {
                    $user->is_mobile_verified = 1;
                    $user->save();
                    Auth::login($user);
                    Session::forget('mobile_no');
                    $url = url('/dashboard');
                    return response()->json(['message' => 'Mobile number successfully verified', 'url' => $url]);
                } else {
                    return response()->json(['message' => 'Invalid OTP'], 422);
                }
            }
        } else {
            return response()->json(['message' => 'Something went wrong !'], 422);
        }
    }


    public function verifyEmail( $token )
    {
        $user = User::where('token', $token)->first();
        if ($user) {
            $user->is_email_verified = 1;
            $user->save();
            return view('web.email_verify');
        } else {
            abort(404);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->to('/');
    }

    public function passwordReset( Request $request )
    {
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $request->email)->first();
            if ($user != null) {
                $user->verification_code = rand(111111, 999999);
                $user->save();
                flash('Email sent successfully')->success();
                return back();
            } else {
                flash('No account exists with this email')->error();
                return back();
            }
        }

    }
}
