<?php

namespace App\Http\Controllers;

use App\Helpers\ImageUploadHelper;
use App\Mail\AdminApprovalMail;
use App\Mail\ProfileSubmitMail;
use App\Models\City;
use App\Models\Country;
use App\Models\ExpressInterest;
use App\Models\Gotra;
use App\Models\HighestEducation;
use App\Models\Hobby;
use App\Models\IgnoredUser;
use App\Models\Mangalik;
use App\Models\ProfileMatch;
use App\Models\ReportedUser;
use App\Models\Setting;
use App\Models\State;
use App\Models\User;
use App\Models\UserInfo;
use App\Models\UserProfile;
use App\Models\WorkProfile;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Validator;


class ProfileController extends Controller
{
    public function member_profile( $id )
    {
        $user = User::findOrFail($id);

        if (Auth::user()->gender == 'male' && $user->gender == 'female') {
            $do_expressed_interest = ExpressInterest::
            where(function ( $query ) use ( $id ) {
                $query->where('user_id', Auth::user()->id);
                $query->where('interested_by', $id);
            })
                ->orwhere(function ( $query ) use ( $id ) {
                    $query->where('interested_by', Auth::user()->id);
                    $query->where('user_id', $id);
                })
                ->where('status', 1)
                ->count();

            if (!$do_expressed_interest || $do_expressed_interest == 0) {
                return redirect()->back();
            }
        }

        $similar_profiles = ProfileMatch::orderBy('match_percentage', 'desc')
            ->where('user_id', Auth::user()->id)
            ->where('match_id', '!=', $id)
            ->where('match_percentage', '>=', 50)
            ->limit(20);

        $ignored_to = IgnoredUser::where('ignored_by', Auth::user()->id)->pluck('user_id')->toArray();
        if (count($ignored_to) > 0) {
            $similar_profiles = $similar_profiles->whereNotIn('match_id', $ignored_to);
        }

        $ignored_by_ids = IgnoredUser::where('user_id', Auth::user()->id)->pluck('ignored_by')->toArray();
        if (count($ignored_by_ids) > 0) {
            $similar_profiles = $similar_profiles->whereNotIn('match_id', $ignored_by_ids);
        }
        $similar_profiles = $similar_profiles->get();


        return view('web.member.public_profile.index', compact('user', 'similar_profiles'));
    }

    public function profile_settings()
    {
        $member = Auth::user();
        $user = Auth::user();

        $countries = Country::where('status', 'active')->get();
        $gotras = Gotra::get();
        $mangaliks = Mangalik::get();
        $workProfiles = WorkProfile::get();
        $highestEducations = HighestEducation::get();
        $states = State::where('country_id', 101)->orderBy('name', 'asc')->get();
        $cities = City::where('state_id', $member->state_id)->orderBy('name', 'asc')->get();
        $native_states = State::where('country_id', 101)->orderBy('name', 'asc')->get();
        $native_cities = City::where('state_id', $member->info->native_state)->orderBy('name', 'asc')->get();

        $nanajiStates = State::where('country_id', 101)->orderBy('name', 'asc')->get();
        $nanajiCities = City::where('state_id', $member->profile->nanaji_state)->orderBy('name', 'asc')->get();
        return view('web.member.profile.index',
            [
                'member'            => $member,
                'user'              => $user,
                'gotras'            => $gotras,
                'mangaliks'         => $mangaliks,
                'highestEducations' => $highestEducations,
                'workProfiles'      => $workProfiles,
                'countries'         => $countries,
                'states'            => $states,
                'cities'            => $cities,
                'native_states'     => $native_states,
                'native_cities'     => $native_cities,
                'nanajiStates'      => $nanajiStates,
                'nanajiCities'      => $nanajiCities,
            ]);
    }

    public function introductionUpdate( Request $request )
    {
        $user = Auth::user();
        $user->is_self_intro = 1;
        $user->save();
        $userProfile = UserProfile::where('user_id', $user->id)->first();
        $userProfile->self_intro_text = $request['self_info_text'];
        if ($request->hasFile('self_intro_video')) {
            $video = ImageUploadHelper::ImageUpload($request['self_intro_video']);
            $userProfile->self_intro_video = $video;
        }
        $userProfile->save();
//        flash('Member introduction info has been updated successfully')->success();
//        return redirect()->back();

        return response()->json(['message' => 'Member introduction info has been updated successfully']);
    }

    public function changeEmail( Request $request )
    {
        $user = Auth::user();
        $user->email = $request['email'];
        $user->save();
        flash('Email has been updated successfully')->success();
        return redirect()->back();
    }

    public function remove_video()
    {
        $user = Auth::user();
        UserProfile::where('user_id', $user->id)->update(['self_intro_video' => null]);
        return response()->json(['success' => true]);
    }

    public function basic_info_update( Request $request )
    {
        $user = Auth::user();
        if ($request->hasFile('image')) {
            $image = ImageUploadHelper::ImageUpload($request['image']);
            $user->image = $image;
        }
        $user->is_basic_info = 1;
        $user->save();

        $userProfile = UserProfile::where('user_id', $user->id)->first();
        $userProfile->marital_status = $request['marital_status'];
        $userProfile->height = $request['height'];
        $userProfile->height_inch = $request['height_inch'];
        $userProfile->contact_no = $request['contact_no'];
        $userProfile->mangalik = $request['mangalik'];
        $userProfile->gotra = $request['gotra'];
        $userProfile->time_of_birth = $request['time_of_birth'];
        $userProfile->place_of_birth = $request['place_of_birth'];

        if ($request->hasFile('aadhar_photo')) {
            $aadhar_photo = ImageUploadHelper::ImageUpload($request['aadhar_photo']);
            $userProfile->aadhar_photo = $aadhar_photo;
        }
        if ($request->hasFile('aadhar_back_photo')) {
            $aadhar_back_photo = ImageUploadHelper::ImageUpload($request['aadhar_back_photo']);
            $userProfile->aadhar_back_photo = $aadhar_back_photo;
        }
        $userProfile->save();

        return response()->json(['message' => 'Basic Info has been updated successfully']);

    }

    public function photosUpdate( Request $request )
    {
        $user = Auth::user();
        $user->is_gallery_photo = 1;
        $user->save();
        $userProfile = UserProfile::where('user_id', $user->id)->first();
        if ($request->hasFile('photo1')) {
            $photo1 = ImageUploadHelper::ImageUpload($request['photo1']);
            $userProfile->photo1 = $photo1;
        }
        if ($request->hasFile('photo2')) {
            $photo2 = ImageUploadHelper::ImageUpload($request['photo2']);
            $userProfile->photo2 = $photo2;
        }
        if ($request->hasFile('photo3')) {
            $photo3 = ImageUploadHelper::ImageUpload($request['photo3']);
            $userProfile->photo3 = $photo3;
        }
        if ($request->hasFile('photo4')) {
            $photo4 = ImageUploadHelper::ImageUpload($request['photo4']);
            $userProfile->photo4 = $photo4;
        }
        $userProfile->save();
//        flash('Photos has been updated successfully')->success();
//        return redirect()->back();
        return response()->json(['message' => 'Photos has been updated successfully']);
    }

    public function addressUpdate( Request $request )
    {
        $user = Auth::user();
        $user->country_id = $request['country_id'];
        $user->state_id = $request['state_id'];
        $user->city_id = $request['city_id'];
        $user->pincode = $request['pincode'];
        $user->address = $request['address'];
        $user->village = $request['village'];
        $user->is_nri = $request['is_nri'];
        $user->is_present_address = 1;
        $user->save();
//        flash('Address has been updated successfully')->success();
//        return redirect()->back();
        return response()->json(['message' => 'Address has been updated successfully']);
    }

    public function educationUpdate( Request $request )
    {
        $user = Auth::user();
        $user->is_education = 1;
        $user->save();
        $userInfo = UserInfo::where('user_id', $user->id)->first();
        $userInfo->higher_education = $request['higher_education'];
        $userInfo->work_profile = $request['work_profile'];
        $userInfo->degree = $request['degree'];
        if ($request->hasFile('ten_certificate')) {
            $ten_certificate = ImageUploadHelper::ImageUpload($request['ten_certificate']);
            $userInfo->ten_certificate = $ten_certificate;
        }
        if ($request->hasFile('higher_education_pdf')) {
            $higher_education_pdf = ImageUploadHelper::ImageUpload($request['higher_education_pdf']);
            $userInfo->higher_education_pdf = $higher_education_pdf;
        }
        $userInfo->save();
//        flash('Education details has been updated successfully')->success();
//        return redirect()->back();
        return response()->json(['message' => 'Education details has been updated successfully']);
    }

    public function hobbiesUpdate( Request $request )
    {
        $user = Auth::user();
        $user->is_hobby = 1;
        $user->save();
        $hobby = Hobby::where('user_id', $user->id)->first();
        if (empty($hobby)) {
            $hobby = new Hobby();
        }
        $hobby->user_id = $user['id'];
        $hobby->hobbies = $request['hobbies'];
        $hobby->interests = $request['interests'];
        $hobby->music = $request['music'];
        $hobby->books = $request['books'];
        $hobby->movies = $request['movies'];
        $hobby->tv_shows = $request['tv_shows'];
        $hobby->sports = $request['sports'];
        $hobby->fitness_activities = $request['fitness_activities'];
        $hobby->cuisines = $request['cuisines'];
        $hobby->dress_styles = $request['dress_styles'];
        $hobby->save();
//
//        flash('Hobby details has been updated successfully')->success();
//        return redirect()->back();

        return response()->json(['message' => 'Hobby details has been updated successfully']);
    }

    public function nativeAddressUpdate( Request $request )
    {
        $user = Auth::user();
        $user->is_native_address = 1;
        $user->save();

        $userInfo = UserInfo::where('user_id', $user->id)->first();
        $userInfo->native_village = $request['native_village'];
        $userInfo->native_city = $request['native_city'];
        $userInfo->native_state = $request['native_state'];
        $userInfo->native_country = $request['native_country'];
        $userInfo->pincode = $request['pincode'];
        $userInfo->save();
//
//        flash('Native Address has been updated successfully')->success();
//        return redirect()->back();

        return response()->json(['message' => 'Native Address has been updated successfully']);
    }

    public function familiesUpdate( Request $request )
    {
        $user = Auth::user();
        $user->is_family_info = 1;
        $user->save();

        $userProfile = UserProfile::where('user_id', $user->id)->first();
        $userProfile->father_name = $request['father_name'];
        $userProfile->mother_name = $request['mother_name'];
        $userProfile->grand_father_name = $request['grand_father_name'];
        $userProfile->father_profession = $request['father_profession'];
        $userProfile->father_no = $request['father_no'];
        $userProfile->mother_no = $request['mother_no'];
        $userProfile->nanaji_name = $request['nanaji_name'];
        $userProfile->nanaji_gotra = $request['nanaji_gotra'];
        $userProfile->nanaji_village = $request['nanaji_village'];
        $userProfile->nanaji_city = $request['nanaji_city'];
        $userProfile->nanaji_state = $request['nanaji_state'];
        $userProfile->brother_married = $request['brother_married'];
        $userProfile->brother_unmarried = $request['brother_unmarried'];
        $userProfile->sister_married = $request['sister_married'];
        $userProfile->sister_unmarried = $request['sister_unmarried'];
        $userProfile->nanaji_is_nri = $request['nanaji_is_nri'];
        $userProfile->nanaji_address = $request['nanaji_address'];
        $userProfile->save();

//        flash('Family Details has been updated successfully')->success();
//        return redirect()->back();

        return response()->json(['message' => 'Family Details has been updated successfully']);
    }

    public function socialMediaUpdate( Request $request )
    {
        $user = Auth::user();
        $user->is_social_media = 1;
        $user->save();
        $userProfile = UserProfile::where('user_id', $user->id)->first();
        $userProfile->facebook_url = $request['facebook_url'];
        $userProfile->linkedin_url = $request['linkedin_url'];
        $userProfile->instagram_url = $request['instagram_url'];
        $userProfile->youtube_url = $request['youtube_url'];
        $userProfile->twitter_url = $request['twitter_url'];
        $userProfile->save();

//        flash('Social Media has been updated successfully')->success();
//        return redirect()->back();

        return response()->json(['message' => 'Social Media has been updated successfully']);
    }

    public function reportUsers( Request $request )
    {
        $report_member = new ReportedUser();
        $report_member->user_id = $request->member_id;
        $report_member->reported_by = \Auth::user()->id;
        $report_member->reason = $request->reason;
        if ($report_member->save()) {
            flash('Reported to this member successfully.')->success();
            return back();
        } else {
            flash('Sorry! Something went wrong.')->error();
            return back();
        }
    }

    public function add_to_ignore_list( Request $request )
    {
        $ignore = new IgnoredUser();
        $ignore->user_id = $request->id;
        $ignore->ignored_by = \Auth::user()->id;
        $ignore->save();

        return 1;
    }

    public function change_password()
    {
        return view('web.member.password_change');
    }

    public function password_update( Request $request )
    {
        $rules = [
            'old_password'     => ['required'],
            'password'         => ['min:8', 'required_with:confirm_password', 'same:confirm_password'],
            'confirm_password' => ['min:8'],
        ];

        $messages = [
            'old_password.required'  => 'Old Password is required',
            'password.required_with' => 'Password and Confirm password are required',
            'password.same'          => 'Password and Confirmed password did not matched',
            'confirm_password.min'   => 'Max 8 characters',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            flash('Sorry! Something went wrong')->error();
            return Redirect::back()->withErrors($validator);
        }
        $user = \Auth::user();

        if (Hash::check($request->old_password, $user->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
            flash('Password Updated successfully.')->success();
            return redirect()->route('change_password');
        } else {
            flash('Old password do not matched.')->error();
            return back();
        }
    }

    public function update_account_deactivation_status( Request $request )
    {
        $user = Auth::user();
        if ($user->status == 'inActive') {
            flash('reactivation_sent')
                ->success();
            $user->is_reactive = 1;
            $user->save();

            return back();
        }
        $user->status = 'inActive';
        $user->is_approved = 0;
        $user->save();
        flash('Your account Deactivated Successfully!')->success();
        return redirect()->route('dashboard');
    }

    public function submitForApproval()
    {
        $user = Auth::user();
        if ($user->is_basic_info == 0) {
            flash('Basic info is required for admin approval')->warning();
            return back();
        }
        if ($user->is_family_info == 0) {
            flash('Family info is required for admin approval')->warning();
            return back();
        }
        if ($user->is_native_address == 0) {
            flash('Native Address info is required for admin approval')->warning();
            return back();
        }
        if ($user->is_present_address == 0) {
            flash('Present Address info is required for admin approval')->warning();
            return back();
        }
        if ($user->is_education == 0) {
            flash('Education info is required for admin approval')->warning();
            return back();
        }
        $user->is_profile_updated = 1;
        if ($user->save()) {
            if (config('email_notification') == 1) {
                Mail::to($user['email'])->send(new ProfileSubmitMail([
                    'name' => $user->name,
                ]));
                $setting = Setting::where('meta_key', 'to_mail')->first();
                if ($setting->meta_value) {
                    Mail::to($setting->meta_value)->send(new AdminApprovalMail([
                        'name'          => $user->name,
                        'gender'        => $user->profile->gender,
                        'age'           => \Carbon\Carbon::parse($user->date_of_birth)->age,
                        'mobile_number' => $user->mobile_no,
                    ]));
                }
            }
            flash('thank_you')->success();
            return redirect()->back();
        }
        flash('Something Went Wrong!')->error();
        return back();
    }

    public function checkProfile()
    {
        $user = Auth::user();
        if ($user->is_basic_info == 0) {
            return response()->json(['is_pending' => 1]);
        }
        if ($user->is_family_info == 0) {
            return response()->json(['is_pending' => 1]);
        }
        if ($user->is_native_address == 0) {
            return response()->json(['is_pending' => 1]);
        }
        if ($user->is_present_address == 0) {
            return response()->json(['is_pending' => 1]);
        }
        if ($user->is_education == 0) {
            return response()->json(['is_pending' => 1]);
        }
        return response()->json(['is_pending' => 0]);
    }
}
