<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Models\Mangalik;
use Flash;
use Config;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;
use Yajra\DataTables\DataTables;

class MangalikController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $mangaliks = Mangalik::select('mangaliks.*');

            return Datatables::of($mangaliks)
                ->addColumn('action', function ( $mangaliks ) {
                    $delete_button = '';
                    $edit_button = '<a href="' . route('admin.mangalik.edit', [$mangaliks->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Edit"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $mangaliks->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . Config::get('languageString.Delete') . '"><i class="mdi mdi-trash-can"></i></button>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('admin.mangalik.index');
    }

    public function create()
    {
        return view('admin.mangalik.create');
    }

    public function store( Request $request )
    {
        $id = $request['id'];
        if ($id == NULL) {
            $cities = new Mangalik();
            $cities->name = $request['name'];
            $cities->save();

            return response()->json(['success' => true, 'message' => "Mangalik Added"], 200);
        } else {
            $cities = Mangalik::find($id);
            $cities->name = $request['name'];
            $cities->save();
            return response()->json(['success' => true, 'message' => "Mangalik Updated"], 200);
        }
    }

    public function edit( $id )
    {
        $mangalik = Mangalik::find($id);
        return view('admin.mangalik.edit', [
            "mangalik" => $mangalik,
        ]);
    }

    public function destroy( $id )
    {
        Mangalik::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => "Mangalik Deleted"], 200);
    }
}
