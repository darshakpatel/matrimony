<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePermissionRequest;
use App\Http\Requests\UpdatePermissionRequest;
use App\Models\Country;
use App\Models\Permission;
use App\Models\User;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;
use Flash;
use Response;
use Yajra\DataTables\DataTables;

class PermissionController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $members = Permission::select('id', 'module_name')->get()->unique('module_name');
            return Datatables::of($members)
                ->addColumn('action', function ($members) {
                    return view('admin.permissions.datatables_actions', ['id' => $members->id]);
                })
                ->addColumn('name', function ($members) {
                    return ucfirst($members->module_name);
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.permission.index');
    }


    public function create()
    {
        $array = [
            'create' => 'create',
            'read' => 'read',
            'update' => 'update',
            'delete' => 'delete',
        ];

        return view('admin.permission.create', ['array' => $array]);
    }

    public function store(Request $request)
    {
        $array = [
            'create' => 'create',
            'read' => 'read',
            'update' => 'update',
            'delete' => 'delete',
        ];
        foreach ($array as $value) {
            if ($request->input($value)) {
                $permission[] = [
                    'module_name' => strtolower($request['module_name']),
                    'guard_name' => 'web',
                    'name' => str_replace(' ', '-', strtolower($request['module_name'])) . '-' . $value,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }
        }
        Permission::insert($permission);
        return response()->json(['success' => true, 'message' => "Permission Added"], 200);
    }

    public function edit($id)
    {
        $permission = Permission::find($id);
        $array = [
            'create' => 'create',
            'read' => 'read',
            'update' => 'update',
            'delete' => 'delete',
        ];
        return view('admin.permissions.edit', ['permission' => $permission, 'array' => $array]);
    }

    public function destroy($id)
    {
        $permission = Permission::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => "Permission Deleted"], 200);
    }
}
