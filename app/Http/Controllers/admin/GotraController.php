<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Models\Gotra;
use Flash;
use Config;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;
use Yajra\DataTables\DataTables;

class GotraController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $gotras = Gotra::select('gotras.*');

            return Datatables::of($gotras)
                ->addColumn('action', function ( $gotras ) {
                    $delete_button = '';
                    $edit_button = '<a href="' . route('admin.gotra.edit', [$gotras->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Edit"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $gotras->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . Config::get('languageString.Delete') . '"><i class="mdi mdi-trash-can"></i></button>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('admin.gotra.index');
    }

    public function create()
    {
        return view('admin.gotra.create');
    }

    public function store( Request $request )
    {
        $id = $request['id'];
        if ($id == NULL) {
            $cities = new Gotra();
            $cities->name = $request['name'];
            $cities->save();

            return response()->json(['success' => true, 'message' => "Gotra Added"], 200);
        } else {
            $cities = Gotra::find($id);
            $cities->name = $request['name'];
            $cities->save();
            return response()->json(['success' => true, 'message' => "Gotra Updated"], 200);
        }
    }

    public function edit( $id )
    {
        $gotra = Gotra::find($id);
        return view('admin.gotra.edit', [
            "gotra" => $gotra,
        ]);
    }

    public function destroy( $id )
    {
        Gotra::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => "Gotra Deleted"], 200);
    }
}
