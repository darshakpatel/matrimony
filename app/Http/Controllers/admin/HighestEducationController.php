<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Models\HighestEducation;
use Flash;
use Config;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;
use Yajra\DataTables\DataTables;

class HighestEducationController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $gotras = HighestEducation::select('*');

            return Datatables::of($gotras)
                ->addColumn('action', function ( $gotras ) {
                    $delete_button = '';
                    $edit_button = '<a href="' . route('admin.highestEducation.edit', [$gotras->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Edit"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $gotras->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . Config::get('languageString.Delete') . '"><i class="mdi mdi-trash-can"></i></button>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('admin.highestEducation.index');
    }

    public function create()
    {
        return view('admin.highestEducation.create');
    }

    public function store( Request $request )
    {
        $id = $request['id'];
        if ($id == NULL) {
            $cities = new HighestEducation();
            $cities->name = $request['name'];
            $cities->save();

            return response()->json(['success' => true, 'message' => "Highest Education Added"], 200);
        } else {
            $cities = HighestEducation::find($id);
            $cities->name = $request['name'];
            $cities->save();
            return response()->json(['success' => true, 'message' => "Highest Education Updated"], 200);
        }
    }

    public function edit( $id )
    {
        $highestEducation = HighestEducation::find($id);
        return view('admin.highestEducation.edit', [
            "highestEducation" => $highestEducation,
        ]);
    }

    public function destroy( $id )
    {
        HighestEducation::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => "Highest Education Deleted"], 200);
    }
}
