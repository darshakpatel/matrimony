<?php

namespace App\Http\Controllers\admin;

use App\Helpers\AdminLogHelper;
use App\Helpers\ImageDeleteHelper;
use App\Helpers\ImageUploadHelper;
use App\Http\Requests\UserStoreRequest;
use App\Imports\ExcelImport;
use App\Mail\UserApproveMail;
use App\Mail\WelcomeMail;
use App\Models\AdminLog;
use App\Models\City;
use App\Models\Company;
use App\Models\Country;
use App\Models\Package;
use App\Models\ProductQuestion;
use App\Models\State;
use App\Models\User;
use App\Models\UserAnswer;
use App\Models\UserPackage;
use App\Models\UserReview;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Mail;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{


    public function index( Request $request )
    {
        if ($request->ajax()) {
            if ($request['type'] == 'pending') {
                $members = User::where('is_profile_updated', 1)->where('is_approved', 0)->select('users.*');
            } elseif ($request['type'] == 'incomplete') {
                $members = User::where('is_profile_updated', 0)->select('users.*');
            } elseif ($request['type'] == 'approved') {
                $members = User::where('is_approved', 1)->select('users.*');
            } else {
                $members = User::where('is_profile_updated', 1)->select('users.*');
            }

            if ($request->input('is_approved') != null) {
                $members->where('is_approved', $request->input('is_approved'));
            }
            return Datatables::of($members)
                ->addColumn('action', function ( $members ) use ( $request ) {
                    $unban_button = $status_button = $ban_button = $approve_button = $edit_button = $delete_button = '';
                    if (auth()->user()->can('user-read')) {
                        $edit_button = '<a href="' . route('admin.user.show', [$members->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Show"><i class="mdi mdi-eye"></i></a>';
                    }
                    if (auth()->user()->can('user-delete')) {
                        $delete_button = '<button data-id="' . $members->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Delete"><i class="mdi mdi-trash-can"></i></button>';
                    }
                    if (auth()->user()->can('user-status-update')) {
                        if ($request['type'] == 'active' || ($request['type'] == 'pending')) {
                            if ($members->status == 'active') {
                                $status_button = '<button data-id="' . $members->id . '" data-status="inActive" class="status-change btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.inactive') . '"><i class="fas fa-ban"></i></button>';
                            } elseif ($members->status == 'inActive') {
                                $status_button = '<button data-id="' . $members->id . '" data-status="active" class="status-change btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.active') . '"><i class="fas fa-check-square"></i></button>';
                            }
                            if ($members->is_approved == 0) {
                                $approve_button = '<button data-id="' . $members->id . '" data-status="1" class="approve-button btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Approve"><i class="fa fa-check"></i></button>';
                            }
                            if ($members->status != 'banned' && $members->status != 'inActive') {
                                $ban_button = '<button data-id="' . $members->id . '" data-status="banned" class="ban-button btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Block"><i class="fa fa-lock"></i></button>';
                            }
                            if ($members->status == 'banned') {
                                $unban_button = '<button data-id="' . $members->id . '" data-status="active" class="status-change btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Unblock"><i class="fa fa-unlock"></i></button>';
                            }
                        }
                        if (($request['type'] == 'approved')) {
                            if ($members->status == 'active') {
                                $status_button = '<button data-id="' . $members->id . '" data-status="inActive" class="status-change btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.inactive') . '"><i class="fas fa-ban"></i></button>';
                            } elseif ($members->status == 'inActive') {
                                $status_button = '<button data-id="' . $members->id . '" data-status="active" class="status-change btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.active') . '"><i class="fas fa-check-square"></i></button>';
                            }
                            if ($members->status != 'banned' && $members->status != 'inActive') {
                                $ban_button = '<button data-id="' . $members->id . '" data-status="banned" class="ban-button btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Block"><i class="fa fa-lock"></i></button>';
                            }
                            if ($members->status == 'banned') {
                                $unban_button = '<button data-id="' . $members->id . '" data-status="active" class="status-change btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Unblock"><i class="fa fa-unlock"></i></button>';
                            }
                        }
                    }
                    return $edit_button . ' ' . $delete_button . ' ' . $status_button . ' ' . $ban_button . ' ' . $unban_button . ' ' . $approve_button;
                })
                ->addColumn('date', function ( $members ) {
                    return $members->created_at->format('Y-m-d');
                })
                ->addColumn('status', function ( $cuisines ) {
                    $approve = '';
                    if ($cuisines->status == 'active') {
                        $status = '<span class="badge bg-soft-success text-success p-1">' . trans('messages.active') . '</span>';
                    } elseif ($cuisines->status == 'banned') {
                        $status = '<span class="badge bg-soft-danger text-danger p-1">Banned</span>';
                    } else {
                        $status = '<span class="badge bg-soft-danger text-danger p-1">' . trans('messages.inactive') . '</span>';
                    }
                    if ($cuisines->status != 'banned') {
                        if ($cuisines->is_approved == 1) {
                            $approve = '<span class="badge bg-soft-primary text-primary p-1">Approved</span>';
                        } else if ($cuisines->is_reactive == 1 && $cuisines->is_approved == 0) {
                            $approve = '<span class="badge bg-soft-primary text-primary p-1">Reactivation Request</span>';
                        } else {
                            $approve = '<span class="badge bg-soft-danger text-danger p-1">Not Approved</span>';
                        }
                    }
                    return $status . '<br>' . $approve;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view("admin.user.index");
    }

    public function pending()
    {
        return view("admin.user.pending");
    }

    public function approved()
    {
        return view("admin.user.approved");
    }

    public function incomplete()
    {
        return view("admin.user.incomplete");
    }

    public function create()
    {
        $countries = Country::where('status', 'active')->get();
        return view('admin.user.create', [
            'countries' => $countries,
        ]);
    }

    public function store( UserStoreRequest $request )
    {
        $id = $request['edit_value'];
        $image = null;

        if ($id == 0) {
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
            }
            $user = User::create([
                'name'             => $request['first_name'] . ' ' . $request['last_name'],
                'user_type'        => $request['user_type'],
                'first_name'       => $request['first_name'],
                'company_type'     => $request['company_type'],
                'company_name'     => $request['company_type'],
                'middle_name'      => $request['middle_name'],
                'last_name'        => $request['last_name'],
                'country_code'     => $request['country_code'],
                'mobile_no'        => $request['mobile_no'],
                'country_id'       => $request['country_id'],
                'state_id'         => $request['state_id'],
                'city_id'          => $request['city_id'],
                'email'            => $request['email'],
                'date_of_birth'    => $request['date_of_birth'],
                'gender'           => $request['gender'],
                'image'            => $image,
                'password'         => Hash::make($request['password']),
                'is_6a'            => isset($request['is_6a']) ? 1 : 0,
                'distributor_id'   => $request['distributor_id'],
                'a6_id'            => $request['a6_id'],
                'residency_status' => $request['residency_status'],
            ]);

            UserPackage::create([
                'user_id'    => $user->id,
                'package_id' => $request['package_id'],
            ]);
            return response()->json(['message' => "User Added"]);
        } else {
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $old_image = User::where('id', $id)->first();
                if (!empty($old_image->image)) {
                    ImageDeleteHelper::deleteImage($old_image->image);
                }
                User::where('id', $id)->update(['image' => $image]);
            }
            User::where('id', $id)->update([
                'name'             => $request['first_name'] . ' ' . $request['last_name'],
                'user_type'        => $request['user_type'],
                'first_name'       => $request['first_name'],
                'middle_name'      => $request['middle_name'],
                'last_name'        => $request['last_name'],
                'country_code'     => $request['country_code'],
                'mobile_no'        => $request['mobile_no'],
                'country_id'       => $request['country_id'],
                'state_id'         => $request['state_id'],
                'city_id'          => $request['city_id'],
                'date_of_birth'    => $request['date_of_birth'],
                'gender'           => $request['gender'],
                'email'            => $request['email'],
                'is_6a'            => isset($request['is_6a']) ? 1 : 0,
                'a6_id'            => $request['a6_id'],
                'distributor_id'   => $request['distributor_id'],
                'company_name'     => $request['company_name'],
                'company_type'     => $request['company_type'],
                'residency_status' => $request['residency_status'],
                //                'role_id' => $request['role_id'],
            ]);
            return response()->json(['message' => "User Updated"]);
        }

    }


    public function edit( $id )
    {
        $user = User::find($id);
        $countries = Country::all();
        $states = State::where('country_id', $user->country_id)->get();
        $cities = City::where('state_id', $user->state_id)->get();
        $roles = Role::where('name', '!=', 'Master Admin')->get();
        $a6 = User::where('is_6a', 1)->where('id', '!=', $user->id)->get();
        $companies = Company::all();
        return view('admin.user.edit',
            [
                'companies' => $companies,
                'countries' => $countries,
                'a6'        => $a6,
                'user'      => $user,
                'cities'    => $cities,
                'states'    => $states,
                'roles'     => $roles,
            ]);
    }

    public function changeStatus( $id, $status )
    {
        $user = User::where('id', $id)->first();
        User::where('id', $id)->update(['status' => $status]);
        AdminLogHelper::log($id, $status == 'active' ? $user->name . ' Activated by ' . \Auth::user()->name : $user->name . " Deactivated By " . \Auth::user()->name);
        return response()->json(['success' => true, 'message' => trans('messages.status_changed')]);
    }

    public function changeApproveStatus( $id, $is_approved )
    {
        $user = User::where('id', $id)->first();
        if ($is_approved == 1) {
            $status = 'active';
        } else {
            $status = 'inActive';
        }
        User::where('id', $id)->update(['is_approved' => $is_approved, 'status' => $status]);
        if (config('email_notification') == 1) {
            if ($is_approved == 1) {
                Mail::to($user['email'])->send(new UserApproveMail([
                    'name' => $user->name,
                    'is_reactive' => $user->is_reactive,
                ]));
            }
        }
        AdminLogHelper::log($id, $is_approved == 1 ? $user->name . ' Approved by ' . \Auth::user()->name : $user->name . " Rejected By " . \Auth::user()->name);
        return response()->json(['success' => true, 'message' => trans('messages.status_changed')]);
    }

    public function banUser( Request $request )
    {
        $user = User::where('id', $request['user_id'])->first();
        User::where('id', $request['user_id'])->update(['status' => 'banned', 'ban_reason' => $request['reason']]);
        AdminLogHelper::log($request['user_id'], $user->name . " Banned By " . \Auth::user()->name . " due to " . $request['reason']);
        return response()->json(['success' => true, 'message' => "Banned Successfully"]);
    }

    public function destroy( $id )
    {
        $user = User::where('id', $id)->first();
        AdminLogHelper::log($id, $user->name . " Deleted By " . \Auth::user()->name);
        User::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => "User Deleted"], 200);
    }

    public function show( $id )
    {
        $user = User::where('id', $id)->first();
        return view('admin.user.details', ['user' => $user]);
    }

    public function fileImportExport()
    {
        return view('admin.user.file-import');
    }

    public function fileImport( Request $request )
    {
        $state_id = [];
        $res = Excel::toArray(new ExcelImport, $request->file('file'));
        foreach ($res[0] as $row) {
            $state = State::where('name', $row[1])->first();
            if (empty($state)) {
                $state = new State();
                $state->name = $row[1];
                $state->country_id = 101;
                $state->country_code = 'IN';
                $state->save();
            }
            $city = new City();
            $city->name = $row[0];
            $city->state_id = $state->id;
            $city->country_id = $state->country_id;
            $city->save();
        }

    }
}
