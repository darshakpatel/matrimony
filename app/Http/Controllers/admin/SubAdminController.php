<?php

namespace App\Http\Controllers\admin;

use App\Helpers\ImageDeleteHelper;
use App\Helpers\ImageUploadHelper;
use App\Http\Requests\UserStoreRequest;
use App\Imports\UsersImport;
use App\Models\Admin;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Excel;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class SubAdminController extends Controller
{
    public function pending()
    {
        return view('admin.subAdmin.pending');
    }

    public function approved()
    {
        $roles = Role::where('name', '!=', 'Admin')->get();
        return view('admin.subAdmin.approved', ['roles' => $roles]);
    }

    public function rejected()
    {
        return view('admin.subAdmin.rejected');
    }

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $members = Admin::where('id', '!=', 1)->where('register_status', $request['type'])->select('admins.*');
            return Datatables::of($members)
                ->addColumn('action', function ( $members ) {
                    $reject_button=$role_button = $register_status_button = $status_button = '';
                    $details_button = '<button data-id="' . $members->id . '" class="details-single btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Show"><i class="mdi mdi-eye"></i></button>';
                    $delete_button = '<button data-id="' . $members->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Delete"><i class="mdi mdi-trash-can"></i></button>';
                    if ($members->register_status == 'pending') {
                        $register_status_button = '<button data-id="' . $members->id . '" data-status="approved" class="status-change btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Approve"><i class="fas fa-check-square"></i></button>';
                        $reject_button = '<button data-id="' . $members->id . '" data-status="rejected" class="reject-button btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Reject"><i class="fas fa-ban"></i></button>';
                    }
                    if ($members->register_status == 'approved') {
                        if ($members->status == 'active') {
                            $status_button = '<button data-id="' . $members->id . '" data-status="inActive" class="status-change btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="InActive"><i class="fas fa-ban"></i></button>';
                        } else {
                            $status_button = '<button data-id="' . $members->id . '" data-status="active" class="status-change btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Active"><i class="fas fa-check-square"></i></button>';
                        }
                    }

                    if ($members->register_status == 'approved' && empty($members->modelHasRole)) {
                        $role_button = '<button data-id="' . $members->id . '" class="assign-role btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Assign Role"><i class="fa fa-user-shield"></i></button>';
                    }
                    return $details_button . '  ' . $delete_button . ' ' . $register_status_button.' ' . $reject_button . ' ' . $status_button . ' ' . $role_button;
                })
                ->addColumn('date', function ( $members ) {
                    return $members->created_at->format('Y-m-d');
                })
                ->addColumn('role', function ( $members ) {
                    return $members->modelHasRole ? $members->modelHasRole->role->name : '';
                })
                ->addColumn('status', function ( $cuisines ) {
                    if ($cuisines->status == 'active') {
                        $status = '<span class="badge bg-soft-success text-success p-1">' . trans('messages.active') . '</span>';
                    } else {
                        $status = '<span class="badge bg-soft-danger text-danger p-1">' . trans('messages.inactive') . '</span>';
                    }
                    return $status;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
    }

    public function create()
    {
        $countries = Country::where('status', 'active')->get();
        return view('admin.user.create', [
            'countries' => $countries,
        ]);
    }

    public function store( UserStoreRequest $request )
    {
        $id = $request['edit_value'];
        $image = null;

        if ($id == 0) {
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
            }
            $user = User::create([
                'name'             => $request['first_name'] . ' ' . $request['last_name'],
                'user_type'        => $request['user_type'],
                'first_name'       => $request['first_name'],
                'company_type'     => $request['company_type'],
                'company_name'     => $request['company_type'],
                'middle_name'      => $request['middle_name'],
                'last_name'        => $request['last_name'],
                'country_code'     => $request['country_code'],
                'mobile_no'        => $request['mobile_no'],
                'country_id'       => $request['country_id'],
                'state_id'         => $request['state_id'],
                'city_id'          => $request['city_id'],
                'email'            => $request['email'],
                'date_of_birth'    => $request['date_of_birth'],
                'gender'           => $request['gender'],
                'image'            => $image,
                'password'         => Hash::make($request['password']),
                'is_6a'            => isset($request['is_6a']) ? 1 : 0,
                'distributor_id'   => $request['distributor_id'],
                'a6_id'            => $request['a6_id'],
                'residency_status' => $request['residency_status'],
            ]);

            UserPackage::create([
                'user_id'    => $user->id,
                'package_id' => $request['package_id'],
            ]);
            return response()->json(['message' => "User Added"]);
        } else {
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $old_image = User::where('id', $id)->first();
                if (!empty($old_image->image)) {
                    ImageDeleteHelper::deleteImage($old_image->image);
                }
                User::where('id', $id)->update(['image' => $image]);
            }
            User::where('id', $id)->update([
                'name'             => $request['first_name'] . ' ' . $request['last_name'],
                'user_type'        => $request['user_type'],
                'first_name'       => $request['first_name'],
                'middle_name'      => $request['middle_name'],
                'last_name'        => $request['last_name'],
                'country_code'     => $request['country_code'],
                'mobile_no'        => $request['mobile_no'],
                'country_id'       => $request['country_id'],
                'state_id'         => $request['state_id'],
                'city_id'          => $request['city_id'],
                'date_of_birth'    => $request['date_of_birth'],
                'gender'           => $request['gender'],
                'email'            => $request['email'],
                'is_6a'            => isset($request['is_6a']) ? 1 : 0,
                'a6_id'            => $request['a6_id'],
                'distributor_id'   => $request['distributor_id'],
                'company_name'     => $request['company_name'],
                'company_type'     => $request['company_type'],
                'residency_status' => $request['residency_status'],
                //                'role_id' => $request['role_id'],
            ]);
            return response()->json(['message' => "User Updated"]);
        }

    }


    public function edit( $id )
    {
        $user = User::find($id);
        $countries = Country::all();
        $states = State::where('country_id', $user->country_id)->get();
        $cities = City::where('state_id', $user->state_id)->get();
        $roles = Role::where('name', '!=', 'Master Admin')->get();
        $a6 = User::where('is_6a', 1)->where('id', '!=', $user->id)->get();
        $companies = Company::all();
        return view('admin.user.edit',
            [
                'companies' => $companies,
                'countries' => $countries,
                'a6'        => $a6,
                'user'      => $user,
                'cities'    => $cities,
                'states'    => $states,
                'roles'     => $roles,
            ]);
    }

    public function changeStatus( $id, $status )
    {
        Admin::where('id', $id)->update(['status' => $status]);
        return response()->json(['success' => true, 'message' => trans('messages.status_changed')]);
    }

    public function changeRegisterStatus( $id, $status )
    {
        Admin::where('id', $id)->update(['register_status' => $status]);
        return response()->json(['success' => true, 'message' => trans('messages.status_changed')]);
    }
    public function rejectSubAdmin( Request $request)
    {
        Admin::where('id', $request['id'])->update(['register_status' => 'rejected','reject_reason'=>$request['reason']]);
        return response()->json(['success' => true, 'message' => "Rejected Successfully"]);
    }

    public function destroy( $id )
    {
        Admin::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => "User Deleted"], 200);
    }

    public function show( $id )
    {
        $subAdmin = Admin::where('id', $id)->first();
        $view = view('admin.subAdmin.show', ['subAdmin' => $subAdmin])->render();
        return response()->json(['data' => $view]);
    }

    public function fileImportExport()
    {
        return view('admin.user.file-import');
    }

    public function fileImport( Request $request )
    {
        Excel::import(new UsersImport, $request->file('file')->store('temp'));
        return response()->json(['success' => true, 'message' => "User Imported"], 200);
    }

    public function assignRole( Request $request )
    {
        $subAdmin = Admin::where('id', $request['sub_admin_id'])->first();
        $subAdmin->assignRole($request['role_id']);
        return response()->json(['success' => true, 'message' => "Role Assigned Successfully"], 200);
    }
}
