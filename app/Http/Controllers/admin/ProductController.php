<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\ProductStoreRequest;
use App\Models\Country;
use App\Models\Product;
use App\Models\State;
use App\Helpers\ImageUploadHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index']]);
        $this->middleware('permission:product-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $companies = Product::select('products.*');
            return DataTables::of($companies)
                ->addColumn('action', function ($companies) {
                    $delete_button = '';
                    $edit_button = '<a href="' . route('admin.product.edit', [$companies->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . Config::get('languageString.edit') . '"><i class="mdi mdi-pencil"></i></a>';
//                    $delete_button = '<button data-id="' . $companies->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . Config::get('languageString.Delete') . '"><i class="mdi mdi-trash-can"></i></button>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('admin.product.index');
    }


    public function create()
    {
        return view('admin.product.create');
    }


    public function store(ProductStoreRequest $request)
    {
        $validated = $request->validated();
        $id = $request['edit_value'];
        if ($validated) {
            if ($id == 0) {
                $image = ImageUploadHelper::ImageUpload($request->file('image'));
                $product = new Product();
                $product->name = $validated['name'];
                $product->description = $validated['description'];
                $product->stock = $request['stock'];
                $product->unit = $request['unit'];
                $product->unit_value = $request['unit_value'];
                $product->image = $image;
                $product->save();

                return response()->json(['success' => true, 'message' => "Product Added"], 200);

            } else {
                $product = Product::find($id);
                $product->name = $validated['name'];
                $product->description = $validated['description'];
                $product->stock = $request['stock'];
                $product->unit = $request['unit'];
                $product->unit_value = $request['unit_value'];
                if ($request->hasFile('image')) {
                    $image = ImageUploadHelper::ImageUpload($request->file('image'));
                    $product->image = $image;
                }
                $product->save();
                return response()->json(['success' => true, 'message' => "Product Updated"], 200);
            }

        }
    }

    public function edit(int $id)
    {
        $product = Product::findorfail($id);
        return view('admin.product.edit', [
            "product" => $product,
        ]);
    }

    public function destroy(int $id)
    {
        Product::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => "Product Deleted"], 200);
    }
}
