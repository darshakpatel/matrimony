<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StateStoreRequest;
use App\Models\Country;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Yajra\DataTables\Facades\DataTables;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:state-list|state-create|state-edit|state-delete', ['only' => ['index']]);
        $this->middleware('permission:state-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:state-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:state-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            $states = State::select('states.*');
            return DataTables::of($states)
                ->addColumn('action', function($states){
                    $delete_button = '';
                    $edit_button = '<a href="' . route('admin.state.edit', [$states->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . Config::get('languageString.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    //                    $delete_button = '<button data-id="' . $states->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . Config::get('languageString.Delete') . '"><i class="mdi mdi-trash-can"></i></button>';
                    if($states->status == 'active'){
                        $status_button = '<button data-id="' . $states->id . '" data-status="inActive" class="status-change btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.inactive') . '"><i class="fas fa-ban"></i></button>';
                    } else{
                        $status_button = '<button data-id="' . $states->id . '" data-status="active" class="status-change btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.active') . '"><i class="fas fa-check-square"></i></button>';
                    }
                    return $edit_button . ' ' . $delete_button . ' ' . $status_button;
                })
                ->addColumn('country', function($states){
                    return $states->country->name;
                })
                ->addColumn('status', function($states){
                    if($states->status == 'active'){
                        $status = '<span class="badge bg-soft-success text-success p-1">' . trans('messages.active') . '</span>';
                    } else{
                        $status = '<span class="badge bg-soft-danger text-danger p-1">' . trans('messages.inactive') . '</span>';
                    }
                    return $status;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('admin.state.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $countries = Country::all();
        return view('admin.state.create', ['countries' => $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StateStoreRequest $request)
    {
        $validated = $request->validated();

        if($validated){
            if($request->id == NULL){
                $states = new State();
                $states->name = $validated['name'];
                $states->country_id = $validated['country_id'];
                $states->save();

                return response()->json(['success' => true, 'message' => trans('messages.admin.state.state_added')], 200);

            } else{
                $states = State::find($request['id']);
                $states->name = $validated['name'];
                $states->country_id = $validated['country_id'];
                $states->save();

                return response()->json(['success' => true, 'message' => trans('messages.admin.state.state_updated')], 200);
            }

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $state = State::findorfail($id);
        $countries = Country::all();
        return view('admin.state.edit', [
            "countries" => $countries,
            "state"     => $state,
        ]);

    }

    public function destroy(int $id)
    {
        State::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => trans('messages.admin.state.state_deleted')], 200);
    }

    public function changeStatus($id, $status)
    {
        State::where('id', $id)->update(['status' => $status]);
        return response()->json(['success' => true, 'message' => trans('messages.status_changed')]);
    }

    public function getState(Request $request)
    {
        $country_id = $request->input('country_id');
        $states = State::where('country_id', $country_id)->where('status', 'active')->get();
        if(count($states) > 0){
            echo "<option value=''>Please Select State</option>";
            foreach($states as $state){
                echo "<option value='" . $state->id . "'>" . $state->name . "</option>";
            }
        } else{
            echo "<option value=''>No State Found in this State</option>";
        }
    }
}
