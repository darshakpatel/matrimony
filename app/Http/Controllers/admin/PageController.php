<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageDeleteHelper;
use App\Helpers\ImageUploadHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\PageStoreRequest;
use App\Models\Language;
use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\ShopCategory;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class PageController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $pages = Page::select('pages.*');

            return DataTables::of($pages)
                ->addColumn('action', function ($pages) {
                    $edit_button = '<a href="' . route('admin.page.edit', [$pages->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    return $edit_button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.page.index');
    }

    public function create()
    {
        $languages = Language::all();
        return view('admin.page.create', ['languages' => $languages]);
    }


    public function store(Request $request)
    {

        $id = $request['id'];
        $name = $request->input('name');

        if ($id == NULL) {
            $page = new Page();
            $page->description = $request['description'];
            $page->name = $request['name'];
            $page->slug = SlugService::createSlug(Page::class, 'page_slug', $name['en']);
            $page->save();

            return response()->json(['message' => trans('messages.admin.page.page_added')], 200);
        } else {
            $page = Page::find($id);
            $page->name = $request['name'];
            $page->description = $request['description'];
            $page->save();
            return response()->json(['message' => trans('messages.admin.page.page_updated')], 200);
        }
       
    }


    public function edit(int $id)
    {

        $page = Page::findorfail($id);
        return view('admin.page.edit', ["page" => $page]);
    }

}
