<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\State;
use App\Models\User;
use App\Models\Pincode;
use App\Models\PlayedReminder;
use App\Models\Admin;
use App\Models\Reminder;
use App\Models\UserDevice;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Analytics;
use Spatie\Analytics\Period;

class HomeController extends Controller
{

    public function index()
    {
        $active_users = User::where('is_profile_updated', 1)->where('is_approved', 1)->count();
        $active_admins = Admin::where('register_status', 'approved')->count();
        $pending_admins = Admin::where('register_status', 'pending')->count();
        $pending_users = User::where('is_profile_updated', 1)->where('is_approved', 0)->count();

        $month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        foreach ($month as $a) {
            $user[] = User::where('is_profile_updated', 1)->whereMonth('created_at', '0' . $a)->count();
        }
        $userChart = app()->chartjs
            ->name('UserChart')
            ->type('bar')
            ->size(['width' => 500, 'height' => 100])
            ->labels([
                'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
                'November', 'December'
            ])
            ->datasets([
                [
                    "label"                     => trans('messages.admin.sidebar.users'),
                    'backgroundColor'           => "rgba(38, 185, 154, 0.31)",
                    'borderColor'               => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor"          => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor"      => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor"     => "rgba(220,220,220,1)",
                    'data'                      => $user,
                ]
            ])
            ->options([
                'responsive' => true,
                'animation'  => [
                    'animateScale'  => true,
                    'animateRotate' => true,
                ],
            ]);
        $analyticsData = Analytics::fetchVisitorsAndPageViews(Period::months(6));
        $visitors=$page_visit = 0;
        if (isset($analyticsData[0]['visitors'])) {
            $visitors = $analyticsData[0]['visitors'];
            $page_visit = $analyticsData[0]['pageViews'];
        }
        return view('admin.dashboard', [
            'active_users'   => $active_users,
            'pending_users'  => $pending_users,
            'pending_admins' => $pending_admins,
            'active_admins'  => $active_admins,
            'userChart'      => $userChart,
            'visitors'      => $visitors,
            'page_visit'      => $page_visit,
        ]);
    }

    public function getState( Request $request )
    {
        $states = State::where('country_id', $request['country_id'])->get();
        if (count($states) > 0) {
            echo "<option value=''>Please Select State</option>";
            foreach ($states as $state) {
                echo "<option value='" . $state->id . "'>" . $state->name . "</option>";
            }
        } else {
            echo "<option value=''>No State Found</option>";
        }
    }

    public function getCity( Request $request )
    {
        $cities = City::where('state_id', $request['state_id'])->get();
        if (count($cities) > 0) {
            echo "<option value=''>Please Select City</option>";
            foreach ($cities as $city) {
                echo "<option value='" . $city->id . "'>" . $city->name . "</option>";
            }
        } else {
            echo "<option value=''>No City Found</option>";
        }
    }

    public function test()
    {
//        $cities = City::where('country_id', 101)->where('pincode', null)->take(500)->get();
//        foreach ($cities as $city) {
//            $response = Http::get('https://api.postalpincode.in/postoffice/' . $city->name);
//            $result = json_decode($response);
//            if ($result[0]->Status == 'Success') {
//                $pincode = $result[0]->PostOffice[0]->Pincode;
//                $city->pincode = $pincode;
//                $city->save();
//            }
//        }

        $pincodes = Pincode::where('circle', 'Gujarat')->get();
        dd(count($pincodes));
        $array = [];
        if (!empty($pincode)) {
            $city = City::where('name', $pincode->taluka)->first();
        }

    }

    public function analytics()
    {
        $analyticsData = Analytics::fetchVisitorsAndPageViews(Period::months(6));
        dd($analyticsData);
    }
}
