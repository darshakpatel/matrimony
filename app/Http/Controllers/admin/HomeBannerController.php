<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageDeleteHelper;
use App\Helpers\ImageUploadHelper;
use App\Http\Controllers\Controller;
use App\Models\HomeBanner;
use DB;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class HomeBannerController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $sliders = HomeBanner::select("*");

            return DataTables::of($sliders)
                ->addColumn('action', function ( $sliders ) {
                    $delete_button = '';
                    $edit_button = '<a href="' . route('admin.homeBanner.edit', [$sliders->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
//                    $delete_button = '<button data-id="' . $sliders->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="mdi mdi-trash-can"></i></button>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->addColumn('image', function ( $sliders ) {
                    return '<img class="img-responsive" width="100px" src="' . url($sliders->image) . '">';
                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        }
        return view('admin.homeBanner.index');
    }

    public function create()
    {

        return view('admin.homeBanner.create');
    }

    public function store( Request $request )
    {
        $id = $request->input('edit_value');

        if ($id == 0) {
            $image = ImageUploadHelper::ImageUpload($request->file('image'));
            DB::beginTransaction();
            try {
                $slider = new HomeBanner();
                $slider->image = $image;
                $slider->save();

                DB::commit();
                return response()->json([
                    'message' => "Banner Added"
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'message' => $e->getMessage(),
                ], 422);
            }

        } else {
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request->file('image'));
            } else {
                $image = HomeBanner::where('id', $id)->first()->image;
            }
            DB::beginTransaction();
            try {
                $slider = HomeBanner::findorfail($id);
                $slider->image = $image;
                $slider->save();
                DB::commit();

                return response()->json([
                    'message' => "Banner Updated"
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'message' => $e->getMessage(),
                ], 422);
            }

        }

    }

    public function edit( int $id )
    {
        $homeBanner = HomeBanner::findorfail($id);
        return view('admin.homeBanner.edit', ["homeBanner" => $homeBanner]);
    }


    public function destroy( $id )
    {
        $slider = HomeBanner::where('id', $id)->first()->image;
        if ($slider) {
            ImageDeleteHelper::deleteImage($slider);
            HomeBanner::where('id', $id)->delete();
        }
        return response()->json(['success' => true, 'status_code' => 200, 'message' => "Banner Deleted"]);
    }


}
