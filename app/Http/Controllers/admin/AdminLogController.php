<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\Auth;
use App\Models\AdminLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Yajra\DataTables\DataTables;

class AdminLogController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $admin_logs = AdminLog::select('admin_logs.*');

            return Datatables::of($admin_logs)
                ->addColumn('user', function ( $members ) {
                    return $members->user ? $members->user->name : "Deleted User";
                })
                ->addColumn('admin', function ( $members ) {
                    return $members->admin ? $members->admin->name : "Deleted Admin";
                })
                ->addColumn('created_at', function ( $members ) {
                    return $members->created_at->format('d-m-Y H:i:s');
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('admin.adminLog.index');
    }

}