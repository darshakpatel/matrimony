<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Models\WorkProfile;
use Flash;
use Config;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;
use Yajra\DataTables\DataTables;

class WorkProfileController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $gotras = WorkProfile::select('work_profiles.*');

            return Datatables::of($gotras)
                ->addColumn('action', function ( $gotras ) {
                    $delete_button = '';
                    $edit_button = '<a href="' . route('admin.workProfile.edit', [$gotras->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Edit"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $gotras->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . Config::get('languageString.Delete') . '"><i class="mdi mdi-trash-can"></i></button>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('admin.workProfile.index');
    }

    public function create()
    {
        return view('admin.workProfile.create');
    }

    public function store( Request $request )
    {
        $id = $request['id'];
        if ($id == NULL) {
            $cities = new WorkProfile();
            $cities->name = $request['name'];
            $cities->save();

            return response()->json(['success' => true, 'message' => "Work Profile Added"], 200);
        } else {
            $cities = WorkProfile::find($id);
            $cities->name = $request['name'];
            $cities->save();
            return response()->json(['success' => true, 'message' => "Work Profile Updated"], 200);
        }
    }

    public function edit( $id )
    {
        $workProfile = WorkProfile::find($id);
        return view('admin.workProfile.edit', [
            "workProfile" => $workProfile,
        ]);
    }

    public function destroy( $id )
    {
        WorkProfile::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => "Work Profile Deleted"], 200);
    }
}
