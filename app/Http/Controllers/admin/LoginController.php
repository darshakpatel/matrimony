<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function index()
    {
        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin.dashboard');
        }
        return view('admin.auth.login');
    }

    public function loginCheck( Request $request )
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('admin.login')
                ->withErrors($validator)
                ->withInput();

        }
        $admin = Admin::where('email', $request->input('email'))
            ->first();
        if (empty($admin)) {
            $admin = Admin::where('mobile_no', $request->input('email'))
                ->first();
        }
        if (!$admin) {
            return redirect()
                ->route('admin.login')
                ->withErrors(['email' => ["Invalid Email or Mobile No"]])
                ->withInput();
        }

        if (!Hash::check($request->input('password'), $admin->password)) {
            return redirect()
                ->route('admin.login')
                ->withErrors(['password' => ["Invalid Password"]])
                ->withInput();
        }

        if ($admin) {
            if (!$admin->hasRole('Admin')) {
                if ($admin->register_status == 'pending') {
                    return redirect()
                        ->route('admin.login')
                        ->withErrors(['email' => ["Your account is under approval"]])
                        ->withInput();
                }
                if ($admin->register_status == 'rejected') {
                    return redirect()
                        ->route('admin.login')
                        ->withErrors(['email' => ["Your account is rejected"]])
                        ->withInput();
                }
                if ($admin->status == 'inActive') {
                    return redirect()
                        ->route('admin.login')
                        ->withErrors(['email' => ["Your account is deactivated"]])
                        ->withInput();
                }
                Auth::guard('admin')->login($admin);
            } else {
                Auth::guard('admin')->login($admin);
            }

            return redirect()->route('admin.dashboard');
        }
    }

    public function logout( Request $request )
    {
        Auth::guard('admin')->logout();
        \Illuminate\Support\Facades\Session::flush();
        return redirect()->route('admin.login');
    }
}
