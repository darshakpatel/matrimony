<?php

namespace App\Http\Controllers\admin;

use App\DataTables\CountryDataTable;
use App\Http\Requests;
use App\Http\Requests\CountryStoreRequest;
use App\Http\Requests\CreateCountryRequest;
use App\Http\Requests\UpdateCountryRequest;
use App\Models\City;
use App\Models\Country;
use App\Repositories\CountryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;
use Yajra\DataTables\DataTables;

class CountryController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $countries = Country::select('countries.*');

            return Datatables::of($countries)
                ->addColumn('action', function ($cities) {
                    $delete_button = '';
                    $edit_button = '<a href="' . route('admin.country.edit', [$cities->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Edit"><i class="mdi mdi-pencil"></i></a>';
                    //                    $delete_button = '<button data-id="' . $cities->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . Config::get('languageString.Delete') . '"><i class="mdi mdi-trash-can"></i></button>';
                    if ($cities->status == 'active') {
                        $status_button = '<button data-id="' . $cities->id . '" data-status="inActive" class="status-change btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.inactive') . '"><i class="fas fa-ban"></i></button>';
                    } else {
                        $status_button = '<button data-id="' . $cities->id . '" data-status="active" class="status-change btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.active') . '"><i class="fas fa-check-square"></i></button>';
                    }
                    return $edit_button . ' ' . $delete_button . ' ' . $status_button;
                })
                ->addColumn('status', function ($cuisines) {
                    if ($cuisines->status == 'active') {
                        $status = '<span class="badge bg-soft-success text-success p-1">' . trans('messages.active') . '</span>';
                    } else {
                        $status = '<span class="badge bg-soft-danger text-danger p-1">' . trans('messages.inactive') . '</span>';
                    }
                    return $status;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('admin.country.index');
    }

    public function create()
    {
        return view('admin.country.create');
    }

    public function store(CountryStoreRequest $request)
    {
        $validated = $request->validated();
        $id = $request['id'];
        if ($validated) {
            if ($id == NULL) {
                $cities = new Country();
                $cities->name = $validated['name'];
                $cities->save();

                return response()->json(['success' => true, 'message' => "Country Added"], 200);
            } else {
                $cities = Country::find($id);
                $cities->name = $validated['name'];
                $cities->save();
                return response()->json(['success' => true, 'message' => "Country Updated"], 200);
            }
        }
    }

    public function edit($id)
    {
        $country = Country::find($id);
        return view('admin.country.edit', [
            "country" => $country,
        ]);
    }


    public function changeStatus($id, $status)
    {
        Country::where('id', $id)->update(['status' => $status]);
        return response()->json(['success' => true, 'message' => trans('messages.status_changed')]);
    }

    public function destroy($id)
    {
        Country::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => "Country Deleted"], 200);
    }
}
