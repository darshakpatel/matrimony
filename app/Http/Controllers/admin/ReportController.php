<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ReportedUser;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class ReportController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $pages = ReportedUser::select('reported_users.*');

            return DataTables::of($pages)
                ->addColumn('reported_by', function ( $pages ) {
                    return $pages->reportedBy ? $pages->reportedBy->name : "Deleted user";
                })
                ->addColumn('user', function ( $pages ) {
                    return $pages->user ? $pages->user->name : "Deleted user";
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.report.index');
    }
}
