<?php

namespace App\Http\Controllers\admin;


use App\Helpers\ImageUploadHelper;
use App\Http\Requests\VideoStoreRequest;
use App\Models\BusinessVideo;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\BusinessVideoLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{

    public function profile()
    {

        $user = \Auth::guard('admin')->user();
        return view('admin.auth.profile', ['user' => $user]);
    }

    public function updateProfile(Request $request)
    {
        $user = \Auth::guard('admin')->user();


        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->save();

        return response()->json(['message' => 'User Profile Successfully Updated']);
    }

    public function changePassword()
    {
        return view('admin.auth.changePassword');
    }

    public function updatePassword(Request $request)
    {
        $validator_array = [
            'old_password'     => 'required|min:6',
            'new_password'     => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6',
        ];
        $validator = Validator::make($request->all(), $validator_array);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()->first(), 'data' => $validator->errors()]);
        }

        $user = \Auth::guard('admin')->user();

        if (Hash::check($request->input('old_password'), $user->password)) {
            User::where('id', $user->id)
                ->update([
                    'password' => bcrypt($request->input('new_password')),
                ]);
            return response()->json(['success' => true, 'message' => "Password Updated Successfully"]);
        } else {
            return response()->json(['success' => false, 'message' => "Old password is wrong"]);
        }

    }
}