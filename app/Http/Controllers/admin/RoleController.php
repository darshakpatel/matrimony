<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleStoreRequest;
use App\Models\BusinessVideo;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $roles = Role::where('name', '!=', 'Admin')->select('roles.*');
            return Datatables::of($roles)
                ->addColumn('action', function ($roles) {
                    $delete_button = '';
                    $edit_button = '<a href="' . route('admin.role.edit', [$roles->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Edit"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $roles->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Delete"><i class="mdi mdi-trash-can"></i></button>';
                    if ($roles->status == 'active') {
                        $status_button = '<button data-id="' . $roles->id . '" data-status="inActive" class="status-change btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.inactive') . '"><i class="fas fa-ban"></i></button>';
                    } else {
                        $status_button = '<button data-id="' . $roles->id . '" data-status="active" class="status-change btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.active') . '"><i class="fas fa-check-square"></i></button>';
                    }
                    return $edit_button . ' ' . $delete_button . ' ' . $status_button;
                })
                ->addColumn('status', function ($cuisines) {
                    if ($cuisines->status == 'active') {
                        $status = '<span class="badge bg-soft-success text-success p-1">' . trans('messages.active') . '</span>';
                    } else {
                        $status = '<span class="badge bg-soft-danger text-danger p-1">' . trans('messages.inactive') . '</span>';
                    }
                    return $status;
                })
                ->rawColumns(['action','status'])
                ->make(true);
        }
        return view('admin.role.index');
    }

    public function create()
    {
        $permissions = Permission::select('id', 'module_name')->get()->unique('module_name');
        return view('admin.role.create', ['permissions' => $permissions]);
    }

    public function store(RoleStoreRequest $request)
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $role = Role::create([
                'name' => $request->input('name'),
                'guard_name' => 'admin',
            ]);
            $role->syncPermissions($request->input('permission'));
            return response()->json(['success' => true, 'message' => "Role Added"], 200);
        } else {
            $role = Role::find($id);
            $role->name = $request->input('name');
            $role->save();
            $role->syncPermissions($request->input('permission'));
            return response()->json(['success' => true, 'message' => "Role Updated"], 200);
        }
    }

    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = Permission::get()->unique('module_name');

        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();

        return view('admin.role.edit', [
            'role' => $role,
            'permissions' => $permissions,
            'rolePermissions' => $rolePermissions
        ]);
    }

    public function destroy($id)
    {
        Role::where('id', $id)->delete();
        return response()->json(['success' => true, 'message' => "Role Deleted"], 200);
    }

    public function changeStatus($id, $status)
    {
        Role::where('id', $id)->update(['status' => $status]);
        return response()->json(['success' => true, 'message' => trans('messages.status_changed')]);
    }
}
