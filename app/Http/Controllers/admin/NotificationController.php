<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\NotificationStoreRequest;
use App\Models\UserDevice;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        return view('admin.notification.create');
    }

    public function store(NotificationStoreRequest $request): \Illuminate\Http\JsonResponse
    {
        $validated = $request->validated();

        $total_device = UserDevice::count();
        if ($total_device > 900) {

        }

        return response()->json(['message' => 'Notification Sent Successfully'], 200);
    }
}
