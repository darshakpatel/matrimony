<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'mobile_no' => $this->mobile_no,
            'country_id' => $this->country_id,
            'village' => $this->village,
            'country' => isset($this->country) ? $this->country->name : null,
            'state_id' => $this->state_id,
            'state' => isset($this->state) ? $this->state->name : null,
            'city_id' => $this->city_id,
            'city' => isset($this->city) ? $this->city->name : null,
            'pincode' => $this->pincode,
            'gender' => $this->gender,
            'gotra' => $this->profile->gotra,
            'date_of_birth' => $this->date_of_birth,
            'image' => $this->image,
            'is_approved' => $this->is_approved,
            'is_profile_updated' => $this->is_profile_updated,
            'is_basic_info' => $this->is_basic_info,
            'is_family_info' => $this->is_family_info,
            'is_native_address' => $this->is_native_address,
            'is_present_address' => $this->is_present_address,
            'is_education' => $this->is_education,
            'is_hobby' => $this->is_hobby,
            'is_self_intro' => $this->is_self_intro,
            'is_social_media' => $this->is_social_media,
            'is_gallery_photo' => $this->is_gallery_photo,
        ];
    }
}
