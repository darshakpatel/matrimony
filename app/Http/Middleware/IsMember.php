<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Cache;
use Carbon\Carbon;
use App\Mail\OtpMail;
use App\Helpers\SmsHelper;

class IsMember
{

    public function handle( $request, Closure $next )
    {
        if (Auth::check()) {
            $user = Auth::user();
            $expiresAt = Carbon::now()->addMinutes(3);
            Cache::put('user-is-online-' . $user->id, true, $expiresAt);

            if ($user->status == 'banned') {
                return redirect()->route('user.blocked');
            } else {
                if ($user->is_mobile_verified == 0) {
                    $user->otp = SmsHelper::sendSms(['mobile_no' => $user->mobile_no]);
                    $user->save();

                    Session::put('mobile_no', $user->mobile_no);
                    return redirect()->to(url('verify-otp'));
                }
                if ($user->is_email_verified == 0) {
                    if (config('email_notification') == 1) {
                        $user->email_otp = rand(111111, 999999);
                        $user->save();
                        \Session::put('email', $user->email);
                        \Mail::to($user->email)->send(new OtpMail([
                            'name' => $user->name,
                            'otp'  => $user->email_otp,
                        ]));
                        return redirect()->to(url('verify-otp'));
                    } else {
                        $user->email_otp = 123456;
                        $user->save();
                        \Session::put('email', $user->email);
                        return redirect()->to(url('verify-otp'));
                    }
                }
                if ($user->is_approved == 0 && \Request::route()->getName() != "profile_settings") {
                    return redirect()->route('profile_settings');
                }
                return $next($request);
            }
        } else {
            session(['link' => url()->current()]);
            return redirect()->route('login');
        }
    }
}
