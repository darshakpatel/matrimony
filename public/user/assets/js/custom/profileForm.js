$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    $('.update-form').on('submit', function (e) {
        $form = $(this)
        e.preventDefault();
        loaderView();
        let formData = new FormData($form[0])
        var action = $(this).attr('action')
        var update_type = $(this).data('update-type')

        if (update_type == 'basic_info') {
            $("#basic_info_update").text('(Completed)').css('color', 'green')
        }
        if (update_type == 'photos_update') {
            $("#gallery_photo_update").text('(Completed)').css('color', 'green')
        }
        if (update_type == 'hobby_update') {
            $("#hobby_update").text('(Completed)').css('color', 'green')
        }
        if (update_type == 'self_info_update') {
            $("#self_intro_update").text('(Completed)').css('color', 'green')
        }
        if (update_type == 'social_media_update') {
            $("#social_media_update").text('(Completed)').css('color', 'green')
        }
        if (update_type == 'present_address') {
            $("#present_address_update").text('(Completed)').css('color', 'green')
        }
        if (update_type == 'perminent-address') {
            $("#native_update").text('(Completed)').css('color', 'green')
        }
        if (update_type == 'education') {
            $("#education_update").text('(Completed)').css('color', 'green')
        }
        if (update_type == 'family_info') {
            $("#family_info_update").text('(Completed)').css('color', 'green')
        }

        axios
            .post(action, formData)
            .then(function (response) {
                loaderHide();
                successToast(response.data.message, 'success');
                // setTimeout(function () {
                //     window.location.href = response.data.url;
                // }, 1000)
            })
            .catch(function (error) {
                console.log(error)
                loaderHide();
                successToast(error.response.data.message, 'warning')
            });
    })
})