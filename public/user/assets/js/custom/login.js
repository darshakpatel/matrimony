$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        e.preventDefault();
        $form.parsley().validate();
        if ($form.parsley().isValid()) {
            loaderView();
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/login', formData)
                .then(function (response) {
                    loaderHide();
                    if(response.data.is_email_verify==0){
                        Swal.fire(
                            'Email Verification',
                            response.data.message,
                            'success'
                        ).then(function() {
                            window.location.href = response.data.url;
                        });
                    }else{
                        successToast(response.data.message, 'success')
                        setTimeout(function (){
                            window.location.href = response.data.url;
                        },1000)
                    }
                })
                .catch(function (error) {
                    loaderHide();
                    successToast(error.response.data.message, 'warning')
                });
        }
    })
})