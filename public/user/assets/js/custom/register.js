$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        e.preventDefault();
        e.preventDefault()
        $form.parsley().validate();
        if ($form.parsley().isValid()) {
            loaderView();
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/register', formData)
                .then(function (response) {
                    loaderHide();
                    successToast(response.data.message, 'success');
                    setTimeout(function () {
                        window.location.href = '/verify-otp';
                    }, 2000)
                })
                .catch(function (error) {
                    loaderHide();
                    successToast(error.response.data.message, 'warning')
                });
        }
    })
    $("#country_id").trigger('change')
})
