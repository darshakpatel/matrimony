$(document).ready(function () {
    countdown()
})

function countdown() {
    var counter = resend_otp_seconds;
    var interval = setInterval(function () {
        counter--;
        // Display 'counter' wherever you want to display it.
        if (counter <= 0) {
            clearInterval(interval);
            $("#resend_otp").attr('disabled', false)
            $("#resend_otp_text").hide()
            return;
        } else {
            $('#countdown').text(counter);
        }
        // console.log("Timer --> " + counter);
    }, 1000);
}

$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })


    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        e.preventDefault();
        $form.parsley().validate();
        if ($form.parsley().isValid()) {
            loaderView();
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/verifyOtp', formData)
                .then(function (response) {
                    loaderHide();
                    successToast(response.data.message, 'success');
                    setTimeout(function () {
                        window.location.href = response.data.url;
                    }, 1000)
                })
                .catch(function (error) {
                    loaderHide();
                    successToast(error.response.data.message, 'warning')
                });
        }
    })
    $("#resend_otp").on('click', function () {
        $(this).attr('disabled', true)
        $("#resend_otp_text").show()
        loaderView()
        axios
            .get(APP_URL + '/resend-otp')
            .then(function (response) {
                countdown()
                loaderHide();
                successToast(response.data.message, 'success');
            })
            .catch(function (error) {
                loaderHide();
                successToast(error.response.data.message, 'warning')
            });
    })
})