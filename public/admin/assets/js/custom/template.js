$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    const table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: APP_URL + '/template',
            type: 'GET',
            data: function (d) {
                d.status = $('#bulk-option').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'type', name: 'type'},
            {data: 'title', name: 'title'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        drawCallback: function () {
            funDataTableCheck('datable_check')
            funDataTableUnCheck('datable_check')
            funTooltip()
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[1, 'ASC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    })

    $(document).on('click', '#bulk-option-btn', function () {
        table.draw();
    });

    $(document).on('click', '.delete-single', function () {
        const value_id = $(this).data('id')
        Swal.fire({
            title: "Delete Template?",
            text: delete_message,
            type: 'warning',
            showCancelButton: !0,
            confirmButtonColor: '#556ee6',
            cancelButtonColor: '#f46a6a'
        }).then(function (t) {
            if (t.value) {
                deleteRecord(value_id)
            }
        })
    })

    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            loaderView();
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/template', formData)
                .then(function (response) {
                    loaderHide();
                    $form.removeClass('was-validated');
                    if ($("#form-method").val() === 'add') {
                        $form[0].reset();
                        $(".dropify-clear").trigger("click");
                    } else {
                        setTimeout(function () {
                            window.location.href = APP_URL + '/template'
                        }, 1000);
                    }
                    successToast(response.data.message, 'success');
                })
                .catch(function (error) {
                    loaderHide();
                    successToast(error.response.data.message, 'warning')
                });
        }
    })

    function deleteRecord(value_id) {
        axios
            .delete(APP_URL + '/template/' + value_id)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw();
                funTooltipHide();
                loaderHide();
                successToast(response.data.message, 'success');
            })
            .catch(function (error) {
                funTooltipHide();
                successToast(error.response.data.message, 'warning')
            });
    }

    $(document).on('click', '.status-change', function () {
        const value_id = $(this).data('id')
        const status = $(this).data('status')
        if (status === 'InActive') {
            Swal.fire({
                title: "Deactivate Template",
                text: status_message,
                type: 'warning',
                showCancelButton: !0,
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#f46a6a'
            }).then(function (t) {
                if (t.value) {
                    statusChangeRecord(value_id, status)
                }
            })
        } else {
            Swal.fire({
                title: "Active Template",
                text: status_message,
                type: 'success',
                showCancelButton: !0,
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#f46a6a'
            }).then(function (t) {
                if (t.value) {
                    statusChangeRecord(value_id, status)
                }
            })
        }
    })

    function statusChangeRecord(value_id, status) {
        axios
            .get(APP_URL + '/template/status/' + value_id + '/' + status)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide();
                funTooltipHide();
                successToast(response.data.message, 'success');
            })
            .catch(function (error) {
                funTooltipHide();
                successToast(error.response.data.message, 'warning')
            });
    }
    $('.description').summernote({
        placeholder: 'Write something...',
        height: 230,
        focus: true,
        required: true,
        callbacks: {
            // fix broken checkbox on link modal
        }
    });
})



