$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    const table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: APP_URL + '/report',
            type: 'GET',
        },
        columns: [
            {data: 'user', name: 'user'},
            {data: 'reported_by', name: 'reported_by'},
            {data: 'reason', name: 'reason'},
        ],
        drawCallback: function () {
            funDataTableCheck('datable_check')
            funDataTableUnCheck('datable_check')
            funTooltip()
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[0, 'DESC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    })

})
