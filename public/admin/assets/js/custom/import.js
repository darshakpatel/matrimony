$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    $('.dropify').dropify()
    /* Add Edit Form */
    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            loaderView();
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/fileImport', formData)
                .then(function (response) {
                    $form[0].reset();
                    $form.removeClass('was-validated');
                    $(".dropify-clear").trigger("click");
                    // window.location.href = APP_URL + '/user'
                    successToast(response.data.message, 'success');
                })
                .catch(function (error) {
                    successToast(error.response.data.message, 'warning')
                });

            loaderHide();
        }
    })
})
