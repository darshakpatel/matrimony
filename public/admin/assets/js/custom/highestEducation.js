$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    const table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: APP_URL + '/highestEducation',
            type: 'GET',
            data: function (d) {

            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        drawCallback: function () {
            funDataTableCheck('datable_check')
            funDataTableUnCheck('datable_check')
            funTooltip()
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[0, 'DESC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    })

    $('.dropify').dropify()

    $('.status-data li').on('click', 'a', function () {
        $('a').removeClass('active')
        $(this).addClass('active')
        table.draw()
    })


    $(document).on('click', '.delete-single', function () {
        const value_id = $(this).data('id')

        Swal.fire({
            title: "Delete Higher Education",
            text: destroy_warning,
            type: 'warning',
            showCancelButton: !0,
            confirmButtonColor: '#556ee6',
            cancelButtonColor: '#f46a6a'
        }).then(function (t) {
            if (t.value) {
                deleteRecord(value_id)
            }
        })
    })


    function deleteRecord(value_id) {
        axios
            .delete(APP_URL + '/highestEducation' + '/' + value_id)
            .then(function (response) {
                successToast(response.data.message, 'success');
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide();
            })
            .catch(function (error) {
                successToast(error.response.data.message, 'warning')
                loaderHide();
            });
    }


    /* Add Edit Form */

    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            loaderView();
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/highestEducation', formData)
                .then(function (response) {
                    if ($("#form-method").val() === 'add') {
                        $form[0].reset();
                        $form.removeClass('was-validated');
                        $(".dropify-clear").trigger("click");
                    }
                    window.location.href = APP_URL + '/highestEducation'
                    successToast(response.data.message, 'success');
                })
                .catch(function (error) {
                    successToast(error.response.data.message, 'warning')
                });

            loaderHide();
        }
    })

    $(document).on('click', '.reset', function () {
        $("#addEditForm")[0].reset();
        $(".dropify-clear").trigger("click");
    });
})