$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: APP_URL + '/userPolicyDetails',
            type: 'POST',
            data: function (d) {
                d.user_id = $('#user_id').val();
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'plate_number', name: 'plate_number'},
            {data: 'car_model.name', name: 'car_model.name'},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'},
            {data: 'status', name: 'status'},
        ],
        drawCallback: function () {
            funTooltip();
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[1, 'DESC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    });
});
