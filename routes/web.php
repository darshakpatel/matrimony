<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'HomeController@index')->name('home');
Route::get('/verify-email', 'AuthController@verifyEmail')->name('verify-email');
Route::get('/register', 'AuthController@register')->name('register');
Route::get('/contact-us', 'HomeController@contactUs')->name('contact-us');
Route::post('/register', 'AuthController@registerSubmit')->name('register');
Route::get('/login', 'AuthController@login')->name('login');
Route::get('/page/{slug}', 'PageController@index')->name('page');
Route::get('/forgot-password', 'AuthController@forgotPasswordForm')->name('forgot-password');
Route::get('/reset-password/{id}', 'ResetPasswordController@resetPasswordForm')->name('reset-password');
Route::post('/resetPassword', 'ResetPasswordController@resetPassword')->name('resetPassword');
Route::post('/forgotPassword', 'AuthController@forgotPassword')->name('forgotPassword');
Route::post('/login', 'AuthController@loginSubmit')->name('login');
Route::get('/passwordReset', 'AuthController@passwordReset')->name('password.request');
Route::get('/logout', 'AuthController@logout')->name('logout');
Route::get('/happy_stories', 'HomeController@happy_stories')->name('happy_stories');
Route::get('/getState/{id}', 'HomeController@getState')->name('getState');
Route::get('/getCity/{id}', 'HomeController@getCity')->name('getCity');
Route::get('/verify-otp', 'AuthController@verifyOtpForm')->name('verify-otp');
Route::post('/verifyOtp', 'AuthController@verifyOtp')->name('verifyOtp');
Route::get('/admin-form', 'AuthController@adminForm')->name('admin-form');
Route::get('/blocked', 'HomeController@user_account_blocked')->name('user.blocked');
Route::get('resend-otp', 'AuthController@resend_otp')->name('resend-otp');
Route::post('/adminForm', 'AuthController@adminFormSubmit')->name('adminForm');

Route::group(['middleware' => ['auth:web']], function () {
    Route::post('/introduction-update', 'ProfileController@introductionUpdate')->name('introduction-update');
    Route::post('/basic_info_update', 'ProfileController@basic_info_update')->name('basic_info_update');
    Route::post('/update_education_present_status', 'ProfileController@update_education_present_status')->name('update_education_present_status');
    Route::post('/address-update', 'ProfileController@addressUpdate')->name('address-update');
    Route::post('/hobbies-update', 'ProfileController@hobbiesUpdate')->name('hobbies-update');
    Route::post('/recidencies-update', 'ProfileController@recidenciesUpdate')->name('recidencies-update');
    Route::post('/photos-update', 'ProfileController@photosUpdate')->name('photos-update');
    Route::post('/native-address-update', 'ProfileController@nativeAddressUpdate')->name('native-address-update');
    Route::post('/families-update', 'ProfileController@familiesUpdate')->name('families-update');
    Route::post('/education-update', 'ProfileController@educationUpdate')->name('education-update');
    Route::post('/change-email', 'ProfileController@changeEmail')->name('change-email');
    Route::post('/add_to_ignore_list', 'ProfileController@add_to_ignore_list')->name('add_to_ignore_list');
    Route::post('/reportUsers', 'ProfileController@reportUsers')->name('reportUsers');
    Route::post('approve-status-update', 'ProfileController@submitForApproval')->name('approve-status-update');
    Route::post('socialMediaUpdate', 'ProfileController@socialMediaUpdate')->name('socialMediaUpdate');
    Route::get('checkProfile', 'ProfileController@checkProfile')->name('checkProfile');
    Route::get('update_account_deactivation_status', 'ProfileController@update_account_deactivation_status')->name('update_account_deactivation_status');
    Route::group(['middleware' => ['member']], function () {
        Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
        Route::get('/memberListing', 'MemberListingController@index')->name('memberListing');
        Route::get('/change_password', 'ProfileController@change_password')->name('change_password');
        Route::get('/profile_settings', 'ProfileController@profile_settings')->name('profile_settings');
        Route::get('/my_interests', 'ProfileController@my_interests')->name('my_interests');

        Route::get('/member_profile/{id}', 'ProfileController@member_profile')->name('member_profile');

        Route::get('/view_contacts', 'ProfileController@view_contacts')->name('view_contacts');
        Route::get('/remove_video', 'ProfileController@remove_video')->name('remove_video');
        Route::get('/add_to_ignore_list', 'ProfileController@add_to_ignore_list')->name('add_to_ignore_list');
        Route::resource('happy-story', 'HappyStoryController');

        Route::get('/ignored-list', 'IgnoredUserController@index')->name('my_ignored_list');
        Route::post('/remove_from_ignored_list-list', 'IgnoredUserController@remove_from_ignored_list')->name('remove_from_ignored_list');

        Route::get('/my-interests', 'ExpressInterestController@index')->name('my_interests.index');
        Route::get('/interest/requests', 'ExpressInterestController@interest_requests')->name('interest_requests');
        Route::post('/interest/accept', 'ExpressInterestController@accept_interest')->name('accept_interest');
        Route::post('/interest/reject', 'ExpressInterestController@reject_interest')->name('reject_interest');
        Route::resource('/express-interest', 'ExpressInterestController');
        Route::get('change-password', 'ProfileController@change_password')->name('change_password');
        Route::post('password_update', 'ProfileController@password_update')->name('password_update');


    });
});

