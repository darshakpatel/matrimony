<?php


use Illuminate\Support\Facades\Route;

Route::get('/', 'LoginController@index')->name('login');
Route::get('login', 'LoginController@index')->name('login');
Route::post('login', 'LoginController@loginCheck')->name('loginCheck');
Route::group(['middleware' => ['auth:admin', 'adminLanguage']], function () {

    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    Route::resource('category', 'CategoryController');
    Route::resource('user', 'UserController');
    Route::get('userDetails/{id}', 'UserController@userDetails')->name('userDetails');
    Route::get('user/status/{id}/{status}', 'UserController@changeStatus')->name('user.status.change');
    Route::get('user/approve/{id}/{status}', 'UserController@changeApproveStatus')->name('user.approve.change');

    Route::get('profile', 'AdminController@profile')->name('profile');
    Route::post('updateProfile', 'AdminController@updateProfile')->name('updateProfile');
    Route::get('changePassword', 'AdminController@changePassword')->name('changePassword');
    Route::post('updatePassword', 'AdminController@updatePassword')->name('updatePassword');

    Route::post('editProfile', 'HomeController@editProfile')->name('editProfile');
    Route::post('logout', 'LoginController@logout')->name('logout');
    Route::get('/', 'LoginController@index');
    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/logout', 'LoginController@logout')->name('logout');

    Route::post('getState', 'HomeController@getState')->name('getState');
    Route::post('getCity', 'HomeController@getCity')->name('getCity');


    Route::resource('role', 'RoleController');
    Route::resource('permission', 'PermissionController');


    Route::resource('users', 'UserController');
    Route::resource('country', 'CountryController');
    Route::resource('state', 'StateController');
    Route::resource('city', 'CityController');
    Route::get('/pendingUser', 'UserController@pending')->name('pendingUser');
    Route::get('/incompleteUser', 'UserController@incomplete')->name('incompleteUser');
    Route::get('/approvedUser', 'UserController@approved')->name('approvedUser');

    Route::post('getState', 'StateController@getState')->name('getState');
    Route::get('country/status/{id}/{status}', 'CountryController@changeStatus')->name('country.status.change');
    Route::get('state/status/{id}/{status}', 'StateController@changeStatus')->name('state.status.change');
    Route::post('getCity', 'CityController@getCity')->name('getCity');
    Route::get('city/status/{id}/{status}', 'CityController@changeStatus')->name('city.status.change');
    Route::get('role/status/{id}/{status}', 'RoleController@changeStatus')->name('role.status.change');


    Route::resource('page', 'PageController');
    Route::resource('notification', 'NotificationController');

    Route::get('setting', 'SettingController@index')->name('setting');
    Route::get('sendNotification', 'SendController@sendNotification')->name('sendNotification');
    Route::post('sendNotification', 'SendController@sendNotificationProcess')->name('sendNotification');


    Route::post('updateSetting', 'SettingController@updateSetting');

    Route::get('test', 'HomeController@test')->name('test');

    Route::get('export/{id}', 'DirectSellerController@export')->name('export');
    Route::post('customerSearch', 'PurchaseOrderController@customerSearch')->name('customerSearch');

    Route::resource('slider', 'SliderController');
    Route::resource('template', 'TemplateController');
    Route::resource('gotra', 'GotraController');
    Route::resource('workProfile', 'WorkProfileController');
    Route::resource('subAdmin', 'SubAdminController');
    Route::resource('mangalik', 'MangalikController');
    Route::resource('highestEducation', 'HighestEducationController');
    Route::resource('adminLog', 'AdminLogController');
    Route::get('pending-subAdmin', 'SubAdminController@pending')->name('subAdmin.pending');
    Route::get('approved-subAdmin', 'SubAdminController@approved')->name('subAdmin.approved');
    Route::get('rejected-subAdmin', 'SubAdminController@rejected')->name('subAdmin.rejected');
    Route::get('subAdmin/registerStatus/{id}/{status}', 'SubAdminController@changeRegisterStatus')->name('subAdmin.register-status.change');
    Route::get('subAdmin/status/{id}/{status}', 'SubAdminController@changeStatus')->name('subAdmin.status.change');
    Route::post('assignRole', 'SubAdminController@assignRole')->name('assignRole');
    Route::post('rejectSubAdmin', 'SubAdminController@rejectSubAdmin')->name('rejectSubAdmin');
    Route::post('banUser', 'UserController@banUser')->name('banUser');
    Route::get('fileImportExport', 'UserController@fileImportExport')->name('fileImportExport');
    Route::post('fileImport', 'UserController@fileImport')->name('fileImport');
    Route::get('report', 'ReportController@index')->name('report');
    Route::resource('homeBanner', 'HomeBannerController');
    Route::get('analytics', 'HomeController@analytics')->name('analytics');
});
