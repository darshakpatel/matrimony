<?php

use Illuminate\Support\Facades\Route;
//dd(request());
Route::prefix('v1')->name('api.v1.')->namespace('Api\V1')->group(function () {
    Route::post('login', 'LoginController@login');
    Route::post('mobileLogin', 'LoginController@mobileLogin');
    Route::post('verifyOtp', 'LoginController@verifyOtp');
    Route::post('register', 'RegisterController@register');
    Route::post('signUp', 'RegisterController@register')->name('signUp');
    Route::get('country', 'CountryController');
    Route::get('products', 'HomeController@products')->name('products');
    Route::get('sliders', 'HomeController@sliders')->name('sliders');
    Route::get('state/{id}', 'StateController@index')->name('state');
    Route::get('city/{id}', 'CityController@index')->name('city');
    Route::get('gotras', 'UserController@gotras')->name('gotras');
    Route::get('highestEducation', 'UserController@highestEducation')->name('highestEducation');
    Route::get('workProfile', 'UserController@workProfile')->name('workProfile');
    Route::post('forgotPassword', 'LoginController@forgotPassword')->name('forgotPassword');
    Route::post('contactUs', 'ContactUsController@getRecentViewItems')->name('getRecentViewItems');

    Route::get('page/{id}', 'PageController@show');
    Route::get('notification', 'NotificationController@index');

    Route::get('searchPinCode/{pincode}', 'UserController@searchPinCode')->name('searchPinCode');
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('userStatus', 'UserController@userStatus')->name('userStatus');
        Route::get('refreshToken', 'LoginController@refreshToken')->name('refreshToken');
        Route::get('getProfile', 'UserController@show')->name('getProfile');
        Route::post('/introductionUpdate', 'UserController@introductionUpdate')->name('introductionUpdate');
        Route::post('/basicInfoUpdate', 'UserController@basic_info_update')->name('basicInfoUpdate');
        Route::post('/update_education_present_status', 'UserController@update_education_present_status')->name('update_education_present_status');
        Route::post('/addressUpdate', 'UserController@addressUpdate')->name('addressUpdate');
        Route::post('/hobbiesUpdate', 'UserController@hobbiesUpdate')->name('hobbiesUpdate');
        Route::post('/recidencies-update', 'UserController@recidenciesUpdate')->name('recidencies-update');
        Route::post('/photosUpdate', 'UserController@photosUpdate')->name('photosUpdate');
        Route::post('/nativeAddressUpdate', 'UserController@nativeAddressUpdate')->name('nativeAddressUpdate');
        Route::post('/familiesUpdate', 'UserController@familiesUpdate')->name('familiesUpdate');
        Route::post('/educationUpdate', 'UserController@educationUpdate')->name('educationUpdate');
        Route::get('/submitForApproval', 'UserController@submitForApproval')->name('submitForApproval');
        Route::post('/add_to_ignore_list', 'UserController@add_to_ignore_list')->name('add_to_ignore_list');
        Route::post('/remove_from_ignore_list', 'UserController@remove_from_ignore_list')->name('remove_from_ignore_list');
        Route::get('/ignore_list', 'UserController@ignore_list')->name('ignore_list');
        Route::get('/remove_video', 'UserController@remove_video')->name('remove_video');
        Route::post('/report_user', 'UserController@report_user')->name('report_user');
        Route::post('/send_interest_request', 'ExpressInterestController@store')->name('send_interest_request');
        Route::post('/approve_interest_request', 'ExpressInterestController@accept_interest')->name('approve_interest_request');
        Route::get('/interest_list', 'ExpressInterestController@index')->name('interest_list');
        Route::post('/reject_interest_request', 'ExpressInterestController@reject_interest')->name('reject_interest_request');
        Route::get('/interest_request_list', 'ExpressInterestController@interest_requests')->name('interest_request_list');
        Route::post('/member_listing', 'MemberListingController@index')->name('member_listing');
        Route::get('/member_details/{id}', 'MemberListingController@memberDetails')->name('member_details');
        Route::get('/account_deactivate', 'UserController@account_deactivate')->name('account_deactivate');
        Route::post('/change_password', 'UserController@change_password')->name('change_password');
        Route::post('/social_media_update', 'UserController@social_media_update')->name('social_media_update');
    });
});
