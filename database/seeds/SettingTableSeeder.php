<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'meta_key'   => 'commission',
            'meta_value' => '20',
        ]);

        Setting::create([
            'meta_key'   => 'smtp_host',
            'meta_value' => 'smtp_host',
        ]);

        Setting::create([
            'meta_key'   => 'smtp_username',
            'meta_value' => 'smtp_username',
        ]);

        Setting::create([
            'meta_key'   => 'smtp_password',
            'meta_value' => 'smtp_password',
        ]);

        Setting::create([
            'meta_key'   => 'smtp_port',
            'meta_value' => 'smtp_port',
        ]);

        Setting::create([
            'meta_key'   => 'smtp_from_email',
            'meta_value' => 'smtp_from_email',
        ]);
    }
}
